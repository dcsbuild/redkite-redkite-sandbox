<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Used for all service team member case related matters including Telegroups, Phone Support, Email Support etc.</description>
    <label>Case Management</label>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Campaign</tab>
    <tab>Budget_Allocation__c</tab>
    <tab>Jimmy_Teens__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Batch_Management</tab>
    <tab>Evaluation_del__c</tab>
</CustomApplication>
