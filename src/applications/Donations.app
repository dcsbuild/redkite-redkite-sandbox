<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Used for general and individual donation processing.  Displays tabs relevent to individual donation processing.</description>
    <label>Donations</label>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>Relationship__c</tab>
    <tab>standard-Campaign</tab>
    <tab>Income__c</tab>
    <tab>standard-Opportunity</tab>
    <tab>Svc_Sup__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Evaluation_del__c</tab>
</CustomApplication>
