<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Used for the events team and for persons wanting to create Campaigns and associated records</description>
    <label>Events &amp; Community Fundraising</label>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>Relationship__c</tab>
    <tab>standard-Opportunity</tab>
    <tab>Income__c</tab>
    <tab>standard-Campaign</tab>
    <tab>Budget_Allocation__c</tab>
    <tab>PurchaseOrder__c</tab>
    <tab>Inventory__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Evaluation_del__c</tab>
</CustomApplication>
