<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <description>Community Based Support - How are we performing?</description>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>Record Count = Number of Casesnotes</footer>
            <header>Casenotes - Client Contacts</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_opened_this_month</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Casenotes - Client Contacts</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_Last_Month</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>Last Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Casenotes - 1st Contact to CBS Team</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboards_CBS_Cases_1st_contact_by_mth</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Year - Grouped by Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <header>All Casenotes - Method of Contact</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboards_Cases_by_Method_of_Contact</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Year - Grouped by Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Casenotes -  AYA Client</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboards_CBS_Cases_Diagnosed_Child</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Year - Grouped by Month</title>
        </components>
        <components>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Telegroup Participants</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Services_Family_Reports/Telegroup_Participants</report>
            <sortBy>RowLabelDescending</sortBy>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>Record Count = Number of Casesnotes</footer>
            <header>Casenotes - Professional Contacts</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_This_Month</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Casenotes - Professional Contacts</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_Last_Month_Prof</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>Last Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>Record count = Method of Contact  - &quot;Phone&quot;</footer>
            <header>How callers heard about Redkite?</header>
            <legendPosition>Right</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_Source_by_Phone</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>Last Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <header>All Casenotes - Illness Context</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboards_CBS_Cases_by_Illness_Context</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>Last Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Casenotes - Family Client Role</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_Household_Type</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Year - Grouped by Month</title>
        </components>
        <components>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Telegroup Activities/Follow Up</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Services_Family_Reports/Telegroup_Activities</report>
            <sortBy>RowLabelDescending</sortBy>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>Record Count - Number of Casenotes</footer>
            <header>All Casenotes - This Year vs Last Year</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/CBS_Cumulative_Prev_Current_Yr</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>Grouped by Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Casenotes - All Contacts in 2014</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_This_Year</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Year</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <header>All Casenotes - Purpose of Contact</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboards_CBS_Cases_Purpose_of_Contact</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>Last Month</title>
        </components>
        <components>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Casenotes - Outside Guidelines</header>
            <legendPosition>Bottom</legendPosition>
            <report>Services_Family_Reports/Dashboard_CBS_Cases_Outside_Guideline</report>
            <sortBy>RowLabelAscending</sortBy>
            <title>This Year - Grouped by Month</title>
        </components>
        <components>
            <componentType>Table</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Camp Quality Participants</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>ARCHIVED_Services_Reports/Camp_Quality_Particpants</report>
            <sortBy>RowLabelDescending</sortBy>
        </components>
        <components>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Camp Quality Activities/Follow Up</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Services_Family_Reports/Camp_Activities</report>
            <sortBy>RowLabelDescending</sortBy>
        </components>
    </rightSection>
    <runningUser>cibbott@redkite.org.au.redkite</runningUser>
    <textColor>#000000</textColor>
    <title>Services - CBS Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
