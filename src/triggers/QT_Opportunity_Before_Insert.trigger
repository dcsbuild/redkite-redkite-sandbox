trigger QT_Opportunity_Before_Insert on Opportunity (before insert) 
{
	/*
		Simple trigger that just populates the LeadSource with a default from the campaign unless one has been provided
	*/
	
	map<id, Campaign> mapCampaign = new map<id, Campaign>();
	map<id, id> mapAccountToCampaign = new map<id, id>();
		
	// extract the campaigns we need
	for(Opportunity s : trigger.new)
	{
		if(s.CampaignId != null)
		{
			mapCampaign.put(s.CampaignId, null);
			mapAccountToCampaign.put(s.AccountId, s.CampaignId);
		}
	}
	
	// load the data
	for(Campaign s : [select id, Default_Lead_Source_for_new_Opportunity__c from Campaign where id in :mapCampaign.keySet()])
	{
		mapCampaign.put(s.Id, s);
	}
	
	// update the opportunities
	for(Opportunity s : trigger.new)
	{
		if(s.CampaignId != null && (s.LeadSource == null || s.LeadSource == ''))
		{
			s.LeadSource = mapCampaign.get(s.CampaignId).Default_Lead_Source_for_new_Opportunity__c;
		}
	}
	
	// now the person accouts
	for(Account[] a : [select id, IsPersonAccount, PersonLeadSource from Account where id in :mapAccountToCampaign.keySet()])
	{
		for(Account s : a)
		{
			if(s.IsPersonAccount && (s.PersonLeadSource == null || s.PersonLeadSource == ''))
			{
				s.PersonLeadSource = mapCampaign.get(mapAccountToCampaign.get(s.Id)).Default_Lead_Source_for_new_Opportunity__c;
			}
		}
		
		update a;
	}
}