trigger QAccount_BeforeInsert on Account (before insert) 
{
	Integer iMaxClientId = 0;
	for(AggregateResult [] ARarr : [select  MAX(IPP_Client_Id__c)MAXCLIENTID
                                                from    Account
                                                ])
    {
    	for(AggregateResult AR : ARarr)
        {
        	if(iMaxClientId < Integer.valueOf(AR.get('MAXCLIENTID')) )
        		iMaxClientId = Integer.valueOf(AR.get('MAXCLIENTID'));
        	break;
        }
    }
    for(AggregateResult [] ARarr : [select  MAX(IPP_Client_Id_Auto__c)MAXCLIENTID
                                    from    Contact
                                    ])
    {
    	for(AggregateResult AR : ARarr)
        {
        	if(iMaxClientId < Integer.valueOf(AR.get('MAXCLIENTID')) )
        		iMaxClientId = Integer.valueOf(AR.get('MAXCLIENTID'));
        	break;
        }
    }
    
    for(Account a : trigger.new)
    {
    	iMaxClientId++;
    	a.IPP_Client_Id__c = ''+iMaxClientId;
    }
}