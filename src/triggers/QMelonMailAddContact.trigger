trigger QMelonMailAddContact on Contact (after insert)
{
	/**************************************
	
		TODO
		====
		
		* Add some error handling
	
	**************************************/
	integer iCounter = 0;
	integer iMaxThreads = 10;
	
	QMelonMailProcessing processor;
	QMelonMailProcessing.QMelonMailProcessingRet ret = null;

	// operations have to be done async has triggers aren't allowed to make callouts
	QMelonMailProcessing.QMelonMailProcessingArgs args = new QMelonMailProcessing.QMelonMailProcessingArgs();
	
	// these limits don't concern us here as we'll need a maximum of 4 callouts to add all of the ids
	args.m_iMaxCallouts = 10;
	args.m_iMaxThreads = iMaxThreads;
	args.m_eDatabase = QMelonMailProcessing.QMelonMailDB.DB_MARKETING;
	args.m_eBatchParam = QMelonMailProcessing.QMelonMailBatchParam.MM_ADD_CONTACTS_ASYNC;
	
	set<id>	setMarketingContacts = new set<id>();
	set<id>	setFurtherInfoContacts = new set<id>();
	
	list<Contact> liUpdateContacts = new list<Contact>();

	for(Contact sContact : trigger.new)
	{
		if(sContact.HasOptedOutOfEmail)
		{
			// skip this contact
			// do we need to add the contact and unsub them from both DBs?
			continue;
		}

		if(sContact.Email_Communication__c != null && sContact.Email_Communication__c.contains('Event Material'))
		{
			setMarketingContacts.add(sContact.id);			
		} 

		if(sContact.Email_Communication__c != null && sContact.Email_Communication__c.contains('Newsletters'))
		{
			setFurtherInfoContacts.add(sContact.id);
		}
	}

	if(setMarketingContacts.size() > 0)
	{
		// add these
		args.m_setContactIds = setMarketingContacts;
		
		processor = new QMelonMailProcessing(args);
		ret = processor.ProcMain();
		iMaxThreads -= ret.m_iNumThreadsFired;
		
		for(id sId : args.m_setContactIds)
		{
			if(iCounter >= ret.m_iNumRecordsProcessed)
			{
				// we didn't process this one so we flag it
				trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c = trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c.intValue() | QMelonMailProcessing.m_iMarketingOptIn;
			}
			iCounter ++;
		}
	}

	if(setFurtherInfoContacts.size() > 0)
	{
		integer iNumProcessed = 0;
		
		if(iMaxThreads >= 1)
		{
			args.m_iMaxThreads = iMaxThreads;
			args.m_eDatabase = QMelonMailProcessing.QMelonMailDB.DB_FURTHER_INFO;
			args.m_setContactIds = setFurtherInfoContacts;
			
			processor = new QMelonMailProcessing(args);
			ret = processor.ProcMain();
			
			iNumProcessed = ret.m_iNumRecordsProcessed;
		}

		iCounter = 0;

		for(id sId : args.m_setContactIds)
		{
			if(iCounter >= iNumProcessed)
			{
				trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c = trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c.intValue() | QMelonMailProcessing.m_iFurtherInfoOptIn;
			}
			iCounter ++;
		}
	}

	QMelonMailProcessing.m_bDisableUpdateTrigger = true;

	// update contacts that we've flagged - we don't have to worry about old values as this is an insert
	for(Contact sContact : trigger.newMap.values())
	{
		integer iFlags = sContact.Melon_Mail_Sync_Flags__c.intValue();
		
		if((iFlags & QMelonMailProcessing.m_iMarketingOptIn) != 0 || (iFlags & QMelonMailProcessing.m_iFurtherInfoOptIn) != 0)
		{
			liUpdateContacts.add(sContact);
		}
	}

	try
	{
		update liUpdateContacts;
	}
	catch(Exception e)
	{
		System.Debug('Exception Caught: ' + e);
	}
	
	QMelonMailProcessing.m_bDisableUpdateTrigger = false;
}