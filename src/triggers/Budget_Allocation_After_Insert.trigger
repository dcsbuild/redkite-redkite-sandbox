trigger Budget_Allocation_After_Insert on Budget_Allocation__c (after insert) 
{
	QBudgetAllocationProcessing.LinkParentToBudgetAllocation(trigger.new);
	
	QBudgetAllocationProcessing.PushDetailToParentCampaign(trigger.new);	
}