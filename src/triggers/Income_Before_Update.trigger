trigger Income_Before_Update on Income__c (before update) 
{
    list<Contact>       liContacts          = new list<Contact>();
    list<Contact>       liPAContacts        = new list<Contact>();
    map<id, id>         mapContact2Income   = new map<id, id>();
    
    for(Id sId : trigger.newMap.keySet())
    {
        Income__c sIncome = trigger.newMap.get(sId);
        
        if(sIncome.Credit_Card_No_A__c != null)
        {
            if(sIncome.Reconciled_A__c == true)
            {
                if(sIncome.Credit_Card_No_A__c.length() > 4)
                {
                    sIncome.Credit_Card_No_A__c = '**** **** **** ' + sIncome.Credit_Card_No_A__c.substring(sIncome.Credit_Card_No_A__c.length() - 4, sIncome.Credit_Card_No_A__c.length());
                }
                else
                {
                    sIncome.Credit_Card_No_A__c = '';
                }
            }
        }
        
        if(sIncome.Credit_Card_No_B__c != null)
        {
            if(sIncome.Reconciled_B__c == true)
            {
                if(sIncome.Credit_Card_No_B__c.length() > 4)
                {
                    sIncome.Credit_Card_No_B__c = '**** **** **** ' + sIncome.Credit_Card_No_B__c.substring(sIncome.Credit_Card_No_B__c.length() - 4, sIncome.Credit_Card_No_B__c.length());
                }
                else
                {
                    sIncome.Credit_Card_No_B__c = '';
                }
            }
        }
        
        if(
            (   
                    sIncome.Income_Code__c.endsWith('115')
                ||  sIncome.Income_Code__c.endsWith('120')
                ||  sIncome.Income_Code__c.endsWith('126')
                ||  sIncome.Income_Code__c.endsWith('240')
                ||  sIncome.Income_Code__c.endsWith('245')
                ||  sIncome.Income_Code__c.endsWith('260')
                ||  sIncome.Income_Code__c.endsWith('270')
            )
            &&
            (
                sIncome.All_Reconciled__c == true //&& trigger.oldMap.get(sId).All_Reconciled__c == false
            )
        )
        {
            //mapContact2Income.put(sIncome.Contact__c, sIncome.Id);
        }
    }
    /*
    map<string, double> mapLastIncomeByCOntact = new map<string, double>();
    for(Income__c sIncome : [   select   id, Total_Amount__c, Contact__c 
                                from     Income__c 
                                where    Contact__c in :mapContact2Income.keySet() 
                                order by CreatedDate DESC])
    {   
        if(mapLastIncomeByCOntact.get(sIncome.Contact__c) == null ) 
        {
            mapLastIncomeByCOntact.put(sIncome.Contact__c,sIncome.Total_Amount__c );    
         }

    }
        
    for(Contact[] arrContact : [select      id, Last_Donation_Amount__c, IsPersonAccount
                                    from    Contact where Id in : mapContact2Income.keySet()])
    {
        for(Contact sContact : arrContact)
        {
            //sContact.Last_Donation_Amount__c = trigger.newMap.get(mapContact2Income.get(sContact.Id)).Total_Amount__c;
            sContact.Last_Donation_Amount__c = mapLastIncomeByCOntact.get(sContact.Id);
            
            if(sContact.IsPersonAccount)
            {
                liPAContacts.add(sContact);
            }
            else
            {
                liContacts.add(sContact);
            }
        }
    }
    
    if(liContacts.size() > 0)
    {
        try
        {
            update liContacts;
        }
        catch (Exception e)
        {
            System.Debug('Exception thrown updating contacts: ' + e.getMessage());
        }
    }

    if(liPAContacts.size() > 0)
    {
        try
        {
            update liPAContacts;
        }
        catch (Exception e)
        {
            System.Debug('Exception thrown updating PersonAccount contacts: ' + e.getMessage());
        }
    }*/
}