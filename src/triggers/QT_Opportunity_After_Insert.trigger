trigger QT_Opportunity_After_Insert on Opportunity (after insert) 
{
	map<id, id> mapOpportunityId2ContactId = new map<id, id>();
	list<OpportunityContactRole> liOCRInsert = new list<OpportunityContactRole>(); 
	
	for(Opportunity s : trigger.new)
	{
		if(s.Contact__c != null)
		{
			mapOpportunityId2ContactId.put(s.Id, s.Contact__c);
		}
	}
	
	// now we need to check to see if there is a record there already
	for(OpportunityContactRole s : [select id, OpportunityId, ContactId from OpportunityContactRole where OpportunityId in :mapOpportunityId2ContactId.keySet()])
	{
		// same person?
		if(mapOpportunityId2ContactId.get(s.OpportunityId) == s.ContactId)
		{
			mapOpportunityId2ContactId.remove(s.OpportunityId);
		}
	}
	
	for(id idOpportunity : mapOpportunityId2ContactId.keySet())
	{
		liOCRInsert.add(new OpportunityContactRole(OpportunityId = idOpportunity, ContactId = mapOpportunityId2ContactId.get(idOpportunity), IsPrimary = true));
	}
	
	insert liOCRInsert;
}