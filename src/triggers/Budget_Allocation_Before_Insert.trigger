trigger Budget_Allocation_Before_Insert on Budget_Allocation__c (before insert) 
{
	//////////////////////////////////////
	// Singular
	boolean bDiagnosedPersonProcessed = false;
	
	// lets get the record type we need to use for the Income Records
	Schema.DescribeSObjectResult 	schemaDescribe 		= Budget_Allocation__c.SObjectType.getDescribe();
	map<Id, Schema.RecordTypeInfo> 	mapRecordTypeId 	= schemaDescribe.getRecordTypeInfosById();
		
	
	// checks singular parents (campaign and fin assist)
	//QBudgetAllocationProcessing.CheckBudgetAllocationParentIsNull(trigger.new);
	if(Test.isRunningTest())
	{
		QBudgetAllocationProcessing.CheckBudgetAllocationParentIsNull(trigger.new);
	}
	
	
	// only process budget allocation for singular records
	for(Budget_Allocation__c sBudgetAllocation : trigger.new)
	{
		if(mapRecordTypeId.get(trigger.new[0].RecordTypeId).getName() == 'Fin Assist BillsVouchers')
		{
			if(!bDiagnosedPersonProcessed)
			{
				bDiagnosedPersonProcessed = true;
				
				if(trigger.new[0].FA_Commencement__c != null)
				{
				
					Datetime tmEffectiveDate = Datetime.newInstance(
																	trigger.new[0].FA_Commencement__c.Year(),
																	trigger.new[0].FA_Commencement__c.Month(),
																	trigger.new[0].FA_Commencement__c.Day(),
																	0, 0, 0);
					try
					{
						QBudgetAllocationProcessing.SetPrior12MonthsFinancialAssistanceForDiagnosedPerson(trigger.new[0].Diagnosed_Person__c, tmEffectiveDate);
					}
					catch(Exception e)
					{
						System.Debug('Exception Thrown - Diagnosed Person 12mth Budget Processing - ' + e);
						sBudgetAllocation.addError('Diagnosed Person 12mth Budget Processing Error - ' + e);
					}
				}
				else
				{
					// do nothing
				}
			}
			else
			{
				//sBudgetAllocation.addError('The system can not bulk process Diagnosed Person Budget Allocations, please load seperatly by setting the batch size to 1');
			}
		}
	}
}