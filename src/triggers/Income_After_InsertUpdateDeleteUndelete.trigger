trigger Income_After_InsertUpdateDeleteUndelete on Income__c (after delete, after insert, after undelete, 
after update)
{
    /* potentially an income could be moved between opportunities, and after delete will only have trigger.old
        so we'll grab the oppty IDs from trigger.old and those from trigger.new to make sure all oppties are
        kept up to date. */
    /*
    set<id> setOpptyIds = new set<id>();
    list<Opportunity> liOppties = new list<Opportunity>();
       
    if(trigger.isDelete || trigger.isUpdate)
    {
        for(Income__c sIncome : trigger.old)
        {
            setOpptyIds.add(sIncome.Opportunity__c);
        }
    }
    
    if(!trigger.isDelete)
    {
        // have to get the oppty IDs from the old incomes 
        for(Income__c sIncome : trigger.new)
        {
            setOpptyIds.add(sIncome.Opportunity__c);
        }
    }
    
    System.Debug('We will be loading : ' + setOpptyIds);
    
    for(AggregateResult [] ARarr : [select  Sum(Amount_A__c)AmountA, sum(Amount_B__c)AccountB, Opportunity__r.Id OpptyId
                                                from    Income__c
                                                where   Opportunity__r.Id in : setOpptyIds
                                                    and Declined__c = false
                                                    and All_Income_Processed__c = true
                                                group by Opportunity__r.Id])
    {
        for(AggregateResult AR : ARarr)
        {
            System.Debug((Decimal)AR.get('AmountA') + ' ' + (id)AR.get('OpptyId'));
            
            Opportunity sUpdate = new Opportunity(Id = (id)AR.get('OpptyId'));
            sUpdate.Amount = (double)AR.get('AmountA') + (double)AR.get('AccountB');
            
            liOppties.add(sUpdate);
            
            if(liOppties.size() == 200)
            {
                update liOppties;
				liOppties.clear();
            }
        }
    }   
    
    if(liOppties.size() > 0)
    {
        update liOppties;
    }
    /*
    /////////////////////////// from David 04/04/2012
    if(trigger.isInsert)
    {
        map<string, Income__c> mapRefundIncome = new map<string, Income__c>();
        
        string strRTRefundIncomeId = [select id from RecordType where DeveloperName = :'Income_Refund_Payment_Gateway'].Id;
        PricebookEntry sRefundProduct = [select Id from PricebookEntry where Product2.Name = :'Refund Product' limit 1];
        
        for(Income__c sIncome : trigger.new)
        {
            if(sIncome.RecordTypeId == strRTRefundIncomeId && !sIncome.Declined__c)
            {
                mapRefundIncome.put(sIncome.Opportunity__c,sIncome);
            }
        }
        
        list<OpportunityLineItem> liOpptyLineItemsToInsert = new list<OpportunityLineItem>();
        
        for(AggregateResult [] ARarr : [select  Sum(TotalPrice)TotalPrice, OpportunityId OpptyId, Sum(Quantity)TotalQuantity
                                                from    OpportunityLineItem
                                                where   OpportunityId in :mapRefundIncome.keySet()
                                                group by OpportunityId])
        {
            for(AggregateResult AR : ARarr)
            {
                System.Debug((Decimal)AR.get('TotalPrice') + ' ' + (id)AR.get('OpptyId'));
                                
                OpportunityLineItem ol = new OpportunityLineItem();
                ol.OpportunityId = (id)AR.get('OpptyId');
                ol.TotalPrice = mapRefundIncome.get(ol.OpportunityId).Total_Amount__c;
                ol.Quantity = 1;
                ol.PricebookEntryId =  sRefundProduct.Id;
                
                liOpptyLineItemsToInsert.add(ol);
                
                if(liOpptyLineItemsToInsert.size() == 200)
                {
                    insert liOpptyLineItemsToInsert;
                    liOpptyLineItemsToInsert.clear();
                }
            }
        }
        
        if(liOpptyLineItemsToInsert.size() > 0)
        {
            insert liOpptyLineItemsToInsert;
        }
    }
    */
}