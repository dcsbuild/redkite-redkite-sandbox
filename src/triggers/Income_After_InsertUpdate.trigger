trigger Income_After_InsertUpdate on Income__c (after insert, after update) 
{
	/*
	set<string>			setAccountId 		= new set<string>();
	list<string> 		liOppToUpdate 		= new list<string>();
	map<string, Account> mapAccountById		= new map<string, Account>();
	
	
	for(Income__c i : trigger.new )
	{
		liOppToUpdate.add(i.Opportunity__c);
	}
	
	try{
		list<Opportunity> liOpps = [select Id, Club_Red_Income__c, AccountId from Opportunity where Id IN :liOppToUpdate];
		for(Opportunity o : liOpps)
		{
			o.Amount = o.Club_Red_Income__c;
			if(!setAccountId.contains(o.AccountId))
			{
				setAccountId.add(o.AccountId);
			}
		}
		//update liOpps;
	}
	catch(Exception e)
	{
		System.debug('No opportunity to update');
	}
	//****last donation amount + last donation date******
	for(Income__c sIncome : [	select	 id, Total_Amount__c, Opportunity__r.AccountId, All_Income_Processed_Date__c
								from 	 Income__c 
								where	 Opportunity__r.AccountId in :setAccountId
								and		 Account_Code__c in ('120','240','245','260')  
								and		 Declined__c = false
								and		 All_Income_Processed__c = true
								and 	 All_Income_Processed_Date__c != null
								and 	 RecordType.DeveloperName not in ('Income_Refund_Payment_Gateway','Club_Red_Refund')
								order by All_Income_Processed_Date__c desc])
	{	
   		if(mapAccountById.get(sIncome.Opportunity__r.AccountId) == null ) 
 		{
      		mapAccountById.put(sIncome.Opportunity__r.AccountId , 
      								new Account( Id = sIncome.Opportunity__r.AccountId, 
      										Last_Donation_Amount__c = sIncome.Total_Amount__c, 
      										Last_Donation_Date__c = sIncome.All_Income_Processed_Date__c ) );    
  		 }
	}
	//****volunteering Ind ****
	for(AggregateResult [] ARarr : [select   SUM(Volunteer_Value__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Declined__c = false
									and		 Personal_Donation__c = true
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Volunteering_Ind__c = (Double)AR.get('Field');
        }
    }
    //**** club red Commencement *****
	for(AggregateResult [] ARarr : [select   MIN(All_Income_Processed_Date__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Account_Code__c = '245'
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Club_Red_Commencement__c = (Datetime)AR.get('Field');
        }
    }
    //**** corporate income total 2010 ****
	for(AggregateResult [] ARarr : [select   SUM(Total_Amount__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Account_Code__c in ('810','820','830','840')
                                    and 	 All_Income_Processed_Date__c >= :Datetime.newInstance(2010,01,01,0,0,0)
                                    and 	 All_Income_Processed_Date__c <= :Datetime.newInstance(2010,12,31,23,59,59)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Corporate_Income_Total_2010__c = (Decimal)AR.get('Field');
        }
    }
    //***** corporate income total 2011 ****
	for(AggregateResult [] ARarr : [select   SUM(Total_Amount__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Account_Code__c in ('810','820','830','840')
                                    and 	 All_Income_Processed_Date__c >= :Datetime.newInstance(2011,01,01,0,0,0)
                                    and 	 All_Income_Processed_Date__c <= :Datetime.newInstance(2011,12,31,23,59,59)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Corporate_Income_Total_2011__c = (Decimal)AR.get('Field');
        }
    }
    //***** Donation Money this year *****
	for(AggregateResult [] ARarr : [select   SUM(Donation_Money__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Declined__c = false
                                    and 	 All_Income_Processed_Date__c >= :Datetime.newInstance(System.today().year(),01,01,0,0,0)
                                    and 	 All_Income_Processed_Date__c <= :Datetime.newInstance(System.today().year(),12,31,23,59,59)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Donation_Money_This_Year__c = (Decimal)AR.get('Field');
        }
    }
    //***** Event/Other Money *****
	for(AggregateResult [] ARarr : [select   SUM(EventOther_Monies__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Declined__c = false
                                    and 	 All_Reconciled_Date__c >= :Datetime.newInstance(2007,01,01,0,0,0)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).EventOther_Money__c = (Decimal)AR.get('Field');
        }
    }
    //***** First Donation date *****
	for(AggregateResult [] ARarr : [select   MIN(All_Income_Processed_Date__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Donation_Money__c > 0
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).First_Donation_Date__c = (Datetime)AR.get('Field');
        }
    }
    //***** First Non-Donation date *****
	for(AggregateResult [] ARarr : [select   MIN(All_Income_Processed_Date__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 EventOther_Monies__c > 0
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).First_Non_Donation_Income_Date__c = (Datetime)AR.get('Field');
        }
    }
    //***** GIK Ind *****
	for(AggregateResult [] ARarr : [select   SUM(GIKPB_Amount_Received__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Declined__c = false
									and		 Personal_Donation__c = true
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Gik_Ind__c = (Double)AR.get('Field');
        }
    }
    //***** Income Qty *****
	for(AggregateResult [] ARarr : [select   COUNT(Id) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Donation_Money__c > 0
                                    and 	 All_Reconciled_Date__c >= :Datetime.newInstance(2007,01,01,0,0,0)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Income_Qty__c = (Integer)AR.get('Field');
        }
    }
    //***** Last Non-Donation Date *****
	for(AggregateResult [] ARarr : [select   MAX(All_Income_Processed_Date__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 EventOther_Monies__c > 0
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Last_Non_Donation_Date__c = (Datetime)AR.get('Field');
        }
    }
    //***** Max donation this year *****
	for(AggregateResult [] ARarr : [select   MAX(Donation_Money__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Account_Code__c in ('120','240','245','260')
                                    and		 Declined__c = false 
                                    and 	 All_Income_Processed_Date__c >= :Datetime.newInstance(System.today().year(),01,01,0,0,0)
                                    and 	 All_Income_Processed_Date__c <= :Datetime.newInstance(System.today().year(),12,31,23,59,59)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Max_Donation_Amount_This_Year__c = (Double)AR.get('Field');
        }
    }
    //***** Money Donated 12 Months *****
    datetime dt = Datetime.newInstance(System.today().year(), System.today().addMonths(-1).month(), 31);
	for(AggregateResult [] ARarr : [select   SUM(Donation_Money__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Account_Code__c in ('120','240','245','260')  
                                    and 	 All_Income_Processed_Date__c >= :dt.addYears(-1)
                                    and 	 All_Income_Processed_Date__c <= :dt
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Money_Donated_12_Months__c = (Double)AR.get('Field');
        }
    }
    //***** Money Donated 2010 *****
	for(AggregateResult [] ARarr : [select   SUM(Donation_Money__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Account_Code__c in ('120','240','245','260') 
                                    and 	 All_Income_Processed_Date__c >= :Datetime.newInstance(2010,01,01,0,0,0)
                                    and 	 All_Income_Processed_Date__c <= :Datetime.newInstance(2010,12,31,23,59,59)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Money_Donated_2010__c = (Decimal)AR.get('Field');
        }
    }
    //***** Money Donated Last year *****
	for(AggregateResult [] ARarr : [select   SUM(Donation_Money__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Account_Code__c in ('120','240','245','260') 
                                    and		 Declined__c = false 
                                    and 	 All_Income_Processed_Date__c >= :Datetime.newInstance(System.today().addYears(-1).year(),01,01,0,0,0)
                                    and 	 All_Income_Processed_Date__c <= :Datetime.newInstance(System.today().addYears(-1).year(),12,31,23,59,59)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Money_Donated_Last_Year__c = (Decimal)AR.get('Field');
        }
    }
     //***** Pro Bono (Ind)*****
	for(AggregateResult [] ARarr : [select   SUM(Pro_Bono_Estimated_Value__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Declined__c = false
									and		 Personal_Donation__c = true
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Pro_Bono_Ind__c = (Double)AR.get('Field');
        }
    }
    //***** All income this year *****
	for(AggregateResult [] ARarr : [select   SUM(Total_Value__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and 	 All_Income_Processed_Date__c >= :Datetime.newInstance(System.today().year(),01,01,0,0,0)
                                    and 	 All_Income_Processed_Date__c <= :Datetime.newInstance(System.today().year(),12,31,23,59,59)
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).All_Income_This_Year__c = (Decimal)AR.get('Field');
        }
    }
    //***** All contributions *****
	for(AggregateResult [] ARarr : [select   SUM(Total_Value__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Declined__c = false
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).All_Contributions__c = (Decimal)AR.get('Field');
        }
    }
    //***** Club red *****
	for(AggregateResult [] ARarr : [select   SUM(Total_Value__c) Field, Opportunity__r.AccountId AccountId
                                    from     Income__c
                                    where    Opportunity__r.AccountId in :setAccountId
                                    and		 Declined__c = false
                                    and		 Account_Code__c = '245'
                                    group by Opportunity__r.AccountId])
    {
        for(AggregateResult AR : ARarr)
        {
            if(mapAccountById.get((string)AR.get('AccountId')) == null ) 
	 		{
	      		mapAccountById.put((string)AR.get('AccountId') , new Account(Id = (string)AR.get('AccountId') ) );    
	  		 }
	  		 mapAccountById.get((string)AR.get('AccountId')).Club_Red__c = (Decimal)AR.get('Field');
        }
    }
    //***********
	if(!mapAccountById.IsEmpty())
	{
		try
		{
			update mapAccountById.values();
		}
		catch (Exception e)
		{
			System.Debug('Exception thrown updating accounts: ' + e.getMessage());
		}
	}
	*/
}