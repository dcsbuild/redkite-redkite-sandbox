trigger Web_To_Lead_Notification_After_Insert on Lead (after insert)
{
	/*
	// sends a notification email to the relevant address when a lead is
	// created as a result of a web to lead form action
	list<Messaging.SingleEmailMessage> liEmails = new list<Messaging.SingleEmailMessage>();
		
	if(trigger.new.size() > 10)
	{
		return;
	}
	
	for(Lead sLead : trigger.new)
	{
		string strEmailAddress = '';
		string strFormName = '';

		RecordType sRT = [select Id, Name from RecordType where id = :sLead.RecordTypeId limit 1];
		
		System.Debug('\nLead: ' + sLead);
		System.Debug('\nRT Name: ' + sRT.Name);
				
		if(sRT.Name == 'Subscribe')
		{
			// Subscribe
			strFormName = 'Subscribe';
			strEmailAddress = QCacheWebsiteMetadata.getWebToLeadNotificationAddressForSubscribe();
		}
		else if(sRT.Name == 'Family Services')
		{
			// Email Support form
			strFormName = 'Email Support';
			strEmailAddress = QCacheWebsiteMetadata.getWebToLeadNotificationAddressForEmailSupport();
		}
		else if(sRT.Name == 'Community Fund Raising')
		{
			// Community Fundraising form
			strFormName = 'Community Fund Raising';
			strEmailAddress = QCacheWebsiteMetadata.getWebToLeadNotificationAddressForState(sLead.State);
		}
		else if(sRT.Name == 'Volunteer')
		{
			// Guess what?
			strFormName = 'Volunteer';
			strEmailAddress = QCacheWebsiteMetadata.getWebToLeadNotificationAddressForVolunteer();
		}
		else if(sRT.Name == 'General Contact Us')
		{
			// Contact Us form
			strFormName = 'Contact Us';
			strEmailAddress = QCacheWebsiteMetadata.getWebToLeadNotificationAddressForContactUs();
		}
		
		if(strEmailAddress != '')
		{
			
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			
			mail.setToAddresses(new String[] {strEmailAddress});
			mail.setBccSender(false);
			mail.setUseSignature(false);
			mail.setSubject(strFormName + ' Web to Lead Form Notification ' + strEmailAddress);
			mail.setSaveAsActivity(false);
			mail.setHtmlBody('<html><body><p>A new lead has been created as a result of a ' + strFormName + 
								' web to lead form submission. Please review and action, see <a href="http://emea.salesforce.com/' +
								 sLead.Id + '">http://emea.salesforce.com/' + sLead.Id + '</a> for more information.</body></html>');
			liEmails.add(mail);
		}
	}
	
	if(liEmails.size() > 0)
	{
		try
		{
			Messaging.sendEmail(liEmails);
		}
		catch(Exception e)
		{
			QUtils.CreateErrorTask(null, null, 'Corporate Website Issue - Web to Lead Notification Email', 'Exception raised: ' + e);
			System.Debug('Corporate Website Issue - Web to Lead Notification Email: Exception raised: ' + e);
		}
	}
	*/
}