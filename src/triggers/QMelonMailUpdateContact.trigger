trigger QMelonMailUpdateContact on Contact (before update)
{
	/**************************************
	
		TODO
		====
		
		* Add some error handling
	
	**************************************/
	integer iCounter = 0;
	integer iMaxThreads = 10;
	integer iMaxCallouts = 10;
	integer iFlags = 0;
	
	QMelonMailProcessing processor;
	QMelonMailProcessing.QMelonMailProcessingRet ret = null;

	// operations have to be done async has triggers aren't allowed to make callouts
	// we'll do the unsubs first as each one only requires 2 out of our 10 callouts
	QMelonMailProcessing.QMelonMailProcessingArgs args = new QMelonMailProcessing.QMelonMailProcessingArgs();
	args.m_iMaxThreads = iMaxThreads;
	args.m_iMaxCallouts = iMaxCallouts;
	args.m_eDatabase = QMelonMailProcessing.QMelonMailDB.DB_MARKETING;
	args.m_eBatchParam = QMelonMailProcessing.QMelonMailBatchParam.MM_ADD_CONTACTS_ASYNC;
	
	set<id>	setMarketingContacts = new set<id>();
	set<id>	setFurtherInfoContacts = new set<id>();
	set<id> setUnsubMarketingContacts = new set<id>();
	set<id> setUnsubFurtherInfoContacts = new set<id>();
	
	list<Contact> liUpdateContacts = new list<Contact>();

	for(Contact sContact : trigger.new)
	{
		if(sContact.HasOptedOutOfEmail)
		{
			// skip this contact
			// do we need to add the contact and unsub them from both DBs?
			continue;
		}
		
		Contact sOldContact = trigger.oldMap.get(sContact.Id);

		if(sContact.Email_Communication__c != null && sContact.Email_Communication__c.contains('Event Material'))
		{
			if(sOldContact.Email_Communication__c == null || !sOldContact.Email_Communication__c.contains('Event Material'))
			{
				setMarketingContacts.add(sContact.Id);
			}
		}
		else
		{
			if(sOldContact.Email_Communication__c != null && sOldContact.Email_Communication__c.contains('Event Material'))
			{
				setUnsubMarketingContacts.add(sContact.Id);
			}
		}

		if(sContact.Email_Communication__c != null && sContact.Email_Communication__c.contains('Newsletters'))
		{
			if(sOldContact.Email_Communication__c == null || !sOldContact.Email_Communication__c.contains('Newsletters'))
			{
				setFurtherInfoContacts.add(sContact.Id);
			}
		}
		else
		{
			if(sOldContact.Email_Communication__c != null && sOldContact.Email_Communication__c.contains('Newsletters'))
			{
				setUnsubFurtherInfoContacts.add(sContact.Id);
			}
		}
	}

	// marketing opt-in
	if(setMarketingContacts.size() > 0)
	{
		// add these
		args.m_setContactIds = setMarketingContacts;
		
		processor = new QMelonMailProcessing(args);
		ret = processor.ProcMain();

		iMaxThreads -= ret.m_iNumThreadsFired;
		iMaxCallouts -= ret.m_iNumCallouts;
		iCounter = 0;
		
		for(id sId : args.m_setContactIds)
		{
			if(iCounter >= ret.m_iNumRecordsProcessed)
			{
				iFlags = trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c.intValue();
				iFlags |= QMelonMailProcessing.m_iMarketingOptIn;
				iFlags &= ~QMelonMailProcessing.m_iMarketingOptOut;
				trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c = iFlags;
			}
			iCounter ++;
		}
	}

	// further info opt-in
	if(setFurtherInfoContacts.size() > 0)
	{
		integer iNumProcessed = 0;
		
		if(iMaxThreads >= 1)
		{
			args.m_iMaxThreads = iMaxThreads;
			args.m_iMaxCallouts = iMaxCallouts;
			args.m_eDatabase = QMelonMailProcessing.QMelonMailDB.DB_FURTHER_INFO;
			args.m_setContactIds = setFurtherInfoContacts;
			
			processor = new QMelonMailProcessing(args);
			ret = processor.ProcMain();
			
			iNumProcessed = ret.m_iNumRecordsProcessed;
		}

		iMaxThreads -= ret.m_iNumThreadsFired;
		iMaxCallouts -= ret.m_iNumCallouts;
		iCounter = 0;

		for(id sId : args.m_setContactIds)
		{
			if(iCounter >= iNumProcessed)
			{
				iFlags = trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c.intValue();
				iFlags |= QMelonMailProcessing.m_iFurtherInfoOptIn;
				iFlags &= ~QMelonMailProcessing.m_iFurtherInfoOptOut;
				trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c = iFlags;
			}
			iCounter ++;
		}
	}
	
	// marketing opt-out
	if(setUnsubMarketingContacts.size() > 0)
	{
		integer iNumProcessed = 0;
		
		if(iMaxThreads >= 1)
		{
			args.m_iMaxThreads = iMaxThreads;
			args.m_iMaxCallouts = iMaxCallouts;
			args.m_eDatabase = QMelonMailProcessing.QMelonMailDB.DB_MARKETING;
			args.m_eBatchParam = QMelonMailProcessing.QMelonMailBatchParam.MM_UNSUBSCRIBE_ASYNC;
			args.m_setContactIds = setUnsubMarketingContacts;
			
			processor = new QMelonMailProcessing(args);
			ret = processor.ProcMain();
			
			iNumProcessed = ret.m_iNumRecordsProcessed;
		}

		iMaxThreads -= ret.m_iNumThreadsFired;
		iMaxCallouts -= ret.m_iNumCallouts;
		iCounter = 0;

		for(id sId : args.m_setContactIds)
		{
			if(iCounter >= iNumProcessed)
			{
				iFlags = trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c.intValue();
				iFlags |= QMelonMailProcessing.m_iMarketingOptOut;
				iFlags &= ~QMelonMailProcessing.m_iMarketingOptIn;
				trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c = iFlags;
			}
			iCounter ++;
		}
	}

	// marketing opt-out
	if(setUnsubFurtherInfoContacts.size() > 0)
	{
		integer iNumProcessed = 0;
		
		if(iMaxThreads >= 1)
		{
			args.m_iMaxThreads = iMaxThreads;
			args.m_iMaxCallouts = iMaxCallouts;
			args.m_eDatabase = QMelonMailProcessing.QMelonMailDB.DB_FURTHER_INFO;
			args.m_eBatchParam = QMelonMailProcessing.QMelonMailBatchParam.MM_UNSUBSCRIBE_ASYNC;
			args.m_setContactIds = setUnsubFurtherInfoContacts;
			
			processor = new QMelonMailProcessing(args);
			ret = processor.ProcMain();
			
			iNumProcessed = ret.m_iNumRecordsProcessed;
		}

		iCounter = 0;

		for(id sId : args.m_setContactIds)
		{
			if(iCounter >= iNumProcessed)
			{
				iFlags = trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c.intValue();
				iFlags |= QMelonMailProcessing.m_iFurtherInfoOptOut;
				iFlags &= ~QMelonMailProcessing.m_iFurtherInfoOptIn;
				trigger.newMap.get(sId).Melon_Mail_Sync_Flags__c = iFlags;
			}
			iCounter ++;
		}
	}	
}