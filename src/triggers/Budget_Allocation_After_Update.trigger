trigger Budget_Allocation_After_Update on Budget_Allocation__c (after update) 
{
	QBudgetAllocationProcessing.PushDetailToParentCampaign(trigger.new);
}