public with sharing class QMelonMailProcessing
{
	/*********************************************
		TODO
		====
		
		* Add database IDs to meta data
		* Check return values
		* Hide public methods used for testing

	*********************************************/

	public class QMelonMailProcessingException	extends Exception{}

	public enum QMelonMailDB			{DB_MARKETING, DB_FURTHER_INFO, DB_BOTH}
	public enum QMelonMailBatchParam	{MM_ADD_CONTACTS, MM_UNSUBSCRIBE, MM_ADD_CONTACTS_ASYNC, MM_UNSUBSCRIBE_ASYNC}
	
	///////////////////////////////////////////////////////////////////////////
	// STATIC VARIABLES

	// static used to control triggers associated with melon mail sync fields
	public static boolean m_bDisableUpdateTrigger	{get; set;}
	
	// final (const) values used when examining the 
	// contact.Melon_Mail_Sync_Flags__c bit field
	public static final integer	m_iMarketingOptIn		= 1;
	public static final integer	m_iMarketingOptOut		= 2;
	public static final integer	m_iFurtherInfoOptIn		= 4;
	public static final integer m_iFurtherInfoOptOut	= 8;

	///////////////////////////////////////////////////////////////////////////
	// XMLRPC Parameter class + enum
	public enum	QXMLRPCParameterType {P_INTEGER, P_BOOLEAN, P_STRING}
	
	public class QXMLRPCParameter
	{
		string					m_strName;
		string					m_strValue;
		QXMLRPCParameterType	m_eType;

		QXMLRPCParameter(string strName, string strValue, QXMLRPCParameterType eType)
		{
			m_strName	= strName;
			m_strValue	= strValue;
			m_eType		= eType;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Args class	
	public class QMelonMailProcessingArgs
	{
		public	set<id>					m_setContactIds	{get; set;}
		public	QMelonMailBatchParam	m_eBatchParam	{get; set;}
		public	QMelonMailDB			m_eDatabase		{get; set;}
		public	integer					m_iMaxThreads	{get; set;}
		public	integer					m_iMaxCallouts	{get; set;}
	}

	///////////////////////////////////////////////////////////////////////////
	// Return value class
	public class QMelonMailProcessingRet
	{
		public integer		m_iNumRecordsProcessed;
		public integer		m_iNumThreadsFired;
		public integer		m_iNumCallouts;
		public boolean 		m_bError;
		public list<string>	m_liErrorMessages;

		QMelonMailProcessingRet()
		{
			m_iNumRecordsProcessed = 0;
			m_iNumThreadsFired = 0;
			m_iNumCallouts = 0;
			m_bError = false;
			m_liErrormessages = new list<string>();
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Singular member variables
	private string 			m_strMelonMailURL;
	private integer			m_iMarketingDatabaseId;
	private	integer			m_iFurtherInfoDatabaseId;
	private integer			m_iNumHTTPCallouts;

	private QMelonMailProcessingArgs	m_Args;
	private QMelonMailProcessingRet		m_Ret;
	
	// set this to false for TESTING ONLY
	private boolean	m_bAllowCallouts = true;
	
	///////////////////////////////////////////////////////////////////////////
	// Collections
	private list<QXMLRPCParameter>	m_liParameters					= new list<QXMLRPCParameter>();
	private list<QXMLRPCParameter>	m_liPostContactArrayParameters	= new list<QXMLRPCParameter>();

	private list<string>	m_liStringValues	= new list<string>();
	private list<boolean>	m_liBooleanValues	= new list<boolean>();
	private list<integer>	m_liIntegerValues	= new list<integer>();

	public QMelonMailProcessing(QMelonMailProcessingArgs oArgs)
	{
		m_Args = oArgs;
		m_Ret = new QMelonMailProcessingRet();

		m_strMelonMailURL = '';
		m_iNumHTTPCallouts = 0;
		
		// to get these values (when filling in the metadata) use the db search method in this class
		// these lines will need to be commented out for that to work as they'll crash if the data
		// has not been set
		m_iMarketingDatabaseId = QCacheWebsiteMetadata.getMelonMailMarketingDatabaseId();
		m_iFurtherInfoDatabaseId = QCacheWebsiteMetadata.getMelonMailFurtherInfoDatabaseId();	
	}

	public QMelonMailProcessingRet ProcMain()
	{
		System.Debug('QMelonMailProcessingRet ProcMain() args: ' + m_Args);

		if(m_Args.m_setContactIds == null)
		{
			m_Ret.m_liErrorMessages.add('No contact IDs specified.');
		}
		else
		{
			if(m_Args.m_eBatchParam == QMelonMailBatchParam.MM_ADD_CONTACTS)
			{
				AddContacts();
			}
			else if(m_Args.m_eBatchParam == QMelonMailBatchParam.MM_ADD_CONTACTS_ASYNC)
			{
				SetupAddContactsAsync();
			}
			else if(m_Args.m_eBatchParam == QMelonMailBatchParam.MM_UNSUBSCRIBE)
			{
				UnsubscribeContacts();
			}
			else if(m_Args.m_eBatchParam == QMelonMailBatchParam.MM_UNSUBSCRIBE_ASYNC)
			{
				SetupUnsubscribeContactsAsync();	
			}
		}
				
		System.Debug(m_Ret);

		m_Ret.m_bError = (m_Ret.m_liErrorMessages.size() > 0);
		return m_Ret;
	}
	
	public void TESTONLYDisableCallouts()
	{
		m_bAllowCallouts = false;
	}

	/***********************************************************************************************************
		@Future Methods
	***********************************************************************************************************/
	@future(callout=true)
	private static void FireAddContacts(set<id> setProcessList, string strDBName)
	{
		QMelonMailProcessingArgs 	oArgs = new QMelonMailProcessingArgs();
		QMelonMailProcessingRet		oRet;
		QMelonMailProcessing		oProcesser;
		
		set<id> 		setContactIdsToUpdate = new set<id>();
		list<Contact>	liContactsToUpdate;	

		oArgs.m_setContactIds	= setProcessList;
		oArgs.m_eDatabase		= (strDBName == 'MKT' ? QMelonMailDB.DB_MARKETING : QMelonMailDB.DB_FURTHER_INFO);
		oArgs.m_eBatchParam		= QMelonMailBatchParam.MM_ADD_CONTACTS;

		oProcesser = new QMelonMailProcessing(oArgs);		
		oRet = oProcesser.ProcMain();
		
		if(oRet.m_bError)
		{
			integer iCounter = 0;
			integer iFlags = 0;
			
			if(oArgs.m_eDatabase == QMelonMailDB.DB_MARKETING)
			{
				iFlags |= m_iMarketingOptIn;
			}
			else if(oArgs.m_eDatabase == QMelonMailDB.DB_FURTHER_INFO)
			{
				iFlags |= m_iFurtherInfoOptIn;
			}
			
			// flag the unprocessed contacts as needing to be updated with MM
			for(id sId : oArgs.m_setContactIds)
			{
				if(iCounter >= oRet.m_iNumRecordsProcessed)
				{
					setContactIdsToUpdate.add(sID);
				}
			}
			
			try
			{
				liContactsToUpdate = [select id, Melon_Mail_Sync_Flags__c from Contact where id in :setContactIdsToUpdate];
				
				for(Contact sContact : liContactsToUpdate)
				{
					sContact.Melon_Mail_Sync_Flags__c = iFlags;	
				}
				
				m_bDisableUpdateTrigger = true;
				update(liContactsToUpdate);
				m_bDisableUpdateTrigger = false;
			}
			catch(Exception e)
			{
				System.Debug('Exception thrown updating melon mail sync flags on contacts: ' + e);
			}
		}
		
		System.Debug(oRet);
	}

	@future(callout=true)
	private static void FireUnsubscribeContacts(set<id> setProcessList, string strDBName)
	{
		QMelonMailProcessingArgs 	oArgs = new QMelonMailProcessingArgs();
		QMelonMailProcessingRet		oRet;
		QMelonMailProcessing		oProcesser;
		
		set<id> 		setContactIdsToUpdate = new set<id>();
		list<Contact>	liContactsToUpdate;

		oArgs.m_setContactIds	= setProcessList;
		oArgs.m_eBatchParam		= QMelonMailBatchParam.MM_UNSUBSCRIBE;
		
		if(strDBName == 'MKT')
		{
			oArgs.m_eDatabase = QMelonMailDB.DB_MARKETING;
		}
		else if(strDBName == 'FI')
		{
			oArgs.m_eDatabase = QMelonMailDB.DB_FURTHER_INFO;
		}
		else
		{
			oARgs.m_eDatabase = QMelonMailDB.DB_BOTH;
		}

		oProcesser = new QMelonMailProcessing(oArgs);
		oRet = oProcesser.ProcMain();
		
		if(oRet.m_bError)
		{
			integer iCounter = 0;
			integer iFlags = 0;
			
			if(oArgs.m_eDatabase == QMelonMailDB.DB_MARKETING || oARgs.m_eDatabase == QMelonMailDB.DB_BOTH)
			{
				iFlags |= m_iMarketingOptOut;
			}
			
			if(oArgs.m_eDatabase == QMelonMailDB.DB_FURTHER_INFO || oARgs.m_eDatabase == QMelonMailDB.DB_BOTH)
			{
				iFlags |= m_iFurtherInfoOptOut;
			}
			
			// flag the unprocessed contacts as needing to be updated with MM
			for(id sId : oArgs.m_setContactIds)
			{
				if(iCounter >= oRet.m_iNumRecordsProcessed)
				{
					setContactIdsToUpdate.add(sID);
				}
			}
			
			try
			{
				liContactsToUpdate = [select id, Melon_Mail_Sync_Flags__c from Contact where id in :setContactIdsToUpdate];
				
				for(Contact sContact : liContactsToUpdate)
				{
					sContact.Melon_Mail_Sync_Flags__c = iFlags;	
				}
				
				m_bDisableUpdateTrigger = true;
				update(liContactsToUpdate);
				m_bDisableUpdateTrigger = false;
			}
			catch(Exception e)
			{
				System.Debug('Exception thrown updating melon mail sync flags on contacts: ' + e);
			}
		}
		
		System.Debug(oRet);
	}

	/***********************************************************************************************************
		Setup methods for future calls
	***********************************************************************************************************/	
	private void SetupAddContactsAsync()
	{
		if(m_Args.m_eDatabase == QMelonMailDB.DB_BOTH)
		{
			M_Ret.m_liErrorMessages.add('DB_BOTH is invalid for AddContacts.');
			return;
		}
		
		if(m_Args.m_iMaxThreads > 0 && m_Args.m_iMaxCallouts > 1)
		{
			if(m_Args.m_eDatabase == QMelonMailDB.DB_MARKETING)
			{
				FireAddContacts(m_Args.m_setContactIds, 'MKT');
			}
			else if(m_Args.m_eDatabase == QMelonMailDB.DB_FURTHER_INFO)
			{
				FireAddContacts(m_Args.m_setContactIds, 'FI');
			}
			
			// with one login and one api call we can process all of these contacts
			m_Ret.m_iNumCallouts += 2;
			m_Ret.m_iNumThreadsFired ++;
			m_Ret.m_iNumRecordsProcessed = m_Args.m_setContactIds.size();
		}
		else
		{
			m_Ret.m_liErrorMessages.add('No more @future threads allowed.');
		}
	}
	
	private void SetupUnsubscribeContactsAsync()
	{
		// this job needs to be split up so that we don't go over the number of HTTP requests allowed per
		// @future method call (10)
		integer iNumRecordsToProcess = m_Args.m_setContactIds.size();

		string	strType;

		if(m_Args.m_eDatabase == QMelonMailDB.DB_MARKETING)
		{
			strType = 'MKT';
		}
		else if(m_Args.m_eDatabase == QMelonMailDB.DB_FURTHER_INFO)
		{
			strType = 'FI';
		}
		else
		{
			strType = 'BOTH';
		}

		set<id>	setIdsToProcess = new set<id>();
		
		if(m_Args.m_iMaxThreads > 0 && m_Args.m_iMaxCallouts > 1)
		{
			for(id idContact : m_Args.m_setContactIds)
			{
				setIdsToProcess.add(idContact);

				// do we have enough callouts for this set?
				if(setIdsToProcess.size() + 1 == m_Args.m_iMaxCallouts)
				{
					FireUnsubscribeContacts(setIdsToProcess, strType);
					setIdsToProcess.clear();
					
					m_Ret.m_iNumThreadsFired ++;				
					m_Ret.m_iNumRecordsProcessed += setIdsToProcess.size();
					m_Ret.m_iNumCallouts += setIdsToProcess.size() + 1;
					
					// this will happen everytime for now if we do a large batch but the gov might change in the future
					if(m_Ret.m_iNumThreadsFired == m_Args.m_iMaxThreads || m_Ret.m_iNumCallouts == m_Args.m_iMaxCallouts)
					{
						// we've run the maximum that we're allowed to so we have to bail
						m_Ret.m_liErrorMessages.add('Only processed ' + m_Ret.m_iNumRecordsProcessed + ' records out of ' + m_Args.m_setContactIds.size());
						break;
					}
				}
			}
		}
		
		// do we have any stragglers that we can process?
		if(setIdsToProcess.size() > 0 && m_Ret.m_iNumThreadsFired < m_Args.m_iMaxThreads && m_Ret.m_iNumCallouts < 10)
		{
			FireUnsubscribeContacts(setIdsToProcess, strType);
			m_Ret.m_iNumThreadsFired ++;
			m_Ret.m_iNumCallouts = setIdsToProcess.size() + 1;
			m_Ret.m_iNumRecordsProcessed += setIdsToProcess.size();
		}
	}
	
	/***********************************************************************************************************
		Login + Basic Calls
	***********************************************************************************************************/	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public boolean DoLoginRequest()
	{
		httpResponse response;
		
		m_liParameters.clear();
		m_liPostContactArrayParameters.clear();
		m_liParameters.add(new QXMLRPCParameter('username', QCacheWebsiteMetadata.getMelonMailLoginUsername(), QXMLRPCParameterType.P_STRING));
		m_liParameters.add(new QXMLRPCParameter('password', QCacheWebsiteMetadata.getMelonMailLoginPassword(), QXMLRPCParameterType.P_STRING));
		
		/*'http://spellr.us/capture_red/xmlrpcserver.php?version=1.2'*/	// test URL
		
		if(m_bAllowCallouts)
		{
			response = BuildAndSendHttpRequest(QCacheWebsiteMetadata.getMelonMailLoginURL(), BuildXMLRPCBody('login', null));
	
			if(response == null)
			{
				return false;
			}
			
			ParseXML(response.getBody());
		}
		else
		{
			ParseXML('<?xml version="1.0" encoding="utf-8"?>' +
								'<methodResponse>' +
									'<params><param>' +
										'<value><string>https://melonmail.melon.com.au/api/xmlrpcserver.php?version=1.2&#38;v6_session=e3e2db6b1a5003900b19116454d5622d</string></value>' +
									'</param></params>' +
								'</methodResponse>');
		}

		// should have ony returned 1 string
		if(m_liStringValues.size() != 1)				
		{
			System.debug('Invalid number of string values returned.\nm_liStringValues = ' + m_liStringValues);
			m_strMelonMailURL = null;
		}
		else
		{
			m_strMelonMailURL = m_liStringValues.get(0);
			System.Debug('URL to use: ' + m_strMelonMailURL);
		}

		return (m_strMelonMailURL != null);
	}
	
	private boolean DoLoginCheck()
	{
		m_liParameters.clear();
		m_liPostContactArrayParameters.clear();

		httpResponse response = BuildAndSendHttpRequest(m_strMelonMailURL, BuildXMLRPCBody('isLoggedIn', null));

		if(response == null)
		{
			return false;
		}
		
		ParseXML(response.getBody());
	
		// should only be 1 boolean in the response
		if(m_liBooleanValues.size() != 1)
		{
			return false;
		}
		else
		{
			return m_liBooleanValues[0];	
		}
	}

	// debugging only - just returns all the contacts
	/*
	public boolean SearchContacts()
	{
		m_liParameters.clear();
		m_liPostContactArrayParameters.clear();
		m_liParameters.add(new QXMLRPCParameter('database_id', '' + m_iMarketingDatabaseId, QXMLRPCParameterType.P_INTEGER));

		httpResponse response = BuildAndSendHttpRequest(m_strMelonMailURL, BuildXMLRPCBody('searchContacts', null));

		if(response == null)
		{
			return false;
		}
		
		ParseXML(response.getBody());
		return true;
	}

	// for testing purposes only - we should hide this again later
	public integer GetDataBaseByName(string strName)
	{
		if(m_strMelonMailURL == '')
		{
			DoLoginRequest();
		}
		
		m_liParameters.clear();
		m_liPostContactArrayParameters.clear();
		m_liParameters.add(new QXMLRPCParameter('database_name', strName, QXMLRPCParameterType.P_STRING));
		
		httpResponse response = BuildAndSendHttpRequest(m_strMelonMailURL, BuildXMLRPCBody('getDatabaseByName', null));
	
		if(response == null)
		{
			return -1;
		}
		
		XMLDom dom = new XMLDom(response.getBody());
//		dom.dumpAll();
		
		list<XMLDom.Element> liElements = dom.getElementsByTagName('struct');
		string strDB = '';

		if(liElements.size() > 0)
		{
			for(XMLDom.Element e : liElements)
			{
				if(e.getValue('name') == 'id')
				{
					strDB = e.parentNode.getValue('string');
					break;
				}
			}
		}
		
		System.Debug('*********************\n' + strDB);
		
		if(strDB == null || strDB == '')
		{
			System.Debug('Failed to get database ID');
			return -1;
		}
		else
		{
			return integer.valueOf(strDB);
		}
	}
	*/
	
	/***********************************************************************************************************
		Register Contacts
	***********************************************************************************************************/		
	private void AddContacts()
	{
		// login if we need to
		if(m_strMelonMailURL == null || m_strMelonMailURL == '')
		{
			if(!DoLoginRequest())
			{
				m_Ret.m_liErrorMessages.add('Error logging into Melon Mail');
				m_Ret.m_bError = true;
				return;
			}
			
			m_Ret.m_iNumCallouts ++;
		}
		
		if(m_Ret.m_iNumCallouts >= m_Args.m_iMaxCallouts)
		{
			return;
		}

		if(m_Args.m_setContactIds == null || m_Args.m_setContactIds.size() == 0)
		{
			m_Ret.m_liErrorMessages.add('No contact ids specified.');
		}
		else
		{
			try
			{
				m_liParameters.clear();
				
				if(m_Args.m_eDatabase == QMelonMailDB.DB_MARKETING)
				{
					m_liParameters.add(new QXMLRPCParameter('database_id', '' + m_iMarketingDatabaseId, QXMLRPCParameterType.P_INTEGER));
				}
				else
				{
					m_liParameters.add(new QXMLRPCParameter('database_id', '' + m_iFurtherInfoDatabaseId, QXMLRPCParameterType.P_INTEGER));
				}

				m_liPostContactArrayParameters.clear();
				m_liPostContactArrayParameters.add(new QXMLRPCParameter('overwrite', '1', QXMLRPCParameterType.P_INTEGER));
				m_liPostContactArrayParameters.add(new QXMLRPCParameter('remove_unsubscribers', '1', QXMLRPCParameterType.P_INTEGER));
				
				list<Contact> liContacts =  [select		id, Firstname, LastName, EMail, MobilePhone
												from	Contact
												where	id in :m_Args.m_setContactIds];

				string strXML = BuildXMLRPCBody('addContacts', liContacts);
				
				if(m_bAllowCallouts)
				{
					httpResponse response = BuildAndSendHttpRequest(m_strMelonMailURL, strXML);
					
					if(response.getBody().contains('faultCode'))
					{
						m_Ret.m_liErrorMessages.add('Fault code returned by Melon Mail: ' + response.getBody());
					}
					else
					{
						m_Ret.m_iNumRecordsProcessed = liContacts.size();
						m_Ret.m_iNumCallouts ++;
					}
				}
				else
				{
					m_Ret.m_iNumRecordsProcessed = liContacts.size();
					m_Ret.m_iNumCallouts ++;					
				}
			}
			catch(Exception e)
			{
				m_Ret.m_liErrorMessages.add('Exception thrown trying to register contacts with Melon Mail: ' + e);
			}
		}
	}

	/***********************************************************************************************************
		Unsubscribe Contact
	***********************************************************************************************************/		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private void UnsubscribeContacts()
	{
		// login if we need to
		if(m_strMelonMailURL == null || m_strMelonMailURL == '')
		{
			if(!DoLoginRequest())
			{
				m_Ret.m_liErrorMessages.add('Error logging into Melon Mail');
				m_Ret.m_bError = true;
				return;
			}
			
			m_Ret.m_iNumCallouts ++;
		}

		if(m_Ret.m_iNumCallouts >= m_Args.m_iMaxCallouts)
		{
			return;
		}
		
		if(m_Args.m_setContactIds == null || m_Args.m_setContactIds.size() == 0)
		{
			m_Ret.m_liErrorMessages.add('No contact ids specified.');
		}
		else
		{
			try
			{
				for(Contact sContact : [select	id, EMail
										from	Contact
										where	id in :m_Args.m_setContactIds])
				{
					m_liParameters.clear();
					m_liParameters.add(new QXMLRPCParameter('email', sContact.EMail, QXMLRPCParameterType.P_STRING));

					if(m_Args.m_eDatabase == QMelonMailDB.DB_MARKETING)
					{
						m_liParameters.add(new QXMLRPCParameter('database_id', '' + m_iMarketingDatabaseId, QXMLRPCParameterType.P_INTEGER));
					}
					else if(m_Args.m_eDatabase == QMelonMailDB.DB_FURTHER_INFO)
					{
						m_liParameters.add(new QXMLRPCParameter('database_id', '' + m_iFurtherInfoDatabaseId, QXMLRPCParameterType.P_INTEGER));
					}
					else if(m_Args.m_eDatabase == QMelonMailDB.DB_BOTH)
					{
						m_liParameters.add(new QXMLRPCParameter('database_id', '0', QXMLRPCParameterType.P_INTEGER));
					}
				
					string strXML = BuildXMLRPCBody('unsubscribeContact', null);
					
					if(m_bAllowCallouts)
					{
						httpResponse response = BuildAndSendHttpRequest(m_strMelonMailURL, strXML);
						
						if(response.getBody().contains('faultCode'))
						{
							// don't process any more
							m_Ret.m_liErrorMessages.add('Fault code returned by Melon Mail: ' + response.getBody())	;
							break;
						}
					}
					
					m_Ret.m_iNumRecordsProcessed ++;
					m_Ret.m_iNumCallouts ++;
					
					if(m_Ret.m_iNumCallouts >= m_Args.m_iMaxCallouts)
					{
						break;
					}
				}
			}
			catch(Exception e)
			{
				m_Ret.m_liErrorMessages.add('Exception thrown trying to register contacts with Melon Mail: ' + e);
			}
		}
	}

	/***********************************************************************************************************
		Utility Functions
	***********************************************************************************************************/		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	private HttpResponse BuildAndSendHttpRequest(string strURL, string strBody)
	{
		Http 			http;
		HttpRequest		httpRequest;
		HttpResponse 	httpResponse;

		if(m_iNumHTTPCallouts == 10)
		{
			m_Ret.m_liErrorMessages.add('Too many HTTP callouts attempted');
			return null;
		}

		try
		{
			http = new Http();
			
			httpRequest = new HttpRequest();
			
			httpRequest.setEndpoint(strURL);
			httpRequest.setBody(strBody);
			httpRequest.setMethod('POST');
			httpRequest.setCompressed(false);
			httpRequest.setHeader('Content-Type', 'text/xml');
			httpRequest.setHeader('User-Agent', 'XMLRPC');
			
/*			System.Debug('End Point: ' + httpRequest.getEndpoint());
			System.Debug('Request Body: ' + httpRequest.getBody());
			System.Debug('Request: ' + httpRequest);
*/
			httpResponse = http.send(httpRequest);
			m_iNumHTTPCallouts ++;
			
			if(httpResponse.getStatusCode() != 200)
			{
				if(httpResponse != null)
					System.Debug(httpResponse);

				System.Debug('Melon Mail Webservices: HTTP request unsuccessful - ' + httpResponse.getStatusCode());
				throw new QMelonMailProcessingException('Error Code - ' + httpResponse.getStatusCode());
			}
			
//			System.Debug('Melon Mail Webservices: HTTP response recieved - ' + httpResponse);
			System.Debug('Melon Mail Webservices: HTTP response body     - ' + httpResponse.getBody());
		}
		catch(QMelonMailProcessingException mmpe)
		{
			System.debug('Melon Mail Processing Exception - ' + mmpe);
			System.Debug('Melon Mail Website HTTP Request - ' + httpRequest);
						
			if(httpResponse != null)
				System.Debug(httpResponse.getBody());

			m_Ret.m_liErrorMessages.add('Problem making HTTP request: ' + mmpe);
			return null;
		}
		catch(Exception e)
		{
			System.debug('Web Service Exception - ' + e);
			System.Debug('Melon Mail Website HTTP Request - ' + httpRequest);
						
			if(httpResponse != null)
				System.Debug(httpResponse.getBody());

			m_Ret.m_liErrorMessages.add('Exception thrown while making HTTP callout: ' + e);
			return null;
		}

		return httpResponse;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public string BuildXMLRPCBody(string strMethodName, list<Contact> liContacts)
	{
		string strXMLRPC = '<?xml version="1.0" encoding="utf-8"?>\n';
		strXMLRPC += '<methodCall>\n' +
						'<methodName>' + strMethodName + '</methodName>\n';
		
		strXMLRPC += '<params>\n';

		System.Debug('\n\nParameters: ' + m_liParameters);

		// general arguments if we have any
		strXMLRPC += GetParametersBlock(m_liParameters);
		
		// contact array parameter if we need it
		if(liContacts != null && liContacts.size() > 0)
		{
			strXMLRPC += '<param>\n' +
							'<struct>\n';
			integer ic = 0;
				
			for(Contact sContact : liContacts)
			{
				strXMLRPC +=	'<member>\n' +
									'<name>' + ic + '</name>\n';
			
				strXMLRPC += 		'<value>\n' +
										'<struct>\n' + 
											'<member>\n' +
												'<name>Email</name>\n' +
												'<value><string>' + sContact.Email + '</string></value>\n' +
											'</member>\n' +
											'<member>\n' +
												'<name>Mobile</name>\n' +
												'<value><string>' + sContact.MobilePhone + '</string></value>\n' +
											'</member>\n' +
											'<member>\n' +
												'<name>First Name</name>\n' +
												'<value><string>' + sContact.FirstName + '</string></value>\n' +
											'</member>\n' +
											'<member>\n' +
												'<name>Last Name</name>\n' +
												'<value><string>' + sContact.LastName + '</string></value>\n' +
											'</member>\n' +
										'</struct>\n' +
									'</value>\n' +
								'</member>';		
				ic++;
			}

			strXMLRPC +=	'</struct>\n' +
						'</param>\n';
		}

		// post-contact array arguments if we have any
		// strXMLRPC += GetParametersBlock(m_liPostContactArrayParameters);

		strXMLRPC += '</params>\n';		
		strXMLRPC += '</methodCall>';

		system.debug('**************************\nstrXMLRPC = ' + strXMLRPC);

		return strXMLRPC;
	}

	private string GetParametersBlock(list<QXMLRPCParameter> liParameters)
	{
		if(liParameters == null || liParameters.size() == 0)
		{
			return '';
		}

		string strRet;

		boolean bFirst = true;
		
		for(QXMLRPCParameter param : liParameters)
		{
			if(bFirst)
			{
				strRet = '<param>\n' +
								'<value>\n';
				bFirst = false;
			}
			else
			{
				strRet += '<param>\n' +
								'<value>\n';				
			}
							
			if(param.m_eType == QXMLRPCParameterType.P_INTEGER)
			{
				strRet +=	'<int>' + param.m_strValue + '</int>\n';
			}
			else if(param.m_eType == QXMLRPCParameterType.P_BOOLEAN)
			{
				strRet +=	'<boolean>' + param.m_strValue + '</boolean>\n';
			}
			else if(param.m_eType == QXMLRPCParameterType.P_STRING)
			{
				strRet +=	'<string>' + param.m_strValue + '</string>\n';
			}
			
			strRet += 	'</value>\n' +
						'</param>\n';
		}
	
		return strRet;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public void ParseXML(string strXML)
	{
		XmlStreamReader xmlReader = new XmlStreamReader(strXML);

		m_liStringValues.clear();
		m_liBooleanValues.clear();
		m_liIntegerValues.clear();

		while(xmlReader.hasNext())
		{
			if(xmlReader.isStartElement())
			{
				if(xmlReader.getLocalName() == 'value')
				{
					// find out what type this is....
					xmlReader.nextTag();

					if(xmlReader.getLocalName() == 'string')
					{
						xmlReader.next();
						string strVal = '';

						while(!xmlReader.isEndElement())
						{
							strVal += xmlReader.getText();
							xmlReader.next();
						}

						System.Debug('Found string value: ' + strVal);
						m_liStringValues.add(strVal);
					}
					else if(xmlReader.getLocalName() == 'boolean')
					{
						xmlReader.next();
					
						// for safety
						if(!xmlReader.isEndElement())
						{
							m_liBooleanValues.add(xmlReader.getText() == '1');
						}
					}
					else if(xmlReader.getLocalName() == 'int')
					{
						xmlReader.next();
						
						if(!xmlReader.isEndElement())
						{
							m_liIntegerValues.add(integer.valueOf(xmlReader.getText()));
						}
					}
				}
			}

			xmlReader.next();
		}
	}
	
	/***********************************************************************************************************
		Test methods
	***********************************************************************************************************/
	public static testmethod void testQMelonMailProcessing()
	{
		Contact sContact = new Contact();
		sContact.FirstName = 'Test';
		sContact.LastName = 'Man';
		sContact.MobilePhone = '040404040404';
		sContact.Email = 'surethisaddress@doesnotexist.com';
		insert sContact;
		
		QMelonMailProcessingArgs args = new QMelonMailProcessingArgs();
		args.m_eBatchParam = QMelonMailBatchParam.MM_ADD_CONTACTS;
		args.m_eDatabase = QMelonMailDB.DB_MARKETING;
		args.m_iMaxCallouts = 10;
		args.m_iMaxThreads = 10;

		// no contacts
		QMelonMailProcessing mmp = new QMelonMailProcessing(args);
		QMelonMailProcessingRet ret = mmp.ProcMain();
		System.Assert(ret.m_bError);

		args.m_setContactIds = new set<id>();
		args.m_setContactIds.add(sContact.id);
		
		// marketing db add
		mmp = new QMelonMailProcessing(args);
		mmp.TESTONLYDisableCallouts();		
		ret = mmp.ProcMain();
		System.Assert(!ret.m_bError);
	
		// marketing db remove
		args.m_eBatchParam = QMelonMailBatchParam.MM_UNSUBSCRIBE;	
		mmp = new QMelonMailProcessing(args);
		mmp.TESTONLYDisableCallouts();		
		ret = mmp.ProcMain();
		System.Assert(!ret.m_bError);
		
		// further info db add
		args.m_eBatchParam = QMelonMailBatchParam.MM_ADD_CONTACTS;
		args.m_eDatabase = QMelonMailDB.DB_FURTHER_INFO;
		mmp = new QMelonMailProcessing(args);
		mmp.TESTONLYDisableCallouts();		
		ret = mmp.ProcMain();
		System.Assert(!ret.m_bError);

		// further info db remove
		args.m_eBatchParam = QMelonMailBatchParam.MM_UNSUBSCRIBE;
		mmp = new QMelonMailProcessing(args);
		mmp.TESTONLYDisableCallouts();		
		ret = mmp.ProcMain();
		System.Assert(!ret.m_bError);
		
		// Async calls
		args.m_eBatchParam = QMelonMailBatchParam.MM_ADD_CONTACTS_ASYNC;
		mmp = new QMelonMailProcessing(args);
		mmp.TESTONLYDisableCallouts();		
		ret = mmp.ProcMain();
		System.Assert(!ret.m_bError);	
		
		args.m_eDatabase = QMelonMailDB.DB_MARKETING;
		mmp = new QMelonMailProcessing(args);
		mmp.TESTONLYDisableCallouts();		
		ret = mmp.ProcMain();
		System.Assert(!ret.m_bError);	

		args.m_eBatchParam = QMelonMailBatchParam.MM_UNSUBSCRIBE_ASYNC;
		mmp = new QMelonMailProcessing(args);
		mmp.TESTONLYDisableCallouts();		
		ret = mmp.ProcMain();
		System.Assert(!ret.m_bError);	
	}
}