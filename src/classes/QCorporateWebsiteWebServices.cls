global without sharing class QCorporateWebsiteWebServices
{
	public class QCorporateWebsiteWebServicesParameterException extends Exception{}
	public class QCorporateWebsiteWebServicesException 			extends Exception{}

	/***********************************************************************************************************
		UpdateCampaign
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QUpdateCampaignRequest
	{
		webservice string	UniqueCampaignCode			{get; set;}	
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QUpdateCampaignResults
	{
		webservice string	UniqueCampaignCode				{get; set;}
		webservice boolean	IsActive						{get; set;}
		webservice string	Name							{get; set;}
		webservice string	Description						{get; set;}
		webservice blob		MainImage						{get; set;}
		webservice blob		SubImage1						{get; set;}
		webservice blob		SubImage2						{get; set;}
		webservice blob		SubImage3						{get; set;}
		webservice blob		SubImage4						{get; set;}
		webservice blob		SubImage5						{get; set;}
		
		webservice integer	SubImageAmount1					{get; set;}
		webservice integer	SubImageAmount2					{get; set;}
		webservice integer	SubImageAmount3					{get; set;}
		webservice integer	SubImageAmount4					{get; set;}
		webservice integer	SubImageAmount5					{get; set;}
		
		webservice string	Enquiries						{get; set;}
		webservice string	MicrositeLocation				{get; set;} 
		webservice boolean 	IsRecurringDonation				{get; set;}
	} 
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QUpdateCampaignResults CampaignUpdateRequest(QUpdateCampaignRequest args)
	{	
		if (args.UniqueCampaignCode == null || args.UniqueCampaignCode == '')
			throw new QCorporateWebsiteWebServicesParameterException('Unique Campaign Code is a required field.');

		
		return QCorporateWebsiteCampaignProcessing.GetCampaignDetails(args);

//		return null; 
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static boolean TriggerCampaignUpdateRequest(string strCampaignCode)
	{
		// send the request
		BuildAndSendHttpRequest(QCacheWebsiteMetadata.getTriggerCampaignUpdateURL() + strCampaignCode);
		
		return true;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static boolean TriggerCampaignDeactivateRequest(string strCampaignCode)
	{
		// send the request
		BuildAndSendHttpRequest(QCacheWebsiteMetadata.getTriggerCampaignDeactivateURL() + strCampaignCode);
		
		return true;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QCampaignRegisterUserRequest
	{
		webservice string	Title					{get; set;}
		webservice string	FirstName				{get; set;}
		webservice string	LastName				{get; set;}
		webservice string	PhoneNumber				{get; set;}
		webservice string	OtherPhoneNumber		{get; set;}
		webservice string	Email					{get; set;}
		webservice string	Street					{get; set;}
		webservice string	Suburb					{get; set;}
		webservice string	State					{get; set;}
		webservice string	Country					{get; set;}
		webservice string	PostalCode				{get; set;}
		webservice string	Website					{get; set;}
		webservice boolean	MarketingOptIn			{get; set;}
		webservice boolean	FurtherInformationOptIn	{get; set;}
		
		webservice date		DOB						{get; set;}
		webservice string	PhoneType				{get; set;}
		webservice string	DonationSource			{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QCampaignRegisterUserResults
	{
		webservice boolean	InError					{get; set;}
		webservice string	Error					{get; set;}
		webservice string	UserId					{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QCampaignRegisterUserResults CampaignRegisterUserRequest(QCampaignRegisterUserRequest args)
	{
		System.Debug('Campaign Register User Request has been fired with the following Details - ' + args);
		
		if (args.Title == null || args.Title == '')
			throw new QCorporateWebsiteWebServicesParameterException('Title is a required field.');

		if (args.FirstName == null || args.FirstName == '')
			throw new QCorporateWebsiteWebServicesParameterException('First Name is a required field.');

		if (args.LastName == null || args.LastName == '')
			throw new QCorporateWebsiteWebServicesParameterException('Last Name is a required field.');

		if (args.PhoneNumber == null || args.PhoneNumber == '')
			throw new QCorporateWebsiteWebServicesParameterException('Phone Number is a required field.');

		if (args.Email == null || args.Email == '')
			throw new QCorporateWebsiteWebServicesParameterException('Email is a required field.');

		if (args.Street == null || args.Street == '')
			throw new QCorporateWebsiteWebServicesParameterException('Street is a required field.');

		if (args.Suburb == null || args.Suburb == '')
			throw new QCorporateWebsiteWebServicesParameterException('Suburb is a required field.');

		if (args.State == null || args.State == '')
			throw new QCorporateWebsiteWebServicesParameterException('State is a required field.');

		if (args.Country == null || args.Country == '')
			throw new QCorporateWebsiteWebServicesParameterException('Country is a required field.');

		if (args.PostalCode == null || args.PostalCode == '')
			throw new QCorporateWebsiteWebServicesParameterException('PostalCode is a required field.');

		if (args.MarketingOptIn == null)
			args.MarketingOptIn = false;

		if (args.FurtherInformationOptIn == null)
			args.FurtherInformationOptIn = false;

		// lets send to the processor
		return QCorporateWebsiteCampaignProcessing.RegisterUser(args);

//		return null;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QCampaignRegisterUserResults RegisterOrganisaiton(QCampaignRegisterUserRequest args)
	{
		System.Debug('Campaign Register Organisation Request has been fired with the following Details - ' + args);
		
		if (args.FirstName == null || args.FirstName == '')
			throw new QCorporateWebsiteWebServicesParameterException('Name is a required field.');

		// dont wnat the silly null turning up in the client search bits n bobs
		args.LastName = ''; 
		
		if (args.PhoneNumber == null || args.PhoneNumber == '')
			throw new QCorporateWebsiteWebServicesParameterException('Phone Number is a required field.');

		if (args.Email == null || args.Email == '')
			throw new QCorporateWebsiteWebServicesParameterException('Email is a required field.');

		if (args.Street == null || args.Street == '')
			throw new QCorporateWebsiteWebServicesParameterException('Street is a required field.');

		if (args.Suburb == null || args.Suburb == '')
			throw new QCorporateWebsiteWebServicesParameterException('Suburb is a required field.');

		if (args.State == null || args.State == '')
			throw new QCorporateWebsiteWebServicesParameterException('State is a required field.');

		if (args.Country == null || args.Country == '')
			throw new QCorporateWebsiteWebServicesParameterException('Country is a required field.');

		if (args.PostalCode == null || args.PostalCode == '')
			throw new QCorporateWebsiteWebServicesParameterException('PostalCode is a required field.');

		if (args.MarketingOptIn == null)
			args.MarketingOptIn = false;

		if (args.FurtherInformationOptIn == null)
			args.FurtherInformationOptIn = false;

		// lets send to the processor
		return QCorporateWebsiteCampaignProcessing.RegisterOrganisaiton(args);

//		return null;
	}


	/***********************************************************************************************************
		MakeDonation
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QCampaignDonationRequest
	{
		webservice string	UserId					{get; set;}
		webservice string	CreditCardNumber		{get; set;}
		webservice string	CardName				{get; set;}
		webservice integer	ExpiryYear				{get; set;}
		webservice integer	ExpiryMonth				{get; set;}
		webservice string	CVC						{get; set;}
		webservice integer	Amount					{get; set;}
		webservice date		EndDate					{get; set;}
		webservice string	CampaignId				{get; set;}
		webservice string	Comments				{get; set;}
		webservice boolean	PostReceipt				{get; set;}
		
		webservice string	DonationSource			{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QCampaignDonationResults
	{
		webservice boolean	InError					{get; set;}
		webservice string	Error					{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QCampaignDonationResults CampaignDonationRequest(QCampaignDonationRequest args)
	{
		if (args.UserId == null || args.UserId == '')
			throw new QCorporateWebsiteWebServicesParameterException('User Id is a required field.');

		if (args.CreditCardNumber == null || args.CreditCardNumber == '')
			throw new QCorporateWebsiteWebServicesParameterException('Credit Card Number is a required field.');
	
		if (args.CardName == null || args.CardName == '')
			throw new QCorporateWebsiteWebServicesParameterException('Card Name is a required field.');

		if (args.ExpiryYear == null)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Year is a required field.');

		if (args.ExpiryMonth == null)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Month is a required field.');
		
		if (args.CVC == null || args.CVC == '')
			throw new QCorporateWebsiteWebServicesParameterException('CVC is a required field.');

		if (args.Amount == null)
			throw new QCorporateWebsiteWebServicesParameterException('Amount is a required field.');
		else if (args.Amount <= 0)
			throw new QCorporateWebsiteWebServicesParameterException('Amount must be a postive number.');

		if(args.EndDate != null)
		{
			if (args.EndDate < System.Today())
				throw new QCorporateWebsiteWebServicesParameterException('End Date must be in the future.');
		}

		// check the expiry year is valid
		integer iExpiryYear = Integer.valueOf(args.ExpiryYear);
		
		if (iExpiryYear < 100)
		{			
			iExpiryYear += 2000;
		}

		if (iExpiryYear < datetime.now().year())
		{	
			throw new QCorporateWebsiteWebServicesException('The Expiry Year specified is invalid: ' + iExpiryYear + '(' + args.ExpiryYear + ')');
		}
		
		// check the month
		integer iExpiryMonth = Integer.valueOf(args.ExpiryMonth);

		if (iExpiryMonth < 0 || iExpiryMonth > 12)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Month must be in the range 1 to 12.');
		else if(iExpiryYear == datetime.now().year() && iExpiryMonth < datetime.now().month())
			throw new QCorporateWebsiteWebServicesParameterException('Card Expiry date is invalid.');

		if(args.PostReceipt == null)
		{
			args.PostReceipt = false;
		}
		
		// lets send to the processor
		return QCorporateWebsiteCampaignProcessing.ProcessDonation(args, true, true);

//		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QCampaignDonationResults CampaignBusinessDonationRequest(QCampaignDonationRequest args)
	{
		if (args.UserId == null || args.UserId == '')
			throw new QCorporateWebsiteWebServicesParameterException('User Id is a required field.');

		if (args.CreditCardNumber == null || args.CreditCardNumber == '')
			throw new QCorporateWebsiteWebServicesParameterException('Credit Card Number is a required field.');
	
		if (args.CardName == null || args.CardName == '')
			throw new QCorporateWebsiteWebServicesParameterException('Card Name is a required field.');

		if (args.ExpiryYear == null)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Year is a required field.');

		if (args.ExpiryMonth == null)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Month is a required field.');
		
		if (args.CVC == null || args.CVC == '')
			throw new QCorporateWebsiteWebServicesParameterException('CVC is a required field.');

		if (args.Amount == null)
			throw new QCorporateWebsiteWebServicesParameterException('Amount is a required field.');
		else if (args.Amount <= 0)
			throw new QCorporateWebsiteWebServicesParameterException('Amount must be a postive number.');

		if(args.EndDate != null)
		{
			if (args.EndDate < System.Today())
				throw new QCorporateWebsiteWebServicesParameterException('End Date must be in the future.');
		}

		// check the expiry year is valid
		integer iExpiryYear = Integer.valueOf(args.ExpiryYear);
		
		if (iExpiryYear < 100)
		{			
			iExpiryYear += 2000;
		}

		if (iExpiryYear < datetime.now().year())
		{	
			throw new QCorporateWebsiteWebServicesException('The Expiry Year specified is invalid: ' + iExpiryYear + '(' + args.ExpiryYear + ')');
		}
		
		// check the month
		integer iExpiryMonth = Integer.valueOf(args.ExpiryMonth);

		if (iExpiryMonth < 0 || iExpiryMonth > 12)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Month must be in the range 1 to 12.');
		else if(iExpiryYear == datetime.now().year() && iExpiryMonth < datetime.now().month())
			throw new QCorporateWebsiteWebServicesParameterException('Card Expiry date is invalid.');

		if(args.PostReceipt == null)
		{
			args.PostReceipt = false;
		}
		
		// lets send to the processor
		return QCorporateWebsiteCampaignProcessing.ProcessDonation(args, true, false);

//		return null;
	}


	/***********************************************************************************************************
		UpdateEvent
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QEventUpdateRequest
	{
		webservice string	UniqueEventCode			{get; set;}	
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QEventUpdateResults
	{
		webservice string	UniqueEventCode					{get; set;}
		webservice boolean	IsActive						{get; set;}
		webservice string	Name							{get; set;}
		webservice string	Description						{get; set;}
		webservice blob		MainImage						{get; set;}
		webservice blob		SubImage1						{get; set;}
		webservice blob		SubImage2						{get; set;}
		webservice string	State							{get; set;}
		webservice string	Location						{get; set;}
		webservice string	LocationLink					{get; set;}
		webservice date		EventDate						{get; set;}
		webservice date		EventEndDate 					{get; set;}
		webservice string	EventTime						{get; set;}
		webservice string	Cost							{get; set;}
		webservice string	Enquiries						{get; set;}
		webservice boolean 	IsTicketedEvent					{get; set;}
		webservice string	TicketProductId					{get; set;}
		webservice boolean	IsRegistrationEvent				{get; set;}
		webservice boolean	IsYearLongEvent					{get; set;}
		webservice string	RegistrationCode				{get; set;}
		webservice string	MicrositeLocation				{get; set;}
		webservice string	RRule							{get; set;}
		webservice string	EventLink						{get; set;}
		webservice string	EventLinkDescription			{get; set;}
	} 
 
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QEventUpdateResults EventUpdateRequest(QEventUpdateRequest args)
	{	
		if (args.UniqueEventCode == null || args.UniqueEventCode == '')
			throw new QCorporateWebsiteWebServicesParameterException('Unique Event Code is a required field.');

		return QCorporateWebsiteEventProcessing.GetEventDetails(args);

//		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static boolean TriggerEventUpdateRequest(string strCampaignCode)
	{
		BuildAndSendHttpRequest(QCacheWebsiteMetadata.getTriggerEventUpdateURL() + strCampaignCode);

		return true;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static boolean TriggerEventDeactivateRequest(string strCampaignCode)
	{
		BuildAndSendHttpRequest(QCacheWebsiteMetadata.getTriggerEventDeactivateURL() + strCampaignCode);

		return true;
	}
	
	
	/***********************************************************************************************************
		ShopUpdateProduct
	***********************************************************************************************************/

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QShopUpdateProductRequest
	{
		webservice string	UniqueProductCode		{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QShopUpdateProductResults
	{
		webservice string	UniqueProductCode				{get; set;}
		webservice string	ProductCode						{get; set;}
		webservice boolean	IsActive						{get; set;}
		webservice blob		MainImage						{get; set;}
		webservice string	Name							{get; set;}
		webservice string	Description						{get; set;}
		webservice boolean	GSTApplicable					{get; set;}
		webservice double	Height							{get; set;}
		webservice double	Width							{get; set;}
		webservice double	Depth							{get; set;}
		webservice double	Weight							{get; set;}
		webservice string	Category						{get; set;}
		webservice double	Price							{get; set;}
		webservice double	MinShippingCost					{get; set;}
		webservice boolean	UseExternalShippingCalculation	{get; set;}
		webservice integer	Quantity						{get; set;}
		webservice string	CampaignId						{get; set;}
		webservice boolean	IsCampaignTicket				{get; set;}		
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QShopUpdateProductResults ShopUpdateProductRequest(QShopUpdateProductRequest args)
	{ 
		if (args.UniqueProductCode == null || args.UniqueProductCode == '')
			throw new QCorporateWebsiteWebServicesParameterException('Unique Product Code is a required field.');

		return QCorporateWebsiteShopProcessing.GetProductDetails(args);

//		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static boolean TriggerShopProductUpdateRequest(string strProductCode)
	{
		BuildAndSendHttpRequest(QCacheWebsiteMetadata.getTriggerShopProductUpdateURL() + strProductCode);

		return true; 
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static boolean TriggerShopProductDeactivateRequest(string strProductCode)
	{
		BuildAndSendHttpRequest(QCacheWebsiteMetadata.getTriggerShopProductDeactivateURL() + strProductCode);

		return true;
	}
	
	/***********************************************************************************************************
		ShopRegisterUser
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QShopRegisterUserRequest
	{
		webservice string	Title					{get; set;}
		webservice string	FirstName				{get; set;}
		webservice string	LastName				{get; set;}
		webservice string	PhoneNumber				{get; set;}
		webservice string	OtherPhoneNumber		{get; set;}
		webservice string	Email					{get; set;}
		webservice string	Street					{get; set;}
		webservice string	Suburb					{get; set;}
		webservice string	State					{get; set;}
		webservice string	Country					{get; set;}
		webservice string	PostalCode				{get; set;}
		webservice boolean	MarketingOptIn			{get; set;}
		webservice boolean	FurtherInformationOptIn	{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QShopRegisterUserResults
	{
		webservice boolean	InError					{get; set;}
		webservice string	Error					{get; set;}
		webservice string	UserId					{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QShopRegisterUserResults ShopRegisterUserRequest(QShopRegisterUserRequest args)
	{
		if (args.Title == null || args.Title == '')
			throw new QCorporateWebsiteWebServicesParameterException('Title is a required field.');

		if (args.FirstName == null || args.FirstName == '')
			throw new QCorporateWebsiteWebServicesParameterException('First Name is a required field.');

		if (args.LastName == null || args.LastName == '')
			throw new QCorporateWebsiteWebServicesParameterException('Last Name is a required field.');

		if (args.PhoneNumber == null || args.PhoneNumber == '')
			throw new QCorporateWebsiteWebServicesParameterException('Phone Number is a required field.');

		if (args.Email == null || args.Email == '')
			throw new QCorporateWebsiteWebServicesParameterException('Email is a required field.');

		if (args.Street == null || args.Street == '')
			throw new QCorporateWebsiteWebServicesParameterException('Street is a required field.');

		if (args.Suburb == null || args.Suburb == '')
			throw new QCorporateWebsiteWebServicesParameterException('Suburb is a required field.');

		if (args.State == null || args.State == '')
			throw new QCorporateWebsiteWebServicesParameterException('State is a required field.');

		if (args.Country == null || args.Country == '')
			throw new QCorporateWebsiteWebServicesParameterException('Country is a required field.');

		if (args.PostalCode == null || args.PostalCode == '')
			throw new QCorporateWebsiteWebServicesParameterException('PostalCode is a required field.');

		if (args.MarketingOptIn == null)
			args.MarketingOptIn = false;

		if (args.FurtherInformationOptIn == null)
			args.FurtherInformationOptIn = false;
		
		return QCorporateWebsiteShopProcessing.RegisterUser(args);
		
//		return oRet;
	}
	
	/***********************************************************************************************************
		ShopPurchase
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	global class QProduct
	{
		webservice string	UniqueProductCode		{get; set;}
		webservice integer	Quantity				{get; set;}
		webservice double	UnitPrice				{get; set;}
		webservice double	GSTAmount				{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QShopPurchaseRequest
	{
		webservice string			UserId					{get; set;}
		webservice string			CreditCardNumber		{get; set;}
		webservice string			CardName				{get; set;}
		webservice string			ExpiryYear				{get; set;}
		webservice string			ExpiryMonth				{get; set;}
		webservice string			CVC						{get; set;}
		webservice string			CampaignId				{get; set;}
		webservice integer			TotalProductCount		{get; set;}
		webservice double			ShippingAmount			{get; set;}
		webservice string			ShippingMethod			{get; set;}
		webservice list<QProduct> 	Product					{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	global class QShopPurchaseResults
	{
		webservice boolean	InError					{get; set;}
		webservice string	Error					{get; set;}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	webservice static QShopPurchaseResults ShopPurchaseRequest(QShopPurchaseRequest args)
	{
		if (args.UserId == null || args.UserId == '')
			throw new QCorporateWebsiteWebServicesParameterException('User Id is a required field.');

		if (args.TotalProductCount == 0)
			throw new QCorporateWebsiteWebServicesParameterException('Total Product Count must be greater than zero.');

		if (args.ShippingAmount == null)
			throw new QCorporateWebsiteWebServicesParameterException('Shipping amount is a required value.');
		else if (args.ShippingAmount < 0)
			throw new QCorporateWebsiteWebServicesParameterException('Negative shipping amount specified.');

		if (args.CreditCardNumber == null || args.CreditCardNumber == '')
			throw new QCorporateWebsiteWebServicesParameterException('Credit Card Number is a required field.');
	
		if (args.CardName == null || args.CardName == '')
			throw new QCorporateWebsiteWebServicesParameterException('Card Name is a required field.');

		if (args.ExpiryYear == null)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Year is a required field.');

		if (args.ExpiryMonth == null)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Month is a required field.');
		
		if (args.CVC == null || args.CVC == '')
			throw new QCorporateWebsiteWebServicesParameterException('CVC is a required field.');

		// check the expiry year is valid
		integer iExpiryYear = Integer.valueOf(args.ExpiryYear);
		
		if (iExpiryYear < 100)
		{			
			iExpiryYear += 2000;
		}

		if (iExpiryYear < datetime.now().year())
		{	
			throw new QCorporateWebsiteWebServicesException('The Expiry Year specified is invalid.');
		}

		// check the month
		integer iExpiryMonth = Integer.valueOf(args.ExpiryMonth);

		if (iExpiryMonth < 0 || iExpiryMonth > 12)
			throw new QCorporateWebsiteWebServicesParameterException('Expiry Month must be in the range 1 to 12.');
		else if(iExpiryYear == datetime.now().year() && iExpiryMonth < datetime.now().month())
			throw new QCorporateWebsiteWebServicesParameterException('Card Expiry date is invalid');

		// check the products array
		integer iProductCount = 0;
		double dTotalPrice = 0;
		double dTotalGST = 0;

		for (QProduct product : args.Product)
		{
			// check the unique product code first, if it's present we can use it to be more specific about other errors
			if (product.UniqueProductCode == null || product.UniqueProductCode == '')
				throw new QCorporateWebsiteWebServicesParameterException('Unique Product Code is required');

			// quantity must be present and greater than zero
			if (product.Quantity == null)
				throw new QCorporateWebsiteWebServicesParameterException('Quantity is a required value');
			else if (product.Quantity <= 0)
				throw new QCorporateWebsiteWebServicesParameterException('Invalid quantity specified for product ' + product.UniqueProductCode);

			iProductCount += product.Quantity;

			// maybe the store has free stuff? allow zero for now
			if (product.UnitPrice == null)
				throw new QCorporateWebsiteWebServicesParameterException('Unit price is a required value for product ' + product.UniqueProductCode);
			if (product.UnitPrice < 0)
				throw new QCorporateWebsiteWebServicesParameterException('Negative unit price specified for product ' + product.UniqueProductCode);

			dTotalPrice += product.UnitPrice * product.Quantity;

			if (product.GSTAmount == null)
				throw new QCorporateWebsiteWebServicesParameterException('GST amount is a required value for product ' + product.UniqueProductCode);
			else if (product.GSTAmount < 0)
				throw new QCorporateWebsiteWebServicesParameterException('Negative GST amount specified for product ' + product.UniqueProductCode);

			dTotalGST += product.GSTAmount * product.Quantity;
		}
		
		
		/* THIS IS TEMPORARILY DISABLED WHILE WE WAIT FOR MELON TO FIX THEIR END */
		if (args.TotalProductCount != null && args.TotalProductCount != iProductCount)
		{
			System.Debug(args + '\nProduct Count = ' + iProductCount);
//			throw new QCorporateWebsiteWebServicesParameterException('Total Product Count does not match the number of products to be purcahsed.');
		}
		

		return QCorporateWebsiteShopProcessing.ProcessPurchase(args, true);
		
//		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	private static HttpResponse BuildAndSendHttpRequest(string strURL)
	{
		Http 			http;
		HttpRequest		httpRequest;
		HttpResponse 	httpResponse;
		
		try
		{
			http = new Http();
			
			httpRequest = new HttpRequest();
			
			httpRequest.setEndpoint(strURL);
			
			httpRequest.setMethod('GET');
			
			System.Debug('\nMaking HTTP Request:\n' + httpRequest);
			httpResponse = http.send(httpRequest);
			System.Debug('\nResponse:\n' + httpResponse);
			
			if(httpResponse.getStatusCode() != 200)
			{
				System.Debug('Corporate Website CMS Error thrown - ' + httpResponse.getStatusCode());
				throw new QCorporateWebsiteWebServicesException('Error Code - ' + httpResponse.getStatusCode());
			}
		}
		catch(QCorporateWebsiteWebServicesException cwse)
		{
			System.debug('Web Service Exception - Trying to Trigger Get Campaign Details in CMS - ' + cwse);
			System.Debug('Corporate Website HTTP Request - ' + httpRequest);
						
			if(httpResponse != null)
				System.Debug(httpResponse.getBody());
				
			throw new QCorporateWebsiteWebServicesException('Error Processing CMS Update - ' + cwse.getMessage() + ' - Please turn on the Debug Log and run again to retrieve the full error message error ');
		}
		catch(Exception e)
		{
			System.debug('Web Service Exception - Trying to Trigger Get Campaign Details in CMS - ' + e);
			System.Debug('Corporate Website HTTP Request - ' + httpRequest);
			
			if(httpResponse != null)
				System.Debug(httpResponse.getBody());
				
			throw new QCorporateWebsiteWebServicesException('Fatal Error - Please run the Debug Log to capture the error message');
		}
		
		return httpResponse;
	}
}