public with sharing class QCacheWebsiteMetadata 
{
	private static Website_Metadata__c WebsiteMetadata;
	
	/***********************************************************************************************************
		Donation Notifications
	***********************************************************************************************************/
	public static string getPostalReceiptNotificationEmail()
	{
		if(WebsiteMetadata == null)
			LoadMetadata(); 
			
		return WebsiteMetadata.Post_Receipt_Required_Notification_Email__c;
	}
	
	/***********************************************************************************************************
		Campaign Defaulting Metadata
	***********************************************************************************************************/
	public static string getDefaultCampaignForDonations()
	{
		if(WebsiteMetadata == null)
			LoadMetadata(); 
			
		return WebsiteMetadata.Default_Campaign_For_Donations__c;
	}
	
	public static string getDefaultCampaignForOrgDonations()
	{
		if(WebsiteMetadata == null)
			LoadMetadata(); 
			
		return WebsiteMetadata.Default_Campaign_For_Org_Donations__c;
	}
	
	public static string getDefaultCampaignForClubDonations()
	{
		if(WebsiteMetadata == null)
			LoadMetadata(); 
			
		return WebsiteMetadata.Default_Campaign_For_Club_Donations__c;
	}

	public static string getDefaultCampaignForShopPurcahses()
	{
		if(Websitemetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Default_Campaign_For_Shop_Purchases__c;
	}

	public static string getRedClubRecurringDonationsCampaign()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
		
		return WebsiteMetadata.Red_Club_Recurring_Donations_Campaign__c;
	}

	/***********************************************************************************************************
		Pricebook Metadata
	***********************************************************************************************************/
	public static string getDefaultPricebookEntryForShipping()
	{
		if(Websitemetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Shop_Shipping_Pricebook_Entry__c;
	}

	public static string getDefaultPricebookEntryForExpressShipping()
	{
		if(Websitemetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Shop_Express_Shipping_Pricebook_Entry__c;
	}
	
	public static string getDefaultPricebookEntryForGST()
	{
		if(Websitemetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Shop_GST_Pricebook_Entry__c;
	}

	public static string getShopStandardPricebook()
	{
		if(Websitemetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Shop_Standard_Pricebook__c;
	}
	
	/***********************************************************************************************************
		CMS Processing Metadata
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Donation Processing
	public static string getTriggerCampaignUpdateURL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Trigger_Campaign_Update_URL__c;
	}
	
	public static string getTriggerCampaignDeactivateURL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Trigger_Campaign_Deactivate_URL__c;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Event Processing
	public static string getTriggerEventUpdateURL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Trigger_Event_Update_URL__c;
	}
	
	public static string getTriggerEventDeactivateURL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Trigger_Event_Deactivate_URL__c;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Product Processing
	public static string getTriggerShopProductUpdateURL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Trigger_Shop_Product_Update_URL__c;
	}
	
	public static string getTriggerShopProductDeactivateURL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.Trigger_Shop_Product_Deactivate_URL__c;
	}
	
	/***********************************************************************************************************
		Melon Mail Metadata
	***********************************************************************************************************/
	public static string getMelonMailLoginURL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Melon_Mail_Login_URL__c;
	}

	public static string getMelonMailLoginUsername()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Melon_Mail_Login_Username__c;
	}
	
	public static string getMelonMailLoginPassword()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Melon_Mail_Login_Password__c;
	}
	
	public static integer getMelonMailMarketingDatabaseId()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
		
		return integer.valueOf(WebsiteMetadata.Melon_Mail_Marketing_Database_Id__c);
	}

	public static integer getMelonMailFurtherInfoDatabaseId()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
		
		return integer.valueOf(WebsiteMetadata.Melon_Mail_Further_Info_Database_Id__c);
	}
	
	/***********************************************************************************************************
		Income Codes
	***********************************************************************************************************/
	public static string getIncomeCodeForState(string strState)
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
		
		if(strState.toUpperCase() == 'NSW')
		{
			return WebsiteMetadata.Income_Code_NSW__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else if(strState.toUpperCase() == 'VIC')
		{
			return WebsiteMetadata.Income_Code_VIC__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else if(strState.toUpperCase() == 'TAS')
		{
			return WebsiteMetadata.Income_Code_TAS__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else if(strState.toUpperCase() == 'ACT')
		{
			return WebsiteMetadata.Income_Code_ACT__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else if(strState.toUpperCase() == 'QLD')
		{
			return WebsiteMetadata.Income_Code_QLD__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else if(strState.toUpperCase() == 'NT')
		{
			return WebsiteMetadata.Income_Code_NT__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else if(strState.toUpperCase() == 'SA')
		{
			return WebsiteMetadata.Income_Code_SA__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else if(strState.toUpperCase() == 'WA')
		{
			return WebsiteMetadata.Income_Code_WA__c + WebsiteMetadata.Income_Code_Category__c;
		}
		else
		{
			// use the interational one since we didn't recognise the string
			return WebsiteMetadata.Income_Code_INTL__c + WebsiteMetadata.Income_Code_Category__c;
		}
	}
	
	public static string getIncomeCodeCategory()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
		
		return WebsiteMetadata.Income_Code_Category__c;
	}
	
	/***********************************************************************************************************
		New Customer Account Owners
	***********************************************************************************************************/
	public static string getCorporateDonationOwnerNSW()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerNSW__c;
	}

	public static string getCorporateDonationOwnerACT()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerACT__c;
	}

	public static string getCorporateDonationOwnerTAS()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerTAS__c;
	}

	public static string getCorporateDonationOwnerVIC()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerVIC__c;
	}

	public static string getCorporateDonationOwnerWA()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerWA__c;
	}

	public static string getCorporateDonationOwnerSA()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerSA__c;
	}

	public static string getCorporateDonationOwnerQLD()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerQLD__c;
	}
	
	public static string getCorporateDonationOwnerNT()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerNT__c;
	}
		
	public static string getCorporateDonationOwnerForState(string strState)
	{
		if(strState.toUpperCase() == 'NSW')
		{
			return getCorporateDonationOwnerNSW();
		}
		else if(strState.toUpperCase() == 'VIC')
		{
			return getCorporateDonationOwnerVIC();
		}
		else if(strState.toUpperCase() == 'TAS')
		{
			return getCorporateDonationOwnerTAS();
		}
		else if(strState.toUpperCase() == 'ACT')
		{
			return getCorporateDonationOwnerACT();
		}
		else if(strState.toUpperCase() == 'QLD')
		{
			return getCorporateDonationOwnerQLD();
		}
		else if(strState.toUpperCase() == 'NT')
		{
			return getCorporateDonationOwnerNT();
		}
		else if(strState.toUpperCase() == 'SA')
		{
			return getCorporateDonationOwnerSA();
		}
		else if(strState.toUpperCase() == 'WA')
		{
			return getCorporateDonationOwnerWA();
		}
		else
		{
			// use the interational one since we didn't recognise the string
			return getCorporateDonationOwnerINTL();
		}
	}

	public static string getCorporateDonationOwnerClubRed()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerClubRed__c;
	}

	public static string getCorporateDonationOwnerINTL()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();
			
		return WebsiteMetadata.CorporateDonationOwnerINTL__c;
	}

	/***********************************************************************************************************
		Web to Lead Notifications
	***********************************************************************************************************/
	public static string getWebToLeadNotificationAddressForVolunteer()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Web_To_Lead_Notification_Volunteer__c;
	}
	
	public static string getWebToLeadNotificationAddressForContactUs()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Web_To_Lead_Notification_Contact_Us__c;
	}
	
	public static string getWebToLeadNotificationAddressForSubscribe()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Web_To_Lead_Notification_Subscribe__c;
	}
	
	public static string getWebToLeadNotificationAddressForEmailSupport()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.Web_To_Lead_Notification_Email_Support__c;
	}
		
	public static string getWebToLeadNotificationAddressForState(string strState)
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		if(strState.toUpperCase() == 'NSW')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_NSW__c;
		}
		else if(strState.toUpperCase() == 'VIC')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_VIC__c;
		}
		else if(strState.toUpperCase() == 'TAS')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_TAS__c;
		}
		else if(strState.toUpperCase() == 'ACT')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_ACT__c;
		}
		else if(strState.toUpperCase() == 'QLD')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_QLD__c;
		}
		else if(strState.toUpperCase() == 'NT')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_NT__c;
		}
		else if(strState.toUpperCase() == 'SA')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_SA__c;
		}
		else if(strState.toUpperCase() == 'WA')
		{
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_WA__c;
		}
		else
		{
			// just so it goes somewhere
			return WebsiteMetadata.Web_To_Lead_Notification_Fundraiser_NSW__c;
		}
	}

	/***********************************************************************************************************
		Club Red notifications
	***********************************************************************************************************/	
	public static string getNewClubRedNotificationAddress()
	{
		if(WebsiteMetadata == null)
			LoadMetadata();

		return WebsiteMetadata.New_Club_Red_Member_Notification_Email__c;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		WebsiteMetadata = Website_Metadata__c.getInstance();
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void TestCacheWebsiteMetadata()
	{
		QCacheWebsiteMetadata.getDefaultCampaignForDonations();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getDefaultCampaignForShopPurcahses();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getDefaultPricebookEntryForGST();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getDefaultPricebookEntryForShipping();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getMelonMailFurtherInfoDatabaseId();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getMelonMailLoginPassword();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getMelonMailLoginURL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getMelonMailLoginUsername();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getMelonMailMarketingDatabaseId();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getRedClubRecurringDonationsCampaign();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getTriggerCampaignDeactivateURL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getTriggerCampaignUpdateURL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getTriggerEventDeactivateURL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getTriggerEventUpdateURL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getTriggerShopProductDeactivateURL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getTriggerShopProductUpdateURL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('NSW');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('ACT');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('TAS');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('VIC');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('QLD');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('NT');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('SA');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('WA');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeForState('blah');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getIncomeCodeCategory();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('NSW');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('ACT');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('TAS');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('VIC');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('QLD');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('NT');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('SA');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerForState('WA');
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerNSW();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerACT();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerTAS();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerVIC();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerQLD();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerNT();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerSA();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerWA();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerINTL();
		WebsiteMetadata = null;
		QCacheWebsiteMetadata.getCorporateDonationOwnerClubRed();
		WebsiteMetadata = null;
	}
}