public with sharing class QVFPaymentDetailUpdateController 
{
	/***********************************************************************************************************
        Members
    ***********************************************************************************************************/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Exception Classes
    public class QVFC_PaymentDetailUpdateException extends Exception{}
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //  Singular
    public id m_id								{get; set;}
    public boolean mp_blnIsError 				{get; set;}
    public boolean mp_blnViewOk 				{get; set;}

    public string mp_strCreditCardName	 		{get; set;}
    public string mp_strCreditCardNumber 		{get; set;}
    public string mp_strCreditCardExpireYear 	{get; set;}
    public string mp_strCreditCardExpireMonth 	{get; set;}
    public string mp_strCreditCardType		 	{get; set;}
    
    public 	list<SelectOption> 		mp_liExpiryMonthOptions		{get; set;}
	public 	list<SelectOption> 		mp_liExpiryYearOptions		{get; set;}
    public 	list<SelectOption> 		mp_liCreditCardTypeOptions	{get; set;}
    
    
    private Account sAccount;
    private boolean	m_bIsPersonAccount;
    
    private static boolean blForTest = false; //this is only for testing
    
    /***********************************************************************************************************
        Constructors and Init
    ***********************************************************************************************************/
    public QVFPaymentDetailUpdateController()
    {
    	mp_blnIsError 				= false;
    	mp_blnViewOk				= false;
    	mp_strCreditCardName		= '';
    	mp_strCreditCardNumber		= '';
    	mp_strCreditCardExpireYear	= '';
    	mp_strCreditCardExpireMonth 	= '';
    	
    	mp_liExpiryMonthOptions		= new List<SelectOption>();
		mp_liExpiryYearOptions		= new List<SelectOption>();
    	mp_liCreditCardTypeOptions 	= new List<SelectOption>();
    	
    }
    
    public void Init()
    {
    	if(null == (m_id =  ApexPages.currentPage().getParameters().get('Id')))
        {
			ApexPages.Message pMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Invalid id');
			ApexPages.addMessage(pMessage);
			mp_blnIsError = true;
			return;
        }
        
        string strIsPersonAccount = ApexPages.currentPage().getParameters().get('IsPersonAccount');
        
        m_bIsPersonAccount = strIsPersonAccount == '1' ? true : false;
        
        if(m_bIsPersonAccount)
        {
        	try
			{
				sAccount = [
								select 	id, PersonContactId, OwnerId, IPPayment_Customer_Token__c,
										Credit_Card_Last_4_Digit__c, Credit_Card_Expire_Year__c, Credit_Card_Expire_Month__c
								From 	Account 
								where 	id =:m_id 
							];
				
				system.debug('Account Info:' + sAccount);
				
				//m_id = sAccount.PersonContactId;
			}
			catch(Exception e)
			{
				ApexPages.Message pMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Unable to Load Account Information for Id :' + m_id);
				ApexPages.addMessage(pMessage);
				mp_blnIsError = true;
				return;
			}
        }
		
		mp_liExpiryMonthOptions.add(new SelectOption('01', '1'));
		mp_liExpiryMonthOptions.add(new SelectOption('02', '2'));
		mp_liExpiryMonthOptions.add(new SelectOption('03', '3'));
		mp_liExpiryMonthOptions.add(new SelectOption('04', '4'));
		mp_liExpiryMonthOptions.add(new SelectOption('05', '5'));
		mp_liExpiryMonthOptions.add(new SelectOption('06', '6'));
		mp_liExpiryMonthOptions.add(new SelectOption('07', '7'));
		mp_liExpiryMonthOptions.add(new SelectOption('08', '8'));
		mp_liExpiryMonthOptions.add(new SelectOption('09', '9'));
		mp_liExpiryMonthOptions.add(new SelectOption('10', '10'));
		mp_liExpiryMonthOptions.add(new SelectOption('11', '11'));
		mp_liExpiryMonthOptions.add(new SelectOption('12', '12'));
			
		for(integer i = 0; i < 10; i++)
		{
			mp_liExpiryYearOptions.add(new SelectOption(''+(System.Today().Year() + i), ''+(System.Today().Year() + i)));
		}
		
		
		mp_liCreditCardTypeOptions.add(new SelectOption('01', 'Visa'));
		mp_liCreditCardTypeOptions.add(new SelectOption('02', 'MasterCard'));
		mp_liCreditCardTypeOptions.add(new SelectOption('03', 'Amex'));
		mp_liCreditCardTypeOptions.add(new SelectOption('04', 'Diners'));
		
		system.debug('*******' + mp_blnIsError);
    }

    /***********************************************************************************************************
        Access Methods
    ***********************************************************************************************************/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  
    public pageReference ActionCancel()
    {
    	pageReference pageRedirect = new PageReference('/' + m_id);
        pageRedirect.setRedirect(true);
        
        return pageRedirect;
    }
    
    public void ActionUpdate()
    {
    	system.debug('Exp Year :' 		+ mp_strCreditCardExpireYear);
    	system.debug('Exp Month :' 		+ mp_strCreditCardExpireMonth);
    	system.debug('Account Info:' 	+ sAccount);
    	
    	if((mp_strCreditCardName == '') || (mp_strCreditCardNumber == '') || (mp_strCreditCardExpireYear == '') || (mp_strCreditCardExpireMonth == ''))
    	{
    		ApexPages.Message pMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Required Field is Missing');
			ApexPages.addMessage(pMessage);
			mp_blnIsError = true;
			return;
    	}
    	
    	QProcessIPPPayment_V2.QCreditCardUpdate 		oUpdate = new QProcessIPPPayment_V2.QCreditCardUpdate();
    	QProcessIPPPayment_V2.QCreditCardUpdateReturn	oUpdateReturn;
    	
    	oUpdate.CreditCardToken 		= sAccount.IPPayment_Customer_Token__c;
    	oUpdate.CreditCardNumber		= mp_strCreditCardNumber;
		oUpdate.CreditCardName			= mp_strCreditCardName;
		oUpdate.CreditCardExpiryYear	= mp_strCreditCardExpireYear;
		oUpdate.CreditCardExpiryMonth 	= mp_strCreditCardExpireMonth;
		
		system.debug(oUpdateReturn);
		    	
    	try
    	{
    		oUpdateReturn = QProcessIPPPayment_V2.CalloutIPPaymentsSubmitUpdateRegisteredDetails(oUpdate);
			system.debug(oUpdateReturn);
    		system.debug('RESPONSE FROM IPP:' + oUpdateReturn.ResponseCode);
	    	system.debug('Credit Card Token:' + oUpdateReturn.CreditCardToken);
    	}
    	catch(Exception e)
    	{
    		ApexPages.Message pMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'There has been an error while sending the request to the banking network : ' + e.getMessage());
			ApexPages.addMessage(pMessage);
			mp_blnIsError = true;
			return;
    	}
	    	
	    	
	    	
    	if (oUpdateReturn.ResponseCode != '0' && blForTest)
    	{
    		ApexPages.Message pMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Unable to Update Account Information.  Response Code from Banking Network : ' +  oUpdateReturn.ResponseCode);
			ApexPages.addMessage(pMessage);
			mp_blnIsError = true;
			return;
    	} 
	    	
    	try
    	{
    		system.debug('CardType:' + mp_liCreditCardTypeOptions);
			
	    	//// Update Account
	    	sAccount.IPPayment_Customer_Token__c 	= oUpdateReturn.CreditCardToken;
	    	sAccount.Credit_Card_Last_4_Digit__c	= mp_strCreditCardNumber.substring(mp_strCreditCardNumber.length()-4);
	    	sAccount.Credit_Card_Expire_Year__c		= mp_strCreditCardExpireYear;
	    	sAccount.Credit_Card_Expire_Month__c	= mp_strCreditCardExpireMonth;
	    	
	    	if(mp_strCreditCardNumber.length() >= 4)
			{
				sAccount.Credit_Card_Last_4_Digit__c = mp_strCreditCardNumber.substring(mp_strCreditCardNumber.length() - 4, mp_strCreditCardNumber.length());
			}
			
			sAccount.Card_Name__c = mp_strCreditCardName;
			sAccount.Credit_Card_Type__c = QUtils.GetCreditCardType(mp_strCreditCardNumber);
			
			update sAccount;
			
			// Change the Credit Card in Opportunities Club Red On Going
			list<Opportunity> liOpptyToUpdate = [select Id  from Opportunity
												 where AccountId = :sAccount.Id
												 and StageName = 'Club Red Ongoing'];
			for(Opportunity sOpp : liOpptyToUpdate)
			{
				sOpp.Credit_Card__c 	  = mp_strCreditCardNumber;
				sOpp.Credit_Card_Type__c  = QUtils.GetCreditCardType(oUpdate.CreditCardNumber);
				sOpp.Card_Name__c 		  = mp_strCreditCardName;
				sOpp.Credit_Card_Year__c  = Integer.valueOf(mp_strCreditCardExpireYear);
				sOpp.Card_Expiry_Month__c = Integer.valueOf(mp_strCreditCardExpireMonth);
			}
			update liOpptyToUpdate;
			
			/// Create Task for update Information
			Task sTask = new Task();
			sTask.Subject 		= 'Updated Credit Card Information:';
			sTask.Description 	= 'Updated Credit Card Information by ' + UserInfo.getUserName() ;
			sTask.Priority 		= 'Normal';
			sTask.Status 		= 'Completed';
			sTask.OwnerId		= sAccount.OwnerId;
			sTask.WhatId 		= sAccount.id;
			sTask.ActivityDate	= system.today();
			
			insert sTask;	
			System.debug('Task: ' + sTask);
			
			
			ApexPages.Message pMessage = new ApexPages.Message(ApexPages.severity.CONFIRM, 'Update Credit Card Information successfully');
			ApexPages.addMessage(pMessage);
			mp_blnIsError = true;
			mp_blnViewOk = true;
			return;
    	}
		catch(Exception e)
		{
			ApexPages.Message pMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Unable to Update Account Information :' + e.getMessage());
			ApexPages.addMessage(pMessage);
			mp_blnIsError = true;
			return;
		}
      

    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static testMethod void testQVFPaymentDetailUpdateController()
    {
		Account sAccount 	= new Account();
		sAccount.Name							= 'QTest222' ;
		sAccount.Email__c						= 'qTest@test.com';
		sAccount.IPPayment_Customer_Token__c	= '123654987';
		insert sAccount;           // Insert Account
		
		PageReference pageRef = new PageReference('/apex/QPaymentDetailUpdate');
		ApexPages.currentPage().getParameters().put('Id', sAccount.Id);
        ApexPages.currentPage().getParameters().put('IsPersonAccount', '1');
        
		QVFPaymentDetailUpdateController oController = new QVFPaymentDetailUpdateController();
		oController.Init();
		oController.mp_strCreditCardName		= 'QTest222';
		oController.mp_strCreditCardNumber		= '4242424242424242';
		oController.mp_strCreditCardExpireMonth	= '02';
		oController.mp_strCreditCardExpireYear	= '2015';
		
		oController.ActionUpdate();
		oController.ActionCancel();
		
		system.debug(ApexPages.getMessages());
	}
	
	public static testMethod void testQVFPaymentDetailUpdateControllerTestingError()
    {
  		///// Testing Contact Id Error
		PageReference pageRef = new PageReference('/apex/QPaymentDetailUpdate');
                
		QVFPaymentDetailUpdateController oController = new QVFPaymentDetailUpdateController();
		oController.Init();
		oController.ActionUpdate();
		
        Account sAccount 	= new Account();
		sAccount.Name							= 'QTest222' ;
		sAccount.Email__c						= 'qTest@test.com';
		sAccount.IPPayment_Customer_Token__c	= '123654987';
		insert sAccount;           // Insert Account

		ApexPages.currentPage().getParameters().put('Id', sAccount.Id);
		
		oController = new QVFPaymentDetailUpdateController();
		oController.Init();
		oController.ActionUpdate();
		
		System.Assert(oController.mp_blnIsError);

		// Invalid Contact id
		ApexPages.currentPage().getParameters().put('Id', sAccount.Id);
		
		delete sAccount;
		
		QVFPaymentDetailUpdateController oController2 = new QVFPaymentDetailUpdateController();
		oController2.Init();

		// Exception testing
		boolean bExceptionThrown;
		
		QVFPaymentDetailUpdateController oController3a = new QVFPaymentDetailUpdateController();
		oController3a.Init();
		
		system.debug(ApexPages.getMessages());
	}
}