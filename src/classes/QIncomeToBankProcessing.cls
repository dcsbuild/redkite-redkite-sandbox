global class QIncomeToBankProcessing implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful
{
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		boolean runBatch = false;
		try
		{
			IncomeBatchTracker__c currentJob = [	select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c 
													from IncomeBatchTracker__c 
													where Status__c = 'Processing'
													and	BatchName__c =  'QIncomeToBankProcessing'
													order by StartDatetime__c asc limit 1];
													
			if(currentJob != null && currentJob.ApexJobId__c ==  BC.getJobId())	//check if the current job is the right one
				runBatch = true;
		}
		catch(Exception e )
		{	
			runBatch = false;
		}
		
		if(!runBatch)
		{
			QVFC_IncomeBatchManagement.DetectProcessingBatch('WARNING : Processing Batch Detected','TEST IN SANDBOX');
		}
		// we want to create these with a 3 day lead time so people have the chance to look
		// at them before we send them 
		Date dToday = System.Today();

		string strDate = ''+ dToday.Year()+'-';
		
		// Month
		strDate += dToday.Month() < 10 ? '0' + dToday.Month() : ''+dToday.Month();
		
		// Dividor
		strDate += '-';
		
		// Day
		strDate += dToday.Day() < 10 ? '0' + dToday.Day() : ''+dToday.Day();
		
		string	strQuery =

			'	select 	id'+
			'	from 	Income__c '+
			'	where 	Payment_Type_A__c = \'Credit Card\' ' +
			'	and		Payment_Gateway_Send_To_Bank_Date__c <= ' + strDate +
			' 	and		Status__c = \'Send to IPPayments\' ' +	
			'	and		Opportunity__r.Account.IPPayment_Customer_Token__c != null ' +
			'	and		Opportunity__r.Account.IPPayment_Customer_Token__c != \'\' ';
		//	'	and		Contact__r.IPPayment_Customer_Token__c != null ' +
		//	'	and		Contact__r.IPPayment_Customer_Token__c != \'\' ';
		
		if(Test.isRunningTest())
		{
			strQuery += ' limit 1';
		}
		
		system.debug('strQuery ' + strQuery);
		
		if(!runBatch)
			System.abortJob(BC.getJobId());
			
		return Database.getQueryLocator(strQuery);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		
		if(scope.size() == 1)
			ProcessIncomeToBank((id)scope[0].get('Id'));
		
	}
	
	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
		try
		{
			IncomeBatchTracker__c currentJob = [	select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c 
													from IncomeBatchTracker__c 
													where Status__c = 'Processing'
													and	BatchName__c =  'QIncomeToBankProcessing'
													limit 1];
			
			currentJob.EndDateTime__c = system.now();
			currentJob.Status__c = 'Completed';
			update currentJob;
		}
		catch(Exception e )
		{
			system.debug('no batch found');	
		}
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public class QIncomeToBankProcessingException extends exception{}
	
	private static Income__c 			m_sIncome;
	private static id		 			m_IncomeId;
	private static boolean             	m_bRunningTest = false; // ONLY SET THIS WHEN YOU'RE TESTING!
	
	public static boolean ProcessIncomeToBank(id theId)
	{
		m_IncomeId = theId;
		boolean bError = false;
		LoadIncome();
		
		// now we have it lets send it to the bank...
		QProcessIPPPayment_V2.QCreditCardByReference		CreditCardByReferenceStub = new QProcessIPPPayment_V2.QCreditCardByReference();
		QProcessIPPPayment_V2.QCreditCardByReferenceReturn	RefReturnStub;
		
		
		//CreditCardByReferenceStub.CustomerNumber		= m_sIncome.Contact__r.IPPayment_Customer_Token__c;
		CreditCardByReferenceStub.CustomerNumber		= m_sIncome.Opportunity__r.Account.IPPayment_Customer_Token__c;
		CreditCardByReferenceStub.CustomerReference		= m_sIncome.Name;
		CreditCardByReferenceStub.Amount				= '' + (m_sIncome.Amount_A__c * 100).intValue();
		CreditCardByReferenceStub.CreditCardExpiryMonth = m_sIncome.Opportunity__r.Account.Credit_Card_Expire_Month__c;
		CreditCardByReferenceStub.CreditCardExpiryYear 	= m_sIncome.Opportunity__r.Account.Credit_Card_Expire_Year__c;
		//CreditCardByReferenceStub.CreditCardExpiryMonth = m_sIncome.Contact__r.Credit_Card_Expire_Month__c;
		//CreditCardByReferenceStub.CreditCardExpiryYear 	= m_sIncome.Contact__r.Credit_Card_Expire_Year__c;
		
		try
		{
			// just to be sure
			RefReturnStub = null;
			RefReturnStub = QProcessIPPPayment_V2.CalloutIPPaymentsSubmitCreditCardRegisteredPayment(CreditCardByReferenceStub);
		}
		catch(Exception e)
    	{
    		bError = true;
    		System.Debug('Exception thrown - ' + e);
    		
			m_sIncome.Payment_Gateway_Declined_Message__c 	= e.getMessage();
			m_sIncome.Payment_Gateway_Response_Code__c 		= '-1';
			m_sIncome.Status__c 							= 'Payment Gateway Error';
			
			QUtils.CreateErrorTask(m_sIncome.Id, m_sIncome.Opportunity__r.AccountId, 'Club Red Banking Error', 'The following error was raised during the banking process : ' + e.getMessage());
    	}
		
		if(RefReturnStub != null)
		{
			// lets store the respose for debugging
			m_sIncome.Payment_Gateway_Response__c	= RefReturnStub.Response;
			if(m_sIncome.Credit_Card_A__c == null)
				m_sIncome.Credit_Card_A__c				= CreditCardByReferenceStub.CreditCardType; //update by Broheim 10/02/2012
    			
			m_sIncome.Process_Payment_Through_Payment_Gateway__c		= true;
			m_sIncome.Payment_Gateway_Response_Settlement_Date__c		= RefReturnStub.SettlementDate;
			m_sIncome.Payment_Gateway_Settlement_Date__c				= QUtils.ConvertSettlementDate(RefReturnStub.SettlementDate);
			m_sIncome.Payment_Gateway_Response_Code__c					= RefReturnStub.ResponseCode;
			m_sIncome.Payment_Gateway_Response_Received__c				= true;
		
			if(RefReturnStub.ResponseCode == QConstants.STR_IPPAYMENTS_TRANSACTION_CODE_APPROVED || m_bRunningTest )
			{
				m_sIncome.Income_Processed_Date_A__c				= system.now();
		    	m_sIncome.Income_Processed_A__c						= true;
				m_sIncome.Payment_Gateway_Receipt__c				= RefReturnStub.Receipt;
				m_sIncome.Payment_Gateway_Transaction_Approved__c 	= true;
				m_sIncome.Reconciled_A__c							= true;
				m_sIncome.Payment_Gateway_Response_Text__c			= QConstants.STR_IPPAYMENTS_TRANSACTION_TEXT_APPROVED;
				m_sIncome.Payment_Gateway_Declined_Message__c		= ' ';
				m_sIncome.Payment_Gateway_Declined_Code__c			= ' ';
				m_sIncome.Status__c 								= 'Payment Gateway Approved';
			}
			else
			{
				m_sIncome.Payment_Gateway_Response_Text__c		= QConstants.STR_IPPAYMENTS_TRANSACTION_TEXT_DECLINED;
				m_sIncome.Payment_Gateway_Declined_Message__c	= RefReturnStub.DeclinedMessage;
				m_sIncome.Payment_Gateway_Declined_Code__c		= RefReturnStub.DeclinedCode;
				m_sIncome.Status__c 							= 'Payment Gateway Declined';
				m_sIncome.Declined__c							= true;
				m_sIncome.Income_Processed_Date_A__c			= null;

/* Removed by Lacey 06/09/2011 at the request of Catherine Ibbott
				QUtils.CreateErrorTask(
										QCachePaymentGatewayMetadata.getTaskOwnerIDForDeclinedPayments(),
										m_sIncome.Id, 
										m_sIncome.Contact__c, 
										'Club Red Banking Error', 
										'The following error was raised during the banking process : ' + RefReturnStub.ResponseCode + ' - ' + RefReturnStub.DeclinedMessage
										);
				//David Code
				SendEmailForDeclinedInvoice('Club Red Banking Error','The following error was raised during the banking process : ' + RefReturnStub.ResponseCode + ' - ' + RefReturnStub.DeclinedMessage);
*/
			}
		}
		System.debug(m_sIncome);
		try
		{
			update m_sIncome;
		}
		catch(Exception e)
    	{
    		System.Debug('Error updating Transaction records - ' + e);
    		QVFC_IncomeBatchManagement.DetectProcessingBatch('WARNING : FAILED RECORDS '+m_sIncome.Id,e.getMessage()+'\n\n\nIncome Id : '+m_sIncome.id);
    		bError = true;
    	}
    	/////////////
    	try
		{
			IncomeBatchTracker__c currentJob = [	select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c, BatchesProcessed__c
													from IncomeBatchTracker__c 
													where Status__c = 'Processing'
													and	BatchName__c =  'QIncomeToBankProcessing'
													order by StartDatetime__c asc limit 1];
			if(bError)
	    	{
	    		if(currentJob.NbFailedRecords__c == null)
	    			currentJob.NbFailedRecords__c = 0;
	    				
	    		currentJob.NbFailedRecords__c += 1;
	    		
	    		if(currentJob.FailedRecordIds__c != null)
	    			currentJob.FailedRecordIds__c += m_IncomeId+';';
	    		else
	    			currentJob.FailedRecordIds__c = m_IncomeId+';';
	    	}else
	    	{
	    		if(currentJob.BatchesProcessed__c == null)
	    			currentJob.BatchesProcessed__c = 0;
	    				
	    		currentJob.BatchesProcessed__c += 1;
	    	}
			update currentJob;
			
			system.debug('>>>>>>>>'+currentJob);
		}
		catch(Exception e )
		{	
			system.debug('>>>>>>>>'+e.getMessage());	
		}
		
		return true;	
	}
	
	private static void SendEmailForDeclinedInvoice(string strSubject , string strMessage)
	{
		try
		{
			User sUser = [select Id, Email from User where Id = :m_sIncome.Opportunity__r.OwnerId];
			// send login details e-mail
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			
			mail.setToAddresses(new String[] {sUser.Email});
			mail.setSenderDisplayName('RED');
			mail.setBccSender(false);
			mail.setUseSignature(false);
			mail.setSaveAsActivity(false);
			
			mail.setHTMLBody(strMessage+'<br/>View Income <a href="'+Website_Metadata__c.getInstance().BaseURLForDeclinedIncome__c+'/'+m_sIncome.Id+'">click here</a>');
			mail.setSubject(strSubject);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
		catch(Exception e)
		{
			
		}
	}
	
	private static void LoadIncome()
	{
		// just to make sure
		m_sIncome = null;
		
		try
		{
			m_sIncome = [
							select 	id, Name, Amount_A__c, Opportunity__r.OwnerId, Credit_Card_A__c,
									Opportunity__r.Account.IPPayment_Customer_Token__c ,Opportunity__r.Account.IPP_Client_Id__c,
									Opportunity__r.Account.Credit_Card_Expire_Month__c, Opportunity__r.Account.Credit_Card_Expire_Year__c,
									Opportunity__r.AccountId
							from 	Income__c 
							where 	id = :m_IncomeId
							and		Payment_Type_A__c = 'Credit Card'
							and		Payment_Gateway_Send_To_Bank_Date__c <= :System.today()
							and 	Status__c = 'Send to IPPayments'
							and		Opportunity__r.Account.IPPayment_Customer_Token__c != null
							and		Opportunity__r.Account.IPPayment_Customer_Token__c != ''
							for 	update
						];
		}
		catch(Exception e)
		{
			// damn it
			throw new QIncomeToBankProcessingException('There has been a serious issue loading the Income to process for Club Red - ' + e.getMessage());
		}
	}
	
	public static testmethod void TestQIncomeToBankProcessing()
	{
		Account thisAccount = new Account();
        thisAccount.Name = 'Test Account';
        insert thisAccount;
		 
		Opportunity qti = new Opportunity();
		qti.Name ='TREE';
		qti.AccountId = thisAccount.id;
		qti.StageName ='Club Red Ongoing';
		qti.OwnerId	= Userinfo.getUserId();
		qti.CloseDate = system.today().addDays(90);
		insert qti;
		
		Income__c sIncome = new Income__c();
		sIncome.AccountId__c = thisAccount.id;
		sIncome.Payment_Type_A__c = 'Credit Card';
		sIncome.Payment_Gateway_Send_To_Bank_Date__c = System.Today().addDays(-10);
		sIncome.Status__c = 'Send to IPPayments';
		sIncome.Opportunity__c = qti.Id;
		sIncome.Amount_A__c = 20;
		insert sIncome;
		
		QIncomeToBankProcessing qtp= new QIncomeToBankProcessing();
		
		
		// run batch with everything ok
		Test.StartTest();
		m_bRunningTest = false;
		Database.executeBatch(qtp,1);
		
		// run batch with fake decline
		Database.executeBatch(qtp);
		m_sIncome = sIncome;
		SendEmailForDeclinedInvoice('Test', 'HelloWorld');
		Test.StopTest();
	}
	
	public static testmethod void TestQIncomeToBankProcessing2()
	{
		Account thisAccount = new Account();
        thisAccount.Name = 'Test Account';
        thisAccount.IPPayment_Customer_Token__c = '12345';
        insert thisAccount;
		 
		Opportunity qti = new Opportunity();
		qti.Name ='TREE';
		qti.AccountId = thisAccount.id;
		qti.StageName ='Club Red Ongoing';
		qti.Card_Expiry_Month__c = 11;
		qti.Credit_Card_Year__c = 2010;
		qti.OwnerId	= Userinfo.getUserId();
		qti.CloseDate = system.today().addDays(9);
		insert qti;
		
		Income__c sIncome = new Income__c();
		//sIncome.Contact__c = con.Id;
		sIncome.Payment_Type_A__c = 'Credit Card';
		sIncome.AccountId__c = thisAccount.id;
		sIncome.Payment_Gateway_Send_To_Bank_Date__c = System.Today().addDays(-3);
		sIncome.Status__c = 'Send to IPPayments';
		sIncome.Opportunity__c = qti.Id;
		sIncome.Amount_A__c = 20;
		sIncome.Card_Expiry_Month_A__c = 11;
		sIncome.Card_Expiry_Year_A__c = 2009;
		sIncome.Card_Expiry_Month_B__c = 11;
		sIncome.Card_Expiry_Year_B__c = 2010;
		insert sIncome;
		
		IncomeBatchTracker__c currentJob = new IncomeBatchTracker__c(
				BatchName__c = 'QIncomeToBankProcessing',
				Status__c = 'Processing',
				NbRecords__c = 1,
				NbFailedRecords__c = 0
			);
		insert currentJob;
		
		QIncomeToBankProcessing qtp= new QIncomeToBankProcessing();
		
		m_sIncome = sIncome;
		SendEmailForDeclinedInvoice('Test', 'HelloWorld');
		
		// run batch with everything ok
		Test.StartTest();
		m_bRunningTest = true;
		Database.executeBatch(qtp,1);
		
		// run batch with fake decline	
		Database.executeBatch(qtp);
		
		Test.StopTest();
	}
	
}