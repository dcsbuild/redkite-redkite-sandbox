public with sharing class QConstants 
{
	public static final string 		STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_SENT				= 'Sent';
	public static final string 		STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_APPROVED 			= 'Approved';
	public static final string 		STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_DECLINED 			= 'Declined';
	public static final string 		STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_REFUND 			= 'Refund';
	public static final string 		STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_ERROR 			= 'Error';
	
	public static final string 		STR_IPPAYMENTS_TRANSACTION_CODE_APPROVED			 	= '0';
	public static final string 		STR_IPPAYMENTS_TRANSACTION_TEXT_APPROVED			 	= 'Approved';
	public static final string 		INT_IPPAYMENTS_TRANSACTION_CODE_DECLINED			 	= '1';
	public static final string 		STR_IPPAYMENTS_TRANSACTION_TEXT_DECLINED			 	= 'Declined';
	
	public static final string		STR_RED_PAYMENT_DETAILS_DECLIEND						= 'Declined by Red';
	
	public static final string 		KITETIME_NONE_REGESTERED_CORPORATION_CODE			 	= 'OTHER';
	
	public static final string 		KITETIME_IPP_INCOME_COUNTER_NAME					 	= 'IPP_INCOME_COUNTER';
	public static final string 		KITETIME_IPP_CLIENT_COUNTER_NAME					 	= 'IPP_CLIENT_COUNTER';

	public static final string		CORPORATE_IPP_INCOME_COUNTER_NAME						= 'IPP_INCOME_COUNTER';
}