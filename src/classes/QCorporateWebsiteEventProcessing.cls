public with sharing class QCorporateWebsiteEventProcessing 
{
	public class QCorporateWebsiteWebServicesParameterException extends Exception{}
	public class QCorporateWebsiteWebServicesException 			extends Exception{}		
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public static QCorporateWebsiteWebServices.QEventUpdateResults GetEventDetails(QCorporateWebsiteWebServices.QEventUpdateRequest oArgs)
	{
		// return object
		QCorporateWebsiteWebServices.QEventUpdateResults oRet = new QCorporateWebsiteWebServices.QEventUpdateResults();
		
		Campaign sCampaign; 
		map<id, Attachment> mapIdToAttachment = new map<id, Attachment>();
		
		try
		{
			sCampaign = [
							select 	id,
									Is_Visible_On_Website__c, Title_For_Website__c,
									Description_For_Website__c, Website_Main_Image_Id__c,
									Website_Sub_Image_1_Id__c, Website_Sub_Image_2_Id__c,
									Job_Code_State__c, Event_Location_For_Website__c, Event_Date__c,
									EndDate, Event_Time_For_Website__c, Event_Cost_For_Website__c,
									Enquiries_For_Website__c, Product__c, Year_Long_Event__c,
									Allow_Website_Registrations__c, Registration_Code_For_Website__c,
									Microsite_Location__c, Event_Link__c, Event_Link_Description__c,
									Event_Location_Link__c, Event_State_For_Website__c,
									RRule__r.Start_Date__c, RRule__r.End_Date__c, RRule__r.RRule__c
							from 	Campaign 
							where 	id = :oArgs.UniqueEventCode 
						];

			oRet.UniqueEventCode		= sCampaign.id;
			oRet.IsActive				= sCampaign.Is_Visible_On_Website__c;
			oRet.Name 					= sCampaign.Title_For_Website__c;
			oRet.Description			= sCampaign.Description_For_Website__c;
			//oRet.State					= sCampaign.Job_Code_State__c;
			oRet.State					= sCampaign.Event_State_For_Website__c;
			oRet.Location				= sCampaign.Event_Location_For_Website__c;
			oRet.LocationLink			= sCampaign.Event_Location_Link__c;

			if(sCampaign.RRule__r != null)
			{
				oRet.EventDate				= sCampaign.RRule__r.Start_Date__c;
				oRet.EventEndDate			= sCampaign.RRule__r.End_Date__c;
				oRet.RRule					= sCampaign.RRule__r.RRule__c;
			}
			else
			{
				oRet.EventDate				= sCampaign.Event_Date__c;
				oRet.EventEndDate			= sCampaign.Event_Date__c;
			}
		
			oRet.EventTime				= sCampaign.Event_Time_For_Website__c;
			oRet.Cost					= sCampaign.Event_Cost_For_Website__c;
			oRet.Enquiries				= sCampaign.Enquiries_For_Website__c;
			oRet.IsRegistrationEvent	= sCampaign.Allow_Website_Registrations__c;
			oRet.RegistrationCode		= sCampaign.Registration_Code_For_Website__c;
			oRet.MicrositeLocation		= sCampaign.Microsite_Location__c;

			oRet.IsTicketedEvent 		= (sCampaign.Product__c != null);
			oRet.TicketProductId		= sCampaign.Product__c;
			
			oRet.EventLink				= sCampaign.Event_Link__c;
			oRet.EventLinkDescription	= sCampaign.Event_Link_Description__c;
			oRet.IsYearLongEvent		= sCampaign.Year_Long_Event__c;
		}
		catch(QueryException qe)
		{
			System.debug('Exception - Trying to Load Campaign Details - ' + qe);
			throw new QCorporateWebsiteWebServicesException('Error Loading - ' + oArgs.UniqueEventCode); 
		}
		catch(Exception e)
		{
			System.debug('Exception - Trying to Load Campaign Details - ' + e);
			throw new QCorporateWebsiteWebServicesException('Error Loading - ' + oArgs.UniqueEventCode);
		}

		if(sCampaign.Website_Main_Image_Id__c != null)
		{
			mapIdToAttachment.put(sCampaign.Website_Main_Image_Id__c, null);
		}
		
		if(sCampaign.Website_Sub_Image_1_Id__c != null)
		{
			mapIdToAttachment.put(sCampaign.Website_Sub_Image_1_Id__c, null);
		}
		
		if(sCampaign.Website_Sub_Image_2_Id__c != null)
		{
			mapIdToAttachment.put(sCampaign.Website_Sub_Image_2_Id__c, null);
		}

		try
		{
			for(Attachment sImage : [select Body, id from Attachment where id in :mapIdToAttachment.keyset()])
			{
				mapIdToAttachment.put(sImage.id, sImage);
			}
		}
		catch(QueryException qe)
		{
			System.debug('Exception - Trying to Load Campaign Images - ' + qe);
		}
		catch(Exception e)
		{
			System.debug('Serious Exception - Trying to Load Campaign Images - ' + e);
		}
		
		if(mapIdToAttachment.containsKey(sCampaign.Website_Main_Image_Id__c))
		{
			oRet.MainImage = mapIdToAttachment.get(sCampaign.Website_Main_Image_Id__c).body;
		}
		
		if(mapIdToAttachment.containsKey(sCampaign.Website_Sub_Image_1_Id__c))
		{
			oRet.SubImage1 = mapIdToAttachment.get(sCampaign.Website_Sub_Image_1_Id__c).body;
		}

		if(mapIdToAttachment.containsKey(sCampaign.Website_Sub_Image_2_Id__c))
		{
			oRet.SubImage2 = mapIdToAttachment.get(sCampaign.Website_Sub_Image_2_Id__c).body;
		}

		return oRet;
	}
}