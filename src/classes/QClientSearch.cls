public without sharing class QClientSearch 
{
	/***********************************************************************************************************
		Inner Classes
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes
	public class QClientSearchParameterException extends Exception{}
	public class QClientSearchException extends Exception{}

	public enum DedupeMethod {CORPORATE, CORPORATE_SHOP}
	public enum CreateAction {ACCOUNT, PERSON_ACCOUNT}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The Arguement Object
	public class QClientSearchArgs
	{
		public DedupeMethod DedupeMethod				{get; set;}
		public CreateAction	CreateAction				{get; set;}
		public boolean 		CreateTaskForPossibleDupe	{get; set;}
		
		public string 	Email							{get; set;}
		public string 	FirstName						{get; set;}
		public string 	LastName						{get; set;}
		public string 	State							{get; set;}
		
		public string 	Title							{get; set;}
		public string 	PhoneNumber						{get; set;}
		public string 	OtherPhoneNumber				{get; set;}
		public string 	Street							{get; set;}
		public string 	Suburb							{get; set;}
		public string 	Country							{get; set;}
		public string 	PostalCode						{get; set;}
		public boolean 	MarketingOptIn					{get; set;}
		public boolean 	FurtherInformationOptIn			{get; set;}
		
		public string 	Website							{get; set;}
		
		public boolean	ClubRedDonator					{get; set;}
		public string 	CorporateName					{get; set;}
		
		public id 		AccountIdOnCampaign				{get; set;}
		public string	AccountName						{get; set;}
		
		public integer 	IPPClientId 					{get; set;}
		
		public date		DOB								{get; set;}
		public string	PhoneType						{get; set;}
		
		public string	DonationSource					{get; set;}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The Return Object
	public class QClientSearchRet
	{
		public Id 		ContactId	{get; private set;}
		public Id 		AccountId	{get; private set;}
		public string 	IPPClientId {get; private set;}
	}
	
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	private QClientSearchRet 	mRet;
	private QClientSearchArgs 	mArgs;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	
	/***********************************************************************************************************
		Constructors
	***********************************************************************************************************/
	public QClientSearch(QClientSearchArgs args)
	{
		mRet 	= new QClientSearchRet();
		mArgs 	= args;
	}

	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public QClientSearchRet ProcMain()
	{
		if(mArgs.DedupeMethod == DedupeMethod.CORPORATE)
		{
			if(mArgs.CreateAction == CreateAction.ACCOUNT)
			{
				CorporateAccountSearch();
			}
			else
			{
				CorporateClientSearch();
			}
		}
		else if(mArgs.DedupeMethod == DedupeMethod.CORPORATE_SHOP)
			CorporateShopClientSearch();

		return mRet;	
	}

	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	private void CorporateClientSearch()
	{
		System.Debug('Starting Corporate Client Search');
		
		boolean bFound = false;
		List<Account> liAccount;
		
		// first we search by email
		List<List<SObject>> searchList = [
											FIND 		:mArgs.Email 
											IN 			EMAIL FIELDS
											RETURNING	
													Account(
																		id, OwnerId, IPP_Client_Id__c, IsPersonAccount, PersonContactId
																where 	IsPersonAccount = true 
																order by LastModifiedDate desc
															)
										];
		
		// lets get the list of Person Accounts
		liAccount = ((List<Account>)searchList[0]);
		
		System.Debug('Returned - ' + liAccount.size() + ' Individual SObjects with matching email address');
		
		if(liAccount.size() > 0)
		{
			System.Debug('Working on Person Accounts');
			
			bFound = true;	// we hit something
			mRet.ContactId = liAccount[0].PersonContactId;
			mRet.IPPClientId = ''+liAccount[0].IPP_Client_Id__c;
			
			// set the Account Id to either the Passed Argument or the one on the client
			mRet.AccountId = mArgs.AccountIdOnCampaign == null ? liAccount[0].Id : mArgs.AccountIdOnCampaign;
			
		}
		
		liAccount.clear();
		
		System.Debug('Finished SOSL Search');
		
		if(!bFound)
		{
			string strLookup = mArgs.FirstName.toLowerCase() + mArgs.LastName.toLowerCase();

			if(mArgs.Suburb != null)
				strLookup += mArgs.Suburb.toLowerCase();
			if(mArgs.State != null)
				strLookup += mArgs.State.toLowerCase();
			
			System.Debug('SOQL Search string - ' + strLookup);
			
			// now the First Name, Last Name and State concatenation
			liAccount = [select id, OwnerId, IPP_Client_Id__c, PersonContactId
						 from Account 
						 where 	IsPersonAccount = true
						 and	FirstLastSuburbStateDedupeCheck__c = :strLookup 
						 and 	RecordType.Developername != 'Family_Contact'
						 order by LastModifiedDate desc limit 10];
			
			// we hit!
			if(liAccount.size() > 0)
			{
				bFound = true;
				mRet.ContactId = liAccount[0].PersonContactId;
				mRet.AccountId = liAccount[0].Id;
				mRet.IPPClientId = liAccount[0].IPP_Client_Id__c;
				
			}
			System.Debug('Finished SOQL Search');
		}
		
		// we didnt find anything so we need to create some!
		if(!bFound && mArgs.CreateAction == CreateAction.PERSON_ACCOUNT)
		{
			Account theAccount = new Account();
			
			if(mArgs.FirstName != null)
				theAccount.FirstName = mArgs.FirstName;
			
			if(mArgs.LastName != null)
				theAccount.LastName = mArgs.LastName;
			
			if(mArgs.Title != null)
				theAccount.Salutation = mArgs.Title;

			// Address Information
			if(mArgs.Street != null)
			{
				theAccount.ShippingStreet = mArgs.Street;
				theAccount.BillingStreet = mArgs.Street;
			}
			
			if(mArgs.Suburb != null)
			{
				theAccount.ShippingCity = mArgs.Suburb;
				theAccount.BillingCity = mArgs.Suburb;
			}
			
			if(mArgs.State != null)
			{
				theAccount.ShippingState = mArgs.State;
				theAccount.BillingState = mArgs.State;
			}
			
			if(mArgs.Country != null)
			{
				theAccount.ShippingCountry = mArgs.Country;
				theAccount.BillingCountry = mArgs.Country;
			}
			
			if(mArgs.PostalCode != null)
			{
				theAccount.ShippingPostalCode = mArgs.PostalCode;
				theAccount.BillingPostalCode = mArgs.PostalCode;
			}
				
			// Contact Information
			if(mArgs.PhoneNumber != null)
			{
				// old way
				if(mArgs.PhoneType == null || mArgs.PhoneType == '')
				{
					theAccount.Phone = mArgs.PhoneNumber;
				}
				else if(mArgs.PhoneType == 'Mobile')
				{
					theAccount.PersonMobilePhone = mArgs.PhoneNumber;
				}
				else if(mArgs.PhoneType == 'Work')
				{
					theAccount.Phone = mArgs.PhoneNumber;
				}
				else if(mArgs.PhoneType == 'Home')
				{
					theAccount.Phone = mArgs.PhoneNumber;
					theAccount.PersonHomePhone = mArgs.PhoneNumber;
				}
			}
			
			if(mArgs.DonationSource != null)
				theAccount.PersonLeadSource = mArgs.DonationSource;
				
			if(mArgs.OtherPhoneNumber != null)
				theAccount.PersonOtherPhone = mArgs.OtherPhoneNumber;
				
			if(mArgs.Email != null)
				theAccount.PersonEmail = mArgs.Email;
				
			if(mArgs.DOB != null)
				theAccount.PersonBirthdate = mArgs.DOB;
				
			if(mArgs.FurtherInformationOptIn && mArgs.MarketingOptIn)
			{
				theAccount.Email_Communication__pc = 'Chairmans Appeals; Christmas Appeals; Event Material; Newsletter';
				theAccount.PersonHasOptedOutOfEmail = false;
			}				
			else if(mArgs.FurtherInformationOptIn)
			{
				theAccount.Email_Communication__pc = 'Newsletter';
				theAccount.PersonHasOptedOutOfEmail = false;
			}
			else if(mArgs.MarketingOptIn)
			{
				theAccount.Email_Communication__pc = 'Chairmans Appeals; Christmas Appeals; Event Material';
				theAccount.PersonHasOptedOutOfEmail = false;
			}
			else
			{
				theAccount.PersonHasOptedOutOfEmail = true;
				theAccount.Description = 'Contact chose to opt out of email communications on Corporate website';
			}
			
			system.debug('Ownership:::' + mArgs.ClubRedDonator + ' ::: ' + theAccount);
			
			// set the owner according to the state/country of the account
			if(mArgs.ClubRedDonator != null && mArgs.ClubRedDonator)
			{
				theAccount.OwnerId = QCacheWebsiteMetadata.getCorporateDonationOwnerClubRed();
			}
			else if(theAccount.ShippingCountry == 'Australia')
			{
				theAccount.OwnerId = QCacheWebsiteMetadata.getCorporateDonationOwnerForState(theAccount.ShippingState);
			}
			else
			{
				theAccount.OwnerId = QCacheWebsiteMetadata.getCorporateDonationOwnerINTL();	
			}
			
			insert theAccount;

			theAccount = [select id, PersonContactId, IPP_Client_Id__c, IsPersonAccount from Account where id = :theAccount.id];
			
			// now set the contact id passed back
			mRet.ContactId 		= theAccount.PersonContactId;
			mRet.AccountId 		= theAccount.id;
			mRet.IPPClientId 	= theAccount.IPP_Client_Id__c;
			
			System.Debug('New Account:'+theAccount);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	private void CorporateAccountSearch()
	{
		System.Debug('Starting Corporate Account Search');
		
		boolean bFound = false;
		List<Account> liAccount;
		
		// first we search by email
		List<List<SObject>> searchList = [
											FIND 		:mArgs.Email 
											IN 			EMAIL FIELDS
											RETURNING	Account(id, OwnerId where RecordType.Developername != 'Family_Contact' and PersonContactId = null order by LastModifiedDate desc)
										];
		
		// lets get the list of Contacts
		liAccount = ((List<Account>)searchList[0]);
		
		System.Debug('Returned - ' + liAccount.size() + ' Individual SObjects with matching email address');
		
		if(liAccount.size() > 0)
		{
			System.Debug('Working on Accounts');
			
			bFound = true;	// we hit something
			
			mRet.AccountId = liAccount[0].Id;
			mRet.ContactId = liAccount[0].Id;
			
		}
		
		liAccount.clear();
		
		System.Debug('Finished SOSL Search');
		
		if(!bFound)
		{
			string strLookup = mArgs.CorporateName.toLowerCase();
			
			if(mArgs.PhoneNumber != null)
				strLookup += mArgs.PhoneNumber.toLowerCase();

			System.Debug('SOQL Search string - ' + strLookup);
			
			// now the First Name, Last Name and State concatenation
			liAccount = [select id, OwnerId 
						 from Account 
						 where Name = :strLookup and IsPersonAccount = false
						 order by LastModifiedDate desc limit 10];
			
			// we hit!
			if(liAccount.size() > 0)
			{
				bFound = true;
				mRet.AccountId = liAccount[0].Id;
				mRet.ContactId = liAccount[0].Id;
			}
			System.Debug('Finished SOQL Search');
		}
		
		// we didnt find anything so we need to create some!
		if(!bFound && mArgs.CreateAction == CreateAction.ACCOUNT)
		{
			Account theAccount = new Account();
			theAccount.Name = mArgs.CorporateName;
			
			if(mArgs.PhoneNumber != null)
			{
				theAccount.Phone = mArgs.PhoneNumber;
			}
			
			if(mArgs.Street != null)
			{
				theAccount.BillingStreet = mArgs.Street;
				theAccount.ShippingStreet = mArgs.Street;
			}
			
			if(mArgs.Suburb != null)
			{
				theAccount.BillingCity = mArgs.Suburb;
				theAccount.ShippingCity = mArgs.Suburb;
			}
			
			if(mArgs.State != null)
			{
				theAccount.BillingState = mArgs.State;
				theAccount.ShippingState = mArgs.State;
			}
			
			if(mArgs.Country != null)
			{
				theAccount.BillingCountry = mArgs.Country;
				theAccount.ShippingCountry = mArgs.Country;
			}
			
			if(mArgs.PostalCode != null)
			{
				theAccount.BillingPostalCode = mArgs.PostalCode;
				theAccount.ShippingPostalCode = mArgs.PostalCode;
			}
				
			if(mArgs.Website != null)
			{
				theAccount.Website = mArgs.Website;
			}
			
			if(mArgs.Email != null)
			{
				theAccount.Email__c = mArgs.Email;
			}
				
			insert theAccount;
			
			theAccount = [select id, IPP_Client_Id__c from Account where id = :theAccount.id];
			
			// now set the ids passed back
			mRet.AccountId 		= theAccount.id;
			mRet.ContactId 		= theAccount.id;
			mRet.IPPClientId 	= ''+theAccount.IPP_Client_Id__c;
			
			System.Debug('New Account:'+theAccount);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	private void CorporateShopClientSearch()
	{
		System.Debug('Starting Corporate Shop Client Search');
		
		boolean bFound = false;
		List<Account> liAccount;
		
		// first we search by email
		List<List<SObject>> searchList = [
											FIND 		:mArgs.Email 
											IN 			EMAIL FIELDS
											RETURNING	Account
																(	
																id, OwnerId, IPP_Client_Id__c, IsPersonAccount, PersonContactId
																where IsPersonAccount = true
																order by LastModifiedDate desc
																)
										];
		
		// lets get the list of Contacts
		liAccount = ((List<Account>)searchList[0]);
		
		System.Debug('Returned - ' + liAccount.size() + ' Individual SObjects with matching email address');
		
		if(liAccount.size() > 0)
		{
			System.Debug('Working on Person Accounts');
			
			bFound = true;	// we hit something
			
			mRet.ContactId = liAccount[0].PersonContactId;
			mRet.IPPClientId = liAccount[0].IPP_Client_Id__c;
			
			// set the Account Id to either the Passed Argument or the one on the client
			mRet.AccountId = mArgs.AccountIdOnCampaign == null ? liAccount[0].Id : mArgs.AccountIdOnCampaign;
			
		}
		
		liAccount.clear();
		
		System.Debug('Finished SOSL Search');
		
		if(!bFound)
		{
			string strLookup = mArgs.FirstName.toLowerCase() + mArgs.LastName.toLowerCase();

			// build up the mailing address
			if(mArgs.Street != null)
				strLookup += mArgs.Street.toLowerCase();

			if(mArgs.Suburb != null)
				strLookup += mArgs.Suburb.toLowerCase();

			if (mArgs.State != null)
				strLookup += mArgs.State.toLowerCase();

			if (mArgs.PostalCode != null)
				strLookup += mArgs.PostalCode.toLowerCase();

			System.Debug('SOQL Search string - ' + strLookup);

			// now the First Name, Last Name and State concatenation
			liAccount = [
							select 	id, OwnerId, IPP_Client_Id__c, IsPersonAccount, PersonContactId 
							from 	Account 
							where 	IsPersonAccount = true
							and		FirstLastSuburbStateDedupeCheck__c = :strLookup 
							and 	RecordType.Developername != 'Family_Contact' 
							order by LastModifiedDate desc limit 10
						];
			
			// we hit!
			if(liAccount.size() > 0)
			{
				bFound = true;
				mRet.ContactId = liAccount[0].PersonContactId;
				mRet.IPPClientId = liAccount[0].IPP_Client_Id__c;

				// set the Account Id to either the Passed Argument or the one on the client
				mRet.AccountId = mArgs.AccountIdOnCampaign == null ? liAccount[0].Id : mArgs.AccountIdOnCampaign;

			}
			System.Debug('Finished SOQL Search');
		}

		// new bit - do we still want the old check above?
		if(!bFound)
		{
			string strLookup = mArgs.FirstName.toLowerCase() + mArgs.LastName.toLowerCase();

			// build up the mailing address
			if(mArgs.Street != null)
				strLookup += mArgs.Street.toLowerCase();

			if(mArgs.Suburb != null)
				strLookup += mArgs.Suburb.toLowerCase();

			if (mArgs.State != null)
				strLookup += mArgs.State.toLowerCase();

			if (mArgs.PostalCode != null)
				strLookup += mArgs.PostalCode.toLowerCase();

			System.Debug('SOQL Search string - ' + strLookup);

			// now the First Name, Last Name and State concatenation
			liAccount = [
							select 	id, OwnerId, IPP_Client_Id__c, IsPersonAccount, PersonContactId 
							from 	Account 
							where 	IsPersonAccount = true
							and		FirstLastMailingAddressDedupeCheck__c = :strLookup 
							and 	(RecordType.Developername != 'Family_Contact' or RecordType.DeveloperName != 'Family_Account')
							order by LastModifiedDate desc limit 10
						];
			
			// we hit!
			if(liAccount.size() > 0)
			{
				bFound = true;
				mRet.ContactId = liAccount[0].PersonContactId;
				mRet.IPPClientId = liAccount[0].IPP_Client_Id__c;

				// set the Account Id to either the Passed Argument or the one on the client
				mRet.AccountId = mArgs.AccountIdOnCampaign == null ? liAccount[0].Id : mArgs.AccountIdOnCampaign;

			}
			System.Debug('Finished SOQL Search');
		}
		
		// we didnt find anything so we need to create some!
		if(!bFound && mArgs.CreateAction == CreateAction.PERSON_ACCOUNT)
		{
			Account theAccount = new Account();
			
			if(mArgs.FirstName != null)
				theAccount.FirstName = mArgs.FirstName;
			
			if(mArgs.LastName != null)
				theAccount.LastName = mArgs.LastName;
			
			if(mArgs.Title != null)
				theAccount.Salutation = mArgs.Title;

			// Address Information
			if(mArgs.Street != null)
				//theAccount.BillingStreet = mArgs.Street;
				theAccount.ShippingStreet  = mArgs.Street;
			if(mArgs.Suburb != null)
				//theAccount.BillingCity = mArgs.Suburb;
				theAccount.ShippingCity = mArgs.Suburb;
			if(mArgs.State != null)
				//theAccount.BillingState = mArgs.State;
				theAccount.ShippingState = mArgs.State;
			if(mArgs.Country != null)
				//theAccount.BillingCountry = mArgs.Country;
				theAccount.ShippingCountry = mArgs.Country;
			if(mArgs.PostalCode != null)
				//theAccount.BillingPostalCode = mArgs.PostalCode;
				theAccount.ShippingPostalCode = mArgs.PostalCode;
			if(mArgs.Street != null)
				theAccount.PersonMailingStreet = mArgs.Street;
			if(mArgs.Suburb != null)
				theAccount.PersonMailingCity = mArgs.Suburb;
			if(mArgs.State != null)
				theAccount.PersonMailingState = mArgs.State;
			if(mArgs.Country != null)
				theAccount.PersonMailingCountry = mArgs.Country;
			if(mArgs.PostalCode != null)
				theAccount.PersonMailingPostalCode = mArgs.PostalCode;
			
			// Contact Information
			if(mArgs.PhoneNumber != null)
				theAccount.Phone = mArgs.PhoneNumber;
			if(mArgs.OtherPhoneNumber != null)
				theAccount.PersonOtherPhone = mArgs.OtherPhoneNumber;
			if(mArgs.Email != null)
				theAccount.PersonEmail = mArgs.Email;
				
			if(mArgs.FurtherInformationOptIn && mArgs.MarketingOptIn)
			{
				theAccount.Email_Communication__pc = 'Event Material; Newsletter';
				theAccount.PersonHasOptedOutOfEmail = false;
			}				
			else if(mArgs.FurtherInformationOptIn)
			{
				theAccount.Email_Communication__pc = 'Newsletter';
				theAccount.PersonHasOptedOutOfEmail = false;
			}
			else if(mArgs.MarketingOptIn)
			{
				theAccount.Email_Communication__pc = 'Event Material';
				theAccount.PersonHasOptedOutOfEmail = false;
			}
			else
			{
				theAccount.PersonHasOptedOutOfEmail = true;
				theAccount.Description = 'Contact chose to opt out of email communications on Corporate website';
			}		
			
			// set the owner according to the state/country of the account
			if(mArgs.ClubRedDonator != null && mArgs.ClubRedDonator)
			{
				theAccount.OwnerId = QCacheWebsiteMetadata.getCorporateDonationOwnerClubRed();
			}
			else if(theAccount.PersonMailingCountry == 'Australia')
			{
				theAccount.OwnerId = QCacheWebsiteMetadata.getCorporateDonationOwnerForState(theAccount.PersonMailingState);
			}
			else
			{
				theAccount.OwnerId = QCacheWebsiteMetadata.getCorporateDonationOwnerINTL();				
			}
			
			insert theAccount;

			theAccount = [select id, PersonContactId, IPP_Client_Id__c from Account where id = :theAccount.id];
			
			// now set the contact id passed back
			mRet.ContactId 		= theAccount.PersonContactId;
			mRet.AccountId 		= theAccount.id;
			mRet.IPPClientId 	= ''+theAccount.IPP_Client_Id__c;
			
			System.Debug(theAccount);
		}
	}
}