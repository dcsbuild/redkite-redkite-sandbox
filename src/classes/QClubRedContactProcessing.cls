global class  QClubRedContactProcessing // implements Database.Batchable<SObject>, Schedulable
{
	/*
    global void execute(SchedulableContext SC)
    {
        Database.ExecuteBatch(new QClubRedContactProcessing());    
    } 

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    	string strQuery = 'select Id, IsPersonAccount, (select o.Id from Opportunities__r o where o.StageName=\'Club Red Ongoing\' limit 1) from Contact';
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    	list<Contact> liContacts = new list<Contact>();
    	list<Contact> liPAContacts = new list<Contact>();
    	
        for(sObject o : scope)
        {
        	Contact sContact = (Contact)o;
        	sContact.Is_Club_Red_Member__c = false;
        	
        	for(Opportunity sOppty : sContact.Opportunities__r)
        	{
        		sContact.Is_Club_Red_Member__c = true;
        		break;
        	}
        	
        	if(sContact.IsPersonAccount)
        	{
        		liPAContacts.add(sContact);
        	}
        	else
        	{
        		liContacts.add(sContact);
        	}
        }

       	update liContacts;
       	update liPAContacts;
    }

    global void finish(Database.BatchableContext BC)
    {
    }

    public static testmethod void testThis()
    {
    	Contact sContact 				= new Contact();
    	sContact.FirstName				= 'David';
    	sContact.LastName				= 'The Tester';
    	insert sContact;

    	Opportunity sOpportunity 		= new Opportunity();
    	sOpportunity.Name				= 'My Test Opportunity1';
    	sOpportunity.Contact__c			= sContact.Id;
    	sOpportunity.StageName			= 'Club Red Ongoing';
    	sOpportunity.CloseDate			= Date.today().addDays(10);
    	insert sOpportunity;
    	
    	sOpportunity 		= new Opportunity();
    	sOpportunity.Name				= 'My Test Opportunity2';
    	sOpportunity.Contact__c			= sContact.Id;
    	sOpportunity.StageName			= 'Club Red Ongoing';
    	sOpportunity.CloseDate			= Date.today().addDays(10);
    	insert sOpportunity;
    	
    	sOpportunity 		= new Opportunity();
    	sOpportunity.Name				= 'My Test Opportunity3';
    	sOpportunity.Contact__c			= sContact.Id;
    	sOpportunity.StageName			= 'Club Red Ongoing';
    	sOpportunity.CloseDate			= Date.today().addDays(10);
    	insert sOpportunity;
    	
    	Test.startTest();
    	QClubRedContactProcessing instance = new QClubRedContactProcessing();
    	Test.stopTest();
    }
    */
}