public with sharing class QProcessPayment
{
	/***************************************************************************************
		Exception Classes
	***************************************************************************************/
	public class QPaymentProcessingException 			extends Exception{}
	public class QPaymentProcessingParameterException 	extends Exception{}
	
	public enum PaymentProcess {
									SINGLE_PAYMENT_CALLOUT_ONLY,
									
									CORPORATE_RECORD_SINGLE_PAYMENT,
									CORPORATE_RECORD_PRODUCT_PAYMENT,
									CORPORATE_SETUP_RECURRING_PAYMENT,
									CORPORATE_SETUP_RECURRING_PAYMENT_TOKEN
								}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information for Processing a Credit Card
	public class QProcessPaymentArgs
	{
		public PaymentProcess	PaymentProcess{get; set;}
		 
		// common info
		public integer 	Amount					{get; set;}
		public integer	ShippingComponent		{get; set;}
		public string	ShippingMethod			{get; set;}
		public boolean 	AllowCallout			{get; set;}
		
		public id		AccountId				{get; set;}
		public id		ContactId				{get; set;}
		public id		IncomeId				{get; set;}
		
		public string 	IPPClientId				{get; set;}
		public integer 	IPPTransactionRef		{get; set;}
		
		// single payment info
		public string 	CreditCardNumber		{get; set;}
		public string 	CreditCardName			{get; set;}
		public integer 	CreditCardExpiryYear	{get; set;}
		public integer 	CreditCardExpiryMonth	{get; set;}
		public string 	CreditCardCVN			{get; set;}
		public string	CreditCardType			{get; set;}
		
		// single refund info
		public string 	RefundReason			{get; set;}

		// end date is used for recurring donations 
		public date		EndDate					{get; set;}
		
		// Payment Recording Info
		public QProcessIPPPayment.QProcessIPPPaymentRet IPPResponse		{get; set;}
		public Id										CampaignId		{get; set;}
		public string									AccountName		{get; set;}
		public string 									IncomeComments	{get; set;}
		
		public boolean									PostReceipt		{get; set;}
		
		public string									DonationSource	{get; set;}

		// Product list used in shop purchase requests
		public list<QCorporateWebsiteWebServices.QProduct> 	Products			{get; set;}

		public QProcessPaymentArgs()
		{
			this.CreditCardNumber 		= '';
			this.CreditCardName 		= '';
			this.CreditCardExpiryYear 	= -1;
			this.CreditCardExpiryMonth 	= -1;
			this.CreditCardCVN 			=  '0';
			this.CreditCardType 		= '';
			this.Amount 				=  0;
			this.PostReceipt			= false;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information returned when Processing a Credit Card
	public class QProcessPaymentRet
	{
		public boolean		InError				{get; set;}
		public List<string>	ErrorList			{get; set;}
		
		// what the gateway will responde
		// hack: removed private set
		public QProcessIPPPayment.QProcessIPPPaymentRet IPPResponse {get; set;}
		
		// payment recording info return
		public integer 		CampaignTotal		{get; set;}
		public Income__c	Income				{get; set;}
		public id			CROpportunity		{get; set;}
		
		// if the payment has been made
		public boolean 	IsPaymentSuccessful 	{get; set;}
		
		public QProcessPaymentRet()
		{
			this.IsPaymentSuccessful	= false;
			this.InError				= false;
			this.ErrorList				= new List<string>();
		}
	}
	
	/***************************************************************************************
		Members
	***************************************************************************************/
	string m_strEndPoint, m_strUsername, m_strPassword;
	
	QProcessPaymentArgs	mArgs;
	QProcessPaymentRet	mRet;
	
	/***************************************************************************************
		Constructor
	***************************************************************************************/
	public QProcessPayment(QProcessPaymentArgs oArgs)
	{
		this.mArgs = oArgs;
		mRet = new QProcessPaymentRet();
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	public QProcessPaymentRet ProcMain()
	{
		System.Debug(mArgs);
		
		if(mArgs.PaymentProcess == PaymentProcess.SINGLE_PAYMENT_CALLOUT_ONLY)
		{
			SinglePaymentCallOutOnly(); 
		}
		else if(mArgs.PaymentProcess == PaymentProcess.CORPORATE_RECORD_SINGLE_PAYMENT)
		{
			QProcessPaymentCorporate processor = new QProcessPaymentCorporate(mArgs);
			mRet = processor.CorporateRecordSinglePayment();
		}
		else if(mArgs.PaymentProcess == PaymentProcess.CORPORATE_RECORD_PRODUCT_PAYMENT)
		{
			QProcessPaymentCorporate processor = new QProcessPaymentCorporate(mArgs);
			mRet = processor.CorporateRecordProductPayment();
		}
		else if(mArgs.PaymentProcess == PaymentProcess.CORPORATE_SETUP_RECURRING_PAYMENT)
		{
			QProcessPaymentCorporate processor = new QProcessPaymentCorporate(mArgs);
			mRet = processor.CorporateSetupRecurringPayment();
		}
		else if (mArgs.PaymentProcess == PaymentProcess.CORPORATE_SETUP_RECURRING_PAYMENT_TOKEN) 
		{
			QProcessPaymentCorporate processor = new QProcessPaymentCorporate(mArgs);
			mRet = processor.CorporateSetupRecurringPaymentToken();
		}
		else
		{
			throw new QPaymentProcessingParameterException('Unknown Payment Processing Option - ' + mArgs.PaymentProcess);
		}
		
		return mRet;
	}
	
	/***************************************************************************************
		Payment Processing Logic
	***************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	private void SinglePaymentCallOutOnly()
	{
		QProcessIPPPayment oPaymentProcessor;	// our processor
		
		QProcessIPPPayment.QProcessIPPPaymentArgs 	oPaymentProcessorArgs = new QProcessIPPPayment.QProcessIPPPaymentArgs();
		QProcessIPPPayment.QProcessIPPPaymentRet 	oPaymentProcessorRet;
		
		oPaymentProcessorArgs.CustomerNumber		= mArgs.IPPClientId;
		oPaymentProcessorArgs.CustomerRef			= mArgs.IPPTransactionRef.format().replace(',','');
		
		oPaymentProcessorArgs.PaymentProcess 		= QProcessIPPPayment.PaymentProcess.SINGLE_PAYMENT;
    	oPaymentProcessorArgs.AllowCallout 			= mArgs.AllowCallout;
    	oPaymentProcessorArgs.Amount				= mArgs.Amount.format();
		oPaymentProcessorArgs.CreditCardNumber		= mArgs.CreditCardNumber;
		oPaymentProcessorArgs.CreditCardName		= mArgs.CreditCardName;
		oPaymentProcessorArgs.CreditCardExpiryYear 	= mArgs.CreditCardExpiryYear;
		oPaymentProcessorArgs.CreditCardExpiryMonth = mArgs.CreditCardExpiryMonth;
		oPaymentProcessorArgs.CreditCardCVN			= mArgs.CreditCardCVN;
		oPaymentProcessorArgs.CreditCardType		= mArgs.CreditCardType;
		
		oPaymentProcessor 		= new QProcessIPPPayment(oPaymentProcessorArgs);	// FIRE!!!!!!!
		oPaymentProcessorRet 	= oPaymentProcessor.ProcMain();
		
		// move the payment response to our response
		mRet.IPPResponse 			= oPaymentProcessorRet;
		mRet.InError 				= oPaymentProcessorRet.InError;
		mRet.ErrorList 				= oPaymentProcessorRet.ErrorList;
		mRet.IsPaymentSuccessful 	= oPaymentProcessorRet.ResponseCode == QConstants.STR_IPPAYMENTS_TRANSACTION_CODE_APPROVED;
	}

	/***************************************************************************************
		Test Methods
	***************************************************************************************/
	public static testMethod void testPaymentMethodSinglePaymentCallOutOnly()
	{
		QProcessPayment 	oProcessor;
		QProcessPaymentArgs oArgs = new QProcessPaymentArgs();
		QProcessPaymentRet 	oRet;
		
		oArgs.AllowCallout 		= false;
		oArgs.PaymentProcess 	= PaymentProcess.SINGLE_PAYMENT_CALLOUT_ONLY;
		oArgs.IPPClientId		= '100000';
		oArgs.IPPTransactionRef	= 200000;
		
		oProcessor = new QProcessPayment(oArgs);
		
		oRet = oProcessor.ProcMain();
		
		System.AssertEquals(oRet.IPPResponse.ResponseCode, 			'0');
		System.AssertEquals(oRet.IPPResponse.Timestamp, 			'2009-02-12 19:28:05');
		System.AssertEquals(oRet.IPPResponse.Receipt, 				'100001337');
		System.AssertEquals(oRet.IPPResponse.SettlementDateText, 	'12-feb-2009');
		System.AssertEquals(oRet.IPPResponse.SettlementDate, 		date.newInstance(2009, 2, 12));
		System.AssertEquals(oRet.IPPResponse.DeclinedCode, 			'');
		System.AssertEquals(oRet.IPPResponse.DeclinedMessage, 		'');
	}
	
	public static testMethod void testPaymentMethodKitetimeRecordSinglePaymentAndRefund()
	{
		// create our Objects
		Account theAccount = new Account();
		theAccount.Name = 'Test Account';
		
		insert theAccount;
		
		Contact theContact = new Contact();
		theContact.AccountId = theAccount.Id;
		theContact.FirstName = 'FirstName';
		theContact.LastName = 'LastName';
		
		insert theContact;
		
		// first we need a vaild IPP response object so lets get a test one
		QProcessPayment 	oIPPTokenProcessor;
		QProcessPaymentArgs oIPPTokenArgs = new QProcessPaymentArgs();
		QProcessPaymentRet 	oIPPTokenRet;
		
		oIPPTokenArgs.AllowCallout 		= false;
		oIPPTokenArgs.PaymentProcess 	= PaymentProcess.SINGLE_PAYMENT_CALLOUT_ONLY;
		oIPPTokenArgs.IPPClientId		= '100000';
		oIPPTokenArgs.IPPTransactionRef	= 200000;
		
		oIPPTokenProcessor = new QProcessPayment(oIPPTokenArgs);
		
		oIPPTokenRet = oIPPTokenProcessor.ProcMain();
		
		// now we can fire the record information
		QProcessPayment 	oProcessor;
		QProcessPaymentArgs oArgs = new QProcessPaymentArgs();
		QProcessPaymentRet 	oRet;
		
		oArgs.AllowCallout 			= false;
		oArgs.PaymentProcess 		= PaymentProcess.CORPORATE_RECORD_SINGLE_PAYMENT;
		oArgs.IPPResponse			= oIPPTokenRet.IPPResponse;
		oArgs.AccountId 			= theAccount.Id;
		oArgs.ContactId 			= theContact.Id;
		oArgs.AccountName			= theAccount.Name;
		oArgs.Amount 				= 100;
		oArgs.CreditCardNumber		= '4111111111111111';
		oArgs.CreditCardName		= 'Testing';
		oArgs.CreditCardExpiryYear 	= 2020;
		oArgs.CreditCardExpiryMonth = 10;
		oArgs.CreditCardCVN			= '123';
		oArgs.CreditCardType		= '';
		oArgs.IPPClientId			= '100000';
		oArgs.IPPTransactionRef		= 200000;
		
		oProcessor = new QProcessPayment(oArgs);
		
		oRet = oProcessor.ProcMain();
		
		Opportunity[] 	arrOpportunity 	= [select id, amount from Opportunity where AccountId = :theAccount.Id];
		//Income__c[] 	arrIncome 		= [select id, Opportunity__c from Income__c/* where Contact__c = :theContact.Id*/];
		Income__c[] 	arrIncome 		= [select id, Opportunity__c from Income__c where Opportunity__r.AccountId = :theAccount.Id];

		System.AssertEquals(arrOpportunity.size(), 1);		
		System.AssertEquals(arrIncome.size(), 1);
		System.AssertEquals(arrIncome[0].Opportunity__c, arrOpportunity[0].Id);
	}
}