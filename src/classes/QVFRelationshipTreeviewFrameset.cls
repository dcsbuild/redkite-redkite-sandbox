public class QVFRelationshipTreeviewFrameset 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes
	public class QVFScreenParameterException extends Exception{}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	public string Origin	{get; set;}
	public string ID		{get; set;}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Constructor
	public QVFRelationshipTreeviewFrameset()
	{
		// lets grab the screen parameters
		if(null == (Id = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new QVFScreenParameterException('Id Parameter not found');	
		}
		
		if(null == (Origin = ApexPages.currentPage().getParameters().get('origin')))
		{
			throw new QVFScreenParameterException('Origin Parameter not found');	
		}
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	
	public static testmethod void TestQVFRelationshipTreeviewFrameset()
	{
		/*
			This looks suspiciously like a class that should have been deleted..
		*/
		PageReference pageRef = new PageReference('/apex/RelationshipTimeline');
		Test.setCurrentPage(pageRef);

		try {QVFRelationshipTreeviewFrameset rtf = new QVFRelationshipTreeviewFrameset();}
		catch(Exception e){}
		
		ApexPages.currentPage().getParameters().put('id', '1234');
		
		try {QVFRelationshipTreeviewFrameset rtf = new QVFRelationshipTreeviewFrameset();}
		catch(Exception e){}
		
		ApexPages.currentPage().getParameters().put('origin', 'ofSpecies');
		
		try {QVFRelationshipTreeviewFrameset rtf = new QVFRelationshipTreeviewFrameset();}
		catch(Exception e){}
	}
}