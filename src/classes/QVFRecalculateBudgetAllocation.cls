public class QVFRecalculateBudgetAllocation 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class RecalculateBudgetAllocationScreenParameterException extends Exception{}
	
	////////////////////////////////////////////
	// Singular
	string m_strId;
	string m_strOrigin;
	
	////////////////////////////////////////////
	// Constructor
	public QVFRecalculateBudgetAllocation()
	{
		// lets grab the screen parameters
		if(null == (m_strId = ApexPages.currentPage().getParameters().get('Id')))
		{
			throw new RecalculateBudgetAllocationScreenParameterException('Id Parameter not found');	
		}
		// lets grab the screen parameters
		if(null == (m_strOrigin = ApexPages.currentPage().getParameters().get('Origin')))
		{
			throw new RecalculateBudgetAllocationScreenParameterException('Origin Parameter not found');	
		}
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	public pageReference InitRecalculateBudgetAllocation()
	{
		boolean 		bError = false;
		PageReference 	pageRedirect;
		
		Id				idContactRecord;
		Datetime 		tmEffectiveDate;
		
		try
		{
			if(m_strOrigin == 'Account')
			{
				Account sAccount = [select id, IsPersonAccount, PersonContactId, FA_Commencement__pc from Account where id = :m_strId];
				
				tmEffectiveDate = Datetime.newInstance(
														sAccount.FA_Commencement__pc.Year(),
														sAccount.FA_Commencement__pc.Month(),
														sAccount.FA_Commencement__pc.Day(),
														0, 0, 0);
				idContactRecord = sAccount.PersonContactId;
			}
			else if(m_strOrigin == 'Contact')
			{
				Contact sContact = [select id, FA_Commencement__c from Contact where id = :m_strId];
				
				tmEffectiveDate = Datetime.newInstance(
														sContact.FA_Commencement__c.Year(),
														sContact.FA_Commencement__c.Month(),
														sContact.FA_Commencement__c.Day(),
														0, 0, 0);
				idContactRecord = sContact.Id;
			}
			else
			{
				throw new RecalculateBudgetAllocationScreenParameterException('Invalid Origin Type');
			}
															
			QBudgetAllocationProcessing.SetPrior12MonthsFinancialAssistanceForDiagnosedPerson(idContactRecord, tmEffectiveDate);
		}
		catch(Exception e)
		{
			ApexPages.addMessages(e);
			bError = true;
		}
		
		if(!bError)
		{
			pageRedirect = new PageReference('/' + m_strId);
			pageRedirect.setRedirect(true);
		}
		
		return pageRedirect;
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void testRecalculateParamException()
	{
		PageReference pageRef = new PageReference('/apex/Recalculate_Budget_Allocation');
		Test.setCurrentPage(pageRef);
		
		QVFRecalculateBudgetAllocation controller;
		
		boolean bExceptionThrown = false;
		
		// break it due to no params
		try
		{
			controller = new QVFRecalculateBudgetAllocation();
		}
		catch(Exception e)
		{
			bExceptionThrown = true;
		}
		
		System.AssertEquals(true, bExceptionThrown);
	}
	
	public static testmethod void testAccountOrigin()
	{
		Account thisAccount = new Account();
		
		thisAccount.LastName 				= 'Test Account';
		thisAccount.FA_Commencement__pc		= System.Today();
		
		insert thisAccount;
		
		PageReference pageRef = new PageReference('/apex/Inventory_Management');
		Test.setCurrentPage(pageRef);
		
		QVFRecalculateBudgetAllocation controller;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', 		thisAccount.Id);
		ApexPages.currentPage().getParameters().put('origin', 	'Account');
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFRecalculateBudgetAllocation();
		
		controller.InitRecalculateBudgetAllocation();
	}
}