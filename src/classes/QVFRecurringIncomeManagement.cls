public class QVFRecurringIncomeManagement 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class RecurringIncomeManagementScreenParameterException 	extends Exception{}
	
	
	////////////////////////////////////////////
	// Singular
	public QCampaignProcessingParam.QCampaignProcessingArg m_objArg{get; set;}
	public QCampaignProcessingParam.QCampaignProcessingRet m_objRet{get; set;}
	
	string m_strId;
	
	////////////////////////////////////////////
	// Collections
	
	
	////////////////////////////////////////////
	// Constructor
	public QVFRecurringIncomeManagement()
	{
		// lets grab the screen parameters
		if(null == (m_strId = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new RecurringIncomeManagementScreenParameterException('Id Parameter not found');	
		}
		
		// our param objects
		m_objArg = new QCampaignProcessingParam.QCampaignProcessingArg(QCampaignProcessingParam.ActionCode.PROCESS_ALL_OPPORTUNITIES_BY_CAMPAIGN, m_strId);
		m_objRet = new QCampaignProcessingParam.QCampaignProcessingRet();
		
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	public void InitRecurringIncomeManagement()
	{			
		// our processor object
		QCampaignProcessing objCampaignProcessor = new QCampaignProcessing(m_objArg, m_objRet);
		
		objCampaignProcessor.ProcMain(); 	// GO!
	}
	
	public pageReference ReturnToCampaign()
	{
		PageReference pageRedirect = new PageReference('/' + m_strId);
		pageRedirect.setRedirect(true);
		return pageRedirect;
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void testEnd2End()
	{
		PageReference pageRef = new PageReference('/apex/Recurring_Income_Management');
		Test.setCurrentPage(pageRef);
		
		QVFRecurringIncomeManagement controller;
		
		boolean bExceptionThrown = false;
		
		// break it due to no params
		try
		{
			controller = new QVFRecurringIncomeManagement();
		}
		catch(Exception e)
		{
			bExceptionThrown = true;
		}
		
		System.AssertEquals(true, bExceptionThrown);
		
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', sCampaign.Id);
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFRecurringIncomeManagement();
		
		// run the load details logic
		controller.InitRecurringIncomeManagement();
		
		string pageReturn = controller.ReturnToCampaign().getURL();
		
		System.assertEquals('/'+sCampaign.Id, pageReturn);
		
	}
}