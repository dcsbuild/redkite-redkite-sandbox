public class QCampaignProcessing 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/

	////////////////////////////////////////////
	// Exception Classes
	public class CampaignProcessingParamException	extends Exception{}
	//public class EventIncomeManagementDataValidationException 	extends Exception{}
	
	////////////////////////////////////////////
	// Singular
	QCampaignProcessingParam.QCampaignProcessingArg m_Arg;
	QCampaignProcessingParam.QCampaignProcessingRet	m_Ret; 
	
	static final integer PARENT_TYPE_PERSON_ACCOUNT = 1;
	static final integer PARENT_TYPE_ACCOUNT = 2;
	
	
	////////////////////////////////////////////
	// Collections
	
	
	////////////////////////////////////////////
	// Constructor
	public QCampaignProcessing(QCampaignProcessingParam.QCampaignProcessingArg thisArgObj, QCampaignProcessingParam.QCampaignProcessingRet thisRetObj)
	{
		m_Arg 	= thisArgObj;
		m_Ret 	= thisRetObj;
	}
	
	/***********************************************************************************************************
		The one and only Entry Point
	***********************************************************************************************************/
	public void ProcMain()
	{
		// Entry Point for sending a Campaign to the processor
		if(m_Arg.ActionCode == QCampaignProcessingParam.ActionCode.PROCESS_ALL_OPPORTUNITIES_BY_CAMPAIGN)
		{
			this.ProcessOpportunityPaymentsByCampaignId();
		}
		// Entry point for sending an Opportunity List to the Processor
		else if(m_Arg.ActionCode == QCampaignProcessingParam.ActionCode.PROCESS_OPPORTUNITY_PAYMENTS)
		{
			this.ProcessPaymentsForOpportunityList();
		}
		else
		{
			throw new CampaignProcessingParamException('Action Code Not Supplied');
		}
	}

	/***********************************************************************************************************
		Future Handler
	***********************************************************************************************************/
	@future
	private static void ProcessOpportunityPaymentsByOpportunityIDList(list <id> liOpportunity, integer iParentType)
	{
		// our param objects
		QCampaignProcessingParam.QCampaignProcessingArg objArg;
		
		if(iParentType == PARENT_TYPE_PERSON_ACCOUNT)
		{
			objArg	= new QCampaignProcessingParam.QCampaignProcessingArg(
														QCampaignProcessingParam.ActionCode.PROCESS_OPPORTUNITY_PAYMENTS, 
														QCampaignProcessingParam.ParentType.PERSON_ACCOUNT, 
														liOpportunity);
		}
		else if(iParentType == PARENT_TYPE_ACCOUNT)
		{
			objArg	= new QCampaignProcessingParam.QCampaignProcessingArg(
														QCampaignProcessingParam.ActionCode.PROCESS_OPPORTUNITY_PAYMENTS, 
														QCampaignProcessingParam.ParentType.ACCOUNT, 
														liOpportunity);
		}
		
		QCampaignProcessingParam.QCampaignProcessingRet objRet 	= new QCampaignProcessingParam.QCampaignProcessingRet();
		
		// our processor object
		QCampaignProcessing objCampaignProcessor = new QCampaignProcessing(objArg, objRet);
		
		objCampaignProcessor.ProcMain(); 	// GO!
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	
	////////////////////////////////////////////////////////////////////////////////
	// Campaign ID processing
	//	This method will split out the person accounts and normal accounts and send
	//	each list of 1000 entities to the future method
	private void ProcessOpportunityPaymentsByCampaignId()
	{
		Campaign sCampaign = QUtils.LoadCampaignById(m_Arg.Target);
		
		list<id> 	liOpportunityAccount 			= new list<id>();
		list<id> 	liOpportunityPersonAccount 		= new list<id>();
		list<id> 	liOpportunityAccountNoPrimary 	= new list<id>();
		
		// lets load our Opportunities
		// we will split into buckets of person accounts, normal accounts and normal accounts without a primary contact
		for(Opportunity[] arrOpportunity : [
												select 	id,
														Name,
														Account.IsPersonAccount,
														Contact__c
												from 	Opportunity 
												where 	CampaignId = :sCampaign.Id
												and		StageName = 'Club Red Ongoing'
												and		Next_Recurring_Payment_Process_Date__c <= :System.Today()
											])
		{
			for(Opportunity sOpportunity : arrOpportunity)
			{
				System.Debug('Processing Opportunity - ' + sOpportunity.Id + ' - ' + sOpportunity);
				m_Ret.TotalOpportunities++;
				
				// we have a person account
				if(sOpportunity.Account.IsPersonAccount)
				{
					m_Ret.TotalPersonAccountOpportunities++;
					
					// gov protection
					if(liOpportunityPersonAccount.size() == 200)
					{
						InitiateFutureMethodForOpportunityList(liOpportunityPersonAccount, PARENT_TYPE_PERSON_ACCOUNT);
						liOpportunityPersonAccount.clear();
					}
					
					liOpportunityPersonAccount.add(sOpportunity.Id);
				}
				// normal account
				else
				{
					m_Ret.TotalAccountOpportunities++;

					if(liOpportunityAccount.size() == 200)
					{
						InitiateFutureMethodForOpportunityList(liOpportunityAccount, PARENT_TYPE_ACCOUNT);
						liOpportunityAccount.clear();
					}
					
					liOpportunityAccount.add(sOpportunity.Id);
				}
			}
		}
		
		if(liOpportunityPersonAccount.size() > 0)
		{
			InitiateFutureMethodForOpportunityList(liOpportunityPersonAccount, PARENT_TYPE_PERSON_ACCOUNT);
		}
			
		if(liOpportunityAccount.size() > 0)
		{
			InitiateFutureMethodForOpportunityList(liOpportunityAccount, PARENT_TYPE_ACCOUNT);
		}
		
		m_Ret.Message = 'Well we did something =O)';
		
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// Opportunitites with Person Accounts
	private void InitiateFutureMethodForOpportunityList(list<id> liProcessList, integer iParentType)
	{
		if(m_Ret.TotalThreadsFired < 10)
		{
			m_Ret.TotalThreadsFired++;
			System.Debug('Firing a Future!');
			ProcessOpportunityPaymentsByOpportunityIDList(liProcessList, iParentType);
			System.Debug('Future Fired!');
		}
		else
		{
			m_Ret.ReprocessRequired = true;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////
	// Opportunitites with Person Accounts
	private void ProcessPaymentsForOpportunityList()
	{
		Database.SaveResult[] dbSaveResult;
		
		map<id, Opportunity> 	mapOpportunity 				= new map<id, Opportunity>();
		map<id, id>				mapAccountId2ContactId		= new map<id, id>();
		
		list<Income__c>			liIncomeInsert				= new list<Income__c>();
		list<Opportunity>		liOpportunityUpdate			= new list<Opportunity>();
		
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Income__c.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
				
		// first we need to load the opportunities and contacts if we can get them
		for(Opportunity[] arrOpportunity : [
											select 	id,
													Next_Recurring_Payment_Process_Date__c,
													Last_Recurring_Payment_Process_Date__c,
													Recurring_Payment_Frequency__c,
													Personal_Donation__c,
													Recurring_Donation_Amount__c,
													AccountId,
													Account.IsPersonAccount,
													Contact__c,
													//(select ContactId from OpportunityContactRoles where IsPrimary = true),
													Amount,
													Credit_Card_Type__c,
													Card_Name__c,
													Credit_Card__c,
													Card_Expiry_Month__c,
													Credit_Card_Year__c
											from 	Opportunity 
											where 	Id in :m_Arg.TargetList
											and		IsRecurringPayment__c = true
										])
		{
			for(Opportunity sOpportunity : arrOpportunity)
			{
				mapOpportunity.put(sOpportunity.Id, sOpportunity);
				
				if(		!sOpportunity.Account.IsPersonAccount
					//&& 	sOpportunity.OpportunityContactRoles.size() == 1
					&& 	sOpportunity.Contact__c != null
					&&	m_Arg.ParentType == QCampaignProcessingParam.ParentType.ACCOUNT)
				{
					//mapAccountId2ContactId.put(sOpportunity.AccountId, sOpportunity.OpportunityContactRoles[0].ContactId);
					mapAccountId2ContactId.put(sOpportunity.AccountId, sOpportunity.Contact__c);
				}
				else
				{
					mapAccountId2ContactId.put(sOpportunity.AccountId, null);
				}
			}
		}
		
		// now if were dealing with person accounts we need to load the contact id
		if(m_Arg.ParentType == QCampaignProcessingParam.ParentType.PERSON_ACCOUNT)
		{
			for(Contact[] arrContact : [
											select 	id,
													AccountId
											from 	Contact 
											where 	AccountId in :mapAccountId2ContactId.keySet()
										])
			{
				for(Contact sContact : arrContact)
				{
					mapAccountId2ContactId.put(sContact.AccountId, sContact.Id);
				}
			}
		}
		
		// lets create all of our Income Objects
		for(Opportunity sOpportunity : mapOpportunity.values())
		{
			m_Ret.TotalIncomeCreated++;
			
			Income__c sIncome = new Income__c();
			
			// yes this can give a null pointer exception if someone changes the RT name but that would break everything anyway =O)
			sIncome.RecordTypeId = mapRecordTypeName.get('Club Red').getRecordTypeId();
			
			sIncome.Opportunity__c 			= sOpportunity.Id;
			//sIncome.Contact__c 				= mapAccountId2ContactId.get(sOpportunity.AccountId);
			sIncome.AccountId__c 			= sOpportunity.AccountId;
			sIncome.Status__c				= 'Send to Finance';
			sIncome.No_of_Payment_Types__c 	= 'One Payment Group';
			sIncome.Payment_Type_A__c 		= 'Credit Card';
			
			sIncome.Personal_Donation__c 	= sOpportunity.Personal_Donation__c;
			
			//if(sOpportunity.Amount != null)
			if(sOpportunity.Recurring_Donation_Amount__c != null)
				sIncome.Amount_A__c 		= sOpportunity.Recurring_Donation_Amount__c;
			
			if(sIncome.Payment_Type_A__c == 'Credit Card')
			{
				sIncome.Credit_Card_A__c 		= sOpportunity.Credit_Card_Type__c;
				sIncome.Card_Name_A__c 			= sOpportunity.Card_Name__c;
				sIncome.Card_Expiry_Month_A__c 	= sOpportunity.Card_Expiry_Month__c;
				sIncome.Card_Expiry_Year_A__c 	= sOpportunity.Credit_Card_Year__c;
				sIncome.Credit_Card_No_A__c 	= sOpportunity.Credit_Card__c;
				
			}
						
			if(liIncomeInsert.size() == 100)
			{
				dbSaveResult = QUtils.InsertIncomeListToDB(liIncomeInsert);
				CheckIncomeSaveResults(dbSaveResult, liIncomeInsert);
			}
			
			liIncomeInsert.add(sIncome);
		}
		
		// final Insert
		dbSaveResult = QUtils.InsertIncomeListToDB(liIncomeInsert);
		CheckIncomeSaveResults(dbSaveResult, liIncomeInsert);
		
		// now we have entered the Incomes we can update the Opportunities
		for(Opportunity sOpportunity : mapOpportunity.values())
		{
			// move everything along
			sOpportunity.Last_Recurring_Payment_Process_Date__c = sOpportunity.Next_Recurring_Payment_Process_Date__c;
			
			if(sOpportunity.Recurring_Payment_Frequency__c == 'Fortnightly')
			{
				sOpportunity.Next_Recurring_Payment_Process_Date__c = sOpportunity.Next_Recurring_Payment_Process_Date__c.addDays(14);
			}
			else if(sOpportunity.Recurring_Payment_Frequency__c == 'Monthly')
			{
				sOpportunity.Next_Recurring_Payment_Process_Date__c = sOpportunity.Next_Recurring_Payment_Process_Date__c.addMonths(1);
			}
			else
			{
				// safty, move along a month incase someone puts an unknown picklist value in there
				sOpportunity.Next_Recurring_Payment_Process_Date__c = sOpportunity.Next_Recurring_Payment_Process_Date__c.addMonths(1);
			} 
			
			if(liOpportunityUpdate.size() == 100)
			{
				update liOpportunityUpdate;
				liOpportunityUpdate.clear();	
			}
			
			liOpportunityUpdate.add(sOpportunity);
		}
		
		// final update
		update liOpportunityUpdate;
		liOpportunityUpdate.clear();
	}
	
	
	private void CheckIncomeSaveResults(Database.SaveResult[] arrResult, list<Income__c> liIncome)
	{
		for(integer i = 0; i < arrResult.size(); i++)
		{
			// we had an error on the Income Insert
			if(!arrResult[i].isSuccess())
			{
				System.Debug('Error Creating Income Record for Opportunity:' + liIncome[i].Opportunity__c);
				
				Database.Error[] arrErrors = arrResult[i].getErrors();
				
				for(integer j = 0; j < arrErrors.size(); j++)
				{
					System.Debug('Error :' + j + ' - ' + arrErrors[j]);
				}
			}
		}
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/	
	public static testmethod void testPersonAccountEnd2End()
	{
		
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Opportunity.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
		
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribeAccount 			= Account.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeNameAccount 		= schemaDescribeAccount.getRecordTypeInfosByName();
		
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;

		list<Account> 		liPersonAccount = new list<Account>();
		list<Opportunity> 	liOpportunity 	= new list<Opportunity>();
		
		list<Id>			liId			= new list<Id>();
		
		for(integer i = 0; i < 10; i++)
		{
			Account thisAccount = new Account();
			thisAccount.LastName = 'Test Person Account ' + i;
			thisAccount.RecordTypeId = mapRecordTypeNameAccount.get('Individual').getRecordTypeId();
			liPersonAccount.add(thisAccount);
		}
		
		
		insert liPersonAccount;
		
		liPersonAccount = [select id, LastName, PersonContactId from Account where id in :liPersonAccount];
		System.debug(liPersonAccount);
		
		for(integer i = 0; i < liPersonAccount.size(); i++)
		{
			Opportunity thisOpportunity = new Opportunity();
			
			thisOpportunity.AccountId 	= liPersonAccount[i].id;
			thisOpportunity.Name 		= liPersonAccount[i].LastName;
			thisOpportunity.Amount 		= 0;
			thisOpportunity.Contact__c	= liPersonAccount[i].PersonContactId;
			thisOpportunity.StageName 	= 'Club Red Ongoing';
			
			thisOpportunity.RecordTypeId = mapRecordTypeName.get('ClubRed').getRecordTypeId();
			
			thisOpportunity.Recurring_Payment_Frequency__c 	= 'Monthly';
			thisOpportunity.Personal_Donation__c			= true;
			thisOpportunity.Recurring_Donation_Amount__c 	= 1000;
			thisOpportunity.Credit_Card_Type__c 			= 'Visa';
			thisOpportunity.Card_Name__c 					= 'Testing';
			thisOpportunity.Credit_Card__c 					= '4242424242424242';
			thisOpportunity.Card_Expiry_Month__c 			= 10;
			thisOpportunity.Credit_Card_Year__c 			= 99;
			
			thisOpportunity.Next_Recurring_Payment_Process_Date__c = System.Today();
			
			// mod by 2
			if((i & 1) == 0)
			{			
				thisOpportunity.Recurring_Payment_Frequency__c 			= 'Monthly';
			}
			else
			{
				thisOpportunity.Recurring_Payment_Frequency__c 			= 'Fortnightly';
			}
			thisOpportunity.IsRecurringPayment__c					= true;
			
			thisOpportunity.CloseDate 	= System.Today().addYears(5);
			
			thisOpportunity.CampaignId = sCampaign.Id;
			
			liOpportunity.add(thisOpportunity);
		}
		
		insert liOpportunity;
		
		for(integer i = 0; i < liOpportunity.size(); i++)
		{
			liOpportunity[i].Next_Recurring_Payment_Process_Date__c = System.Today();
			liId.add(liOpportunity[i].Id);
		}
		
		update liOpportunity;
		
		// Test method will not execute the Future Method
		QCampaignProcessingParam.QCampaignProcessingArg objArg	= new QCampaignProcessingParam.QCampaignProcessingArg(QCampaignProcessingParam.ActionCode.PROCESS_ALL_OPPORTUNITIES_BY_CAMPAIGN, sCampaign.Id);
		QCampaignProcessingParam.QCampaignProcessingRet objRet 	= new QCampaignProcessingParam.QCampaignProcessingRet();
				
		// our processor object
		QCampaignProcessing objCampaignProcessor = new QCampaignProcessing(objArg, objRet);
		
		objCampaignProcessor.ProcMain(); 	// GO!
		
		System.AssertEquals(10, objRet.TotalOpportunities);
		
		System.AssertEquals(10, objRet.TotalPersonAccountOpportunities);
		
		System.AssertEquals(1, objRet.TotalThreadsFired);
		
		System.Debug(objRet);
		
		// therefore.....
		
		objArg	= new QCampaignProcessingParam.QCampaignProcessingArg(
														QCampaignProcessingParam.ActionCode.PROCESS_OPPORTUNITY_PAYMENTS, 
														QCampaignProcessingParam.ParentType.PERSON_ACCOUNT, 
														liId);
		objRet 	= new QCampaignProcessingParam.QCampaignProcessingRet();
				
		// our processor object
		objCampaignProcessor = new QCampaignProcessing(objArg, objRet);
		
		objCampaignProcessor.ProcMain(); 	// GO!
		
		System.Debug(objRet);
		
		System.AssertEquals(10, objRet.TotalIncomeCreated);

	}
	
	public static testmethod void testAccountEnd2End()
	{
		
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Opportunity.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
		
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		list<Account> 					liAccount 					= new list<Account>();
		list<Contact> 					liContact 					= new list<Contact>();
		list<Opportunity> 				liOpportunity			 	= new list<Opportunity>();
		
		list<Id>			liId			= new list<Id>();
		
		for(integer i = 0; i < 10; i++)
		{
			Account thisAccount = new Account();
			thisAccount.Name = 'Test Account ' + i;
			liAccount.add(thisAccount);
		}
		
		insert liAccount;
		
		for(integer i = 0; i < 10; i++)
		{
			Contact thisContact = new Contact();
			thisContact.AccountId = liAccount[i].id;
			thisContact.FirstName = 'Test' + i;
			thisContact.LastName = '' + i;
			liContact.add(thisContact);
		}
		
		insert liContact;
		
		for(integer i = 0; i < liAccount.size(); i++)
		{
			Opportunity thisOpportunity = new Opportunity();
			
			thisOpportunity.AccountId 	= liAccount[i].id;
			thisOpportunity.Name 		= liAccount[i].Name;
			thisOpportunity.Contact__c 	= liContact[i].id;
			thisOpportunity.Amount 		= 0;
			thisOpportunity.StageName 	= 'Club Red Ongoing';
			
			thisOpportunity.RecordTypeId = mapRecordTypeName.get('ClubRed').getRecordTypeId();
			
			thisOpportunity.Recurring_Payment_Frequency__c 	= 'Monthly';
			thisOpportunity.Personal_Donation__c			= true;
			thisOpportunity.Recurring_Donation_Amount__c 	= 1000;
			thisOpportunity.Credit_Card_Type__c 			= 'Visa';
			thisOpportunity.Card_Name__c 					= 'Testing';
			thisOpportunity.Credit_Card__c 					= '4242424242424242';
			thisOpportunity.Card_Expiry_Month__c 			= 10;
			thisOpportunity.Credit_Card_Year__c 			= 99;
			
			thisOpportunity.Next_Recurring_Payment_Process_Date__c = System.Today();
			
			// mod by 2
			if((i & 1) == 0)
			{			
				thisOpportunity.Recurring_Payment_Frequency__c 			= 'Monthly';
			}
			else
			{
				thisOpportunity.Recurring_Payment_Frequency__c 			= 'Fortnightly';
			}
			thisOpportunity.IsRecurringPayment__c					= true;
			
			thisOpportunity.CloseDate 	= System.Today().addYears(5);
			
			thisOpportunity.CampaignId = sCampaign.Id;
			
			liOpportunity.add(thisOpportunity);
		}
		
		insert liOpportunity;
		
		for(integer i = 0; i < liOpportunity.size(); i++)
		{
			liOpportunity[i].Next_Recurring_Payment_Process_Date__c = System.Today();
			liId.add(liOpportunity[i].Id);
		}
		
		update liOpportunity;	// this is to stop that stupid workflow rule screwing with me!
		
		// Test method will not execute the Future Method
		QCampaignProcessingParam.QCampaignProcessingArg objArg	= new QCampaignProcessingParam.QCampaignProcessingArg(QCampaignProcessingParam.ActionCode.PROCESS_ALL_OPPORTUNITIES_BY_CAMPAIGN, sCampaign.Id);
		QCampaignProcessingParam.QCampaignProcessingRet objRet 	= new QCampaignProcessingParam.QCampaignProcessingRet();
				
		// our processor object
		QCampaignProcessing objCampaignProcessor = new QCampaignProcessing(objArg, objRet);
		
		objCampaignProcessor.ProcMain(); 	// GO!
		
		System.AssertEquals(10, objRet.TotalOpportunities);
		
		System.AssertEquals(10, objRet.TotalAccountOpportunities);
		
		System.AssertEquals(1, objRet.TotalThreadsFired);
		
		// therefore.....
		
		objArg	= new QCampaignProcessingParam.QCampaignProcessingArg(
														QCampaignProcessingParam.ActionCode.PROCESS_OPPORTUNITY_PAYMENTS, 
														QCampaignProcessingParam.ParentType.ACCOUNT, 
														liId);
		objRet 	= new QCampaignProcessingParam.QCampaignProcessingRet();
				
		// our processor object
		objCampaignProcessor = new QCampaignProcessing(objArg, objRet);
		
		objCampaignProcessor.ProcMain(); 	// GO!
		
		System.Debug(objRet);
		
		System.AssertEquals(10, objRet.TotalIncomeCreated);

	}
}