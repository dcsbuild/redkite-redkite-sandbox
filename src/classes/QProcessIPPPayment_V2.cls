public with sharing class QProcessIPPPayment_V2 
{
	/***************************************************************************************
		Exception Classes
	***************************************************************************************/
	public class QPaymentProcessingException extends Exception{}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information for Processing a Credit Card Payment
	public class QCreditCard
	{
		public id		AccountId				{get; set;}
		public id		OpportunityId			{get; set;}
		public string 	CreditCardNumber		{get; set;}
		public string 	CreditCardName			{get; set;}
		public string 	CreditCardExpiryYear	{get; set;}
		public string 	CreditCardExpiryMonth	{get; set;}
		public string 	CreditCardCVN			{get; set;}
		public string 	Amount					{get; set;}
		public string	CreditCardType			{get; set;}
		public string 	CustomerNumber			{get; set;}
		
		public QCreditCard()
		{
			this.CustomerNumber				= '';
			this.CreditCardNumber 			= '';
			this.CreditCardName 			= '';
			this.CreditCardExpiryYear 		= '';
			this.CreditCardExpiryMonth 		= '';
			this.CreditCardCVN 				= '';
			this.Amount 					= '';
		}
	}
	
	public class QCreditCardReturn
	{
		public string	ResponseCode	{get; private set;}
		public string	Timestamp		{get; private set;}
		public string 	Receipt			{get; private set;}
		public string 	SettlementDate	{get; private set;}
		public string 	DeclinedCode	{get; private set;}
		public string 	DeclinedMessage	{get; private set;}
		public string 	Request			{get; private set;}
		public string 	Response		{get; private set;}
		
		public QCreditCardReturn()
		{
			this.ResponseCode 		= '';
			this.Timestamp 			= '';
			this.Receipt 			= '';
			this.SettlementDate 	= '';
			this.DeclinedCode 		= '';
			this.DeclinedMessage 	= '';
			this.Request			= '';
			this.Response			= '';
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information for Processing a Refund
	public class QCreditCardRefund
	{
		public string	Receipt	{get; set;}
		public string	Amount	{get; set;}
		
		public QCreditCardRefund()
		{
			this.Receipt 	= '';
			this.Amount 	= '';
		}
	}
	
	public class QCreditCardRefundReturn
	{
		public string	ResponseCode	{get; private set;}
		public string	Timestamp		{get; private set;}
		public string 	Receipt			{get; private set;}
		public string 	SettlementDate	{get; private set;}
		public string 	DeclinedCode	{get; private set;}
		public string 	DeclinedMessage	{get; private set;}
		public string 	Response		{get; private set;}
		
		public QCreditCardRefundReturn()
		{
			this.ResponseCode 		= '';
			this.Timestamp 			= '';
			this.Receipt 			= '';
			this.SettlementDate 	= '';
			this.DeclinedCode 		= '';
			this.DeclinedMessage 	= '';
			this.Response			= '';
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information for Processing a Register
	public class QCreditCardWithRegister
	{
		public string 	CustomerReference		{get; set;}
		public string 	CustomerNumber			{get; set;}
		public string	Amount					{get; set;}
		public string 	CreditCardNumber		{get; set;}
		public string 	CreditCardHolderName	{get; set;}
		public string 	CreditCardExpiryYear	{get; set;}
		public string 	CreditCardExpiryMonth	{get; set;}
		public string 	CreditCardCVN			{get; set;}
		
		public QCreditCardWithRegister()
		{
			this.CustomerReference			= '';
			this.CustomerNumber				= '';
			this.Amount						= '';
			this.CreditCardNumber 			= '';
			this.CreditCardHolderName		= '';
			this.CreditCardExpiryYear 		= '';
			this.CreditCardExpiryMonth 		= '';
			this.CreditCardCVN 				= '';
		}
	}
	
	public class QCreditCardWithRegisterReturn
	{
		public string	ResponseCode	{get; private set;}
		public string 	Timestamp		{get; private set;}
		public string 	Receipt			{get; private set;}
		public string	SettlementDate	{get; private set;}
		public string	DeclinedCode	{get; private set;}
		public string	DeclinedMessage	{get; private set;}
		public string	CreditCardToken	{get; private set;}
		public string	TruncatedCard	{get; private set;}
		public string	ExpM			{get; private set;}
		public string	ExpY			{get; private set;}
		public string	CardType		{get; private set;}
		public string 	Response		{get; private set;}
		
		public QCreditCardWithRegisterReturn()
		{
			this.ResponseCode 		= '';
			this.Timestamp			= '';
			this.Receipt			= '';			
			this.SettlementDate		= '';
			this.DeclinedCode		= '';
			this.DeclinedMessage	= '';
			this.CreditCardToken	= '';
			this.TruncatedCard		= '';
			this.ExpM				= '';
			this.ExpY				= '';
			this.CardType			= '';
			this.Response			= '';
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information for Processing a Register
	public class QCreditCardUpdate
	{
		public string 	CustomerNumber			{get; set;}
		public string	CreditCardToken			{get; set;}
		public string 	CreditCardNumber		{get; set;}
		public string 	CreditCardName			{get; set;}
		public string 	CreditCardExpiryYear	{get; set;}
		public string 	CreditCardExpiryMonth	{get; set;}
		public string 	CreditCardCVN			{get; set;}
		
		public QCreditCardUpdate()
		{
			this.CustomerNumber				= '';
			this.CreditCardToken			= '';
			this.CreditCardNumber 			= '';
			this.CreditCardName 			= '';
			this.CreditCardExpiryYear 		= '';
			this.CreditCardExpiryMonth 		= '';
			this.CreditCardCVN 				= '';
		}
	}
	
	public class QCreditCardUpdateReturn
	{
		public string	ResponseCode	{get; private set;}
		public string	CreditCardToken	{get; private set;}
		public string	DeclinedMessage	{get; private set;}
		
		public QCreditCardUpdateReturn()
		{
			this.ResponseCode 		= '';
			this.CreditCardToken	= '';
			this.DeclinedMessage	= '';
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information for Processing a Payment with a Registered Card
	public class QCreditCardByReference
	{
		public string 	CustomerNumber			{get; set;}
		public string 	CustomerReference		{get; set;}
		public string 	Amount					{get; set;}
		public string 	CreditCardExpiryYear	{get; set;}
		public string 	CreditCardExpiryMonth	{get; set;}
		public string	CreditCardType			{get; set;}
		
		public QCreditCardByReference()
		{
			this.CustomerNumber 		= '';
			this.CustomerReference 		= '';
			this.Amount 				= '';
			this.CreditCardExpiryYear 	= '';
			this.CreditCardExpiryMonth 	= '';
			this.CreditCardType 		= '';
		}
	}
	
	public class QCreditCardByReferenceReturn
	{
		public string	ResponseCode	{get; private set;}
		public string	Timestamp		{get; private set;}
		public string 	Receipt			{get; private set;}
		public string 	SettlementDate	{get; private set;}
		public string 	DeclinedCode	{get; private set;}
		public string 	DeclinedMessage	{get; private set;}
		public string 	Request			{get; private set;}
		public string 	Response		{get; private set;}
		public string 	CardType		{get; private set;}
		
		public QCreditCardByReferenceReturn()
		{
			this.ResponseCode 		= '';
			this.Timestamp 			= '';
			this.Receipt 			= '';
			this.SettlementDate 	= '';
			this.DeclinedCode 		= '';
			this.DeclinedMessage 	= '';
			this.Request			= '';
			this.Response			= '';
			this.CardType			= '';
		}
	}
	
	
	
	/***************************************************************************************
		Members
	***************************************************************************************/
	
	/***************************************************************************************
		Constructor
	***************************************************************************************/
	
	/***************************************************************************************
		Payment Processing Entry Points
	***************************************************************************************/
	
	/***************************************************************************************
		Payment Processing Logic
	***************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	// Payment Processor!
	public static QCreditCardReturn CalloutIPPaymentsSubmitCreditCardPayment(QCreditCard thisCreditCardStub)
	{
		QCreditCardReturn 		thisReturnStub 	= new QCreditCardReturn();
		IPPaymentsDTS.dtsSoap 	IPSOAPMessage 	= new IPPaymentsDTS.dtsSoap(QCachePaymentGatewayMetadata.getDTSServiceLocation());
		
		IPSOAPMessage.timeout_x = 60000;
		
		thisReturnStub.Request = '<Transaction>'+
									'<CustNumber>' 	+ thisCreditCardStub.CustomerNumber	+ '</CustNumber>'+
									'<CustRef>'	 	+ thisCreditCardStub.OpportunityId 	+ '</CustRef>'+
									'<Amount>' 		+ thisCreditCardStub.Amount			+ '</Amount>'+
									'<TrnType>'		+ '1' 								+ '</TrnType>'+
									'<CreditCard Registered="False">'+
										'<CardNumber>'		+thisCreditCardStub.CreditCardNumber+'</CardNumber>'+
										'<ExpM>'			+thisCreditCardStub.CreditCardExpiryMonth+'</ExpM>'+
										'<ExpY>'			+thisCreditCardStub.CreditCardExpiryYear+'</ExpY>'+
										'<CVN>'				+thisCreditCardStub.CreditCardCVN+'</CVN>'+
										'<CardHolderName>'	+thisCreditCardStub.CreditCardName+'</CardHolderName>'+
									'</CreditCard>'+
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername() 	+ '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword()	+ '</Password>'+
									'</Security>'+
									'<UserDefined></UserDefined>'+
								'</Transaction>';
		
		System.Debug('XML Request Message - ' + thisReturnStub.Request);
		
		
		if(!System.Test.isRunningTest())
		{
			thisReturnStub.Response = IPSOAPMessage.SubmitSinglePayment(thisReturnStub.Request);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			thisReturnStub.Response = '<Response><ResponseCode>0</ResponseCode><Timestamp>2009-02-12 19:28:05</Timestamp><Receipt>100001337</Receipt><SettlementDate>01-apr-2010</SettlementDate><DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>';
		}
		
		System.Debug('XML Response Message - ' + thisReturnStub.Response);
		
		XMLDom objXMLDom = new XMLDom(thisReturnStub.Response);
		
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			if(element.nodeName == 'ResponseCode')
				thisReturnStub.ResponseCode = element.nodeValue;
			else if(element.nodeName == 'Timestamp')
				thisReturnStub.Timestamp = element.nodeValue;
			else if(element.nodeName == 'Receipt')
				thisReturnStub.Receipt = element.nodeValue;
			else if(element.nodeName == 'SettlementDate')
				thisReturnStub.SettlementDate = element.nodeValue;
			else if(element.nodeName == 'DeclinedCode')
				thisReturnStub.DeclinedCode = element.nodeValue;
			else if(element.nodeName == 'DeclinedMessage')
				thisReturnStub.DeclinedMessage = element.nodeValue;
		}
		
		return thisReturnStub;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static QCreditCardRefundReturn CalloutIPPaymentsSubmitRefundPayment(QCreditCardRefund thisRefundStub)
	{
		QCreditCardRefundReturn thisReturnStub 	= new QCreditCardRefundReturn();
		IPPaymentsDTS.dtsSoap 	IPSOAPMessage 	= new IPPaymentsDTS.dtsSoap(QCachePaymentGatewayMetadata.getDTSServiceLocation());
		
		IPSOAPMessage.timeout_x = 60000;
		
		string strXMLRequest = '<Refund>'+
									'<Receipt>' + thisRefundStub.Receipt	+ '</Receipt>'+
									'<Amount>'	+ thisRefundStub.Amount		+ '</Amount>'+
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername() + '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword() + '</Password>'+
									'</Security>'+
								'</Refund>';
		
		System.Debug('XML Request Message - ' + strXMLRequest);
		
		if(!System.Test.isRunningTest())
		{
			thisReturnStub.Response = IPSOAPMessage.SubmitSingleRefund(strXMLRequest);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			thisReturnStub.Response = '<Response><ResponseCode>0</ResponseCode><Timestamp>2009-02-12 19:28:05</Timestamp><Receipt>100001337</Receipt><SettlementDate>01-apr-2010</SettlementDate><DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>';
		}
		
		System.Debug('XML Response Message - ' + thisReturnStub.Response);
		
		XMLDom objXMLDom = new XMLDom(thisReturnStub.Response);
				
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			System.Debug(element);

			if(element.nodeName == 'ResponseCode')
				thisReturnStub.ResponseCode = element.nodeValue;
			else if(element.nodeName == 'Timestamp')
				thisReturnStub.Timestamp = element.nodeValue;
			else if(element.nodeName == 'Receipt')
				thisReturnStub.Receipt = element.nodeValue;
			else if(element.nodeName == 'SettlementDate')
				thisReturnStub.SettlementDate = element.nodeValue;
			else if(element.nodeName == 'DeclinedCode')
				thisReturnStub.DeclinedCode = element.nodeValue;
			else if(element.nodeName == 'DeclinedMessage')
				thisReturnStub.DeclinedMessage = element.nodeValue;
		}
		
		return thisReturnStub;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	// 
	public static QCreditCardWithRegisterReturn CalloutIPPaymentsSubmitCreditCardPaymentWithRegister(QCreditCardWithRegister thisRegisterStub)
	{
		QCreditCardWithRegisterReturn 	thisReturnStub 	= new QCreditCardWithRegisterReturn();
		IPPaymentsDTS.dtsSoap 			IPSOAPMessage 	= new IPPaymentsDTS.dtsSoap(QCachePaymentGatewayMetadata.getDTSServiceLocation());
		
		IPSOAPMessage.timeout_x = 60000;
		
		// stick a leading zero on here
		if(thisRegisterStub.CreditCardExpiryMonth.length() == 1)
		{
			thisRegisterStub.CreditCardExpiryMonth = '0' + thisRegisterStub.CreditCardExpiryMonth;
		}
		
		string strXMLRequest = '<Transaction>'+
									//'<CustNumber>' 	+ thisRegisterStub.CustomerNumber	+ '</CustNumber>'+
									'<CustRef>' + thisRegisterStub.CustomerReference + '</CustRef>' +
									'<Amount>' 	+ thisRegisterStub.Amount + '</Amount>' +
									'<TrnType>' + '1'	+	'</TrnType>' +
									
									'<CreditCard>' +
										'<TokeniseAlgorithmID>'	+	'2'	+	'</TokeniseAlgorithmID>' +
										'<CardNumber>' 			+ thisRegisterStub.CreditCardNumber 		+ '</CardNumber>' +
										'<ExpM>' 				+ thisRegisterStub.CreditCardExpiryMonth 	+ '</ExpM>' +
										'<ExpY>' 				+ thisRegisterStub.CreditCardExpiryYear	 	+ '</ExpY>' +
										'<CardHolderName>' 		+ thisRegisterStub.CreditCardHolderName		+ '</CardHolderName>' +					
									'</CreditCard>' +
									
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername() + '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword() + '</Password>'+
									'</Security>'+
									'<UserDefined></UserDefined>'+
								'</Transaction>';

		System.Debug('XML Request Message - ' + strXMLRequest);
		
		if(!System.Test.isRunningTest())
		{
			thisReturnStub.Response = IPSOAPMessage.SubmitSinglePayment(strXMLRequest);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			thisReturnStub.Response = '<Response><ResponseCode>0</ResponseCode><CreditCardToken>100001337</CreditCardToken></Response>';
		}
		
		System.Debug('XML Response Message - ' + thisReturnStub.Response);
		
		XMLDom objXMLDom = new XMLDom(thisReturnStub.Response);
				
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			System.Debug(element);
			
			if(element.nodeName == 'ResponseCode')
				thisReturnStub.ResponseCode = element.nodeValue;
			else if(element.nodeName == 'Timestamp')
				thisReturnStub.Timestamp = element.nodeValue;
			else if(element.nodeName == 'Receipt')
				thisReturnStub.Receipt = element.nodeValue;
			else if(element.nodeName == 'SettlementDate')
				thisReturnStub.SettlementDate = element.nodeValue;	
			else if(element.nodeName == 'DeclinedCode')
				thisReturnStub.DeclinedCode = element.nodeValue;
			else if(element.nodeName == 'DeclinedMessage')
				thisReturnStub.DeclinedMessage = element.nodeValue;
			else if(element.nodeName == 'Timestamp')
				thisReturnStub.Timestamp = element.nodeValue;
			else if(element.nodeName == 'CreditCardToken')
				thisReturnStub.CreditCardToken = element.nodeValue;
			else if(element.nodeName == 'TruncatedCard')
				thisReturnStub.TruncatedCard = element.nodeValue;
			else if(element.nodeName == 'ExpM')
				thisReturnStub.ExpM = element.nodeValue;
			else if(element.nodeName == 'ExpY')
				thisReturnStub.ExpY = element.nodeValue;
			else if(element.nodeName == 'CardType')
				thisReturnStub.CardType = element.nodeValue;
		}
		
		return thisReturnStub;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static QCreditCardUpdateReturn CalloutIPPaymentsSubmitUpdateRegisteredDetails(QCreditCardUpdate thisRegisterStub)
	{
		QCreditCardUpdateReturn 	thisReturnStub 	= new QCreditCardUpdateReturn();
		IPPaymentsSIPP.sippSoap 	IPSOAPMessage 	= new IPPaymentsSIPP.sippSoap(QCachePaymentGatewayMetadata.getSIPPServiceLocation());
		
		IPSOAPMessage.timeout_x = 60000;
		
		string strResults;
		
		string strXMLRequest = '<TokeniseCreditCard>'+
									'<UserName>'			+ QCachePaymentGatewayMetadata.getUsername() 	+ '</UserName>'+
									'<Password>'			+ QCachePaymentGatewayMetadata.getPassword()	+ '</Password>'+
									'<CardNumber>' 			+ thisRegisterStub.CreditCardNumber 			+ '</CardNumber>' +
									'<ExpM>' 				+ thisRegisterStub.CreditCardExpiryMonth 		+ '</ExpM>' +
									'<ExpY>' 				+ thisRegisterStub.CreditCardExpiryYear	 		+ '</ExpY>' +
									'<CardHolderName>' 		+ thisRegisterStub.CreditCardName 				+ '</CardHolderName>' +
									'<TokeniseAlgorithmID>'	+	'2'	+	'</TokeniseAlgorithmID>' +
								'</TokeniseCreditCard>';

		System.Debug('XML Request Message - ' + strXMLRequest);
		
		if(!System.Test.isRunningTest())
		{
			strResults = IPSOAPMessage.TokeniseCreditCard(strXMLRequest);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			strResults = '<Response><ResponseCode>0</ResponseCode><CreditCardToken>100001337</CreditCardToken></Response>';
		}
		
		System.Debug('XML Response Message - ' + strResults);
		
		XMLDom objXMLDom = new XMLDom(strResults);
				
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			System.Debug(element);
			System.Debug('thisReturnStub = ' + thisReturnStub);

			if(element.nodeName == 'ReturnValue')
			{
				thisReturnStub.ResponseCode = element.nodeValue;
				System.Debug('\n\nSetting response code to ' + thisReturnStub.ResponseCode);
			}
			else if(element.nodeName == 'Token')
			{
				thisReturnStub.CreditCardToken = element.nodeValue;
				System.Debug('\n\nSetting credit card token to ' + thisReturnStub.CreditCardToken);
			}
		}
		
		System.Debug('\n\nReturning : ' + thisReturnStub);
		
		return thisReturnStub;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Payment by Registered Card
	public static QCreditCardByReferenceReturn CalloutIPPaymentsSubmitCreditCardRegisteredPayment(QCreditCardByReference thisCreditCardStub)
	{
		QCreditCardByReferenceReturn 	thisReturnStub 	= new QCreditCardByReferenceReturn();
		IPPaymentsDTS.dtsSoap			IPSOAPMessage 	= new IPPaymentsDTS.dtsSoap(QCachePaymentGatewayMetadata.getDTSServiceLocation());
		
		IPSOAPMessage.timeout_x = 60000;
		
		// stick a leading zero on here
		if(thisCreditCardStub.CreditCardExpiryMonth.length() == 1)
		{
			thisCreditCardStub.CreditCardExpiryMonth = '0' + thisCreditCardStub.CreditCardExpiryMonth;
		}
		
		thisReturnStub.Request = '<Transaction>'+
									'<CustNumber>' 	+ thisCreditCardStub.CustomerNumber	+ '</CustNumber>'+
									'<CustRef>'	 	+ thisCreditCardStub.CustomerReference 	+ '</CustRef>'+
									'<Amount>' 		+ thisCreditCardStub.Amount			+ '</Amount>'+
									'<TrnType>'		+ '1' 								+ '</TrnType>'+
									
									'<CreditCard>'+
										'<TokeniseAlgorithmID>'	+ QCachePaymentGatewayMetadata.getTokenAlgorithmId()	+ '</TokeniseAlgorithmID>' +
										'<CardNumber>'			+ thisCreditCardStub.CustomerNumber 					+ '</CardNumber>' +	
										'<ExpM>' 				+ thisCreditCardStub.CreditCardExpiryMonth 				+ '</ExpM>' +
										'<ExpY>' 				+ thisCreditCardStub.CreditCardExpiryYear	 			+ '</ExpY>' +								
									'</CreditCard>'+
									
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername() 	+ '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword()	+ '</Password>'+
									'</Security>'+
									
									'<UserDefined></UserDefined>'+
								'</Transaction>';
		
		System.Debug('XML Request Message - ' + thisReturnStub.Request);
		
		if(!System.Test.isRunningTest())
		{
			thisReturnStub.Response = IPSOAPMessage.SubmitSinglePayment(thisReturnStub.Request);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			thisReturnStub.Response = '<Response><ResponseCode>0</ResponseCode><Timestamp>2009-02-12 19:28:05</Timestamp><Receipt>100001337</Receipt><SettlementDate>01-apr-2010</SettlementDate><DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>';
		}
		
		System.Debug('XML Response Message - ' + thisReturnStub.Response);
		
		XMLDom objXMLDom = new XMLDom(thisReturnStub.Response);
		
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			if(element.nodeName == 'ResponseCode')
				thisReturnStub.ResponseCode = element.nodeValue;
			else if(element.nodeName == 'Timestamp')
				thisReturnStub.Timestamp = element.nodeValue;
			else if(element.nodeName == 'Receipt')
				thisReturnStub.Receipt = element.nodeValue;
			else if(element.nodeName == 'SettlementDate')
				thisReturnStub.SettlementDate = element.nodeValue;
			else if(element.nodeName == 'DeclinedCode')
				thisReturnStub.DeclinedCode = element.nodeValue;
			else if(element.nodeName == 'DeclinedMessage')
				thisReturnStub.DeclinedMessage = element.nodeValue;
			else if(element.nodeName == 'CardType')
				thisReturnStub.CardType = element.nodeValue;
		}
		
		return thisReturnStub;
	}
	
	public static date ConvertSettlementDate(string strSettlementDate)
	{
		date dtRet;
				
		string[] arrStringSettlementDate = strSettlementDate.split('-');
		
		if(arrStringSettlementDate.size() == 3)
		{
			dtRet = date.newInstance(integer.valueOf(arrStringSettlementDate[2]), GetMonth(arrStringSettlementDate[1]), integer.valueOf(arrStringSettlementDate[0]));
		}
		
		return dtRet;
	}
	
	private static integer GetMonth(string strMonth)
	{
		integer iRet = 1;
		
		if(strMonth.toLowerCase() == 'feb')
			iRet = 2;
		else if(strMonth.toLowerCase() == 'mar')
			iRet = 3;
		else if(strMonth.toLowerCase() == 'apr')
			iRet = 4;
		else if(strMonth.toLowerCase() == 'may')
			iRet = 5;
		else if(strMonth.toLowerCase() == 'jun')
			iRet = 6;
		else if(strMonth.toLowerCase() == 'jul')
			iRet = 7;
		else if(strMonth.toLowerCase() == 'aug')
			iRet = 8;
		else if(strMonth.toLowerCase() == 'sep')
			iRet = 9;
		else if(strMonth.toLowerCase() == 'oct')
			iRet = 10;
		else if(strMonth.toLowerCase() == 'nov')
			iRet = 11;
		else if(strMonth.toLowerCase() == 'dec')
			iRet = 12;
			
		return iRet;
	}
	
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void testProcessPayment()
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		QCreditCardWithRegister 		RegisterStub = new QCreditCardWithRegister();
		QCreditCardWithRegisterReturn 	ReturnRegisterStub;
		
		RegisterStub.CreditCardNumber 		= '4242424242424242';
		RegisterStub.CreditCardExpiryMonth 	= '10';
		RegisterStub.CreditCardExpiryYear 	= '2015';
		RegisterStub.CreditCardHolderName	= 'Mr Testy McTest';
		RegisterStub.CustomerReference		= 'REF001';
		RegisterStub.Amount					= '10000';
		RegisterStub.CreditCardCVN			= '123';
		
		ReturnRegisterStub = CalloutIPPaymentsSubmitCreditCardPaymentWithRegister(RegisterStub);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		QCreditCardByReference 			RegisteredPaymentStub = new QCreditCardByReference();
		QCreditCardByReferenceReturn 	ReturnRegisteredPaymentStub;
		
		RegisteredPaymentStub.CustomerNumber 		= ReturnRegisterStub.CreditCardToken;
		RegisteredPaymentStub.CustomerReference 	= '10101010';
		RegisteredPaymentStub.Amount 				= '100';
		
		ReturnRegisteredPaymentStub = CalloutIPPaymentsSubmitCreditCardRegisteredPayment(RegisteredPaymentStub);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		QCreditCard 		CreditCardStub 	= new QCreditCard();	// our stub;
		QCreditCardReturn 	ReturnPaymentStub;
		
		CreditCardStub.CreditCardNumber 		= '4111111111111111';
		CreditCardStub.CreditCardName 			= 'Testing Test Test';
		CreditCardStub.CreditCardExpiryYear 	= '2099';
		CreditCardStub.CreditCardExpiryMonth 	= '12';
		CreditCardStub.CreditCardCVN 			= '132';
		CreditCardStub.Amount 					= '1000';
		
		ReturnPaymentStub = CalloutIPPaymentsSubmitCreditCardPayment(CreditCardStub);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		QCreditCardRefund		RefundStub 		= new QCreditCardRefund();
		QCreditCardRefundReturn ReturnRefundStub;
		
		RefundStub.Receipt 	= '100001337';
		RefundStub.Amount	= '10000';
		
		ReturnRefundStub = CalloutIPPaymentsSubmitRefundPayment(RefundStub);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		QCreditCardUpdate		UpdateStub 		= new QCreditCardUpdate();
		QCreditCardUpdateReturn ReturnUpdateStub;
		
		UpdateStub.CreditCardToken 	= '100001337';
		UpdateStub.CreditCardNumber	= '4242424242424242';
		UpdateStub.CreditCardName	= 'Testing';
		UpdateStub.CreditCardExpiryYear	= '2099';
		UpdateStub.CreditCardExpiryMonth	= '01';
		UpdateStub.CreditCardCVN	= '123';
		
		ReturnUpdateStub = CalloutIPPaymentsSubmitUpdateRegisteredDetails(UpdateStub);
	}
}