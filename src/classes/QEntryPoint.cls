global class QEntryPoint 
{
	webservice static string ProcessClubRedPaymentsByOpportunityId(Id idSource)
	{
		// our param objects
		QCampaignProcessingParam.QCampaignProcessingArg objArg	= new QCampaignProcessingParam.QCampaignProcessingArg(QCampaignProcessingParam.ActionCode.PROCESS_ALL_OPPORTUNITIES_BY_CAMPAIGN, idSource);
		QCampaignProcessingParam.QCampaignProcessingRet objRet 	= new QCampaignProcessingParam.QCampaignProcessingRet();
				
		// our processor object
		QCampaignProcessing objCampaignProcessor = new QCampaignProcessing(objArg, objRet);
		
		objCampaignProcessor.ProcMain(); 	// GO!
		
		// return
		return 'Finished - ' + objRet;
	}
	
	public static testMethod void testProcessClubRedPaymentsByOpportunityId()
	{
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Opportunity.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
		
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		Account	sAccount = new Account();
		sAccount.Name = 'Test Account';
		
		insert sAccount;
		System.Assert(sAccount.Id != null);
		
		Contact sContact = new Contact();
		sContact.AccountId 	= sAccount.id;
		sContact.FirstName 	= 'Test';
		sContact.LastName 	= 'Test';
		
		insert sContact;
		System.Assert(sContact.Id != null);
		
		Opportunity sOpportunity 	= new Opportunity();
		sOpportunity.AccountId 		= sAccount.id;
		sOpportunity.Name 			= sAccount.Name;
		sOpportunity.Contact__c 	= sContact.id;
		sOpportunity.Amount 		= 0;
		sOpportunity.StageName 		= 'Club Red Ongoing';
		
		sOpportunity.RecordTypeId = mapRecordTypeName.get('ClubRed').getRecordTypeId();
		
		sOpportunity.Recurring_Payment_Frequency__c = 'Monthly';
		sOpportunity.Personal_Donation__c			= true;
		sOpportunity.Recurring_Donation_Amount__c 	= 1000;
		sOpportunity.Credit_Card_Type__c 			= 'Visa';
		sOpportunity.Card_Name__c 					= 'Testing';
		sOpportunity.Credit_Card__c 				= '4242424242424242';
		sOpportunity.Card_Expiry_Month__c 			= 10;
		sOpportunity.Credit_Card_Year__c 			= 99;
		
		sOpportunity.Next_Recurring_Payment_Process_Date__c = System.Today();
			
		sOpportunity.Recurring_Payment_Frequency__c 	= 'Monthly';
		sOpportunity.IsRecurringPayment__c				= true;
		
		sOpportunity.CloseDate 	= System.Today().addYears(5);
		
		sOpportunity.CampaignId = sCampaign.Id;
		
		insert sOpportunity;
		System.Assert(sOpportunity.Id != null);
		
		update sOpportunity;	// this is to stop that stupid workflow rule screwing with me!
		
		ProcessClubRedPaymentsByOpportunityId(sCampaign.Id);
	}
}