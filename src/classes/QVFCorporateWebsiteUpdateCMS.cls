public with sharing class QVFCorporateWebsiteUpdateCMS 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	// Exception Classes
	public class QScreenParameterException 	extends Exception{}
	public class QDataValidationException 	extends Exception{}
	 
		
	////////////////////////////////////////////////////////////////////////////////////////
	// Singular
	private	id	 		m_ID;					// the campaign id that we're coming from
	private	boolean		m_bActive;
	private string		m_strCampaignWebsiteEntityType;

	public	boolean		m_bError {get; set;}	// if we have had an error yet

	////////////////////////////////////////////////////////////////////////////////////////
	// static for TESTING ONLY
	private static boolean		m_bAllowCallout = true;
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Collections

	////////////////////////////////////////////////////////////////////////////////////////
	// Constructor
	public QVFCorporateWebsiteUpdateCMS()
	{
		try
		{
			// lets grab the screen parameters
			if(null == (m_ID = ApexPages.currentPage().getParameters().get('id')))
			{
				throw new QScreenParameterException('Id Parameter not found - Please Contact Support');	
			}
			
			string strIsActive;
			// lets grab the screen parameters
			if(null == (strIsActive = ApexPages.currentPage().getParameters().get('IsActive')))
			{
				throw new QScreenParameterException('IsActive Parameter not found - Please Contact Support');	
			}
			
			// lets grab the screen parameters
			if(null == (m_strCampaignWebsiteEntityType = ApexPages.currentPage().getParameters().get('CampaignWebsiteEntityType')))
			{
				throw new QScreenParameterException('CampaignWebsiteEntityType Parameter not found - Please Contact Support');	
			}
			
			m_bActive = (strIsActive == '1');
			
			m_bError = false;
		}
		catch(Exception e)
		{
			ApexPages.addMessages(e);
			m_bError = true;
		}
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	public pageReference Cancel()
	{
		PageReference pageRedirect = new PageReference('/' + this.m_ID);
		pageRedirect.setRedirect(true);
		
		return pageRedirect;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	public pageReference UpdateCMS()
	{
		PageReference pageRedirect;
		
		try
		{
			if(m_bActive)
			{
				if(m_strCampaignWebsiteEntityType == 'Event' && m_bAllowCallout)
				{
					QCorporateWebsiteWebServices.TriggerEventUpdateRequest(m_ID);
				}
				else if(m_strCampaignWebsiteEntityType == 'Donation' && m_bAllowCallout)
				{
					QCorporateWebsiteWebServices.TriggerCampaignUpdateRequest(m_ID);
				}
				else if(m_strCampaignWebsiteEntityType == 'Product' && m_bAllowCallout)
				{
					QCorporateWebsiteWebServices.TriggerShopProductUpdateRequest(m_ID);
				}
				else if(m_bAllowCallout)
				{
					throw new QScreenParameterException('Unknown Enity Type');
				}
			}
			else
			{
				throw new QScreenParameterException(m_strCampaignWebsiteEntityType + ' is not active, you can not send the ' + 
														m_strCampaignWebsiteEntityType.toLowerCase() + ' details to the CMS');
			}	
			
			if(m_strCampaignWebsiteEntityType == 'Donation' || m_strCampaignWebsiteEntityType == 'Event')
			{
				Campaign sCampaign = [select id, CMS_Last_Update__c from Campaign where id = :m_ID];		
				sCampaign.CMS_Last_Update__c = System.Now();		
				update sCampaign;
			}
			else if(m_strCampaignWebsiteEntityType == 'Product')
			{
				Product2 sProduct = [select id, CMS_Last_Update__c from Product2 where id = :m_ID];
				sProduct.CMS_Last_Update__c = System.Now();
				update sProduct;
			}
		}
		catch(Exception e)
		{
			ApexPages.addMessages(e);
			this.m_bError = true;
		}
		
		if(!m_bError)
		{
			pageRedirect = new PageReference('/' + m_ID);
			pageRedirect.setRedirect(true);
		}
		
		return pageRedirect;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	public pageReference Deactivate()
	{
		PageReference pageRedirect;
		
		try
		{
			if(m_strCampaignWebsiteEntityType == 'Event')
			{
				QCorporateWebsiteWebServices.TriggerEventDeactivateRequest(m_ID);
			}
			else if(m_strCampaignWebsiteEntityType == 'Donation')
			{
				QCorporateWebsiteWebServices.TriggerCampaignDeactivateRequest(m_ID);
			}
			else if(m_strCampaignWebsiteEntityType == 'Product')
			{
				QCorporateWebsiteWebServices.TriggerShopProductDeactivateRequest(m_ID);
			}
			else
			{
				throw new QScreenParameterException('Unknown Enity Type');
			}
		}
		catch(Exception e)
		{
			ApexPages.addMessages(e);
			this.m_bError = true;
		}
		
		if(!m_bError)
		{
			pageRedirect = new PageReference('/' + m_ID);
			pageRedirect.setRedirect(true);
		}
		
		return pageRedirect;
	}
	
	/***********************************************************************************************************
    	LIST METHODS
    ***********************************************************************************************************/
    public string GetEntityType()
    {
    	return m_strCampaignWebsiteEntityType.toLowerCase();
    }
    
    /***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testMethod void testQVFCorporateWebsiteUpdateCMS()
	{
		//Find Record type ID RecordType
		Schema.DescribeSObjectResult   		schemaDescribeCampaign    = Campaign.SObjectType.getDescribe();
  		map<string, Schema.RecordTypeInfo>  mapRecordTypeNameCampaign = schemaDescribeCampaign.getRecordTypeInfosByName();
  		
		QVFCorporateWebsiteUpdateCMS oController;
		
		boolean bError = false;
		
		oController = new QVFCorporateWebsiteUpdateCMS();
					
		System.Assert(oController.m_bError);
		
		Account sAccount = new Account(name = 'testing');
		
		//insert New Campaign
		Campaign sCampaign 					= new Campaign();
		sCampaign.Name						= 'Quattro';
		sCampaign.Campaign_Code__c 			= 'QCCode'; 
  		sCampaign.RecordTypeId				= mapRecordTypeNameCampaign.get('Kitetime - KTM').getRecordTypeId();
		sCampaign.Kitetime_Account__c		= sAccount.Id;
		sCampaign.Job_Code_State__c			= 'NSW';
		sCampaign.Job_Code_Year__c			= '09';
		sCampaign.Job_Code_Item__c			= 'Kitetime - KT';		
		sCampaign.IsActive					= true;	
		sCampaign.ExpectedRevenue 			= 1000;
		insert sCampaign;
		
		Product2 sProduct					= new Product2();
		sProduct.Name						= 'Awesome-o 5000';
		sProduct.Description				= 'Awesome';
		sProduct.Description_For_Website__c	= 'Awesome';
		sProduct.Minimum_Shipping_Cost__c	= 0.01;
		sProduct.Title_For_Website__c		= 'Awesome';
		insert sProduct;
		
		PricebookEntry sPBEntry = new PricebookEntry();
		sPBEntry.UnitPrice = 100.0;
		sPBEntry.Product2Id = sProduct.Id;
		sPBEntry.Pricebook2Id = QCacheWebsiteMetadata.getShopStandardPricebook();
		insert sPBEntry;
		
	 	System.Assert(sCampaign.Id != null);
		
		ApexPages.currentPage().getParameters().put('id', sCampaign.Id);
		ApexPages.currentPage().getParameters().put('IsActive', '1');
		
		
		// cause some problems
		ApexPages.currentPage().getParameters().put('CampaignWebsiteEntityType', 'hello');
		oController = new QVFCorporateWebsiteUpdateCMS();
		
		oController.Cancel();
		oController.UpdateCMS();
		oController.Deactivate();
		
		QVFCorporateWebsiteUpdateCMS.m_bAllowCallout = false;
		
		// valid type
		ApexPages.currentPage().getParameters().put('id', sCampaign.Id);
		ApexPages.currentPage().getParameters().put('IsActive', '1');		
		ApexPages.currentPage().getParameters().put('CampaignWebsiteEntityType', 'Event');
		oController = new QVFCorporateWebsiteUpdateCMS();
		
		oController.UpdateCMS();
		oController.Cancel();
		oController.Deactivate();		
		
		// product
		ApexPages.currentPage().getParameters().put('id', sProduct.Id);
		ApexPages.currentPage().getParameters().put('IsActive', '1');		
		ApexPages.currentPage().getParameters().put('CampaignWebsiteEntityType', 'Product');
		oController = new QVFCorporateWebsiteUpdateCMS();
		
		oController.UpdateCMS();
		oController.Cancel();
		oController.Deactivate();		
		QVFCorporateWebsiteUpdateCMS.m_bAllowCallout = true;		
	}
}