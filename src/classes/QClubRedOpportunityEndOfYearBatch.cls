/* Batch to close the Open Opportunities of any Club Red Campaign and associate them to a New Campaign
*/
global class QClubRedOpportunityEndOfYearBatch implements Database.Batchable<SObject>
{	
	private	string		strOldCampaign;
	private	string		strNewCampaign;
	
	/* Create a new QClubRedOpportunityEndOfYearBatch
	@param strOldCampaign	an old Campaign Id
	@param strNewCampaign	a new Campaign Id
	*/
	global QClubRedOpportunityEndOfYearBatch(string strOldCampaign, string strNewCampaign)
	{
		this.strOldCampaign = strOldCampaign;
		this.strNewCampaign = strNewCampaign; 
	}
	/* Init the Query
	*/
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery = '	select 	';
		for(Schema.SObjectField f : Schema.sObjectType.Opportunity.fields.getMap().Values())
		{
			strQuery+= f+' ,';		
		}
		strQuery = strQuery.substring(0,strQuery.length()-2);
		strQuery +=
			'	from 	Opportunity '+
			'	where 	StageName = \'Club Red Ongoing\' '+
			'	and		CampaignId = \''+strOldCampaign+'\'';
		return Database.getQueryLocator(strQuery);
	}
	/* Action for my query
	*/
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<Opportunity> 	liNewOpportunity 	= new list<Opportunity>();
		
		for( sObject o : scope )
		{
			//clone my old Opp
			Opportunity sOldOpportunity 	= (Opportunity)o;
			Opportunity sNewOpportunity 	= sOldOpportunity.clone(false, true);
			//Close the old Opp
			sOldOpportunity.StageName 		= 'Club Red Closed';
			sOldOpportunity.CloseDate		= System.today();
			//Associate the new Opp to the new Campaign
			sNewOpportunity.CampaignId 		= strNewCampaign;
			liNewOpportunity.add(sNewOpportunity);	
		}
		
		insert liNewOpportunity;
		update scope;
	}
	
	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

}