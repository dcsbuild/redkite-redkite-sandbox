@isTest(SeeAllData=true)
public with sharing class QClientSearch_Test 
{
	public static testMethod void testQClientSearch()
	{
	 	QClientSearch oProcesser;
		QClientSearch.QClientSearchRet oRet;
		
	 	// if Contact is not there
	 	string strTestEmailAddress = Crypto.getRandomInteger()+ '@quattroinnovatiom.com';
	 	
	 	List<List<SObject>> searchList = [
											FIND 		:strTestEmailAddress 
											IN 			EMAIL FIELDS
											RETURNING	Account(id, OwnerId order by LastModifiedDate desc)
										];
										
	 	System.assertEquals(searchList.size(),1);
	 	System.assertEquals(searchList[0].size(),0);
	 	
	 	QClientSearch.QClientSearchArgs oArgs = new QClientSearch.QClientSearchArgs();
	 	 	
	 	oArgs.DedupeMethod				= QClientSearch.DedupeMethod.CORPORATE;
		oArgs.Email						= strTestEmailAddress;
		oArgs.CreateAction 				= QClientSearch.CreateAction.PERSON_ACCOUNT;
		oArgs.FirstName 				= 'TestFirstName';
		oArgs.LastName  				= 'LastName';
		oArgs.State						= 'NSW';
		oArgs.Title						= 'Test';
		oArgs.PhoneNumber				= '00000112365';
		oArgs.OtherPhoneNumber		 	= '043232323';
		oArgs.Street					= 'Street';
		oArgs.Suburb					= 'Surry Hills';
		oArgs.State						= 'NSW';
		oArgs.Country					= 'Australia';
		oArgs.PostalCode				= '2000';
		oArgs.MarketingOptIn 			= false;
		oArgs.FurtherInformationOptIn 	= false;

		
		oProcesser 	= new QClientSearch(oArgs);
		oRet 	 	= oProcesser.ProcMain();
		
		Account sTestContact = [select ID, PersonEmail,FirstName, LastName, PersonContactId from Account where PersonEmail = :strTestEmailAddress limit 1];
		System.AssertEquals(sTestContact.PersonEmail, strTestEmailAddress);
		
		
		/// Contact is There
		oArgs = new QClientSearch.QClientSearchArgs();
	 	 	
	 	oArgs.DedupeMethod 				= QClientSearch.DedupeMethod.CORPORATE;
		oArgs.Email						= strTestEmailAddress;
		oArgs.CreateAction 				= QClientSearch.CreateAction.PERSON_ACCOUNT;
		oArgs.FirstName 				= 'TestFirstName';
		oArgs.LastName  				= 'LastName';
		oArgs.Suburb					= 'Surry Hills';
		oArgs.State						= 'NSW';
		oArgs.CreateTaskForPossibleDupe = false;   
		
		oProcesser 	= new QClientSearch(oArgs);
		oRet		= oProcesser.ProcMain();
		
		// check id from email
		System.AssertEquals(sTestContact.Id, oRet.AccountId);
		System.AssertEquals(sTestContact.PersonContactId, oRet.ContactId);

		// If Contact and Account information is not there
		string strTestEmailAddress1 = Crypto.getRandomInteger()+ '@quattroinnovatiom.com';
	 	
	 	List<List<SObject>> searchList1 = [
											FIND 		:strTestEmailAddress1 
											IN 			EMAIL FIELDS
											RETURNING	Account(id, OwnerId order by LastModifiedDate desc)
										];
		
		System.assertEquals(searchList1.size(),1);
	 	System.assertEquals(searchList1[0].size(),0);
	 	
	 	oArgs = new QClientSearch.QClientSearchArgs();
	 	 	
	 	oArgs.DedupeMethod				= QClientSearch.DedupeMethod.CORPORATE;
		oArgs.Email						= strTestEmailAddress1;
		oArgs.CreateAction 				= QClientSearch.CreateAction.PERSON_ACCOUNT;
		oArgs.FirstName 				= 'TestFirstName1';
		oArgs.LastName  				= 'LastName1';
		oArgs.State						= 'SA';
		oArgs.Title						= 'Test1';
		oArgs.PhoneNumber				= '00012365';
		oArgs.OtherPhoneNumber 			= '04300232323';
		oArgs.Street					= 'Surry';
		oArgs.Suburb					= 'Sa';
		oArgs.Country					= 'Australia';
		oArgs.PostalCode				= '2001';
		oArgs.CreateTaskForPossibleDupe = false; 
		oArgs.CorporateName 			= 'Quattro';
		oArgs.MarketingOptIn 			= false;
		oArgs.FurtherInformationOptIn 	= true;
		
		oProcesser 	= new QClientSearch(oArgs);
		oRet 	 	= oProcesser.ProcMain();
		
	
		// Check Contact and Account created
		Account sAccount = [select id, FirstName, LastName from Account where PersonEmail= :strTestEmailAddress1 limit 1];
		
		System.assertEquals('TestFirstName1',sAccount.FirstName);	
		
	 	oArgs = new QClientSearch.QClientSearchArgs();
	 	 	
	 	oArgs.DedupeMethod				= QClientSearch.DedupeMethod.CORPORATE;
		oArgs.Email						= 'some@email.com';
		oArgs.CreateAction 				= QClientSearch.CreateAction.PERSON_ACCOUNT;
		oArgs.FirstName 				= 'TestFirstName2';
		oArgs.LastName  				= 'LastName2';
		oArgs.State						= 'SA';
		oArgs.Title						= 'Test2';
		oArgs.PhoneNumber				= '00012365';
		oArgs.OtherPhoneNumber 			= '04300232323';
		oArgs.Street					= 'Surry';
		oArgs.Suburb					= 'Sa';
		oArgs.Country					= 'Australia';
		oArgs.PostalCode				= '2001';
		oArgs.CreateTaskForPossibleDupe = false; 
		oArgs.CorporateName 			= 'Quattro';
		oArgs.MarketingOptIn 			= false;
		oArgs.FurtherInformationOptIn 	= true;
		
		oProcesser 	= new QClientSearch(oArgs);
		oRet 	 	= oProcesser.ProcMain();
		
	
		// Check Contact and Account created
		sAccount = [select id, FirstName, LastName from Account where PersonEmail= 'some@email.com' limit 1];
		
		System.assertEquals('TestFirstName2',sAccount.FirstName);	
		
		oArgs.Email	= 'someother@email.com';
		oArgs.DedupeMethod				= QClientSearch.DedupeMethod.CORPORATE_SHOP;
		oProcesser	= new QClientSearch(oArgs);
		oRet		= oProcesser.ProcMain();
		
	 }
}