public with sharing class QPoolCounter 
{
	public class QPoolCounterException extends Exception{}
	
	public static integer GetPoolCounter(string strCounterName)
	{
		try
		{
			integer i = [select counter__c from Pool_Counter__c where name = :strCounterName limit 1 for update].counter__c.intValue();
			//integer i = [select counter__c from Pool_Counter__c where name = :strCounterName limit 1].counter__c.intValue();
			
			i++;
			
			return i;
		}
		catch(Exception e)
		{
			throw new QPoolCounterException('Error Loading Pool Counter - ' + strCounterName + ' - ' + e);
		}
		
		return null;
	}
	
	public static void IncrementCounter(string strCounterName)
	{
		try
		{
			Pool_Counter__c sCounter  = [select id, counter__c from Pool_Counter__c where name = :strCounterName limit 1];
			
			sCounter.counter__c++;
			
			update sCounter;
			
		}
		catch(Exception e)
		{
			throw new QPoolCounterException('Error Loading Pool Counter - ' + strCounterName + ' - ' + e);
		}
	}
	
	public static testmethod void TestPoolCounter()
	{
		boolean bError = false;
		Pool_Counter__c sCounter = new Pool_Counter__c();
		sCounter.name = 'test counter';
		sCounter.counter__c = 0;
		insert sCounter;
		
		try
		{
			QPoolCOunter.IncrementCounter('doesnotexist');
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(bError);
		bError = false;
		
		try
		{
			QPoolCOunter.IncrementCounter('test counter');
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(!bError);
		
		try
		{
			QPoolCounter.GetPoolCounter('doesnotexist');
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(bError);
		
		QPoolCounter.GetPoolCounter('test counter');
	}
}