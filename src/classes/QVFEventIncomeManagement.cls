public class QVFEventIncomeManagement 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class EventIncomeManagementScreenParameterException 	extends Exception{}
	public class EventIncomeManagementDataValidationException 	extends Exception{}
	
	////////////////////////////////////////////
	// Campaign Member Classes
	public class QCampaignMember
	{
		public Id		CampaignId{get; set;}
		public Id 		Member{get; set;}
		public string 	Name{get; set;}
		
		public pageReference Redirect()
		{
			PageReference pageRedirect = new PageReference('/apex/Income_Management?origin=CampaignMember');
			
			// Add parameters to page URL
			pageRedirect.getParameters().put('id', 			Member);
			pageRedirect.getParameters().put('returnID', 	CampaignId);
			pageRedirect.getParameters().put('campaignId', 	CampaignId);
			
			pageRedirect.setRedirect(true);
			
			System.Debug(pageRedirect);
			
			return pageRedirect;
		}
		
		public QCampaignMember(Id thisContactId, string thisContactName, Id thisCampaignId)
		{
			this.Member 	= thisContactId;
			this.Name 		= thisContactName;
			this.CampaignId = thisCampaignId;
		}
	}
	
	////////////////////////////////////////////
	// Singular
	Campaign m_sCampaign;
	
	string	m_strID;
	
	integer m_iViewIndex;
	
	////////////////////////////////////////////
	// Collections
	list <QCampaignMember> 			m_liQCampaignMember;
	
	list<list<QCampaignMember>> 	m_li2DQCampaignMember;
	list<QCampaignMember> 			m_liQCampaignMemberView;

	////////////////////////////////////////////
	// Constructor
	public QVFEventIncomeManagement()
	{
		// lets grab the screen parameters
		if(null == (m_strID = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new EventIncomeManagementScreenParameterException('Id Parameter not found');	
		}
		
		m_iViewIndex = -1;
		
		LoadCampaignDetails();
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	public Campaign getCampaignDetails()
	{
		if(m_sCampaign == null)
		{
			LoadCampaignDetails();
		}
		return m_sCampaign;
	}
	
	public list<QCampaignMember> getQCampaignMembers()
	{
		if(m_liQCampaignMember == null || m_liQCampaignMemberView == null || m_li2DQCampaignMember == null)
		{
			m_liQCampaignMember 	= new list <QCampaignMember>();
			m_liQCampaignMemberView = new list<QCampaignMember>();
			m_li2DQCampaignMember 	= new list<list<QCampaignMember>>();
			
			for(integer i = 0; i <= 26; i++)
			{
				m_li2DQCampaignMember.add(new list<QCampaignMember>());
			}
			
			LoadQCampaignMembers();
		}
		
		// clear the view
		m_liQCampaignMemberView.clear();
			
		if(m_iViewIndex >= 0)
		{
			for(integer i = 0; i < m_li2DQCampaignMember[m_iViewIndex].size(); i++)
			{
				m_liQCampaignMemberView.add(m_li2DQCampaignMember[m_iViewIndex][i]);
			}		
		}
		else
		{
			for(integer i = 0; i < m_liQCampaignMember.size(); i++)
			{
				m_liQCampaignMemberView.add(m_liQCampaignMember[i]);
			}
		}
			
		return m_liQCampaignMemberView;
	}
	
	public pageReference Cancel()
	{
		PageReference pageRedirect = new PageReference('/' + m_strID);
		pageRedirect.setRedirect(true);
		
		System.Debug(pageRedirect);
		
		return pageRedirect;
	}
	
	public pageReference LoadA(){m_iViewIndex = 0; return null;}
	public pageReference LoadB(){m_iViewIndex = 1; return null;}
	public pageReference LoadC(){m_iViewIndex = 2; return null;}
	public pageReference LoadD(){m_iViewIndex = 3; return null;}
	public pageReference LoadE(){m_iViewIndex = 4; return null;}
	public pageReference LoadF(){m_iViewIndex = 5; return null;}
	public pageReference LoadG(){m_iViewIndex = 6; return null;}
	public pageReference LoadH(){m_iViewIndex = 7; return null;}
	public pageReference LoadI(){m_iViewIndex = 8; return null;}
	public pageReference LoadJ(){m_iViewIndex = 9; return null;}
	public pageReference LoadK(){m_iViewIndex = 10; return null;}
	public pageReference LoadL(){m_iViewIndex = 11; return null;}
	public pageReference LoadM(){m_iViewIndex = 12; return null;}
	public pageReference LoadN(){m_iViewIndex = 13; return null;}
	public pageReference LoadO(){m_iViewIndex = 14; return null;}
	public pageReference LoadP(){m_iViewIndex = 15; return null;}
	public pageReference LoadQ(){m_iViewIndex = 16; return null;}
	public pageReference LoadR(){m_iViewIndex = 17; return null;}
	public pageReference LoadS(){m_iViewIndex = 18; return null;}
	public pageReference LoadT(){m_iViewIndex = 19; return null;}
	public pageReference LoadU(){m_iViewIndex = 20; return null;}
	public pageReference LoadV(){m_iViewIndex = 21; return null;}
	public pageReference LoadW(){m_iViewIndex = 22; return null;}
	public pageReference LoadX(){m_iViewIndex = 23; return null;}
	public pageReference LoadY(){m_iViewIndex = 24; return null;}
	public pageReference LoadZ(){m_iViewIndex = 25; return null;}
	public pageReference LoadOther(){m_iViewIndex = 26; return null;}
	public pageReference LoadAll(){m_iViewIndex = -1; return null;}
	
	/***********************************************************************************************************
    	LIST METHODS
    ***********************************************************************************************************/

    
    /***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private void LoadQCampaignMembers()
	{
		for(CampaignMember[] arrCampignMembers : [select id, contactId, Contact.Name, Contact.LastName from CampaignMember where CampaignID = :m_strID order by Contact.LastName, Contact.Name])
		{
			for(CampaignMember sCampignMembers : arrCampignMembers)
			{
				if(sCampignMembers.Contact.LastName != null)
				{
					m_li2DQCampaignMember[QUtils.GetCharacterOffset(sCampignMembers.Contact.LastName)].add(new QCampaignMember(sCampignMembers.contactId, sCampignMembers.Contact.Name, m_strID));
				}
				else if(sCampignMembers.Contact.Name != null)
				{
					m_li2DQCampaignMember[QUtils.GetCharacterOffset(sCampignMembers.Contact.Name)].add(new QCampaignMember(sCampignMembers.contactId, sCampignMembers.Contact.Name, m_strID));
				}
				else
				{
					m_li2DQCampaignMember[QUtils.GetCharacterOffset('No Name')].add(new QCampaignMember(sCampignMembers.contactId, 'No Name', m_strID));
				}
				
				if(m_liQCampaignMember.size() < 1000)
				{
					m_liQCampaignMember.add(new QCampaignMember(sCampignMembers.contactId, sCampignMembers.Contact.Name, m_strID));
				}
			}	
		}
	}
	
	private void LoadCampaignDetails()
	{
		try
		{
			m_sCampaign = [select id, Name from Campaign where id = :m_strID];
		}
		catch(Exception e)
		{
			throw new EventIncomeManagementScreenParameterException('ID \'' + m_strID + '\' has caused an error while loading. ' + e);	
		}
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void testEnd2End()
	{
		PageReference pageRef = new PageReference('/apex/Event_Income_Management');
		Test.setCurrentPage(pageRef);
		
		QVFEventIncomeManagement controller;
		
		boolean bExceptionThrown = false;
		
		// break it due to no params
		try
		{
			controller = new QVFEventIncomeManagement();
		}
		catch(Exception e)
		{
			bExceptionThrown = true;
		}
		
		System.AssertEquals(true, bExceptionThrown);
		
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		Account sPersonAccount = new Account();
		sPersonAccount.LastName = 'Person Account Test';
		
		insert sPersonAccount;
		
		sPersonAccount = [select id, PersonContactId from Account where id = :sPersonAccount.Id];
		
		CampaignMember sCM = new CampaignMember();
		sCM.CampaignId = sCampaign.Id;
		sCM.ContactId = sPersonAccount.PersonContactId;
		insert sCM;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', sCampaign.Id);
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFEventIncomeManagement();
		
		// run the load details logic
		controller.getCampaignDetails();
		controller.getQCampaignMembers();
		
		controller.LoadA();
		controller.LoadB();
		controller.LoadC();
		controller.LoadD();
		controller.LoadE();
		controller.LoadF();
		controller.LoadG();
		controller.LoadH();
		controller.LoadI();
		controller.LoadJ();
		controller.LoadK();
		controller.LoadL();
		controller.LoadM();
		controller.LoadN();
		controller.LoadO();
		controller.LoadP();
		controller.LoadQ();
		controller.LoadR();
		controller.LoadS();
		controller.LoadT();
		controller.LoadU();
		controller.LoadV();
		controller.LoadW();
		controller.LoadX();
		controller.LoadY();
		controller.LoadZ();
		controller.LoadOther();
		controller.LoadAll();
				
		System.AssertEquals(1, controller.m_liQCampaignMember.size());
		
		controller.m_liQCampaignMember[0].Redirect();
		
		string pageReturn = controller.Cancel().getURL();
		
		System.assertEquals('/'+sCampaign.Id, pageReturn);
		
	}
}