global with sharing class QMelonMailWebServices
{
	public class QMelonMailWebServicesException 			extends Exception{}
	public class QMelonMailWebServicesParameterException	extends Exception{}
	
	/**************************************************************************
		Modify Contact Subscription Request
	**************************************************************************/
	
	global class QModifyContactSubscriptionRequest
	{
		webservice	boolean	bSubscribe				{get; set;}
		webservice	string	strEmail				{get; set;}
		webservice	string	strMelonMailDatabaseId	{get; set;}
	}
	
	global class QModifyContactSubscriptionResults
	{
		webservice	boolean	bInError				{get; set;}
		webservice	string	strErrorMessage			{get; set;}
		
		 QModifyContactSubscriptionResults()
		 {
		 	bInError = false;
		 	strErrorMessage = ' ';
		 }
	}
		
	webservice static QModifyContactSubscriptionResults ModifyContactSubscriptionRequest(QModifyContactSubscriptionRequest args)
	{
		QModifyContactSubscriptionResults oRet = new QModifyContactSubscriptionResults();
		
		integer iMarketingDB = QCacheWebsiteMetadata.getMelonMailMarketingDatabaseId();
		integer iFurtherInfoDB = QCacheWebsiteMetadata.getMelonMailFurtherInfoDatabaseId();
		string	strToUse = '';
		
		if(args.bSubscribe == null)
		{
			throw new QMelonMailWebServicesParameterException('bSubscribe is a required field.');
		}
		
		if(args.strEmail == null || args.strEmail == '')
		{
			throw new QMelonMailWebServicesParameterException('strEmail is a required field.');
		}
		
		if(args.strMelonMailDatabaseId == null || args.strMelonMailDatabaseId == '')
		{
			throw new QMelonMailWebServicesParameterException('strMelonMailDatabaseId is a required field.');
		}
		else if(integer.valueOf(args.strMelonMailDatabaseId) == iMarketingDB)
		{
			strToUse = 'Event Material';
		}
		else if(integer.valueOf(args.strMelonMailDatabaseId) == iFurtherInfoDB)
		{
			strToUse = 'Newsletter';
		}
		else
		{
			oRet.bInError = true;
			oRet.strErrorMessage = 'strMelonMailDatabaseId (' + args.strMelonMailDatabaseId + ') is not valid';
			return oRet;
		}
		
		list<Contact> liContacts;

		try
		{
			liContacts = [select id, Email_Communication__c from Contact where Email = :args.strEmail];
			
			if(liContacts.size() == 0)
			{
				oRet.bInError = true;
				oRet.strErrorMessage = 'No contact with specified email address found';
				return oRet;
			}
			else if(liContacts.size() > 1)
			{
				QUtils.CreateErrorTask(liContacts[0].id, null, 'Melon Mail Webservices Issue - Duplicate Email',
										'Multiple contacts found with same email address - ' + liContacts);
			}
		}
		catch(Exception e)
		{
			oRet.bInError = true;
			oRet.strErrorMessage = 'Unabled to get contact details for specified email address';
			return oRet;
		}
		
		if(args.bSubscribe)
		{
			for(Contact sContact : liContacts)
			{
				if(!sContact.email_communication__c.contains(strToUse))
				{
					sContact.email_communication__c += ';' + strToUse;
				}
			}
		}
		else
		{
			for(Contact sContact : liContacts)
			{
				if(sContact.email_communication__c != null && sContact.email_communication__c.contains(strToUse))
				{
					sContact.email_communication__c = sContact.email_communication__c.replace(strToUse, '');
					
					if(sContact.email_communication__c != null)
					{
						if(sContact.email_communication__c.contains(';;'))
						{
							sContact.email_communication__c = sContact.email_communication__c.replace(';;',';');
						}
			
						if(sContact.email_communication__c.startswith(';'))
						{
							sContact.email_communication__c = sContact.email_communication__c.substring(1);
						}
						
						if(sContact.email_communication__c.endswith(';'))
						{
							sContact.email_communication__c = sContact.email_communication__c.substring(0, sContact.email_communication__c.length() - 1);
						}
					}
				}
			}
		}
		
		try
		{
			update liContacts;
		}
		catch(Exception e)
		{
			oRet.bInError = true;
			oRet.strErrorMessage = 'Error encountered why trying to update contact details';
		}
		return oRet;
	}
	
	/**************************************************************************
		Test Methods
	**************************************************************************/
	public static testmethod void TestModifyContactSubscriptionRequest()
	{
		integer iMarketingDB = QCacheWebsiteMetadata.getMelonMailMarketingDatabaseId();
		integer iFurtherInfoDB = QCacheWebsiteMetadata.getMelonMailFurtherInfoDatabaseId();
		boolean bError = false;
		
		QModifyContactSubscriptionRequest args = new QModifyContactSubscriptionRequest();
		args.bSubscribe = null;
		args.strEmail = '';
		args.strMelonMailDatabaseId = '0';
		
		QModifyContactSubscriptionResults res;
		
		// fail because bSubscribe is null
		try {res = ModifyContactSubscriptionRequest(args);}
		catch(Exception e) {bError = true;}

		System.Assert(bError);
		bError = false;
		
		args.bSubscribe = false;
				
		// fail because email is empty
		try {res = ModifyContactSubscriptionRequest(args);}
		catch(Exception e) {bError = true;}

		System.Assert(bError);
		bError = false;
	
		args.strEmail = 'thisemail@doesnotexist.xyz';
		
		// fail on invalid database id
		res = ModifyContactSubscriptionRequest(args);
		System.Assert(res.bInError);

		// fail because of email not found
		args.strMelonMailDatabaseId = '' + iMarketingDB;		
		res = ModifyContactSubscriptionRequest(args);
		System.Assert(res.bInError);
		
		// set up valid account + contact
/*		Account sAccount = new Account();
		sAccount.name = 'LaceyTestAccount5000';
		insert(sAccount);
*/		
		Contact sContact = new Contact();
		sContact.FirstName = 'Sonic';
		sContact.LastName = 'The Hedgehog';
		sContact.Email_Communication__c = 'Newsletter; Event material';
		sContact.email = 'sonic@sega.com';
		insert sContact;
		
		args.strEmail = 'sonic@sega.com';
		res = ModifyContactSubscriptionRequest(args);
		System.Assert(!res.bInError);
		
		args.strEmail = 'sonic@sega.com';
		args.bSubscribe = true;
		res = ModifyContactSubscriptionRequest(args);
		System.Assert(!res.bInError);
		
		// other db
		args.strMelonMailDatabaseId = '' + iFurtherInfoDB;
		args.bSubscribe = false;
		res = ModifyContactSubscriptionRequest(args);
		System.Assert(!res.bInError);

		args.strMelonMailDatabaseId = '' + iFurtherInfoDB;
		args.bSubscribe = true;
		res = ModifyContactSubscriptionRequest(args);
		System.Assert(!res.bInError);		
	}
}