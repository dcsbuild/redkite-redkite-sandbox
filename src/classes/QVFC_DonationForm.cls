global without sharing class QVFC_DonationForm 
{
	// JS remote stage 1 resonse
	global class JSRemoteStage1Ret
	{
		public boolean  InError		 	{get; set;}
		public string   Message		 	{get; set;}
		public string   UserId		  	{get; set;}
		
		public string   CCNumber		{get; set;}
		public string   CCName		  	{get; set;}
		public string   CCCVS		   	{get; set;}
		public string   CCEXPM		  	{get; set;}
		public string   CCEXPY		  	{get; set;}
		
		public string   Comments		{get; set;}
		
		public string   Amount		  	{get; set;}
		
		public string   CampaignId	  	{get; set;}
		public string	DonationType	{get; set;}
		
		public string	DonationSource		{get; set;}
		
		public JSRemoteStage1Ret()
		{
			InError = false;
			UserId = '';
			Message = '';
		}
	}
	
	global class JSRemoteStage1Arg
	{
		public string   Title		   {get; set;}
		public string   FirstName	   {get; set;}
		public string   LastName		{get; set;}
		public string   Postal		  {get; set;}
		public string   Suburb		  {get; set;}
		public string   Postcode		{get; set;}
		public string   State		   {get; set;}
		public string   Country		 {get; set;}
		public string   Email		   {get; set;}
		public string   EmailConfirm	{get; set;}
		public string   Phone		   {get; set;}
		public string   PhoneType	   {get; set;}
		public string   DOBD			{get; set;}
		public string   DOBM			{get; set;}
		public string   DOBY			{get; set;}
		
		public string   CCNumber		{get; set;}
		public string   CCName		  {get; set;}
		public string   CCCVS		   {get; set;}
		public string   CCEXPM		  {get; set;}
		public string   CCEXPY		  {get; set;}
		
		public string   Amount		  {get; set;}
		
		public string   ParentName		  {get; set;}
		public string   ParentWebsite	   {get; set;}
		public string   ParentPhone		 {get; set;}
		public string   ParentPostal		{get; set;}
		public string   ParentSuburb		{get; set;}
		public string   ParentPostcode	  {get; set;}
		public string   ParentState		 {get; set;}
		public string   ParentCountry	   {get; set;}
		public string   ParentEmail		 {get; set;}
		public string   ParentEmailConfirm  {get; set;}
		public string   Position			{get; set;}
		
		public string   MainCampaignId	  {get; set;}
		public string   SubCampaignId	   {get; set;}
		
		public string   Comments			{get; set;}
		
		public string   OptIn			   {get; set;}
		
		public string	DonationType		{get; set;}
		
		public string	DonationSource		{get; set;}
	}
	
	// JS remote stage 2 resonse
	global class JSRemoteStage2Arg
	{
		public string   UserId		  {get; set;}
		
		public string   CCNumber		{get; set;}
		public string   CCName		  {get; set;}
		public string   CCCVS		   {get; set;}
		public string   CCEXPM		  {get; set;}
		public string   CCEXPY		  {get; set;}
		
		public string   Amount		  {get; set;}
		
		public string   CampaignId	  {get; set;}
		
		public string   Comments			{get; set;}
		
		public string	DonationType		{get; set;}
		
		public string	DonationSource		{get; set;}
	}
	
	global class JSRemoteStage2Ret
	{
		public boolean  InError	 {get; set;}
		public string   Message	 {get; set;}
		
		public JSRemoteStage2Ret()
		{
			InError = false;
			Message = '';
		}
	}
	
	global class DonationOption
	{
		public string Id {get; set;}
		public string CampaignId {get; set;}
		public string Description {get; set;}
		public string AdditionalDataCapture {get; set;}
		public string DonationSource {get; set;}
		
		public DonationOption(string i, string c, string d, string adc, string ds)
		{
			this.Id = i;
			this.CampaignId = c;
			this.Description = d;
			// just to make sure nothing goes through to the keeper
			this.AdditionalDataCapture = adc == null ? 'None' : adc;
			this.DonationSource = ds;
		}
	}
	
	private static final string key = 'tJ3wrGSCy5yfm6PYGcP4hven9nLqjWjD';
	
	global QVFC_DonationForm()
	{
		
	}
	
	global void init()
	{
		
	}
	
	@RemoteAction
	global static list<DonationOption> getDonationOptions()
	{
		list<DonationOption> mp_DonationOptions = new list<DonationOption>();
		
		for(Donation_Form_Control__c[] a : [
							select  id, Campaign__c, Name, Website_Donation_Form_Description__c, Donation_Form_Additional_Info_Capture__c,
									Source_Override__c
							from	Donation_Form_Control__c 
							where   Campaign__r.IsActive = true
							and		Active__c = true 
							order by Sort_Order_for_Donation_Form__c asc, Website_Donation_Form_Description__c asc
						])
		{
			for(Donation_Form_Control__c s : a)
			{
				mp_DonationOptions.add(new DonationOption(
															s.Id, 
															s.Campaign__c, 
															s.Website_Donation_Form_Description__c, 
															s.Donation_Form_Additional_Info_Capture__c,
															s.Source_Override__c));
			}
		}
				
		return mp_DonationOptions;
	}
	
	@RemoteAction
	global static JSRemoteStage1Ret performIndividualStage1(JSRemoteStage1Arg arg)
	{   
		JSRemoteStage1Ret ret = new JSRemoteStage1Ret();
		
		blob CRYPTO_KEY = blob.valueOf(key);

		// send all these back encrypted - probably overkill but hey its only a few lines of code
		ret.CCName = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCName)));
		ret.CCNumber = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCNumber)));
		ret.CCCVS = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCCVS)));
		ret.CCEXPY = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCEXPY)));
		ret.CCEXPM = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCEXPM)));
		ret.DonationSource = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.DonationSource)));
		
		Campaign sCampaign = getOverridingCampaign(arg.DonationType, arg.MainCampaignId, arg.SubCampaignId);
		
		if(sCampaign != null)
		{
			ret.CampaignId = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(sCampaign.Id)));
		}
		else
		{
			ret.CampaignId = null;
		}
		
		if(arg.Comments != null && arg.Comments != '')
		{
			ret.Comments = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.Comments)));
		}
		else
		{
			ret.Comments = null;
		}
		
		ret.DonationType = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.DonationType)));
		ret.DonationSource = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.DonationSource)));
		
		ret.Amount = arg.Amount;
		
		QCorporateWebsiteWebServices.QCampaignRegisterUserRequest req = new QCorporateWebsiteWebServices.QCampaignRegisterUserRequest();
		
		req.Title = arg.Title;
		req.FirstName = arg.FirstName;
		req.LastName = arg.LastName;
		req.Street = arg.Postal;
		req.Suburb = arg.Suburb;
		req.PostalCode = arg.Postcode;
		req.State = arg.State;
		req.Country = arg.Country;
		req.Email = arg.Email;
		req.PhoneNumber = arg.Phone;
		req.MarketingOptIn = (arg.OptIn == 'true' || arg.OptIn == '1') ? true : false;
		req.FurtherInformationOptIn = (arg.OptIn == 'true' || arg.OptIn == '1') ? true : false;
		
		req.PhoneType = arg.PhoneType;
		
		req.DonationSource = arg.DonationSource;
		
		try
		{
			req.DOB = date.newInstance(integer.valueOf(arg.DOBY), integer.valueOf(arg.DOBM), integer.valueOf(arg.DOBD));
		}
		catch(Exception e)
		{
			System.Debug('Bad DOB : ' + arg.DOBY + ' : ' + arg.DOBM + ' : ' + arg.DOBD);
		}
		
		QCorporateWebsiteWebServices.QCampaignRegisterUserResults res = QCorporateWebsiteWebServices.CampaignRegisterUserRequest(req);
		
		ret.InError = res.InError;
		ret.Message = res.Error;
		ret.UserId = res.UserId;
		
		return ret;
	}

	@RemoteAction
	global static JSRemoteStage1Ret performNonIndividualStage1(JSRemoteStage1Arg arg)
	{   
		JSRemoteStage1Ret ret = new JSRemoteStage1Ret();
		
		blob CRYPTO_KEY = blob.valueOf(key);

		// send all these back encrypted - probably overkill but hey its only a few lines of code
		ret.CCName = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCName)));
		ret.CCNumber = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCNumber)));
		ret.CCCVS = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCCVS)));
		ret.CCEXPY = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCEXPY)));
		ret.CCEXPM = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.CCEXPM)));
		ret.DonationSource = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.DonationSource)));
		
		// organisation cant have subs so if it comes over we just ignore
		Campaign sCampaign = getOverridingCampaign(arg.DonationType, arg.MainCampaignId, null);
		
		if(sCampaign != null)
		{
			ret.CampaignId = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(sCampaign.Id)));
		}
		else
		{
			ret.CampaignId = null;
		}
		
		if(arg.Comments != null && arg.Comments != '')
		{
			ret.Comments = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.Comments)));
		}
		else
		{
			ret.Comments = null;
		}
		
		ret.DonationType = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.DonationType)));
		ret.DonationSource = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTO_KEY, blob.valueOf(arg.DonationSource)));
		
		ret.Amount = arg.Amount;
		
		QCorporateWebsiteWebServices.QCampaignRegisterUserRequest req = new QCorporateWebsiteWebServices.QCampaignRegisterUserRequest();
		
		req.FirstName = arg.ParentName;
		req.Website = arg.ParentWebsite;
		req.PhoneNumber = arg.ParentPhone;
		req.Street = arg.ParentPostal;
		req.Suburb = arg.ParentSuburb;
		req.PostalCode = arg.ParentPostcode;
		req.State = arg.ParentState;
		req.Country = arg.ParentCountry;
		req.Email = arg.ParentEmail;
		req.MarketingOptIn = (arg.OptIn == 'true' || arg.OptIn == '1') ? true : false;
		req.FurtherInformationOptIn = (arg.OptIn == 'true' || arg.OptIn == '1') ? true : false;
		
		req.DonationSource = arg.DonationSource;
		
		try
		{
			req.DOB = date.newInstance(integer.valueOf(arg.DOBY), integer.valueOf(arg.DOBM), integer.valueOf(arg.DOBD));
		}
		catch(Exception e)
		{
			System.Debug('Bad DOB : ' + arg.DOBY + ' : ' + arg.DOBM + ' : ' + arg.DOBD);
		}
		
		QCorporateWebsiteWebServices.QCampaignRegisterUserResults res = QCorporateWebsiteWebServices.RegisterOrganisaiton(req);
		
		ret.InError = res.InError;
		ret.Message = res.Error;
		ret.UserId = res.UserId;
		
		return ret;
	}

	@RemoteAction
	global static JSRemoteStage2Ret performPayment(JSRemoteStage2Arg arg)
	{   
		System.Debug(arg);
		
		JSRemoteStage2Ret ret = new JSRemoteStage2Ret();
		
		blob CRYPTO_KEY = blob.valueOf(key);
		
		QCorporateWebsiteWebServices.QCampaignDonationRequest req = new QCorporateWebsiteWebServices.QCampaignDonationRequest();
		
		req.UserId = arg.UserId;
		req.Amount = integer.valueOf(arg.Amount);
		 
		req.CardName = Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.CCName)).toString();
		req.CreditCardNumber = Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.CCNumber)).toString();
		req.CVC = Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.CCCVS)).toString();
		req.ExpiryYear = integer.valueOf(Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.CCEXPY)).toString());
		req.ExpiryMonth = integer.valueOf(Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.CCEXPM)).toString());
		req.DonationSource = Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.DonationSource)).toString();
		
		string donationType = Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.DonationType)).toString();
		
		if(arg.CampaignId != null && arg.CampaignId != '')
		{
			req.CampaignId = Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.CampaignId)).toString();
		}
		
		if(arg.Comments != null && arg.Comments != '')
		{
			req.Comments = Crypto.decryptWithManagedIV('AES256', CRYPTO_KEY, EncodingUtil.base64Decode(arg.Comments)).toString();
		}
		
		QCorporateWebsiteWebServices.QCampaignDonationResults res;
		
		if(donationType == 'Individual')
		{
			res = QCorporateWebsiteWebServices.CampaignDonationRequest(req);
		}
		else
		{
			res = QCorporateWebsiteWebServices.CampaignBusinessDonationRequest(req);
		}
		
		ret.InError = res.InError;
		ret.Message = res.Error;

		return ret;
	}
	
	// return a campaign means it must load one and so no weird unknown ID problems just big cant load errors
	private static Campaign getOverridingCampaign(string donationType, string main, string sub)
	{
		// club red wins
		if(main == 'ClubRed')
		{
			try
			{
				return [select id from Campaign where id = :QCacheWebsiteMetadata.getRedClubRecurringDonationsCampaign()]; 
			}
			catch(Exception e){}
		}
		
		if(donationType == 'Individual' || donationType == '' || donationType == null)
		{
			// lets go this one then
			try
			{
				return [select id from Campaign where id = :sub];
			}
			catch(Exception e){}
		}
		else if(donationType == 'Organisation')
		{
			try
			{
				return [select id from Campaign where id = :QCacheWebsiteMetadata.getDefaultCampaignForOrgDonations()]; 
			}
			catch(Exception e){}
		}
		else if(donationType == 'Club')
		{
			try
			{
				return [select id from Campaign where id = :QCacheWebsiteMetadata.getDefaultCampaignForClubDonations()]; 
			}
			catch(Exception e){}
		}
		
		// really... something bad must be on the website
		return [select id from Campaign where id = :QCacheWebsiteMetadata.getDefaultCampaignForDonations()];
	}
	
	public list<String> lstStates
	{
		get
		{
			return new list<String>
			{
				'ACT', 'NSW', 'NT', 'QLD', 'SA', 'TAS', 'WA', 'VIC', 'Outside Australia'
			};
		}
		set;
	}
	
	public list<String> lstTitles
	{
		get
		{
			return new list<String>
			{
				'Dr', 'Lady', 'Miss', 'Mr', 'Mrs', 'Ms', 'Prof', 'Sir'
			};
		}
		set;
	}
	
	public list<String> lstMonths
	{
		get
		{
			return new list<String>
			{
				'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dev'
			};
		}
		set;
	}

	public list<String> lstCountries
	{
		get
		{
			return new list<String>
			{
				'Afghanistan','Albania','Algeria','American Samoa','Andorra',
				'Angola','Anguilla','Antigua & Barbuda','Argentina','Armenia',
				'Aruba','Australia','Austria','Azerbaijan','Bahamas','Bahrain',
				'Bangladesh','Barbados','Belarus','Belgium','Belize','Benin',
				'Bermuda','Bhutan','Bolivia','Bonaire','Bosnia & Herzegovina',
				'Botswana','Brazil','British Indian Ocean Ter','Brunei','Bulgaria',
				'Burkina Faso','Burundi','Cambodia','Cameroon','Canada',
				'Canary Islands','Cape Verde','Cayman Islands','Central African Republic','Chad',
				'Channel Islands','Chile','China','Christmas Island','Cocos Island',
				'Colombia','Comoros','Congo','Cook Islands','Costa Rica','Cote DIvoire',
				'Croatia','Cuba','Curaco','Cyprus','Czech Republic','Denmark',
				'Djibouti','Dominica','Dominican Republic','East Timor','Ecuador',
				'Egypt','El Salvador','Equatorial Guinea','Eritrea','Estonia',
				'Ethiopia','Falkland Islands','Faroe Islands','Fiji','Finland',
				'France','French Guiana','French Polynesia','French Southern Ter','Gabon',
				'Gambia','Georgia','Germany','Ghana','Gibraltar','Great Britain',
				'Greece','Greenland','Grenada','Guadeloupe','Guam','Guatemala',
				'Guinea','Guyana','Haiti','Hawaii','Honduras','Hong Kong',
				'Hungary','Iceland','India','Indonesia','Iran','Iraq',
				'Ireland','Isle of Man','Israel','Italy','Jamaica',
				'Japan','Jordan','Kazakhstan','Kenya','Kiribati','Korea North',
				'Korea South','Kuwait','Kyrgyzstan','Laos','Latvia','Lebanon',
				'Lesotho','Liberia','Libya','Liechtenstein','Lithuania',
				'Luxembourg','Macau','Macedonia','Madagascar','Malaysia',
				'Malawi','Maldives','Mali','Malta','Marshall Islands',
				'Martinique','Mauritania','Mauritius','Mayotte','Mexico',
				'Midway Islands','Moldova','Monaco','Mongolia','Montserrat',
				'Morocco','Mozambique','Myanmar','Nambia','Nauru',
				'Nepal','Netherland Antilles','Netherlands','Nevis','New Caledonia',
				'New Zealand','Nicaragua','Niger','Nigeria','Niue',
				'Norfolk Island','Norway','Oman','Pakistan','Palau Island',
				'Palestine','Panama','Papua New Guinea','Paraguay','Peru',
				'Phillipines','Pitcairn Island','Poland','Portugal','Puerto Rico',
				'Qatar','Republic of Montenegro','Republic of Serbia','Reunion','Romania',
				'Russia','Rwanda','St Barthelemy','St Eustatius','St Helena',
				'St Kitts-Nevis','St Lucia','St Maarten','St Pierre & Miquelon','St Vincent & Grenadines',
				'Saipan','Samoa','Samoa American','San Marino','Sao Tome & Principe',
				'Saudi Arabia','Senegal','Seychelles','Sierra Leone','Singapore',
				'Slovakia','Slovenia','Solomon Islands','Somalia','South Africa',
				'Spain','Sri Lanka','Sudan','Suriname','Swaziland','Sweden',
				'Switzerland','Syria','Tahiti','Taiwan','Tajikistan',
				'Tanzania','Thailand','Togo','Tokelau','Tonga','Trinidad & Tobago',
				'Tunisia','Turkey','Turkmenistan','Turks & Caicos Is','Tuvalu',
				'Uganda','Ukraine','United Arab Erimates','United Kingdom','United States of America',
				'Uraguay','Uzbekistan','Vanuatu','Vatican City State','Venezuela',
				'Vietnam','Virgin Islands (Brit)','Virgin Islands (USA)','Wake Island',
				'Wallis & Futana Is','Yemen','Zaire','Zambia','Zimbabwe'
			};
		}
		set;
	}
}