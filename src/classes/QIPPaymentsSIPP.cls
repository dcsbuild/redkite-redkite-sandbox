public with sharing class QIPPaymentsSIPP
{
    public class TokeniseCreditCard_element 
    {
        public String tokeniseCreditCardXML;
        private String[] tokeniseCreditCardXML_type_info = new String[]{'tokeniseCreditCardXML','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ippayments.com.au/interface/api/sipp','true','false'};
        private String[] field_order_type_info = new String[]{'tokeniseCreditCardXML'};
    }
    
    public class TokeniseCreditCardResponse_element 
    {
        public String TokeniseCreditCardResult;
        private String[] TokeniseCreditCardResult_type_info = new String[]{'TokeniseCreditCardResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.ippayments.com.au/interface/api/sipp','true','false'};
        private String[] field_order_type_info = new String[]{'TokeniseCreditCardResult'};
    }
    
    public class sippSoap
    { 
        public String endpoint_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        
        private String[] ns_map_type_info = new String[]{'http://www.ippayments.com.au/interface/api/sipp', 'QIPPaymentsSIPP'};
        
        public Integer timeout_x;
        
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        
		public sippSoap(string strEndPoint)
		{
			endpoint_x = strEndPoint;
		}
        
        public String TokeniseCreditCard(String tokeniseCreditCardXML) 
        {
            QIPPaymentsSIPP.TokeniseCreditCard_element request_x = new QIPPaymentsSIPP.TokeniseCreditCard_element();
            QIPPaymentsSIPP.TokeniseCreditCardResponse_element response_x;
            request_x.tokeniseCreditCardXML = tokeniseCreditCardXML;
            Map<String, QIPPaymentsSIPP.TokeniseCreditCardResponse_element> response_map_x = new Map<String, QIPPaymentsSIPP.TokeniseCreditCardResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.ippayments.com.au/interface/api/sipp/TokeniseCreditCard',
              'http://www.ippayments.com.au/interface/api/sipp',
              'TokeniseCreditCard',
              'http://www.ippayments.com.au/interface/api/sipp',
              'TokeniseCreditCardResponse',
              'QIPPaymentsSIPP.TokeniseCreditCardResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.TokeniseCreditCardResult;
        }
    }
    
    public static testMethod void testIPPSIPP()
    {
    	TokeniseCreditCardResponse_element oRet = new TokeniseCreditCardResponse_element();
    	
    	sippSoap oSOAP = new sippSoap('test.salesforce.com');
    	
    	try{oSOAP.TokeniseCreditCard('<test>Im testing something</test>');}catch(Exception e){}
    }
}