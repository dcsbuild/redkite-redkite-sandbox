global class QSoftCreditRollupBatch implements Database.Batchable<SObject>, Schedulable, Database.Stateful 
{
	/*
		Description : This batch calculate total Soft Credit
	
	*/
	public class DonationInformatoin
	{
		public decimal 	dSoftCredit;
		
		public DonationInformatoin()
		{
		}
	}
	
	global map<id, string> mapAccountIdToError;
	
	global QSoftCreditRollupBatch()
	{	
		mapAccountIdToError = new map<id, string>();
	}
	
	global void execute(SchedulableContext SC) 
	{
		Database.executeBatch(new QSoftCreditRollupBatch());
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery = 	'select 	id, Last_Donation_Amount__c, Last_Donation_Date__c, First_Donation_Date__c, All_Contributions__c, Donations_Total__c, ' +
							' Donations_This_Year__c, Donations_Last_Year__c, Soft_Credits__c from Account';
		
		if(Test.isRunningTest())
		{
				strQuery += ' limit 20';
		}
		return Database.getQueryLocator(strQuery);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		// Get Current Year 
		date dtThisYearStart = date.newInstance(system.today().year(), 1, 1);
		date dtThisYearEnd = date.newInstance(system.today().year(), 12, 31);
		// Get Last Year
		date dtLastYearStart = dtThisYearStart.addYears(-1);
		date dtLastYearEnd = date.newInstance(dtLastYearStart.year(), 12, 31);
		
		map<string, DonationInformatoin>	mapAccountIdToDonationInfo 	= new map<string, DonationInformatoin>(); 
		map<string, Account> 				mapIdToAccount 				= new map<string, Account>();
		list<Account>						liAccountUpdate				= new list<Account>();
		map<integer, string>				mapLocationToID				= new map<integer, string>(); 
		
		for(Account sAccount : (list<Account>)scope )
		{
			mapIdToAccount.put(sAccount.id, sAccount);
		}
		// Get Income
		for(Income__c sIncome : [select Id, 
										Total_Amount__c,
										Opportunity__r.Soft_Credit__c,
										Account_Code__c,
										All_Income_Processed_Date__c
								from 	Income__c 
								where 	Declined__c = false 
								and 	All_Income_Processed__c != null 
								and 	All_Income_Processed_Date__c != null
								and 	Opportunity__r.Soft_Credit__c in : mapIdToAccount.keyset()
								and 	RecordType.DeveloperName not in ('Income_Refund_Payment_Gateway','Club_Red_Refund') 
								order by All_Income_Processed_Date__c desc])
		{
			system.debug('sIncome:' + sIncome);
			// Count Total Donation
			if(!mapAccountIdToDonationInfo.containskey(sIncome.Opportunity__r.Soft_Credit__c))
			{
				DonationInformatoin sDonationInformatoin 	= new DonationInformatoin();
				sDonationInformatoin.dSoftCredit 			= sIncome.Total_Amount__c;
				mapAccountIdToDonationInfo.put(sIncome.Opportunity__r.Soft_Credit__c, sDonationInformatoin);
				
			}
			else
			{
				DonationInformatoin sDonationInformatoin = mapAccountIdToDonationInfo.get(sIncome.Opportunity__r.Soft_Credit__c);
				sDonationInformatoin.dSoftCredit += sIncome.Total_Amount__c;
				mapAccountIdToDonationInfo.put(sIncome.Opportunity__r.Soft_Credit__c, sDonationInformatoin);
			}
		}
		
		// Now check is Account value is changes or now. 
		for(Account sAccount : mapIdToAccount.values())
		{
			boolean bChanged = false;
			
			
			// Checked Total Donation is changed
			if(	mapAccountIdToDonationInfo.containskey(sAccount.id))
			{
				DonationInformatoin sDonationInformatoin = mapAccountIdToDonationInfo.get(sAccount.id);
				
				if((sAccount.Soft_Credits__c == null && sDonationInformatoin.dSoftCredit != null) || (sDonationInformatoin.dSoftCredit != sAccount.Soft_Credits__c))
				{
					bChanged = true;
					sAccount.Soft_Credits__c = sDonationInformatoin.dSoftCredit;
				}
			}
			
			if(bChanged)
			{
				liAccountUpdate.add(sAccount);
				mapLocationToID.put(liAccountUpdate.size(), sAccount.id);
			}
		}
		
		//execute database changes
		Database.SaveResult[] arrSR = Database.Update(liAccountUpdate, false);
		
		for(integer i = 0; i < arrSR.size(); i++)
		{
			// Find out which one not processed. 
			if(!arrSR[i].isSuccess())
			{
				string strError = '';
                for(Database.Error sError : arrSR[i].getErrors())
                {
                    strError += sError.getMessage() + ' ';
                }
				
				mapAccountIdToError.put(mapLocationToID.get(i), strError);
			}
		}
	}
	
	global void finish(Database.BatchableContext BC)
	{
		if(!mapAccountIdToError.isEmpty() || test.isRunningTest())
		{
			string strBody = '';
			strBody = '<table><tr><th>Account Id</th><th>Error</th></tr>';
    		
    		for(string strKey : mapAccountIdToError.keyset())
    		{
    			strBody += '<tr><td>' + strKey + '</td><td>' + mapAccountIdToError.get(strKey) + '</td></tr>';
    		}
    		
    		strBody += '</table>';
    		
			system.debug('Email Body:' + strBody);
			
    		User  sUsers = 	[
								select id
								from User
								where Profile.Name = 'System Administrator' 
								and IsActive = true 
								and Send_Fatal_Error_Processing_Email__c = true
								order by CreatedDate
								limit 1
							];
		
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           mail.setTargetObjectId(sUsers.id);
           mail.setBccSender(false);
           mail.setUseSignature(false);
           mail.setSubject('Account Update fail');
           mail.setHtmlBody(strBody);
           mail.saveAsActivity = false;
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
						
		}

		system.debug('Finished');
	}

	private static testmethod void myUnitTest()
	{
		Account sAccount 	= new Account();
		sAccount.Name 		= 'Test Account';
		insert sAccount;
		
		Account sAccount1 	= new Account();
		sAccount1.Name 		= 'Test Account';
		insert sAccount1;
		
		Opportunity sOpportunity	= new Opportunity();
		sOpportunity.Name 			= 'Test Method Opty 3';
		sOpportunity.StageName     	='Club Red Ongoing';
		sOpportunity.CloseDate		= System.Today().addDays(30);
		sOpportunity.AccountId		= sAccount.Id;
		sOpportunity.Amount			= 500;
		sOpportunity.Soft_Credit__c	= sAccount1.id;
		insert sOpportunity;

		Income__c sIncome				= new Income__c();
		sIncome.Amount_A__c				= 500;
		sIncome.Opportunity__c			= sOpportunity.Id;
		sIncome.All_Income_Processed__c = true;
		insert sIncome;
		
		Income__c sIncome1				= new Income__c();
		sIncome1.Amount_A__c			= 500;
		sIncome1.Opportunity__c			= sOpportunity.Id;
		sIncome1.All_Income_Processed__c = true;
		insert sIncome1;
		
		Opportunity sOpportunity1		= new Opportunity();
		sOpportunity1.Name 				= 'Test Method Opty 3';
		sOpportunity1.StageName     	='Club Red Ongoing';
		sOpportunity1.CloseDate			= System.Today().addDays(30);
		sOpportunity1.AccountId			= sAccount.Id;
		sOpportunity1.Amount			= 500;
		insert sOpportunity1;

		Income__c sIncome2					= new Income__c();
		sIncome2.Amount_A__c				= 700;
		sIncome2.Opportunity__c				= sOpportunity1.Id;
		sIncome2.All_Income_Processed__c 	= true;
		insert sIncome2;
		
		Test.startTest();
		Database.executeBatch(new QSoftCreditRollupBatch());
		Test.stopTest();
	}

}