public class QVFInventoryManagement 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes
	public class QVFScreenParameterException 	extends Exception{}
	public class QVFDataValidationException 	extends Exception{}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Inner Inventory Class
	public class QInventory
	{
		list<selectOption> 		liAmount;
		
		public boolean 			Checked{get; set;}
		public Inventory__c 	Item{get; set;}
		public string 			Amount{get; set;}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		public QInventory(Inventory__c sInventory)
		{
			this.Checked	= false;
			this.Item 		= sInventory;
						
			// lets create a drop down list of the amounts
			liAmount = new list<selectOption>();
			
			for(integer i = 1; i <= sInventory.Current_Units_Available__c && i < 999; i++)
			{
				liAmount.add(new selectOption(string.valueOf(i), string.valueOf(i)));
			}
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		public string getExpiryDateFormated()
		{
			string strRet = '';
			
			if(Item.Use_By_Date__c != null)
			{
				strRet = Item.Use_By_Date__c.Day() + '-' + Item.Use_By_Date__c.Month() + '-' + Item.Use_By_Date__c.Year();
			}
			
			return strRet;
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		public list<selectOption> getAmountList()
		{
			return liAmount;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	public id		Id{get; private set;}
	public string 	Origin{get; private set;}
	public string 	EntityName{get; set;}
	public integer 	NumberOfItems{get; set;}
	
	public string 	InventoryLocation{get; set;}
	public string 	InventoryType{get; set;}
	
	private final string STR_ORIGIN_CAMPAIGN 						= 'Campaign';
	private final string STR_ORIGIN_ACCOUNT 						= 'Account';
	private final string STR_ORIGIN_CONTACT 						= 'Contact';
	private final string STR_ORIGIN_OPPORTUNITY 					= 'Opportunity';
	private final string STR_ORIGIN_CASE 							= 'Case';
	private final string STR_ORIGIN_PURCHASE_ORDER 					= 'PurchaseOrder';
	private final string STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY 	= 'BudgetAllocationVoucherOnly';
	
	private integer m_iSOQLLimit;
	
	////////////////////////////////////////////
	// Collections
	list<QInventory> 		m_liInventoryItems;
	list<Resource__c> 		m_liResource;
	list<PurchaseOrder__c> 	m_liPurchaseOrder;
	list<Task> 				m_liTask;
	
	list<selectOption>		m_liInventoryLocationOption;
	list<selectOption>		m_liInventoryTypeOption;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Constructor
	public QVFInventoryManagement(integer iLimit)
	{
		m_iSOQLLimit = iLimit;
		
		init();
	}
	
	public QVFInventoryManagement()
	{
		m_iSOQLLimit = 5000;
		init();
	}
	
	private void init()
	{
		// lets grab the screen parameters
		if(null == (Id = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new QVFScreenParameterException('Id Parameter not found');	
		}
		
		if(null == (Origin = ApexPages.currentPage().getParameters().get('origin')))
		{
			throw new QVFScreenParameterException('Origin Parameter not found');	
		}
		
		if(		Origin != STR_ORIGIN_CAMPAIGN
			&& 	Origin != STR_ORIGIN_OPPORTUNITY
			&& 	Origin != STR_ORIGIN_ACCOUNT
			&& 	Origin != STR_ORIGIN_CASE
			&& 	Origin != STR_ORIGIN_PURCHASE_ORDER
			&& 	Origin != STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY
			&&	Origin != STR_ORIGIN_CONTACT)
		{
			throw new QVFScreenParameterException('Origin Parameter \''+Origin+'\' not available');
		}
		
		// init all collections
		m_liInventoryItems 		= new list<QInventory>();
		m_liResource 			= new list<Resource__c>();
		m_liPurchaseOrder		= new list<PurchaseOrder__c>();
		m_liTask				= new list<Task>();
	}
	
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	
	public boolean getShowReturnTracked()
	{
		return Origin == STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY ? false : true;
	}
	
	public boolean getShowVoucherValue()
	{
		return Origin == STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY ? true : false;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	List Return
	public list<QInventory> getInventoryItems()
	{
		return m_liInventoryItems;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Search Entry Point - Will cause AJAX refresh of Inventory Table
	public PageReference SearchInventory()
	{
		m_liInventoryItems.clear();
		
		LoadInventory();
		
		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Popup window to Inventory Item
	public PageReference RedirectToSourceObject()
	{
		PageReference pageRedirect = new PageReference('/' + this.Id);
		pageRedirect.setRedirect(true);
		
		return pageRedirect;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Allocation
	public PageReference AllocateInventory()
	{
		this.NumberOfItems = 0;
		
		if(Origin != STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY)
		{
			BuildAndInsertResourceObjects();
		}
		else
		{
			BuildAndInsertVoucherResourceAndPurchaseOrderObjects();
		}
		
		m_liInventoryItems.clear();	// clear the inventory as we have allocated some items
		LoadInventory();			// and reload it
		
		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Vourcher Logic (Resourch & Purchase Order Objects)
	private void BuildAndInsertVoucherResourceAndPurchaseOrderObjects()
	{
		// first lets make a savepoint just incase something goes wrong
		Savepoint dbSavePoint = Database.setSavepoint();
		
		integer iPurchaseOrders = 0, iVouchers = 0;
		
		for(integer i = 0; i < m_liInventoryItems.size(); i++)
		{
			if(m_liInventoryItems[i].Checked)
			{
				PurchaseOrder__c sPurchaseOrder = new PurchaseOrder__c();
						
				sPurchaseOrder.Budget_Allocation__c 		= this.Id;
				
				if(m_liInventoryItems[i].Item.Inventory_Type__c == 'Food Voucher')
				{				
					sPurchaseOrder.Purchase_Order_Type__c 		= 'Food Voucher';
					sPurchaseOrder.Purchase_Order_Subtype__c 	= 'Food Vouchers';
				}
				else if(m_liInventoryItems[i].Item.Inventory_Type__c == 'Fuel Voucher')
				{
					sPurchaseOrder.Purchase_Order_Type__c 		= 'Fuel Voucher';
					sPurchaseOrder.Purchase_Order_Subtype__c 	= 'Fuel Vouchers';
				}
				else
				{
					// not sure what i should do here to be honest
				}
				
				sPurchaseOrder.Invoice_Amount__c			= m_liInventoryItems[i].Item.Voucher_Value__c;
				sPurchaseOrder.PO_Amount__c					= m_liInventoryItems[i].Item.Voucher_Value__c;
				
				sPurchaseOrder.Status__c 					= 'Voucher Allocation';
				
				m_liPurchaseOrder.add(sPurchaseOrder);
				
				Resource__c sResource = new Resource__c();
				
				sResource.Inventory_Item__c 		= m_liInventoryItems[i].Item.Id;
				sResource.Inventory_Type__c 		= m_liInventoryItems[i].Item.Inventory_Type__c;
				sResource.Units_Allocated__c 		= integer.valueOf(m_liInventoryItems[i].Amount);
				
				if(m_liInventoryItems[i].Item.Automatic_Return_Task__c)
				{
					sResource.Scheduled_Return_Date__c 	= m_liInventoryItems[i].Item.Next_Return_Date__c;
					
					Task sTask = new Task();
					
					sTask.Subject 		= 'Check For Item\'s Return';
					sTask.Status 		= 'Not Started';
					sTask.Type 			= 'Inventory Follow Up';
					sTask.WhatId		= m_liInventoryItems[i].Item.Id;
					sTask.ActivityDate 	= m_liInventoryItems[i].Item.Next_Return_Date__c;
					
					m_liTask.add(sTask);
				}
				
				m_liResource.add(sResource);
				
				this.NumberOfItems++;
			}
		}
		
		try
		{
			Database.SaveResult[] dbPurchaseOrderInsertResult = Database.insert(m_liPurchaseOrder, true);
			
			for(integer i = 0; i < dbPurchaseOrderInsertResult.size(); i++)
			{
				if(dbPurchaseOrderInsertResult[i].IsSuccess())
				{
					iPurchaseOrders++;
					m_liResource[i].Purchase_Order__c = m_liPurchaseOrder[i].Id;
				}
				else
				{
					throw new QVFDataValidationException('Purchase Order Creation Error - ' + dbPurchaseOrderInsertResult[i].getErrors());
				}
			}
			
			Database.SaveResult[] dbResourceInsertResult = Database.insert(m_liResource, true);
			
			for(integer i = 0; i < dbResourceInsertResult.size(); i++)
			{
				if(!dbResourceInsertResult[i].IsSuccess())
				{
					throw new QVFDataValidationException('Resource Creation Error - ' + dbResourceInsertResult[i].getErrors());
				}
				else
				{
					iVouchers++;
				}
			}
			
			insert m_liTask;
			
		}
		catch(Exception e)
		{
			// roll it back
			Database.rollback(dbSavePoint);
			
			System.Debug('Exception thrown - ' + e);
			
			ApexPages.addMessages(e);
		}
		
		m_liPurchaseOrder.clear();
		m_liResource.clear();
		m_liTask.clear();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	private void BuildAndInsertResourceObjects()
	{
		// first lets make a savepoint just incase something goes wrong
		Savepoint dbSavePoint = Database.setSavepoint();
		
		for(integer i = 0; i < m_liInventoryItems.size(); i++)
		{
			if(m_liInventoryItems[i].Checked)
			{
				Resource__c sResource = new Resource__c();
				
				sResource.Inventory_Item__c 		= m_liInventoryItems[i].Item.Id;
				sResource.Inventory_Type__c 		= m_liInventoryItems[i].Item.Inventory_Type__c;
				sResource.Units_Allocated__c 		= integer.valueOf(m_liInventoryItems[i].Amount);
				
				if(Origin == STR_ORIGIN_CAMPAIGN)
				{
					sResource.Campaign__c = this.Id;
				}
				else if(Origin == STR_ORIGIN_OPPORTUNITY)
				{
					sResource.Opportunity__c = this.Id;
				}
				else if(Origin == STR_ORIGIN_ACCOUNT)
				{
					sResource.Account__c = this.Id;
				}
				else if(Origin == STR_ORIGIN_CONTACT)
				{
					sResource.Contact__c = this.Id;
				}
				else if(Origin == STR_ORIGIN_CASE)
				{
					sResource.Case__c = this.Id;
				}
				else if(Origin == STR_ORIGIN_PURCHASE_ORDER)
				{
					sResource.Purchase_Order__c = this.Id;
				}
				
				if(m_liInventoryItems[i].Item.Automatic_Return_Task__c)
				{
					sResource.Scheduled_Return_Date__c 	= m_liInventoryItems[i].Item.Next_Return_Date__c;
					
					Task sTask = new Task();
					
					sTask.Subject 		= 'Check For Item\'s Return';
					sTask.Status 		= 'Not Started';
					sTask.Type 			= 'Inventory Follow Up';
					sTask.WhatId		= m_liInventoryItems[i].Item.Id;
					sTask.ActivityDate 	= m_liInventoryItems[i].Item.Next_Return_Date__c;
					
					m_liTask.add(sTask);
				}
				
				m_liResource.add(sResource);
				
				this.NumberOfItems++;
			}
		}
		
		try
		{
			Database.SaveResult[] dbResourceInsertResult = Database.insert(m_liResource, true);
			
			for(integer i = 0; i < dbResourceInsertResult.size(); i++)
			{
				if(!dbResourceInsertResult[i].IsSuccess())
				{
					throw new QVFDataValidationException('Resource Creation Error - ' + dbResourceInsertResult[i].getErrors());
				}
			}
			
			insert m_liTask;
		}
		catch(Exception e)
		{
			// roll it back
			Database.rollback(dbSavePoint);
			
			System.Debug('Exception thrown - ' + e);
			
			ApexPages.addMessages(e);	
		}
		
		m_liResource.clear();
		m_liTask.clear();
	}
	
	/***********************************************************************************************************
    	LIST METHODS
    ***********************************************************************************************************/
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
    public list<selectOption> getInventoryLocationList()
    {
    	if(m_liInventoryLocationOption == null)
    	{
    		m_liInventoryLocationOption = new list<selectOption>();
    		
    		schema.DescribeFieldResult schemaDescribeResults = Inventory__c.Location__c.getDescribe();
    		list <Schema.PicklistEntry> liPickListValues = schemaDescribeResults.getPickListValues();
    		
    		m_liInventoryLocationOption.add(new selectOption('-', '-'));
    		
    		for(integer i = 0; i < liPickListValues.size(); i++)
    		{
    			if(liPickListValues[i].isActive())
    			{
    				m_liInventoryLocationOption.add(new selectOption(liPickListValues[i].getLabel(), 	liPickListValues[i].getValue()));
    			}
    		}
    	}
    	return m_liInventoryLocationOption;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
    public list<selectOption> getInventoryTypeList()
    {
    	if(m_liInventoryTypeOption == null)
    	{
    		m_liInventoryTypeOption = new list<selectOption>();
    		
    		if(Origin != STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY)
    		{
	    		schema.DescribeFieldResult schemaDescribeResults = Inventory__c.Inventory_Type__c.getDescribe();
	    		list <Schema.PicklistEntry> liPickListValues = schemaDescribeResults.getPickListValues();
	    		
	    		m_liInventoryTypeOption.add(new selectOption('-', '-'));
	    		
	    		for(integer i = 0; i < liPickListValues.size(); i++)
	    		{
	    			if(liPickListValues[i].isActive())
	    			{
	    				m_liInventoryTypeOption.add(new selectOption(liPickListValues[i].getLabel(), 	liPickListValues[i].getValue()));
	    			}
	    		}
    		}
    		else
    		{
    			m_liInventoryTypeOption.add(new selectOption('-', '-'));
    			m_liInventoryTypeOption.add(new selectOption('Fuel Voucher', 'Fuel Voucher'));
    			m_liInventoryTypeOption.add(new selectOption('Food Voucher', 'Food Voucher'));
    		}
    	}
    	return m_liInventoryTypeOption;
    }
    
    /***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	private void LoadInventory()
	{
		boolean bLocation = false, bType = false;
		
		for(Inventory__c[] arrInventory : [
										select 	id, 
												Name,
												Inventory_Id__c,
												Inventory_Type__c,
												Inventory_Sub_Type__c,
												Use_By_Date__c,
												Location__c,
												Automatic_Return_Task__c,
												Default_Return_Period__c,
												Next_Return_Date__c,
												Current_Units_Available__c,
												Voucher_Value__c,
												Description__c
										from 	Inventory__c
										where	(
													Use_By_Date__c >= :System.Today()
												or	Use_By_Date__c = null
												)
										and		Status__c = 'Available'
										and		Current_Units_Available__c > 0
										limit 	:m_iSOQLLimit
									])
		{
			for(Inventory__c sInventory : arrInventory)
			{
				if(m_liInventoryItems.size() < 1000)
				{
					if(		((Origin == STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY && sInventory.Inventory_Type__c == 'Food Voucher') || (Origin == STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY && sInventory.Inventory_Type__c == 'Fuel Voucher'))
						||	(Origin != STR_ORIGIN_BUDGET_ALLOCATION_VOUCHER_ONLY && sInventory.Inventory_Type__c <> 'Food Voucher'))
	    			{
		    			if(		 InventoryLocation == '-'
							||	(InventoryLocation != '-' && InventoryLocation == sInventory.Location__c))
						{
							bLocation = true;
						}
						if(		 InventoryType == '-'
							||	(InventoryType != '-' && InventoryType == sInventory.Inventory_Type__c))
						{
							bType = true;
						}
	    			}
					
					if(bLocation && bType)
					{
						QInventory thisInv = new QInventory(sInventory);
						m_liInventoryItems.add(thisInv);
					}
		
					bLocation 	= false;
					bType 		= false;
				}
			}
		}
	}
	
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testmethod void testInventoryParamException()
	{
		PageReference pageRef = new PageReference('/apex/Inventory_Management');
		Test.setCurrentPage(pageRef);
		
		QVFIncomeManagement controller;
		
		boolean bExceptionThrown = false;
		
		
		Account thisAccount = new Account();
		thisAccount.LastName = 'Test Account';
		insert thisAccount;
		
		// break it due to no params
		try
		{
			controller = new QVFIncomeManagement();
		}
		catch(Exception e)
		{
			bExceptionThrown = true;
		}
		
		System.AssertEquals(true, bExceptionThrown);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testmethod void testListMethods()
	{
		Account thisAccount = new Account();
		thisAccount.Name = 'Test Account';
		insert thisAccount;
		
		PageReference pageRef = new PageReference('/apex/Inventory_Management');
		Test.setCurrentPage(pageRef);
		
		QVFInventoryManagement controller;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', thisAccount.Id);
		ApexPages.currentPage().getParameters().put('origin', 'Account');
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFInventoryManagement(100);
		
		controller.getInventoryLocationList();
		controller.getInventoryTypeList();
		
		controller.getInventoryItems();
		controller.RedirectToSourceObject();
		controller.SearchInventory();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testmethod void testInventoryManagementAccount()
	{
		Account thisAccount = new Account();
		thisAccount.Name = 'Test Account';
		insert thisAccount;
		
		PageReference pageRef = new PageReference('/apex/Inventory_Management');
		Test.setCurrentPage(pageRef);
		
		QVFInventoryManagement controller;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', thisAccount.Id);
		ApexPages.currentPage().getParameters().put('origin', 'Account');
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFInventoryManagement(100);
		
		controller.getInventoryLocationList();
		controller.getInventoryTypeList();

		controller.InventoryLocation	= '-';
		controller.InventoryType		= '-';
		
		controller.SearchInventory();
		
		for(integer i = 0; i < controller.m_liInventoryItems.size() && i < 5; i++)
		{
			controller.m_liInventoryItems[i].Checked = true;
			controller.m_liInventoryItems[i].Amount = '1';
		}
		
		controller.AllocateInventory();
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static testmethod void testInventoryBudgetAllocation()
	{
		// lets get the record type we need to use for the Budget_Allocation__c Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Budget_Allocation__c.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
		
		// create person accoutn
		Account sPersonAccount = new Account();
		
		sPersonAccount.LastName = 'Test Person Account';
		sPersonAccount.FA_Commencement__pc = Date.newInstance(System.Today().Year(), 1, 1);
		
		insert sPersonAccount;
		
		sPersonAccount = [select id, PersonContactId from Account where id = :sPersonAccount.Id];
		
		// now the budget allocation
		Budget_Allocation__c sBudgetAllocation = new Budget_Allocation__c();
		sBudgetAllocation.Diagnosed_Person__c	= sPersonAccount.PersonContactId;
		sBudgetAllocation.RecordTypeId 			= mapRecordTypeName.get('Fin Assist BillsVouchers').getRecordTypeId();
		
		sBudgetAllocation.Approval_Hidden__c	= true;
		sBudgetAllocation.Status__c				= 'Approved';
		sBudgetAllocation.Approved_Amount__c	= 1000;
		sBudgetAllocation.Rejected_Amount__c	= 0;
		sBudgetAllocation.Budget_Amount__c		= 1000;
		
		insert sBudgetAllocation;		
		
		PageReference pageRef = new PageReference('/apex/Inventory_Management');
		Test.setCurrentPage(pageRef);
		
		QVFInventoryManagement controller;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', sBudgetAllocation.Id);
		ApexPages.currentPage().getParameters().put('origin', 'BudgetAllocationVoucherOnly');
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFInventoryManagement(100);
		
		controller.getInventoryLocationList();
		controller.getInventoryTypeList();
		
		controller.InventoryLocation	= '-';
		controller.InventoryType		= '-';
				
		controller.SearchInventory();
		
		for(integer i = 0; i < controller.m_liInventoryItems.size() && i < 5; i++)
		{
			controller.m_liInventoryItems[i].Checked = true;
			controller.m_liInventoryItems[i].Amount = '1';
		}
		
		controller.AllocateInventory();
		
	}
	
}