public class QVFRelationshipTimeline 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class QVFScreenParameterException 	extends Exception{}
	public class QVFDataValidationException 	extends Exception{}
	
	////////////////////////////////////////////
	// Singular
	string m_strId;
	string m_strOrigin;
	
	string 		m_strXML;
	datetime	m_tmStartDate;
	boolean		m_bLoadedData;
	
	Account 	m_sAccount;
	Contact		m_sContact;
	
	////////////////////////////////////////////
	// Constructor
	public QVFRelationshipTimeline()
	{
		// lets grab the screen parameters
		if(null == (m_strId = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new QVFScreenParameterException('Id Parameter not found');	
		}
		
		if(null == (m_strOrigin = ApexPages.currentPage().getParameters().get('origin')))
		{
			throw new QVFScreenParameterException('Origin Parameter not found');	
		}
		
		LoadBaseEntityData();
		
		m_bLoadedData = false;
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	public string getStartDate()
	{
		if(!m_bLoadedData)
		{
			CreateTimelineXML();
		}
		
		return BuildTimeStamp(m_tmStartDate);
	}
	
	public string getXML()
	{
		if(!m_bLoadedData)
		{
			CreateTimelineXML();
		}
		
		return m_strXML;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private void CreateTimelineXML()
	{
		m_strXML = '';
		
		m_tmStartDate = System.Now();
		
		m_strXML += 'strXML = "<data>";\n';
		
		for(Relationship__c[] arrRelationship : [
													select 	id,
															AccountPersonAccount__c,
															AccountPersonAccount__r.Name,
															Contact__c,
															Contact__r.Name,
															Contact_at_Account__c,
															Redkite_Primary_Intro__c,
															CreatedDate,
															Relationship__c,
															Estimated_Start_Date__c,
															Estimated_End_Date__c
													from 	Relationship__c 
													where 	AccountPersonAccount__c = :m_strId 
													or 		Contact__c = :m_strId
												])
		{
			for(Relationship__c sRelationship : arrRelationship)
			{
				m_strXML += '\tstrXML += "<event start=\'';
				
				if(sRelationship.Estimated_Start_Date__c != null)
				{
					m_strXML += BuildDateStamp(sRelationship.Estimated_Start_Date__c);
					
					datetime tmEstStartDate = DateTime.newInstance(sRelationship.Estimated_Start_Date__c.Year(), sRelationship.Estimated_Start_Date__c.Month(), sRelationship.Estimated_Start_Date__c.Day(), 0, 0, 0);
										
					m_tmStartDate = tmEstStartDate < m_tmStartDate ? tmEstStartDate : m_tmStartDate;
				}
				else
				{
					m_strXML += BuildDateStamp(sRelationship.CreatedDate);

					m_tmStartDate = sRelationship.CreatedDate < m_tmStartDate ? sRelationship.CreatedDate : m_tmStartDate;
				}
					
				if(sRelationship.Contact__c != m_strId)
				{
					m_strXML += '\' title=\'' + sRelationship.Contact__r.Name.replace('\'', '') + '\'>&lt;a href=\''+ new PageReference('/'+ sRelationship.Id).getUrl() +'\' target=\'_blank\' &gt;' + sRelationship.Relationship__c + '&lt;/a&gt;</event>";\n';
				}
				else if(sRelationship.AccountPersonAccount__c != m_strId)
				{
					m_strXML += '\' title=\'' + sRelationship.AccountPersonAccount__r.Name.replace('\'', '') + '\'>&lt;a href=\''+ new PageReference('/'+ sRelationship.Id).getUrl() +'\' target=\'_blank\' &gt;' + sRelationship.Relationship__c + '&lt;/a&gt;</event>";\n';
				}
			}
		}
		
		if(m_sContact != null)
		{
			Schema.DescribeSObjectResult 		schemaDescribeIncome	= Income__c.SObjectType.getDescribe();
			map<id, Schema.RecordTypeInfo> 		mapRecordTypeIDIncome	= schemaDescribeIncome.getRecordTypeInfosById();
			
			for(Income__c[] arrIncome : [
											select 	id,
													Pro_Bono_Estimated_Value__c,
													GIK_Estimated_Value__c,
													Total_Amount__c,
													Volunteer_Value__c,
													RecordTypeId,
													CreatedDate
											from 	Income__c 
											where 	Opportunity__r.AccountId = :m_sAccount.Id 
										])
			{
				for(Income__c sIncome : arrIncome)
				{
					string strRTName = mapRecordTypeIDIncome.get(sIncome.RecordTypeId).getName();
					
					m_tmStartDate = sIncome.CreatedDate < m_tmStartDate ? sIncome.CreatedDate : m_tmStartDate;
					
					m_strXML += '\tstrXML += "<event start=\'';
					m_strXML += BuildDateStamp(sIncome.CreatedDate);
					
					m_strXML += '\' title=\'$';
					
					if(strRTName == 'Income' || strRTName == 'Club Red' || strRTName == 'Open Alms')
					{
						m_strXML += sIncome.Total_Amount__c;
					}
					else if(strRTName == 'Pro Bono')
					{
						m_strXML += sIncome.Pro_Bono_Estimated_Value__c;
					}
					else if(strRTName == 'Gifts In Kind')
					{
						m_strXML += sIncome.GIK_Estimated_Value__c;
					}
					else if(strRTName == 'Volunteering')
					{
						m_strXML += sIncome.Volunteer_Value__c;
					}
					
					m_strXML += '\'>&lt;a href=\''+ new PageReference('/'+ sIncome.Id).getUrl() +'\' target=\'_blank\' &gt;' + strRTName + '&lt;/a&gt;</event>";\n';
				}
			}
		}
		
		m_strXML += '\tstrXML += "</data>";';
		
		m_bLoadedData = true;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private void LoadBaseEntityData()
	{
		if(m_strOrigin == 'Account')
		{
			m_sAccount 	= [select id, Name from Account where id = :m_strId];
		}
		else if(m_strOrigin == 'Contact')
		{
			m_sContact 	= [select Id, Name, AccountId from Contact where id = :m_strId];
		}
		else if(m_strOrigin == 'PersonAccount')
		{
			m_sAccount = [select id from Account where id = :m_strId];
			m_sContact = [select Id, AccountId from Contact where AccountId = :m_sAccount.Id];
		}
		else
		{
			throw new QVFScreenParameterException('Unknown Origin Passed - ' + m_strOrigin);
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private string BuildTimeStamp(datetime tmEffecDate)
	{
		return tmEffecDate.format('MMM dd yyyy HH:mm:ss');
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private string BuildDateStamp(datetime tmEffecDate)
	{
		tmEffecDate = tmEffecDate.addHours(10); 
		return tmEffecDate.format('MMM dd yyyy');
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private string BuildDateStamp(date tmEffecDate)
	{
		datetime tmEffecDateTime = datetime.newInstance(tmEffecDate.Year(), tmEffecDate.Month(), tmEffecDate.Day(), 0, 0, 0);
		return tmEffecDateTime.format('MMM dd yyyy');
	}
	
	public static testmethod void testEnd2End()
	{
		PageReference pageRef = new PageReference('/apex/RelationshipTimeline');
		Test.setCurrentPage(pageRef);
		
		QVFRelationshipTimeline controller;

		Account thisAccount 	= new Account();
		thisAccount.Name 		= 'Test Account';
		insert thisAccount;
		
		Contact thisContact 	= new Contact();
		thisContact.AccountID 	= thisAccount.Id;
		thisContact.FirstName 	= 'Test';
		thisContact.LastName 	= 'CampaignMember';
		insert thisContact;
		
		Account thisAccount2 	= new Account();
		thisAccount2.Name 		= 'Test Account';
		insert thisAccount2;
		
		Relationship__c sRelationship = new Relationship__c();
		
		sRelationship.AccountPersonAccount__c 	= thisAccount2.Id;
		sRelationship.Contact__c 				= thisContact.Id;
		insert sRelationship;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', thisAccount2.Id);
		ApexPages.currentPage().getParameters().put('origin', 'Account');
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFRelationshipTimeline();
		
		controller.getStartDate();
		controller.getXML();

		
	}
}