global class QLastDonationBatch implements Database.Batchable<SObject>, Schedulable, Database.Stateful
{   
    /*
        Description : This batch calculate total donation amount and last donation amount
    
    */
    public class DonationInformatoin
    {
        public decimal  dTotal;
        public decimal  dLastDonation;
        public datetime dtLastDonationDate;
        public datetime dtFirstDonationDate;
        public decimal  dDonationsTotal;
        public decimal  dThisYearTotal;
        public decimal  dLastYearTotal;
        public decimal  dCurrentFYTotal;
        public decimal  dLastFYTotal;
        public decimal  dAppealDonationsTotal;
        public decimal  dDonationsPrevious2CY;
        
        public DonationInformatoin()
        {
            dAppealDonationsTotal   = 0;
            dDonationsPrevious2CY   = 0;
        }
    }
    
    global map<id, string> mapAccountIdToError;
    
    global QLastDonationBatch()
    {   
        mapAccountIdToError = new map<id, string>();
    }
    
    global void execute(SchedulableContext SC) 
    {
        Database.executeBatch(new QLastDonationBatch());
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        string  strQuery =  'select     id, Last_Donation_Amount__c, Last_Donation_Date__c, First_Donation_Date__c, All_Contributions__c, Donations_Total__c, ' +
                            ' Donations_This_Year__c, Donations_Last_Year__c, Soft_Credits__c, Club_Red_Current_FY__c, Club_Red_Last_FY__c, Appeal_Donations_Total__c,' +
                            ' Donations_Previous_2_CY__c from Account';
                            //' Donations_Previous_2_CY__c from Account where id =\'001W000000CUWO8\'';
        
        if(Test.isRunningTest())
        {
                strQuery += ' limit 20';
        }
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        // Get Current Year 
        date dtThisYearStart    = date.newInstance(system.today().year(), 1, 1);
        date dtThisYearEnd      = date.newInstance(system.today().year(), 12, 31);
        // Get Last Year
        date dtLastYearStart    = dtThisYearStart.addYears(-1);
        date dtLastYearEnd      = date.newInstance(dtLastYearStart.year(), 12, 31);
        
        date dtCurrentFYStart;
        date dtCurrentFYEnd;
        date dtLastFYStart;
        date dtLastFYEnd;
        date dtPrevious2CYStart = date.newInstance(system.today().addYears(-2).year(), 1, 1);
        date dtPrevious2CYEnd   = date.newInstance(system.today().addYears(-2).year(), 12, 31);
         
        system.debug('dtPrevious2CYStart:' + dtPrevious2CYStart + ' dtPrevious2CYEnd:' + dtPrevious2CYEnd);
        
        if(system.today().month() >= 7)
        {
            dtCurrentFYStart    = date.newInstance(system.today().year(), 7, 1);
            dtCurrentFYEnd      = date.newInstance(system.today().addYears(1).year(), 6, 30);
        }
        else
        {
            dtCurrentFYStart    = date.newInstance(system.today().addYears(-1).year(), 7, 1);
            dtCurrentFYEnd      = date.newInstance(system.today().year(), 6, 30);
        }
        // Get Last FY year.
        dtLastFYStart   = dtCurrentFYStart.addYears(-1);
        dtLastFYEnd     = dtCurrentFYEnd.addYears(-1);
        
        map<string, DonationInformatoin>    mapAccountIdToDonationInfo  = new map<string, DonationInformatoin>(); 
        map<string, Account>                mapIdToAccount              = new map<string, Account>();
        set<string>                         setAccountCode              = new set<string>{'240', '260', '245', '126', '120'};
        list<Account>                       liAccountUpdate             = new list<Account>();
        map<integer, string>                mapLocationToID             = new map<integer, string>(); 
        
        for(Account sAccount : (list<Account>)scope )
        {
            mapIdToAccount.put(sAccount.id, sAccount);
        }
        // Get Income
        for(Income__c sIncome : [select Id, 
                                        Total_Amount__c,
                                        Opportunity__r.Accountid,
                                        Account_Code__c,
                                        All_Income_Processed_Date__c
                                from    Income__c 
                                where   Declined__c = false 
                                and     All_Income_Processed__c != null 
                                and     All_Income_Processed_Date__c != null
                                and     Opportunity__r.Accountid in : mapIdToAccount.keyset()
                                //and   RecordType.DeveloperName not in ('Income_Refund_Payment_Gateway','Club_Red_Refund') 
                                order by All_Income_Processed_Date__c desc])
        {
            system.debug('sIncome:' + sIncome);
            // Count Total Donation
            if(!mapAccountIdToDonationInfo.containskey(sIncome.Opportunity__r.Accountid))
            {
                DonationInformatoin sDonationInformatoin = new DonationInformatoin();
                sDonationInformatoin.dTotal = sIncome.Total_Amount__c;
                mapAccountIdToDonationInfo.put(sIncome.Opportunity__r.Accountid, sDonationInformatoin);
                
            }
            else
            {
                DonationInformatoin sDonationInformatoin = mapAccountIdToDonationInfo.get(sIncome.Opportunity__r.Accountid);
                sDonationInformatoin.dTotal += sIncome.Total_Amount__c;
                mapAccountIdToDonationInfo.put(sIncome.Opportunity__r.Accountid, sDonationInformatoin);
            }
            
 			// Get Last Donation
            if(mapAccountIdToDonationInfo.get(sIncome.Opportunity__r.Accountid).dLastDonation == null && sIncome.Total_Amount__c > 0)
            {
            	// New criteria, also check if the last donation was in our set before pushing into the last donation field
            	if(setAccountCode.contains(sIncome.Account_Code__c))
            	{
                	mapAccountIdToDonationInfo.get(sIncome.Opportunity__r.Accountid).dLastDonation = sIncome.Total_Amount__c;
            	}
            }
            
            // Calculation Last Donation
            if(setAccountCode.contains(sIncome.Account_Code__c))
            {
                DonationInformatoin sDonationInformatoin = mapAccountIdToDonationInfo.get(sIncome.Opportunity__r.Accountid);
                // Current FY
                if(sIncome.Account_Code__c == '245')
                {
                    if( sIncome.All_Income_Processed_Date__c >= dtCurrentFYStart && 
                        sIncome.All_Income_Processed_Date__c <= dtCurrentFYEnd)
                    {
                        if(sDonationInformatoin.dCurrentFYTotal == null)
                        {
                            sDonationInformatoin.dCurrentFYTotal = sIncome.Total_Amount__c;
                        }
                        else
                        {
                            sDonationInformatoin.dCurrentFYTotal += sIncome.Total_Amount__c;
                        }
                    }
                    
                    if( sIncome.All_Income_Processed_Date__c >= dtLastFYStart && 
                        sIncome.All_Income_Processed_Date__c <= dtLastFYEnd)
                    {
                        if(sDonationInformatoin.dLastFYTotal == null)
                        {
                            sDonationInformatoin.dLastFYTotal = sIncome.Total_Amount__c;
                        }
                        else
                        {
                            sDonationInformatoin.dLastFYTotal += sIncome.Total_Amount__c;
                        }
                    }
                }
                
                
                // Appeal Donation
                if(sIncome.Account_Code__c == '260')
                {
                    sDonationInformatoin.dAppealDonationsTotal += sIncome.Total_Amount__c;
                }
                // Donations Previous 2CY
                
                if( sIncome.All_Income_Processed_Date__c >= dtPrevious2CYStart && 
                    sIncome.All_Income_Processed_Date__c <= dtPrevious2CYEnd)
                {
                    sDonationInformatoin.dDonationsPrevious2CY += sIncome.Total_Amount__c;
                }
                
                // First Donation
                if(sDonationInformatoin.dtFirstDonationDate == null)
                {
                    sDonationInformatoin.dtFirstDonationDate = sIncome.All_Income_Processed_Date__c;
                }
                else if(sDonationInformatoin.dtFirstDonationDate > sIncome.All_Income_Processed_Date__c)
                {
                    sDonationInformatoin.dtFirstDonationDate = sIncome.All_Income_Processed_Date__c;
                }
                // Last Donation
                if(sDonationInformatoin.dtLastDonationDate == null)
                {
                    sDonationInformatoin.dtLastDonationDate = sIncome.All_Income_Processed_Date__c;
                }
                else if(sDonationInformatoin.dtLastDonationDate < sIncome.All_Income_Processed_Date__c)
                {
                    sDonationInformatoin.dtLastDonationDate = sIncome.All_Income_Processed_Date__c;
                }
                // Donation Total           
                if(sDonationInformatoin.dDonationsTotal == null)
                {
                    sDonationInformatoin.dDonationsTotal = sIncome.Total_Amount__c;
                }
                else
                {
                    sDonationInformatoin.dDonationsTotal += sIncome.Total_Amount__c;
                }
                
                // This Year Donation
                if( sIncome.All_Income_Processed_Date__c >=  dtThisYearStart &&  sIncome.All_Income_Processed_Date__c <= dtThisYearEnd)
                {
                    if(sDonationInformatoin.dThisYearTotal == null)
                    {
                        sDonationInformatoin.dThisYearTotal = sIncome.Total_Amount__c;
                    }
                    else
                    {
                        sDonationInformatoin.dThisYearTotal += sIncome.Total_Amount__c;
                    }
                }
                // Last year donation
                if( sIncome.All_Income_Processed_Date__c >=  dtLastYearStart &&  sIncome.All_Income_Processed_Date__c <= dtLastYearEnd)
                {
                    if(sDonationInformatoin.dLastYearTotal == null)
                    {
                        sDonationInformatoin.dLastYearTotal = sIncome.Total_Amount__c;
                    }
                    else
                    {
                        sDonationInformatoin.dLastYearTotal += sIncome.Total_Amount__c;
                    }
                }
            }
        }
        
        // Now check is Account value is changes or now. 
        for(Account sAccount : mapIdToAccount.values())
        {
            boolean bChanged = false;
            system.debug('Before Change sAccount:' + sAccount);
            
            // Checked Total Donation is changed
            if( mapAccountIdToDonationInfo.containskey(sAccount.id))
            {
                DonationInformatoin sDonationInformatoin = mapAccountIdToDonationInfo.get(sAccount.id);
                system.debug('sDonationInformatoin:' + sDonationInformatoin);
                
                if( sDonationInformatoin.dDonationsPrevious2CY != 0 && 
                    sDonationInformatoin.dDonationsPrevious2CY != sAccount.Donations_Previous_2_CY__c)
                {
                    bChanged = true;
                    sAccount.Donations_Previous_2_CY__c = sDonationInformatoin.dDonationsPrevious2CY;
                }
                
                if( sDonationInformatoin.dAppealDonationsTotal != 0 && 
                    sDonationInformatoin.dAppealDonationsTotal != sAccount.Appeal_Donations_Total__c)
                {
                    bChanged = true;
                    sAccount.Appeal_Donations_Total__c = sDonationInformatoin.dAppealDonationsTotal;
                }
                
                if(sAccount.Club_Red_Current_FY__c != sDonationInformatoin.dCurrentFYTotal)
                {
                    bChanged = true;
                    sAccount.Club_Red_Current_FY__c = sDonationInformatoin.dCurrentFYTotal;
                }
                
                if(sAccount.Club_Red_Last_FY__c != sDonationInformatoin.dLastFYTotal)
                {
                    bChanged = true;
                    sAccount.Club_Red_Last_FY__c = sDonationInformatoin.dLastFYTotal;
                }
                
                if((sAccount.All_Contributions__c == null && sDonationInformatoin.dTotal != null) || (sDonationInformatoin.dTotal != sAccount.All_Contributions__c))
                {
                    bChanged = true;
                    sAccount.All_Contributions__c = sDonationInformatoin.dTotal;
                }
                
                if(sDonationInformatoin.dLastDonation != sAccount.Last_Donation_Amount__c)
                {
                    bChanged = true;
                    sAccount.Last_Donation_Amount__c = sDonationInformatoin.dLastDonation;
                }
                
                if(Date.ValueOf(sDonationInformatoin.dtLastDonationDate) != sAccount.Last_Donation_Date__c)
                {
                    bChanged = true;
                    sAccount.Last_Donation_Date__c = Date.ValueOf(sDonationInformatoin.dtLastDonationDate);
                }
                
                if(Date.ValueOf(sDonationInformatoin.dtFirstDonationDate) != sAccount.First_Donation_Date__c)
                {
                    bChanged = true;
                    sAccount.First_Donation_Date__c = Date.ValueOf(sDonationInformatoin.dtFirstDonationDate);
                }
                // Check Donation Total changes
                if(sDonationInformatoin.dDonationsTotal != sAccount.Donations_Total__c)
                {
                    bChanged = true;
                    sAccount.Donations_Total__c = sDonationInformatoin.dDonationsTotal;
                }
                
                // This year Donation
                if(sDonationInformatoin.dThisYearTotal != sAccount.Donations_This_Year__c)
                {
                    bChanged = true;
                    
                    if(sDonationInformatoin.dThisYearTotal == null)
                    {
                        sAccount.Donations_This_Year__c = null;
                    }
                    else
                    {
                        sAccount.Donations_This_Year__c = sDonationInformatoin.dThisYearTotal;
                    }
                }
                // Last year Donation
                if(sDonationInformatoin.dLastYearTotal != sAccount.Donations_Last_Year__c)
                {
                    bChanged = true;
                    
                    if(sDonationInformatoin.dLastYearTotal == null)
                    {
                        sAccount.Donations_Last_Year__c = null;
                    }
                    else
                    {
                        sAccount.Donations_Last_Year__c = sDonationInformatoin.dLastYearTotal;
                    }
                }
            }
            
            if(bChanged)
            {
                liAccountUpdate.add(sAccount);
                mapLocationToID.put(liAccountUpdate.size(), sAccount.id);
            }
            
            system.debug('After Change sAccount:' + sAccount);
        }
        
        //execute database changes
        Database.SaveResult[] arrSR = Database.Update(liAccountUpdate, false);
        
        for(integer i = 0; i < arrSR.size(); i++)
        {
            // Find out which one not processed. 
            if(!arrSR[i].isSuccess())
            {
                string strError = '';
                for(Database.Error sError : arrSR[i].getErrors())
                {
                    strError += sError.getMessage() + ' ';
                }
                
                mapAccountIdToError.put(mapLocationToID.get(i), strError);
            }
        }
        
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
        if(!mapAccountIdToError.isEmpty() || test.isRunningTest())
        {
            string strBody = '';
            strBody = '<table><tr><th>Account Id</th><th>Error</th></tr>';
            
            for(string strKey : mapAccountIdToError.keyset())
            {
                strBody += '<tr><td>' + strKey + '</td><td>' + mapAccountIdToError.get(strKey) + '</td></tr>';
            }
            
            strBody += '</table>';
            
            system.debug('Email Body:' + strBody);
            
            User  sUsers =  [
                                select id
                                from User
                                where Profile.Name = 'System Administrator' and IsActive = true
                                and Send_Fatal_Error_Processing_Email__c = true
                                order by CreatedDate
                                limit 1
                            ];
        
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           mail.setTargetObjectId(sUsers.id);
           mail.setBccSender(false);
           mail.setUseSignature(false);
           mail.setSubject('Account Update fail');
           mail.setHtmlBody(strBody);
           mail.saveAsActivity = false;
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        
        }

        system.debug('Finished');
    }

    private static testmethod void myUnitTest()
    {
        Account sAccount    = new Account();
        sAccount.Name       = 'Test Account';
        insert sAccount;
        
        Opportunity sOpportunity    = new Opportunity();
        sOpportunity.Name           = 'Test Method Opty 3';
        sOpportunity.StageName      ='Club Red Ongoing';
        sOpportunity.CloseDate      = System.Today().addDays(30);
        sOpportunity.AccountId      = sAccount.Id;
        sOpportunity.Amount         = 500;
        insert sOpportunity;

        Income__c sIncome                       = new Income__c();
        sIncome.Amount_A__c                     = 500;
        sIncome.Opportunity__c                  = sOpportunity.Id;
        sIncome.All_Income_Processed__c         = true;
        sIncome.Account_Code__c                 = '240';
        sIncome.All_Income_Processed__c         = true;
        sIncome.All_Income_Processed_Date__c    = system.today();
        insert sIncome;
        
        Opportunity sOpportunity1       = new Opportunity();
        sOpportunity1.Name              = 'Test Method Opty 3';
        sOpportunity1.StageName         ='Club Red Ongoing';
        sOpportunity1.CloseDate         = System.Today().addDays(30);
        sOpportunity1.AccountId         = sAccount.Id;
        sOpportunity1.Amount            = 500;
        insert sOpportunity1;

        Income__c sIncome1                      = new Income__c();
        sIncome1.Amount_A__c                    = 700;
        sIncome1.Opportunity__c                 = sOpportunity1.Id;
        sIncome1.All_Income_Processed__c        = true;
        sIncome1.All_Income_Processed_Date__c   = system.today();
        sIncome1.Account_Code__c                = '245';
        insert sIncome1;
        
        Income__c sIncome2                      = new Income__c();
        sIncome2.Amount_A__c                    = 700;
        sIncome2.Opportunity__c                 = sOpportunity1.Id;
        sIncome2.All_Income_Processed__c        = true;
        sIncome2.All_Income_Processed_Date__c   = system.today().addYears(-1);
        insert sIncome2;
        
        Income__c sIncome3                      = new Income__c();
        sIncome3.Account_Code__c                = '260';
        sIncome3.Amount_A__c                    = 700;
        sIncome3.Opportunity__c                 = sOpportunity1.Id;
        sIncome3.All_Income_Processed__c        = true;
        sIncome3.All_Income_Processed_Date__c   = system.today().addYears(-2);
        insert sIncome3;
        
        Test.startTest();
        Database.executeBatch(new QLastDonationBatch());
        Test.stopTest();
    }

}