public with sharing class QCorporateWebsiteShopProcessing 
{
	public class QCorporateWebsiteWebServicesParameterException extends Exception{}
	public class QCorporateWebsiteWebServicesException 			extends Exception{}
	
	public static QCorporateWebsiteWebServices.QShopUpdateProductResults GetProductDetails(QCorporateWebsiteWebServices.QShopUpdateProductRequest oArgs)
	{
		// return object
		QCorporateWebsiteWebServices.QShopUpdateProductResults oRet = new QCorporateWebsiteWebServices.QShopUpdateProductResults(); 

		Product2		sProduct;
		PricebookEntry	sEntry;

		try 
		{
			sProduct = [
							select	id, Is_Visible_On_Website__c, Title_For_Website__c, Description_For_Website__c,
									Website_Main_Image_Id__c, GST_Applicable__c, Product_Height__c, Product_Width__c,
									Product_Depth__c, Product_Weight__c, Product_Category_For_Website__c,
									Minimum_Shipping_Cost__c, Use_External_Shipping_Cost__c, Is_Campaign_Ticket__c,
									Campaign_Id_For_Website__c, ProductCode
							from	Product2
							where 	id = :oArgs.UniqueProductCode];
			
			sEntry = [
							select	id, UnitPrice
							from	PricebookEntry
							where	Pricebook2Id = :QCacheWebsiteMetadata.getShopStandardPricebook()
							and		Product2Id = :sProduct.Id];
		}
		catch (QueryException qe)
		{
			System.debug('Exception - Trying to Load Product Details - ' + qe);
			throw new QCorporateWebsiteWebServicesException('Error Loading - ' + oArgs.UniqueProductCode);
		}
		catch (Exception e)
		{
			System.debug('Exception - Trying to Load Product Details - ' + e);
			throw new QCorporateWebsiteWebServicesException('Error Loading - ' + oArgs.UniqueProductCode);
		}
		
		oRet.UniqueProductCode 					= sProduct.id;
		oRet.IsActive 							= sProduct.Is_Visible_On_Website__c;
		oRet.Name								= sProduct.Title_For_Website__c;
		oRet.Description 						= sProduct.Description_For_Website__c;
		oRet.Height 							= sProduct.Product_Height__c;
		oRet.Width 								= sProduct.Product_Width__c;
		oRet.Depth 								= sProduct.Product_Depth__c;
		oRet.Weight								= sProduct.Product_Weight__c;
		oRet.Category 							= sProduct.Product_Category_For_Website__c;
		oRet.Price 								= sEntry.UnitPrice;
		oRet.MinShippingCost 					= sProduct.Minimum_Shipping_Cost__c;
		oRet.UseExternalShippingCalculation 	= sProduct.Use_External_Shipping_Cost__c;
		oRet.CampaignId 						= sProduct.Campaign_Id_For_Website__c;
		oRet.IsCampaignTicket 					= sProduct.Is_Campaign_Ticket__c;
		oRet.GSTApplicable						= sProduct.GST_Applicable__c;

//		oRet.Quantity // should this be here?

		// get the product image
		if(sProduct.Website_Main_Image_Id__c != null)
		{
			try
			{
				Attachment  sWebsiteMainImage = [select Body from Attachment where id = :sProduct.Website_Main_Image_Id__c];
				oRet.MainImage = sWebsiteMainImage.Body;
			}
			catch(QueryException qe)
			{
				System.debug('Exception - Trying to Load Product Main Image - ' + qe);
			}
			catch(Exception e)
			{
				System.debug('Serious Exception - Trying to Load Product Main Image - ' + e);
			}
		}
		
		return oRet;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public static QCorporateWebsiteWebServices.QShopRegisterUserResults RegisterUser(QCorporateWebsiteWebServices.QShopRegisterUserRequest oArgs)
	{	
		System.Debug(oArgs);

		// return object
		QCorporateWebsiteWebServices.QShopRegisterUserResults oRet = new QCorporateWebsiteWebServices.QShopRegisterUserResults();
		
		oRet.InError = false;
		oRet.Error = '';
		
		/*********************************************
			SEARCH FOR THE CLIENT
		*********************************************/
		QClientSearch 					oClientSearcherStage;
		QClientSearch.QClientSearchArgs oClientSearchArgsStage = new QClientSearch.QClientSearchArgs();
		QClientSearch.QClientSearchRet 	oClientSearchRetStage;
				
		oClientSearchArgsStage.DedupeMethod = QClientSearch.DedupeMethod.CORPORATE_SHOP;
		oClientSearchArgsStage.CreateAction	= QClientSearch.CreateAction.PERSON_ACCOUNT;
		
		oClientSearchArgsStage.CreateTaskForPossibleDupe	= true;
		oClientSearchArgsStage.Email						= oArgs.Email;
		oClientSearchArgsStage.FirstName					= oArgs.FirstName;
		oClientSearchArgsStage.LastName						= oArgs.LastName;
		oClientSearchArgsStage.State						= oArgs.State;
		
		oClientSearchArgsStage.Title						= oArgs.Title;
		oClientSearchArgsStage.PhoneNumber					= oArgs.PhoneNumber;
		oClientSearchArgsStage.OtherPhoneNumber				= oArgs.OtherPhoneNumber;
		oClientSearchArgsStage.Street						= oArgs.Street;
		oClientSearchArgsStage.Suburb						= oArgs.Suburb;
		oClientSearchArgsStage.Country						= oArgs.Country;	
		oClientSearchArgsStage.PostalCode					= oArgs.PostalCode;
		oClientSearchArgsStage.MarketingOptIn				= oArgs.MarketingOptIn;
		oClientSearchArgsStage.FurtherInformationOptIn		= oArgs.FurtherInformationOptIn;

		// now lets create our search object
		oClientSearcherStage = new QClientSearch(oClientSearchArgsStage);
		oClientSearchRetStage = oClientSearcherStage.ProcMain();	// SEARCH!
		
		// if either the Account or Contact Id didnt come back we had a massive problem!!!
		if(oClientSearchRetStage.AccountId == null || oClientSearchRetStage.ContactId == null)
		{
			QUtils.CreateErrorEmail('Corporate WebService Issue - Client Search', 'Trying to Search Details - ' + oClientSearchArgsStage);
			System.debug('Corporate WebService Issue - Client Search - Trying to Search Details - ' + oClientSearchArgsStage);
			
			oRet.InError 	= true;
    		oRet.Error		= 'Error - Trying to Register User';
		}
		else
		{
			//oRet.UserId = oClientSearchRetStage.ContactId;
			oRet.UserId = oClientSearchRetStage.AccountId;
		}
				// whatever happened we now store the submitted address in the temporary fields on contact
		try
		{
			Contact sContact = [select	id, IsPersonAccount, OwnerId,
										MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode
								from	Contact
								where	id = : oClientSearchRetStage.ContactId];
			
			if(!sContact.IsPersonAccount)
			{
				sContact.Donation_Addr_Street_1__c	= oArgs.Street;
				sContact.Donation_Addr_City__c		= oArgs.Suburb;
				sContact.Donation_Addr_State__c		= oArgs.State;
				sContact.Donation_Addr_Postcode__c	= oArgs.PostalCode;
				sContact.Donation_Addr_Country__c	= oArgs.Country;
				
				if(
							(sContact.MailingStreet 	!= oArgs.Street)
						||	(sContact.MailingCity 		!= oArgs.Suburb)
						||	(sContact.MailingState 		!= oArgs.State)
						||	(sContact.MailingCountry 	!= oArgs.Country)
						||	(sContact.MailingPostalCode != oArgs.PostalCode)
					)
				{
					try
					{
						Task sTask;
		
						sTask = new Task();
						//sTask.OwnerId		= sContact.OwnerId;
						sTask.OwnerId		= QCacheWebsiteMetadata.getCorporateDonationOwnerForState(oArgs.State);
						sTask.WhoId 		= sContact.Id;
						sTask.Subject 		= 'Address Check Required';
						sTask.Description 	= 'Contact has different address to what has been supplied through website donation process';
						sTask.ActivityDate	= System.Today();
						
						Database.saveResult saveResult = Database.insert(sTask, false);
						
						QUtils.CreateEmail(
												QCacheWebsiteMetadata.getCorporateDonationOwnerForState(oArgs.State),
												'Address Check Required',
												'Contact has different address to what has been supplied through website donation process ' + 'https://eu3.salesforce.com/' + sContact.Id
											);
					}
					catch(Exception e)
					{
						
					}
				}
				
				update sContact;
			}
			else
			{
				Account sAccount = [
										select id, PersonMailingStreet, PersonMailingCity, PersonMailingState, PersonMailingCountry, PersonMailingPostalCode 
										from Account 
										where id = :oClientSearchRetStage.AccountId];
				
				sAccount.Donation_Addr_Street_1__pc	= oArgs.Street;
				sAccount.Donation_Addr_City__pc		= oArgs.Suburb;
				sAccount.Donation_Addr_State__pc	= oArgs.State;
				sAccount.Donation_Addr_Postcode__pc	= oArgs.PostalCode;
				sAccount.Donation_Addr_Country__pc	= oArgs.Country;
				
				if(
							(sAccount.PersonMailingStreet 		!= oArgs.Street)
						||	(sAccount.PersonMailingCity 		!= oArgs.Suburb)
						||	(sAccount.PersonMailingState 		!= oArgs.State)
						||	(sAccount.PersonMailingCountry 		!= oArgs.Country)
						||	(sAccount.PersonMailingPostalCode 	!= oArgs.PostalCode)
					)
				{
					try
					{
						Task sTask;
		
						sTask = new Task();
						sTask.OwnerId		= sAccount.OwnerId;
						sTask.WhoId 		= sAccount.Id;
						sTask.Subject 		= 'Address Check Required';
						sTask.Description 	= 'PersonAccount has different address to what has been supplied through website donation process';
						sTask.ActivityDate	= System.Today();
						
						Database.saveResult saveResult = Database.insert(sTask, false);
					}
					catch(Exception e)
					{
						
					}
				}
				
				update sAccount;
			}
		}
		catch(Exception e)
		{
			System.Debug('Error storing address on contact : ' + e.getMessage());
			oRet.InError = true;
			oRet.Error = 'Error storing address on contact.';
		}
		
		return oRet;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static QCorporateWebsiteWebServices.QShopPurchaseResults ProcessPurchase(QCorporateWebsiteWebServices.QShopPurchaseRequest oArgs, boolean bAllowCallOut)
	{
		// return object
		QCorporateWebsiteWebServices.QShopPurchaseResults oRet = new QCorporateWebsiteWebServices.QShopPurchaseResults();
		
		oRet.InError 	= false;
		oRet.Error 		= 'Success';
				
		// who and what we are dealing with
		Account sAccount;
		Campaign sCampaign;
		Contact sPersonContactForEmail;
		
		System.Debug(oArgs);
		System.Debug(bAllowCallOut);
		
		try
		{
			sAccount 	= [
							select 	id, OwnerId, Name, BillingState, ShippingState, IPP_Client_Id__c, IsPersonAccount, PersonContactId 
							from	Account 
							where 	id = :oArgs.UserId
						];
			
			if(sAccount.IsPersonAccount)
			{
				sPersonContactForEmail = [select id, Email from Contact where id = :sAccount.PersonContactId];
			}
		}
		catch(Exception e)
		{
			oArgs.CreditCardNumber = '****-****-****-****';
			
			QUtils.CreateErrorEmail('Corporate WebService Issue - Donation Processing', 'Trying to Load Contact Details - ' + oArgs);
			System.debug('Corporate WebService Issue - Donation Processing - Trying to Load Contact Details - ' + oArgs);
    		
    		oRet.InError 			= true;
    		oRet.error				= 'Error - Invalid User ID Specified';
    		return oRet;
		}
		
		// if we passed one
		if(oArgs.CampaignId != null)
		{
			try
			{
				System.Debug('Campaign tried for product purchase: ' + oArgs.CampaignId);
				sCampaign = [select id from Campaign where id = :oArgs.CampaignId];
			}
			catch(Exception e)
			{
				//QUtils.CreateErrorEmail('Corporate WebService Warning - Shop Product Purchase', 'Invalid Campaign Reference Passed from the Web Service - ' + oArgs + ' - \n\n' + e);
			}
		}
		
		// looks like we havent loaded anything yet so lets get the default
		if(sCampaign == null) 
		{
			try
			{
				sCampaign = [select id from Campaign where id = :QCacheWebsiteMetadata.getDefaultCampaignForShopPurcahses()];
			}
			catch(Exception e)
			{
				QUtils.CreateErrorEmail('Corporate WebService Issue - Shop Product Purchase', 'Trying to Load Default Campaign for Processing');
				System.debug('Corporate WebService Issue - Shop Product Purchase - Trying to Load Default Campaign for Processing');
	    		
	    		oRet.InError 	= true;
	    		oRet.error		= 'Error - Unable to Locate Campaign for Shop Purchase';
    			return oRet;
			}
		}

		/******************************************************************************************
		 	Calculate the total cost of the order (inc. tax + shipping etc.)
		*******************************************************************************************/
		integer iTotalAmount = (oArgs.ShippingAmount * 100).intValue();
		integer iNumProducts = 0;

		for(QCorporateWebsiteWebServices.QProduct product : oArgs.product)
		{
			// amount is the value in cents so multiplying by 100 here should mean we have no rounding issues to worry about
			iTotalAmount += (100 * product.Quantity * (product.UnitPrice + product.GSTAmount)).intValue();
			System.Debug('\n\n********\n\n' + iNumProducts + ', ' + product.Quantity);
			iNumProducts += product.Quantity;
		}
		System.Debug('\n\noArgs.TotalProductCount = ' + oArgs.TotalProductCount);
		
		/* CURRENTLY DISABLED WHILE WE WAIT FOR MELON MAIL TO FIX THEIR END */
		if (iNumProducts != oArgs.TotalProductCount && false)
		{
			// whoops - something's up with this request so let's not make an order for a bazillion items  
			QUtils.CreateErrorEmail('Corporate WebService Issue - Shop Product Purchase', 'Number of products in order does not match TotalProductCount');
			System.debug('Corporate WebService Issue - Shop Product Purchase - Number of products in order does not match TotalProductCount');
			
			oRet.InError 			= true;
    		oRet.error				= 'There was an error processing your order, please try again later';
    		return oRet;
		}
		
		/******************************************************************************************
		 	SEND THE PAYMENT!  NEED TO SEND THE PAYMENT BEFORE WE START HITTING THE DATABASE
		*******************************************************************************************/
		QProcessPayment 					oPaymentProcessorStage1;
		QProcessPayment.QProcessPaymentArgs oPaymentProcessorStage1Args;
		QProcessPayment.QProcessPaymentRet	oPaymentProcessorStage1Ret;

		oPaymentProcessorStage1Args = new QProcessPayment.QProcessPaymentArgs();
		
		oPaymentProcessorStage1Args.AllowCallout			= bAllowCallout;
		oPaymentProcessorStage1Args.IPPClientId				= ''+sAccount.IPP_Client_Id__c;
		
		integer iIPPPaymentRef = QPoolCounter.GetPoolCounter(QConstants.CORPORATE_IPP_INCOME_COUNTER_NAME);
		
		oPaymentProcessorStage1Args.IPPTransactionRef		= iIPPPaymentRef;

		oPaymentProcessorStage1Args.Amount 					= iTotalAmount;
		oPaymentProcessorStage1Args.ShippingComponent		= (oArgs.ShippingAmount * 100).IntValue();
		oPaymentProcessorStage1Args.ShippingMethod			= oArgs.ShippingMethod;
		oPaymentProcessorStage1Args.CreditCardNumber		= oArgs.CreditCardNumber;
		oPaymentProcessorStage1Args.CreditCardName			= oArgs.CardName;
		oPaymentProcessorStage1Args.CreditCardExpiryYear 	= Integer.valueOf(oArgs.ExpiryYear);
		oPaymentProcessorStage1Args.CreditCardExpiryMonth 	= Integer.valueOf(oArgs.ExpiryMonth);
		oPaymentProcessorStage1Args.CreditCardCVN			= oArgs.CVC;
		oPaymentProcessorStage1Args.CreditCardType			= QUtils.GetCreditCardType(oArgs.CreditCardNumber);
		oPaymentProcessorStage1Args.PaymentProcess			= QProcessPayment.PaymentProcess.SINGLE_PAYMENT_CALLOUT_ONLY;
		
		oPaymentProcessorStage1Args.Products				= oArgs.Product;
		
		oPaymentProcessorStage1 	= new QProcessPayment(oPaymentProcessorStage1Args);
		oPaymentProcessorStage1Ret 	= oPaymentProcessorStage1.ProcMain();						// FIRE!!!
		
		QPoolCounter.IncrementCounter(QConstants.CORPORATE_IPP_INCOME_COUNTER_NAME);
		
		// so we had big failure sending to the bank, this is a major exception not insufficient funds etc
		if(oPaymentProcessorStage1Ret.InError || oPaymentProcessorStage1Ret.IPPResponse == null)
    	{
    		QUtils.CreateErrorTask(sAccount.Id, null, 'Corporate WebService Issue - Payment Processing', 'Trying to Make Payment - ' + oPaymentProcessorStage1Ret);
    		QUtils.CreateErrorEmail('Corporate WebService Issue - Shop Product Purchase', 'Trying to Make Payment - ' + oPaymentProcessorStage1Ret + 'bAllowCallout = ' + bAllowCallout);
			System.debug('Corporate WebService Issue - Shop Product Purchase - Trying to Make Payment - ' + oPaymentProcessorStage1Ret + 'bAllowCallout = ' + bAllowCallout);
    		
    		oRet.InError 				= true;
    		oRet.Error					= 'The Payment Portal is currently unavailable, please try again later - 001';
    		return oRet;
    	}
    	// otherwise send back the banking error, this is the insufficient funds type errors
    	else if(!oPaymentProcessorStage1Ret.IsPaymentSuccessful)
    	{
    		QUtils.CreateErrorTask(sAccount.Id, null, 'Corporate WebService Issue - Payment Processing', 'Trying to Make Payment - ' + oPaymentProcessorStage1Ret);
    		QUtils.CreateErrorEmail('Corporate WebService Issue - Shop Product Purchase', 'Trying to Make Payment - ' + oPaymentProcessorStage1Ret + '\nbAllowCallout = ' + bAllowCallout);
			System.debug('Corporate WebService Issue - Shop Product Purchase - Trying to Make Payment - ' + oPaymentProcessorStage1Ret + '\nbAllowCallout = ' + bAllowCallout);
    		
    		oRet.InError 			= true;
    		//oRet.Error				= 'The Payment Portal is currently unavailable, please try again later -002';
    		oRet.Error				= 'There has been a problem processing your credit card, please check your details and try again or call us on 1800 REDKITE (1800 733 548). Thank you.';
    		return oRet;
    	}

		/*********************************************
			 RECORD THE PAYMENT
		*********************************************/
		QProcessPayment 					oPaymentProcessorStage2;
		QProcessPayment.QProcessPaymentArgs oPaymentProcessorStage2Args = new QProcessPayment.QProcessPaymentArgs();
		QProcessPayment.QProcessPaymentRet	oPaymentProcessorStage2Ret;
		
		oPaymentProcessorStage2Args.AllowCallout			= bAllowCallout;
		oPaymentProcessorStage2Args.IPPTransactionRef		= iIPPPaymentRef;
		oPaymentProcessorStage2Args.AccountId 				= sAccount.Id;
		oPaymentProcessorStage2Args.CampaignId 				= sCampaign.Id;
		
		oPaymentProcessorStage2Args.Amount 					= iTotalAmount;
		oPaymentProcessorStage2Args.ShippingComponent		= (oArgs.ShippingAmount * 100).intValue();
		oPaymentProcessorStage2Args.ShippingMethod			= oArgs.ShippingMethod;
		oPaymentProcessorStage2Args.CreditCardNumber		= oArgs.CreditCardNumber;
		oPaymentProcessorStage2Args.CreditCardName			= oArgs.CardName;
		oPaymentProcessorStage2Args.CreditCardExpiryYear 	= Integer.valueOf(oArgs.ExpiryYear);
		oPaymentProcessorStage2Args.CreditCardExpiryMonth 	= Integer.valueOf(oArgs.ExpiryMonth);
		oPaymentProcessorStage2Args.CreditCardCVN			= oArgs.CVC;
		oPaymentProcessorStage2Args.CreditCardType			= QUtils.GetCreditCardType(oArgs.CreditCardNumber);
		oPaymentProcessorStage2Args.PaymentProcess			= QProcessPayment.PaymentProcess.CORPORATE_RECORD_PRODUCT_PAYMENT;
		
		oPaymentProcessorStage2Args.Products				= oArgs.Product;
		
		// super important, send the processor the response that we got when we made the payment
		oPaymentProcessorStage2Args.IPPResponse				= oPaymentProcessorStage1Ret.IPPResponse;
		
		oPaymentProcessorStage2 	= new QProcessPayment(oPaymentProcessorStage2Args);
		oPaymentProcessorStage2Ret 	= oPaymentProcessorStage2.ProcMain();					// FIRE!!!
		
		// something bad happened while recording the payment information
		if(oPaymentProcessorStage2Ret.InError)
    	{
    		oPaymentProcessorStage2Args.CreditCardNumber = '****-****-****-****';
    		
    		QUtils.CreateErrorTask(sAccount.Id, null, 'Corporate WebService Issue - Shop Product Purchase Recording', 'Trying to Record Details - ' + oPaymentProcessorStage2Args);
			System.debug('Corporate WebService Issue - Shop Product Purchase Recording - Trying to Record Details - ' + oPaymentProcessorStage2Args);

    		oRet.InError 	= true;
    		oRet.Error		= 'An error has been encountered while processing your Payment, a Redkite representative will contact you shortly';
    		return oRet;
    	}

    	/*************************************************
			 Now send a shop purchase email
		**************************************************/
    	if(oPaymentProcessorStage1Ret.IsPaymentSuccessful)
    	{
			// Now we need to send the Donor the Redkite Corporate Reciept Email
			try
			{
				// just a double check but this should never happen
				if(sPersonContactForEmail != null)
				{
					EmailTemplate 			sEmailTemplate 	= [SELECT Id, Name FROM EmailTemplate WHERE Name = :'Corporate Shop Visualforce Template' AND IsActive = TRUE];
					OrgWideEmailAddress 	sEmailAddress 	= [select id, Address from OrgWideEmailAddress where DisplayName = 'Redkite'];
					
					// send login details e-mail
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	
					mail.setWhatId(oPaymentProcessorStage2Ret.Income.Id);
					mail.setTargetObjectId(sPersonContactForEmail.Id);
					mail.setBccSender(false);
					mail.setUseSignature(false);
					mail.setOrgWideEmailAddressId(sEmailAddress.Id);
					
					mail.setTemplateId(sEmailTemplate.Id);
					
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}
			}
			catch(Exception e)
			{
				QUtils.CreateErrorTask(oPaymentProcessorStage2Args.IncomeId, null, 'Corporate Website Issue - Voucher Email', 'Voucher Email Template - Corporate Donation Visualforce Template - ' + e);
			}
    	}
			
		return oRet;
	}
}