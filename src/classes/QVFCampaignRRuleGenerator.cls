public with sharing class QVFCampaignRRuleGenerator
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class CampaignRRuleGeneratorScreenParameterException 	extends Exception{}
	
	////////////////////////////////////////////
	// Singular
	private	id			m_idCampaign		{get; set;}

	public	RRule__c	m_sRRule			{get; set;}
	public	Campaign	m_sCampaign			{get; set;}
	public	date		m_dateStart			{get; set;}
	public	string		m_strPlainTextRule	{get; set;}
	public	boolean		m_bSaveDisabled		{get; set;}	
	
	////////////////////////////////////////////
	// Constructor
	public QVFCampaignRRuleGenerator(ApexPages.StandardController sc)
	{
		// store the campaign Id
		m_idCampaign = sc.getId();

		m_bSaveDisabled = true;
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	public void InitCampaignRRuleGenerator()
	{			
		try
		{
			m_sCampaign = [select	Id, Name, StartDate, EndDate, RRule__c
							from	Campaign
							where	Id = :m_idCampaign];
							
			// looks like there wasnt one there
			if(m_sCampaign.RRule__c == null)
			{
				m_sRRule = new RRule__c();
			}
			else
			{
				m_sRRule	= [select	Id, Start_Date__c, End_Date__c, Interval_Type__c, Interval_Length__c, Recurrance_Count__c, RRule__c
								from	RRule__c
								where	Id = :m_sCampaign.RRule__c];
			}
		}
		catch(QueryException qe)
		{
			System.Debug('RRule Generator: Query Exception Getting Campaign or RRule for campaign id: ' + m_idCampaign + ': ' + qe);
			apexPages.addMessage(new apexPages.Message(apexPages.Severity.ERROR, 'Query Exception caught: ' + qe));
		}
		catch(Exception e)
		{
			System.Debug('RRule Generator: Exception Getting Campaign or RRule for campaign id: ' + m_idCampaign + ': ' + e);
			apexPages.addMessage(new apexPages.Message(apexPages.Severity.ERROR, 'Exception caught: ' + e));
		}
	}

	public PageReference Save()
	{
		if(VerifyInput())
		{
			GenerateRule();

			// save the rule to the db and return to the campaign
			try
			{
				upsert(m_sRRule);

				if(m_sCampaign.RRule__c == null)
				{
					m_sCampaign.RRule__c = m_sRRule.Id;
					update(m_sCampaign);
				}
			}
			catch(QueryException qe)
			{
				apexPages.addMessage(new apexPages.Message(apexPages.Severity.ERROR, 'Query Exception caught while saving RRule: ' + qe));
				return null;				
			}
			catch(Exception e)
			{
				apexPages.addMessage(new apexPages.Message(apexPages.Severity.ERROR, 'Exception caught while saving RRule: ' + e));		
				return null;
			}
			
			PageReference pr = new PageReference('/'+m_idCampaign);
			pr.setRedirect(true);
		
			return pr;
		}
		
		return null;
	}

	public PageReference Cancel()
	{
		PageReference pr = new PageReference('/'+m_idCampaign);
		pr.setRedirect(true);
		
		return pr;
	}

	public PageReference Preview()
	{
		if(VerifyInput())
		{
			GenerateRule();
		}
		return null;
	}

	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private boolean VerifyInput()
	{
		boolean bRetVal = true;

		if(m_sRRule.Interval_Length__c == null || m_sRRule.Interval_Length__c < 1)
		{
			apexPages.addMessage(new apexPages.Message(apexPages.Severity.ERROR, 'Interval length must be greater than or equal to 1'));
			bRetVal = false;
		}

		if(m_sRRule.Recurrance_Count__c == null || m_sRRule.Recurrance_Count__c < 1)
		{
			apexPages.addMessage(new apexPages.Message(apexPages.Severity.ERROR, 'Recurrance count must be greater than or equal to 1'));
			bRetVal = false;
		}
		
		if(m_sRRule.Start_Date__c == null)
		{
			apexPages.addMessage(new apexPages.Message(apexPages.Severity.ERROR, 'Start Date must have a value'));
			bRetVal = false;			
		}

		return bRetVal;
	}

	private void GenerateRule()
	{
		integer iLength = m_sRRule.Interval_Length__c.intValue();
		integer iCount = m_sRRule.Recurrance_Count__c.intValue();

		if(iLength < 1)
		{
			iLength = 1;
		}

		if(iCount <= 0)
		{
			m_sRRule.Recurrance_Count__c = 1;
			iCount = 1;
		}
		
		m_sRRule.End_Date__c = m_sRRule.Start_Date__c;
		
		// lets start off with the constant bits
		string strRRule = 'DTSTART;TZID=AU-Australia/Sydney:';
		
		// now the year
		System.Debug('********************************\nCampaign Start Date: ' + m_sCampaign.StartDate + '\nRRule Start Date: ' + m_sRRule.Start_Date__c);

		strRRule += m_sRRule.Start_Date__c.Year();
		
		// month, need to look after leading 0		
		if(m_sRRule.Start_Date__c.Month() < 10)
		{
			strRRule += '0';
		}
		
		strRRule += m_sRRule.Start_Date__c.Month();
		
		// month, need to look after leading 0
		if(m_sRRule.Start_Date__c.Day() < 10)
		{
			strRRule += '0';
		}
		
		strRRule += m_sRRule.Start_Date__c.Day();
		
		// hard coding the time at the moment
		strRRule += 'T090000\n';
		
		// now the Frequency
		strRRule += 'RRULE:FREQ=';

		if(m_sRRule.Interval_Type__c.toLowerCase().startsWith('d'))
		{
			strRRule += 'DAILY;';

			if(iCount > 1)
			{
				m_sRRule.End_Date__c = m_sRRule.End_Date__c.addDays((iCount - 1) * iLength);
			}
		}
		else if(m_sRRule.Interval_Type__c.toLowerCase().startsWith('w'))
		{
			strRRule += 'WEEKLY;';

			if(iCount > 1)
			{
				m_sRRule.End_Date__c = m_sRRule.End_Date__c.addDays((iCount - 1) * iLength * 7);
			}
		}
		else if(m_sRRule.Interval_Type__c.toLowerCase().startsWith('m'))
		{
			strRRule += 'MONTHLY;';

			if(iCount > 1)
			{
				m_sRRule.End_Date__c = m_sRRule.End_Date__c.addMonths((iCount - 1) * iLength);
			}
		}
		else if(m_sRRule.Interval_Type__c.toLowerCase().startsWith('y'))
		{
			strRRule += 'YEARLY;';

			if(iCount > 1)
			{
				m_sRRule.End_Date__c = m_sRRule.End_Date__c.addYears((iCount - 1) * iLength);
			}
		}

		// interval
		if(iLength > 1)
		{
			strRRule += 'INTERVAL=' + iLength + ';';
		}

		// count
		strRRule += 'COUNT=' + iCount;

		// done so save it
		m_sRRule.RRule__c = strRRule;

		m_strPlainTextRule = 'Starting on ' + m_sRRule.Start_Date__c.format() + ', repeat every ' + iLength + ' ' + m_sRRule.Interval_Type__c + ' ' + iCount
								+ ' times, finishing on ' + m_sRRule.End_Date__c.format() + '.';
								
		m_sRRule.RRule_Translation__c = m_strPlainTextRule;
	}

	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/	
	public static testmethod void TestQVFCampaignRRuleGenerator()
	{
		System.Debug('Testing this');
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'RRule Test';
		sCampaign.RecordTypeId = '012200000009DoK';
		sCampaign.Type = 'Event Redkite - EVR';
		sCampaign.StartDate = date.today().addDays(2);
		insert sCampaign;
		
    	ApexPages.PageReference pageRef = new ApexPages.PageReference('/apex/Campaign_RRule_Setup');
		Test.setCurrentPage(pageRef);

		ApexPages.currentPage().getParameters().put('id', sCampaign.id);
		ApexPages.StandardController standardController = new ApexPages.StandardController(sCampaign);

		QVFCampaignRRuleGenerator rr = new QVFCampaignRRuleGenerator(standardController);
		rr.InitCampaignRRuleGenerator();
		
		// set some fields
		rr.m_dateStart = date.today();
		rr.m_sRRule.Interval_Length__c = 0;
		rr.m_sRRule.Interval_Type__c = 'days';
		rr.m_sRRule.Recurrance_Count__c = 0;

		// invalid interval
		rr.Preview();
		
		rr.m_sRRule.Interval_Length__c = 3;
		
		// invalid recurrance count
		rr.Preview();
		
		rr.m_sRRule.Recurrance_Count__c = 3;
		
		// invalid date (null)
		rr.Preview();
		
		rr.m_sRRule.Start_Date__c = system.today();
		
		// should be valid now
		rr.Preview();
		System.Assert(rr.m_sRRule.RRule_Translation__c != '');
		rr.m_sRRule.RRule_Translation__c = '';
		
		rr.m_sRRule.Interval_Type__c = 'weeks';
		rr.Preview();
		System.Assert(rr.m_sRRule.RRule_Translation__c != '');
		
		rr.m_sRRule.Interval_Type__c = 'months';
		rr.Preview();
		System.Assert(rr.m_sRRule.RRule_Translation__c != '');
		
		rr.m_sRRule.Interval_Type__c = 'years';
		rr.Preview();
		System.Assert(rr.m_sRRule.RRule_Translation__c != '');
		
		rr.Save();
		rr.Cancel();
	}
}