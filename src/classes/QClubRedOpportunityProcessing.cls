global with sharing class QClubRedOpportunityProcessing  implements Database.Batchable<sObject>, Database.Stateful
{
	private static integer INCOME_CREATION_LEADTIME_IN_DAYS = 3;
	global static integer  JobItemsProcessed = 0;
	global static integer  NumberOfErrors = 0;
	
	public static integer GetIncomeCreationLeadtimeInDays()
	{
		return INCOME_CREATION_LEADTIME_IN_DAYS;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		boolean runBatch = false;
		try
		{
			IncomeBatchTracker__c currentJob = [	select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c 
													from IncomeBatchTracker__c 
													where Status__c = 'Processing'
													and	BatchName__c =  'QClubRedOpportunityProcessing'
													order by StartDatetime__c asc limit 1];
													
			if(currentJob != null && currentJob.ApexJobId__c ==  BC.getJobId())	//check if the current job is the right one
				runBatch = true;
		}
		catch(Exception e )
		{
			runBatch = false;
		}
		
		if(!runBatch)
		{
			QVFC_IncomeBatchManagement.DetectProcessingBatch('WARNING : Processing Batch Detected','TEST IN SANDBOX');
		}
		// we want to create these with a 3 day lead time so people have the chance to look
		// at them before we send them 
		Date dToday = System.Today().addDays(INCOME_CREATION_LEADTIME_IN_DAYS);

		string strDate = ''+ dToday.Year()+'-';
		
		// Month
		strDate += dToday.Month() < 10 ? '0' + dToday.Month() : ''+dToday.Month();
		
		// Dividor
		strDate += '-';
		
		// Day
		strDate += dToday.Day() < 10 ? '0' + dToday.Day() : ''+dToday.Day();
		
		//strDate = Datetime.now().addDays(INCOME_CREATION_LEADTIME_IN_DAYS).format('yyyy-MM-dd')
		
		string	strQuery =

			'	select 	id '+
			'	from 	Opportunity '+
			'	where 	StageName = \'Club Red Ongoing\' ' +
			'	and		Next_Recurring_Payment_Process_Date__c <= ' + strDate;
			
		system.debug('strQuery ' + strQuery);
		
		if(!runBatch)
			System.abortJob(BC.getJobId());
		
		return Database.getQueryLocator(strQuery);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		
		list<id> theProcessList = new list<id>();

		for(sobject s : scope)
		{
			theProcessList.add((id)s.get('Id'));
		}
		
		ProcessOpportunitiesForClubRed(theProcessList);
		
	}
	
	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
		try
		{
			IncomeBatchTracker__c currentJob = [	select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c 
													from IncomeBatchTracker__c 
													where Status__c = 'Processing'
													and	BatchName__c =  'QClubRedOpportunityProcessing'
													limit 1];
			
			currentJob.EndDateTime__c = system.now();
			currentJob.Status__c = 'Completed';
			update currentJob;
		}
		catch(Exception e )
		{	
			
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public class QClubRedOpportunityProcessingException extends exception{}
	
	private static map<id, Opportunity> m_mapOpportunities;
	private static list<id> 			m_liOpportunityIds;
	
	public static boolean ProcessOpportunitiesForClubRed(list<id> theProcessList)
	{
		list<Income__c> liIncomeInsert = new list<Income__c>();
		list<Task> 		liTaskInsert = new list<Task>();
		
		boolean bError = false;
		
		m_liOpportunityIds = theProcessList;
		// so first off lets load all of our opportunities
		LoadOpportunities();
		
		// ok, now for each one we need to create something....
		// 1) No CC info stored - create activity & Income
		// 2) Expired CC 		- create activity & Income
		// 3) No errors			- create Income record
		string opptyIds = '';
		for(Opportunity s : m_mapOpportunities.values())
		{
			opptyIds += s.Id+';';
			Income__c sIncome = new Income__c();
			
			sIncome.RecordTypeId 			= QCachePaymentGatewayMetadata.getClubRedPaymentRecordTypeId();
			system.debug('look!!RecordTypeId='+sIncome.RecordTypeId );
			sIncome.Opportunity__c 			= s.Id;
			//sIncome.Contact__c 				= s.Contact__c;
			sIncome.AccountId__c			= s.AccountId;
			sIncome.No_of_Payment_Types__c 	= 'One Payment Group';
			sIncome.Payment_Type_A__c 		= 'Credit Card';
			sIncome.Personal_Donation__c 	= s.Personal_Donation__c;
			sIncome.Account_Code__c			= '245';										//update by David 14/11/11
			
			sIncome.AccountId__c = s.AccountId;
			
			// set the offset back so the send to back date is correct
			sIncome.Payment_Gateway_Send_To_Bank_Date__c = s.Next_Recurring_Payment_Process_Date__c;
			
			// well no point sending this to the bank.....
			if(s.Recurring_Donation_Amount__c == null || s.Recurring_Donation_Amount__c <= 0)
			{
				continue;
			}
			
			sIncome.Amount_A__c 		= s.Recurring_Donation_Amount__c;
			sIncome.Credit_Card_No_A__c = '****-****-****-'+s.Account.Credit_Card_Last_4_Digit__c;
			//David 02-06-2011   -    store the type of the credit card			
			sIncome.Credit_Card_A__c 	= s.Credit_Card_Type1__c;
						
			// now some validation
			if((s.Account.IPPayment_Customer_Token__c == null || 
				s.Account.IPPayment_Customer_Token__c == '')
				&& s.Payment_Method__c != 'Direct Deposit')
			{
				// bad so someone needs to fix it but we will still create the Income record
				sIncome.Status__c 								= 'Send to Finance';
				sIncome.Payment_Gateway_Send_To_Bank_Date__c 	= null;
				
				Task sTask = new Task();
				
				sTask.OwnerId 		= s.OwnerId;
				sTask.WhatId 		= s.Id;
				//sTask.WhoId 		= s.Contact__c;
				sTask.ActivityDate	= System.Today();
				sTask.Priority		= 'High';
				sTask.Subject 		= 'Club Red Recurring Payment Problem - Missing Details';
				sTask.Description 	= 'Club Red Payment Scheduled however there is no IPP token stored on the contact';
				
				liTaskInsert.add(sTask);
			}
			else
			{
				// off to the bank!
				sIncome.Status__c 	= 'Send to IPPayments';
			}
			
			liIncomeInsert.add(sIncome);
		}
		
		try
		{
			insert liIncomeInsert;
			insert liTaskInsert;
		
	//TODO : check everything went ok
	
		// now we have entered the Incomes we can update the Opportunities
			for(Opportunity s : m_mapOpportunities.values())
			{
				// move everything along
				s.Last_Recurring_Payment_Process_Date__c = System.Today();
				
				if(s.Recurring_Payment_Frequency__c == 'Daily')
				{
					s.Next_Recurring_Payment_Process_Date__c = s.Next_Recurring_Payment_Process_Date__c.addDays(1);
				}
				else if(s.Recurring_Payment_Frequency__c == 'Fortnightly')
				{
					s.Next_Recurring_Payment_Process_Date__c = s.Next_Recurring_Payment_Process_Date__c.addDays(14);
				}
				else if(s.Recurring_Payment_Frequency__c == 'Monthly')
				{
					s.Next_Recurring_Payment_Process_Date__c = s.Next_Recurring_Payment_Process_Date__c.addMonths(1);
				}
				else
				{
					// safty, move along a month incase someone puts an unknown picklist value in there
					s.Next_Recurring_Payment_Process_Date__c = s.Next_Recurring_Payment_Process_Date__c.addMonths(1);
				} 
			}
			
			update m_mapOpportunities.values();
		
		}catch(Exception e)
		{
			bError = true;
			QVFC_IncomeBatchManagement.DetectProcessingBatch('WARNING : FAILED RECORDS',e.getMessage()+'\n\n\n'+opptyIds);
		}
//TODO : check everything went ok
		try
		{
			IncomeBatchTracker__c currentJob = [	select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c, BatchesProcessed__c
													from IncomeBatchTracker__c 
													where Status__c = 'Processing'
													and	BatchName__c =  'QClubRedOpportunityProcessing'
													limit 1];
			if(bError)
	    	{
	    		if(currentJob.NbFailedRecords__c == null)
	    			currentJob.NbFailedRecords__c = 0;
	    				
	    		currentJob.NbFailedRecords__c += 1;
	    	}else
	    	{
	    		if(currentJob.BatchesProcessed__c == null)
	    			currentJob.BatchesProcessed__c = 0;
	    				
	    		currentJob.BatchesProcessed__c += 1;
	    	}
			update currentJob;
			
			system.debug('>>>>>>>>'+currentJob);
		}
		catch(Exception e )
		{	
		}
		return true;
	}
	
	private static void LoadOpportunities()
	{
		m_mapOpportunities = new map<id, Opportunity>();
		
		try
		{
			for(Opportunity s : [
									select 	id, Name, OwnerId, Contact__c, Account.IPPayment_Customer_Token__c,
											Account.Credit_Card_Last_4_Digit__c, Credit_Card_Type__c, AccountId,
											Personal_Donation__c, Recurring_Donation_Amount__c, Payment_Method__c,
											Next_Recurring_Payment_Process_Date__c, Credit_Card_Type1__c
									from 	Opportunity 
									where 	id in :m_liOpportunityIds
								])
			{
				m_mapOpportunities.put(s.Id, s);
			}
		}
		catch(Exception e)
		{
			// damn it
			throw new QClubRedOpportunityProcessingException('There has been a serious issue loading the opportunities to process for Club Red - ' + e.getMessage());
		}
	}
	
	public static testmethod void TestQClubRedOpportunityProcessing()
	{
		Contact sContact = new Contact();
		// fill in contact fields here
		sContact.LastName = 'Tom';
		sContact.FirstName = 'Lastname';
		//sContact.IPPayment_Customer_Token__c = 'sdfjsdklfjldfj';
		insert sContact;
		
		Opportunity sOppty = new Opportunity();
		// fill in fields
		sOppty.Contact__c = sContact.Id;		
		sOppty.Name = 'One';
		sOppty.StageName ='Club Red Ongoing';
		sOppty.CloseDate = System.Today().addDays(15);
		sOppty.Next_Recurring_Payment_Process_Date__c = System.Today();
		sOppty.Recurring_Payment_Frequency__c = 'Daily';
		sOppty.Personal_Donation__c = true;
		sOppty.Recurring_Donation_Amount__c = 90;
		insert sOppty;
		
		QClubRedOpportunityProcessing qrop = new QClubRedOpportunityProcessing();
		// run batch
		Test.StartTest();
		Database.executeBatch(qrop);
		Test.StopTest();
	}
	
	
	public static testmethod void TestQClubRedOpportunityProcessing2()
	{
		Contact sContact = new Contact();
		// fill in contact fields here
		sContact.LastName = 'Mary';
		scontact.FirstName = 'LastName';
		insert sContact;
		
		Opportunity sOppty = new Opportunity();
		// fill in fields
		sOppty.Contact__c = sContact.Id;		
		sOppty.Name = 'two';
		sOppty.StageName ='Club Red Ongoing';
		sOppty.CloseDate = System.Today().addDays(15);
		sOppty.Next_Recurring_Payment_Process_Date__c = System.Today();
		sOppty.Recurring_Payment_Frequency__c = 'Daily';
		sOppty.Personal_Donation__c = true;
		sOppty.Recurring_Donation_Amount__c = 90;
		insert sOppty;
		
		IncomeBatchTracker__c currentJob = new IncomeBatchTracker__c(
				BatchName__c = 'QClubRedOpportunityProcessing',
				Status__c = 'Processing',
				NbRecords__c = 1,
				NbFailedRecords__c = 0
			);
		insert currentJob;
		
		QClubRedOpportunityProcessing qrop = new QClubRedOpportunityProcessing();
		// run batch
		Test.StartTest();
		Database.executeBatch(qrop);
		Test.StopTest();
	}
	
	
}