public with sharing class QVFSubmitSingleRefundPayment 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class QScreenParameterException extends Exception{}
	public class QScreenException extends Exception{}
	
	////////////////////////////////////////////
	// Singular
	string m_strId;
	string m_strReturnId;
	
	private Income__c	m_sIncomeOriginal;
	private Income__c	m_sIncomeRefund;
	private Opportunity m_sOpportunity;
	
	// hack to stop the decimal place disapearing issue
	public 	string 	ScreenAmount{get; set;}
	private integer m_iRefundAmount;
	public 	string 	RefundReason{get; set;}
	
	public string ReturnErrorString{get; set;}
	
	public	QProcessIPPPayment_V2.QCreditCardRefund			CreditCardRefund;
	public	QProcessIPPPayment_V2.QCreditCardRefundReturn	CreditCardRefundReturn;
	
	//This is only for test to change the result from the server.
	//private static boolean blReject = false;
	
	////////////////////////////////////////////
	// Collections
	
	////////////////////////////////////////////
	// Constructor
	public QVFSubmitSingleRefundPayment()
	{
		// 	lets grab the screen parameters - ID we came from		
		if(null == (m_strId = ApexPages.currentPage().getParameters().get('Id')))
		{
			throw new QScreenParameterException('Id Parameter not found');	
		}

		// Where we should return to
		if(null == (m_strReturnId = ApexPages.currentPage().getParameters().get('ReturnId')))
		{
			// No need to throw an error for this one at the moment
			//throw new RecalculateBudgetAllocationScreenParameterException('Origin Parameter not found');	
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
    public void Init()
    {
    	try
    	{
    		m_sIncomeOriginal = [
    								select 	id, Opportunity__c, Amount_A__c, RecordType.DeveloperName,
    										Payment_Gateway_Receipt__c, Opportunity__r.AccountId,/*Contact__c,*/ Credit_Card_A__c
    								from 	Income__c 
    								where 	Id = :m_strId
    							];
    		
    		this.ScreenAmount = String.valueOf(m_sIncomeOriginal.Amount_A__c);
    		
    		m_sOpportunity = [
								select 	id
								from 	Opportunity 
								where	id = :m_sIncomeOriginal.Opportunity__c
							];
    	}
    	catch(Exception e)
    	{
    		System.Debug('Exception thrown - ' + e);
			ApexPages.addMessages(e);
    	}
    }
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Cancel Button - will return to ReturnID if present or the entity we started from
	public pageReference Cancel()
	{
		PageReference pageRedirect;
		
		if(m_strReturnId != null)
		{
			pageRedirect = new PageReference('/' + m_strReturnId);
		}
		else
		{	
			pageRedirect = new PageReference('/' + m_strId);
		}
		
		pageRedirect.setRedirect(true);
		
		return pageRedirect;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Send Payment Button - will send info to payment platform
	public pageReference SendRefund()
	{
		PageReference pageRedirect;
		
		boolean bError = false;
		
		try
		{
			if(ValidateInput())
			{
				ProcessRefund();
			}
		}
		catch(Exception e)
		{
			bError = true;
			ApexPages.addMessages(e);
		}
		
		if(!bError)
		{
			if(m_strReturnId != null)
			{
				pageRedirect = new PageReference('/' + m_strReturnId);
			}
			else if(m_sIncomeRefund != null && m_sIncomeRefund.Id != null)
			{	
				pageRedirect = new PageReference('/' + m_sIncomeRefund.Id);
			}
			else
			{
				pageRedirect = new PageReference('/' + m_strId);
			}
			
			pageRedirect.setRedirect(true);
		}
		
		// if Process Payment returns false the page will reload and show error message
		return pageRedirect;
	}
	
	/**************************************************************************************************************
    	LIST METHODS
    **************************************************************************************************************/
	
    
    /**************************************************************************************************************
    	WORKER METHODS
    **************************************************************************************************************/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
    private void ProcessRefund()
    {
    	CreditCardRefund = new QProcessIPPPayment_V2.QCreditCardRefund();
    	
    	CreditCardRefund.Amount 	= string.valueOf(m_iRefundAmount);
    	CreditCardRefund.Receipt 	= m_sIncomeOriginal.Payment_Gateway_Receipt__c;
    	
		// now lets load the transaction into the database
		m_sIncomeRefund = new Income__c();
		
		m_sIncomeRefund.No_of_Payment_Types__c 				= 'One Payment Group';
		m_sIncomeRefund.Opportunity__c 						= m_sIncomeOriginal.Opportunity__c;
		//m_sIncomeRefund.Contact__c							= m_sIncomeOriginal.Contact__c;
		m_sIncomeRefund.AccountId__c							= m_sIncomeOriginal.Opportunity__r.AccountId;
		m_sIncomeRefund.Credit_Card_A__c					= m_sIncomeOriginal.Credit_Card_A__c;
		m_sIncomeRefund.Payment_Gateway_Transaction_Sent__c	= System.Now();
		if( m_sIncomeOriginal.RecordType.DeveloperName == 'Club_Red' || m_sIncomeOriginal.RecordType.DeveloperName == 'Club_Red_Payment_Gateway')
		{
			m_sIncomeRefund.RecordTypeId						= QCachePaymentGatewayMetadata.getClubRedRefundRecordTypeId();
		}else{
			m_sIncomeRefund.RecordTypeId						= QCachePaymentGatewayMetadata.getRefundRecordTypeId();
		}
		
		m_sIncomeRefund.Payment_Gateway_Original_Receipt__c	= m_sIncomeOriginal.Payment_Gateway_Receipt__c;
		m_sIncomeRefund.Refund_Reason__c					= RefundReason;
		m_sIncomeRefund.Income_Source__c					= 'On-line';
		
		try
		{
			// just to be sure
			CreditCardRefundReturn = null;
    		CreditCardRefundReturn = QProcessIPPPayment_V2.CalloutIPPaymentsSubmitRefundPayment(CreditCardRefund);
		}
		catch(Exception e)
    	{
    		m_sIncomeRefund.Payment_Gateway_Declined_Message__c 	= e.getMessage();
			m_sIncomeRefund.Payment_Gateway_Response_Code__c 		= '-1';
			m_sIncomeRefund.Status__c								= 'Declined';
							
			System.Debug('Exception thrown - ' + e);
    	}
		
		if(CreditCardRefundReturn != null)
		{
			m_sIncomeRefund.Payment_Gateway_Response__c					= CreditCardRefundReturn.Response;
			m_sIncomeRefund.Payment_Gateway_Response_Settlement_Date__c = CreditCardRefundReturn.SettlementDate;
			m_sIncomeRefund.Payment_Gateway_Settlement_Date__c			= QUtils.ConvertSettlementDate(CreditCardRefundReturn.SettlementDate);
			m_sIncomeRefund.Payment_Gateway_Response_Code__c	 		= CreditCardRefundReturn.ResponseCode;
			m_sIncomeRefund.Amount_A__c									= ((double)m_iRefundAmount / 100) * -1;
			m_sIncomeRefund.Payment_Gateway_Response_Received__c		= true;
			
			// everything looked good
			if(CreditCardRefundReturn.ResponseCode == QConstants.STR_IPPAYMENTS_TRANSACTION_CODE_APPROVED)// && blReject) 
			{
				m_sIncomeRefund.Payment_Gateway_Receipt__c					= CreditCardRefundReturn.Receipt;
				m_sIncomeRefund.Payment_Gateway_Transaction_Approved__c 	= true;
				m_sIncomeRefund.Payment_Gateway_Response_Text__c			= QConstants.STR_IPPAYMENTS_TRANSACTION_TEXT_APPROVED;
				
				m_sIncomeRefund.All_Income_Processed__c						= true;
				m_sIncomeRefund.All_Income_Processed_Date__c				= System.Now();
				
				m_sIncomeRefund.All_Reconciled__c							= true;
				m_sIncomeRefund.All_Reconciled_Date__c						= System.Now();
				
				m_sIncomeRefund.Personal_Donation__c						= true;
				m_sIncomeRefund.Process_Payment_Through_Payment_Gateway__c	= true;
				
				m_sIncomeRefund.Status__c									= 'Refund Processed';
				
				m_sIncomeRefund.Reconciled_A__c								= true;
			}
			else
			{
				m_sIncomeRefund.Payment_Gateway_Response_Text__c	= QConstants.STR_IPPAYMENTS_TRANSACTION_TEXT_DECLINED;
				m_sIncomeRefund.Payment_Gateway_Declined_Message__c	= CreditCardRefundReturn.DeclinedMessage;
				m_sIncomeRefund.Payment_Gateway_Declined_Code__c	= CreditCardRefundReturn.DeclinedCode;
				m_sIncomeRefund.Status__c							= 'Declined';
				m_sIncomeRefund.Declined__c							= true;
			}
		}  
		try{
			insert m_sIncomeRefund;
			//we need to update the  Amount of the Opportunity
			Opportunity sOpportunityToUpdate = [select Id, Amount from Opportunity where Id = :m_sOpportunity.Id];
			sOpportunityToUpdate.Amount += m_sIncomeRefund.Amount_A__c;
			update sOpportunityToUpdate;
		}catch(Exception e)
		{
			
		}
    }
    
    private boolean ValidateInput()
    {
    	// now lets parse the Amount
    	try
    	{
    		Pattern pNoDP 	= Pattern.compile('[0-9]*');
			Pattern pOneDP 	= Pattern.compile('[0-9]*[.][0-9]{1}');
			Pattern pTwoDP 	= Pattern.compile('[0-9]*[.][0-9]{2}');
			
			if(pNoDP.matcher(this.ScreenAmount).matches())
			{
				m_iRefundAmount = integer.valueOf(this.ScreenAmount);
				m_iRefundAmount *= 100;
			}
			else if(pOneDP.matcher(this.ScreenAmount).matches())
			{
				m_iRefundAmount = integer.valueOf(this.ScreenAmount.replace('.', ''));
				m_iRefundAmount *= 10;
			}
			else if(pTwoDP.matcher(this.ScreenAmount).matches())
			{
				m_iRefundAmount = integer.valueOf(this.ScreenAmount.replace('.', ''));
			}
			else
			{
				throw new QScreenException('Amount has failed validation rules - please supply a valid payment amount');
			}
    	}
    	catch(Exception e)
    	{
    		throw new QScreenException('Exception thrown - ' + e);
    	}
    	
    	return true;
    }
    
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testMethod void testSubmitRefund()
	{
		
		PageReference pageRef = new PageReference('/apex/SubmitSingleRefundPayment');
		Test.setCurrentPage(pageRef);
		
		Account thisAccount = new Account();
		thisAccount.Name 	= 'Test Account';
		insert thisAccount;
		
		Contact thisContact 	= new Contact();
		thisContact.FirstName 	= 'Test';
		thisContact.LastName 	= 'Contact';
		thisContact.Email 		= 'testing@test.com';
		thisContact.AccountId 	= thisAccount.Id;
		insert thisContact;
		
		Opportunity thisOpportunity = new Opportunity();
		thisOpportunity.Name 		= 'Test Opportunity';
		thisOpportunity.StageName	= 'Automatic Testing';
		thisOpportunity.CloseDate	= System.Today().addMonths(1);
		thisOpportunity.AccountId 	= thisAccount.Id;
		thisOpportunity.Amount 		= 1000;
		insert thisOpportunity;
		
		Income__c sPayment 					= new Income__c();
		sPayment.Opportunity__c 			= thisOpportunity.Id;
		//sPayment.Contact__c					= thisContact.Id;
		sPayment.AccountId__c				=  thisAccount.Id;
		sPayment.Amount_A__c 				= 1000;
		sPayment.Payment_Gateway_Receipt__c = 'testing';
		insert sPayment;
		
		QVFSubmitSingleRefundPayment controller;
		
		boolean bExceptionThrown = false; 
	  /*	
		try
		{
			controller = new QVFSubmitSingleRefundPayment();
		}
		catch(Exception e)
		{
			bExceptionThrown = true;
		}
	
		System.AssertEquals(true, bExceptionThrown);
		*/
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', sPayment.Id);
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFSubmitSingleRefundPayment();
		controller.init();
		
		controller.Cancel();
		
		controller.ScreenAmount = '100';
		controller.SendRefund();
		
		controller.ScreenAmount = '100.0';
		controller.SendRefund();
		
		controller.ScreenAmount = '100.00';
		controller.SendRefund();
		
	}
	
		public static testMethod void testSubmitRefundAsFail()
	{
		
		PageReference pageRef = new PageReference('/apex/SubmitSingleRefundPayment');
		Test.setCurrentPage(pageRef);
		
		Account thisAccount = new Account();
		thisAccount.Name 	= 'Test Account';
		insert thisAccount;
		
		Contact thisContact 	= new Contact();
		thisContact.FirstName 	= 'Test';
		thisContact.LastName 	= 'Contact';
		thisContact.Email 		= 'testing@test.com';
		thisContact.AccountId 	= thisAccount.Id;
		insert thisContact;
		
		Opportunity thisOpportunity = new Opportunity();
		thisOpportunity.Name 		= 'Test Opportunity';
		thisOpportunity.StageName	= 'Automatic Testing';
		thisOpportunity.CloseDate	= System.Today().addMonths(1);
		thisOpportunity.AccountId 	= thisAccount.Id;
		thisOpportunity.Amount 		= 1000;
		insert thisOpportunity;
		
		Income__c sPayment 					= new Income__c();
		sPayment.Opportunity__c 			= thisOpportunity.Id;
		sPayment.AccountId__c				=  thisAccount.Id;
		//sPayment.Contact__c					= thisContact.Id;
		sPayment.Amount_A__c 				= 1000;
		sPayment.Payment_Gateway_Receipt__c = 'testing';
		insert sPayment;
		
		QVFSubmitSingleRefundPayment controller;
		
		boolean bExceptionThrown = false; 

		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', sPayment.Id);
		
		//blReject = true;
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFSubmitSingleRefundPayment();
		controller.init();
		
		controller.Cancel();
		
		controller.ScreenAmount = '100';
		controller.SendRefund();
		
		controller.ScreenAmount = '100.0';
		controller.SendRefund();
		
		controller.ScreenAmount = '100.00';
		controller.SendRefund();
		
	}
}