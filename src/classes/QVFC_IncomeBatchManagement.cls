public with sharing class QVFC_IncomeBatchManagement 
{
	public class RedkiteException extends Exception{}
	
	public	list<IncomeBatchTracker__c> jobs 				{get; set;}
	public	boolean						isActive			{get; set;}
	public	integer						NbRecordsRunning	{get; set;}
	public	IncomeBatchTracker__c		currentJob			{get; set;}
	public string						selectedBatch		{get; set;}
	public boolean						bTestErrorExecute	{get; set;}
	
	public QVFC_IncomeBatchManagement()
	{
			selectedBatch = 'QClubRedOpportunityProcessing';
	}
	
	public Pagereference init()
	{
		isActive = false;
		NbRecordsRunning = 0;
		
		try
		{		//get the last 15 batchJob 
			jobs = [select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c, BatchesProcessed__c, BatchName__c from IncomeBatchTracker__c where BatchName__c = :selectedBatch order by StartDatetime__c desc limit 15];
			
				//check if we can run the batch or not
			if(jobs == null || jobs.IsEmpty() || (jobs.size() > 0 && jobs[0].Status__c == 'Completed') )
			{	
				InitBatch();
			}
			else
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Please wait for the batch to be completed.'));
			}
		}
		catch(Exception e )
		{
			InitBatch();	
		}
		return null;
	}
	
	private void InitBatch()
	{	//the batch is almost ready at this stage, let verify the number of records
		NbRecordsRunning = 0;
		
		if(jobs == null)
			jobs = new list<IncomeBatchTracker__c>();
		
		try
		{
			 if(selectedBatch == 'QIncomeToBankProcessing')
			{
				NbRecordsRunning = [	select count() from Income__c 
										where 	Payment_Type_A__c = 'Credit Card'
										and		Payment_Gateway_Send_To_Bank_Date__c <= :System.today()
										and		Status__c = 'Send to IPPayments'
										and		Opportunity__r.Account.IPPayment_Customer_Token__c != null
										and		Opportunity__r.Account.IPPayment_Customer_Token__c != ''
									];
			}else if(selectedBatch == 'QClubRedOpportunityProcessing')
			{
				NbRecordsRunning = [	select count() from Opportunity 
										where 	StageName = 'Club Red Ongoing'
										and		Next_Recurring_Payment_Process_Date__c <= :System.today().addDays(QClubRedOpportunityProcessing.GetIncomeCreationLeadtimeInDays())];	
			}
		}catch(Exception e )
		{
			
		}
		
		if(NbRecordsRunning > 0)
		{		//ready to run the batch
			isActive = true;
						
			currentJob = new IncomeBatchTracker__c(
				BatchName__c = selectedBatch,
				Status__c = 'Processing',
				NbRecords__c = NbRecordsRunning,
				NbFailedRecords__c = 0
			);
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Batch is available for execution. '+NbRecordsRunning+' records will be executed.'));
		}
		else
		{	//no record need to be run
			isActive = false;		
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'No records need to be executed by the batch.'));	
		}
	}
	
	public Pagereference DisabledButtonBeforeExecute()
	{	//disable the button first for more security
		isActive = false;
		return null;
	}
	
	public Pagereference execute()
	{
		try
		{		//double check if we can run the batch or not 
			jobs = [select id, Status__c, ApexJobId__c, StartDatetime__c, EndDatetime__c, NbRecords__c, NbFailedRecords__c, BatchesProcessed__c from IncomeBatchTracker__c where BatchName__c = :selectedBatch order by StartDatetime__c desc limit 5];
			
			if( (jobs != null && jobs.size() > 0 && jobs[0].Status__c == 'Processing' ) || bTestErrorExecute)
			{	
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'The batch has been run by someone else. Please refresh the page.'));
				QVFC_IncomeBatchManagement.DetectProcessingBatch('WARNING : Processing Batch Detected','TEST IN SANDBOX');
				return null;
			}
		}
		catch(Exception e )
		{
			System.debug('Ready to execute batch!');
		}
		
		if(currentJob != null)
		{
			try
			{
				currentJob.StartDatetime__c = system.now();
				insert currentJob;
				
				//let's run the batch, everything looks good
				String jobId = '';
				String s = '0 ' + String.valueOf(system.now().minute() + 1) + ' ' + String.valueOf(System.now().hour()) + ' ' + String.valueOf(System.now().day()) + ' ' + String.valueOf(System.now().month()) + ' ? ' +String.valueOf(System.now().year());
		  		if(selectedBatch == 'QIncomeToBankProcessing' && !Test.isRunningTest())
				{
					currentJob.apexJobId__c = Database.executeBatch(new QIncomeToBankProcessing(), 1);
				}else if(selectedBatch == 'QClubRedOpportunityProcessing' && !Test.isRunningTest())
				{
					currentJob.apexJobId__c = Database.executeBatch(new QClubRedOpportunityProcessing(), 1);
				}
				update currentJob;
				
				if(jobs.IsEmpty())
					jobs.add(currentJob);
				else
					jobs.add(0,currentJob);
				
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Please wait for the batch to be completed.'));
				
			}catch(Exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Unexpected Error - '+e.getMessage()));	
			}
			
		}
		return null;
	}
	
	public static void DetectProcessingBatch(string strSubject , string strMessage)
	{
		try
		{
			// send login details e-mail
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			
			mail.setToAddresses(new String[] {'david.browaeys@quattroinnovation.com','cibbott@redkite.org.au'});
			mail.setSenderDisplayName('RED');
			mail.setBccSender(false);
			mail.setUseSignature(false);
			mail.setSaveAsActivity(false);
			
			mail.setPlainTextBody(strMessage);
			mail.setSubject(strSubject);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
		catch(Exception e)
		{
			
		}
	}
	
	public static testmethod void myUnitTest()
	{
		Account thisAccount = new Account();
        thisAccount.Name = 'Test Account';
        thisAccount.IPPayment_Customer_Token__c = '12345';
        insert thisAccount;
		 
		Opportunity qti = new Opportunity();
		qti.Name ='TREE';
		qti.AccountId = thisAccount.id;
		qti.StageName ='Club Red Ongoing';
		qti.Card_Expiry_Month__c = 11;
		qti.Credit_Card_Year__c = 2010;
		qti.OwnerId	= Userinfo.getUserId();
		qti.CloseDate = system.today().addDays(9);
		insert qti;
		
		Income__c sIncome = new Income__c();
		//sIncome.Contact__c = con.Id;
		sIncome.Payment_Type_A__c = 'Credit Card';
		sIncome.AccountId__c = thisAccount.id;
		sIncome.Payment_Gateway_Send_To_Bank_Date__c = System.Today().addDays(-3);
		sIncome.Status__c = 'Send to IPPayments';
		sIncome.Opportunity__c = qti.Id;
		sIncome.Amount_A__c = 20;
		sIncome.Card_Expiry_Month_A__c = 11;
		sIncome.Card_Expiry_Year_A__c = 2009;
		sIncome.Card_Expiry_Month_B__c = 11;
		sIncome.Card_Expiry_Year_B__c = 2010;
		insert sIncome;
		
		Test.startTest();
		
		QVFC_IncomeBatchManagement controller= new QVFC_IncomeBatchManagement();
		
		controller.init();
		
		controller.DisabledButtonBeforeExecute();
		
		controller.execute();
		
		System.assert(!controller.isActive);

		controller.init();
		
		controller.bTestErrorExecute = true;
		controller.execute();
		controller.bTestErrorExecute = false;
		
		//////////////////////////////////////////////////////////////////////
		controller= new QVFC_IncomeBatchManagement();
		
		controller.selectedBatch = 'QIncomeToBankProcessing';
		
		controller.init();
		
		controller.DisabledButtonBeforeExecute();
		controller.execute();
		
		controller.execute();
		
		Test.StopTest();
	}
}