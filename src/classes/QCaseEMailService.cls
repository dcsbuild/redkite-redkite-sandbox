global class QCaseEMailService implements Messaging.InboundEmailHandler 
{

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
    	string strEMailBody;
    	boolean bEMailFound = false;
    	
    	list<string> liEscalationPhrase = new list<string>();
    	
    	// lets get the Queue we will add to
    	//QueueSobject[] arrQSobject = [select SobjectType, QueueId, Id From QueueSobject q ]
    	
    	
    	// load the escalation phrases
		for(Case_Escalation_Phrase__c[] arrPhrase : [select Phrase__c from Case_Escalation_Phrase__c where Active__c = true])
		{
			for(Case_Escalation_Phrase__c sPhrase : arrPhrase)
			{
				liEscalationPhrase.add(sPhrase.Phrase__c.toLowerCase());
			}	
		}

	   	// Create an inboundEmailResult object for returning the result of the Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
		// Add the email plain text into the local variable
		strEMailBody = email.plainTextBody;
		
		// lets see if the email address exists in the database
		List<List<SObject>> searchList = [
											FIND :email.fromAddress 
											IN EMAIL FIELDS RETURNING 
											Account 	(id, name, IsPersonAccount, PersonContactId), 
											Contact 	(id, name), 
											Lead 		(id, name)
										];
										
		Account [] 		arrAccount 		= ((List<Account>)searchList[0]);
		Contact [] 		arrContact 		= ((List<Contact>)searchList[1]);
		Lead [] 		arrLead 		= ((List<Lead>)searchList[2]);
		

		Case sCase = new Case();
		
		sCase.Status 		= 'New';
		sCase.Source__c 	= 'Support EMail Address';
		sCase.Priority 		= 'Medium';
		
		for(integer i = 0; i < liEscalationPhrase.size(); i++)
		{
			if(strEMailBody.toLowerCase().contains(liEscalationPhrase[i]))
			{
				sCase.Priority 		= 'High';
			}
		}
		
		sCase.Subject 		= 'EMail Support - ' + email.Subject;
		
		sCase.Description 	 = 'Support Request EMail recieved from ' + email.fromName + ' : ' + email.fromAddress;
		sCase.Description 	+= '\r\n-----------------------------------------------------------------\r\n';
		sCase.Description 	+= strEMailBody;
		
		for(integer i = 0; i < arrAccount.size() && !bEMailFound; i++)
		{
			if(arrAccount[i].IsPersonAccount)
			{
				sCase.ContactId = arrAccount[i].PersonContactId;
				bEMailFound = true;
			}
		}
		
		for(integer i = 0; i < arrContact.size() && !bEMailFound; i++)
		{
			sCase.ContactId = arrContact[i].Id;
			bEMailFound = true;
		}
		
		for(integer i = 0; i < arrContact.size() && !bEMailFound; i++)
		{
			sCase.ContactId = arrContact[i].Id;
			bEMailFound = true;
		}
		
		for(integer i = 0; i < arrLead.size() && !bEMailFound; i++)
		{
			sCase.ContactId = arrLead[i].Id;
			bEMailFound = true;
		}
		
		insert sCase;
		
		result.success = true;
        return result;

    }
   
    /***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/	
	public static testmethod void testCaseEMailHandler()
	{
		
		// lets crate our person account first
		Account sPersonAccountCaseTest = new Account();
		
		sPersonAccountCaseTest.LastName 				= 'Person Account';
		sPersonAccountCaseTest.PersonEmail			= 'Automagical.Testing@QuattroDevelopmentServices.com';
		
		insert sPersonAccountCaseTest;
		
		Case_Escalation_Phrase__c sPhrase = new Case_Escalation_Phrase__c();
		sPhrase.Active__c = true;
		sPhrase.Phrase__c = 'hello';
		insert sPhrase;

		sPersonAccountCaseTest = [select id, PersonContactId from Account where id = :sPersonAccountCaseTest.Id];
		
		// now the processing
		QCaseEMailService objEMailService = new QCaseEMailService();
		
		Messaging.InboundEmail 		email 		= new Messaging.InboundEmail();
		Messaging.InboundEnvelope 	envelope	= new Messaging.InboundEnvelope();
		
		email.fromAddress 		= 'Automagical.Testing@QuattroDevelopmentServices.com';
		email.Subject 			= 'Automatic Testing!';
		email.plainTextBody 	= 'hello - Nothing to see here!';
		
		try
		{
   	 		Messaging.InboundEmailResult handleInboundEmail = objEMailService.handleInboundEmail(email, envelope);
		}
		catch(Exception e)
		{
			// some validation rule has been put into production and is not in this environment........
			// this is what happens when things become misaligned!
		}
   	 	
   	 	//Case sCase = [select id, Subject, Description from Case where ContactId = :sPersonAccount.PersonContactId];
   	 	
   	 	//System.Assert(sCase.Subject.contains(email.Subject));
   	 	//System.Assert(sCase.Description.contains(email.plainTextBody));
 	 	
	}
}