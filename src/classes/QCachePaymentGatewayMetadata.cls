public with sharing class QCachePaymentGatewayMetadata 
{
	private static CachePaymentGatewayMetadata__c m_sCachePaymentGatewayMetadata;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
	public static string getUsername()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Username__c;
	}

	public static string getPassword()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Password__c;
	}
	
	public static string getTokenAlgorithmId()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Token_Algorithm_ID__c;
	}
	
	public static string getTokenProcessId()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Token_Process_ID__c;
	}
	
	public static string getDTSServiceLocation()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.DTS_Service_Location__c;
	}
	
	public static string getSIPPServiceLocation()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.SIPP_Service_Location__c;
	}
	
	public static integer getPaymentBatchSize()
	{
		/*
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata();
			
		return (m_sCachePaymentGatewayMetadata.Payment_Batch_Size__c == null || m_sCachePaymentGatewayMetadata.Payment_Batch_Size__c > 10) ? 1 : m_sCachePaymentGatewayMetadata.Payment_Batch_Size__c.intValue();
		*/
		
		return 1;
	}
	
	public static string getPaymentRecordTypeId()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Payment_Recordtype_Id__c;
	}
	
	public static string getClubRedPaymentRecordTypeId()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Club_Red_Payment_RecordType_Id__c;
	}
	
	public static string getRefundRecordTypeId()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Refund_Recordtype_Id__c;
	}
	
	public static string getClubRedRefundRecordTypeId()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.ClubRed_Refund_RecordType_Id__c;
	}
	
	public static string getTaskOwnerIDForDeclinedPayments()
	{
		if(m_sCachePaymentGatewayMetadata == null)
			LoadMetadata(); 
		
		return m_sCachePaymentGatewayMetadata.Task_Owner_ID_Batch_Declined_Payments__c;
	}
	
	/***********************************************************************************************************
	Worker Methods
	***********************************************************************************************************/
	private static void LoadMetadata()
	{
		m_sCachePaymentGatewayMetadata = CachePaymentGatewayMetadata__c.getInstance();
	}
	
	/***********************************************************************************************************
	Test Methods
	***********************************************************************************************************/
	public static testmethod void testQCacheCMSMetadata()
	{
		getUsername();
		m_sCachePaymentGatewayMetadata = null;
		
		getPassword();
		m_sCachePaymentGatewayMetadata = null;
		
		getDTSServiceLocation();
		m_sCachePaymentGatewayMetadata = null;
		
		getTokenAlgorithmId();
		m_sCachePaymentGatewayMetadata = null;
		
		getTokenProcessId();
		m_sCachePaymentGatewayMetadata = null;
		
		getSIPPServiceLocation();
		m_sCachePaymentGatewayMetadata = null;
	}
}