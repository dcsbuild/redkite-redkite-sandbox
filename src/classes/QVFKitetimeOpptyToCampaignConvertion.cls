public class QVFKitetimeOpptyToCampaignConvertion 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	// Exception Classes
	public class QScreenParameterException 	extends Exception{}
	public class QDataValidationException 	extends Exception{}
	
		
	////////////////////////////////////////////////////////////////////////////////////////
	// Singular
	private	string 			m_strID;				// the opportunity id that we're coming from

	public	Opportunity m_sOpportunity{get; private set;}	// the opportunity that we're coming from
	public 	Campaign	m_sCampaign {get; set;}				// what we're saving
	public	boolean		m_bError 	{get; set;}				// if we have had an error yet
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Collections

	////////////////////////////////////////////////////////////////////////////////////////
	// Constructor
	public QVFKitetimeOpptyToCampaignConvertion()
	{
		// lets grab the screen parameters
		if(null == (m_strID = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new QScreenParameterException('Id Parameter not found');	
		}
		
		Init();
	}
	
	public void Init()
	{	
		this.m_sCampaign = new Campaign();
			
		// set everything up
		this.m_bError = false;
		
		try
		{
			LoadOpportunityDetails();
			
			SetupCampaign();
		}
		catch(QDataValidationException e)
		{
			ApexPages.addMessages(e);
			this.m_bError = true;
		}
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	public pageReference Cancel()
	{
		PageReference pageRedirect = new PageReference('/' + this.m_strID);
		pageRedirect.setRedirect(true);
		
		return pageRedirect;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	public pageReference Save()
	{
		PageReference pageRedirect;
		
		Savepoint dbSavePoint = Database.setSavepoint();
		
		try
		{
			
			insert this.m_sCampaign;
			
			this.m_sOpportunity.CampaignId = this.m_sCampaign.Id;
			
			update this.m_sOpportunity;
			
		}
		catch(Exception e)
		{
			Database.rollback(dbSavePoint);
			
			ApexPages.addMessages(e);
			this.m_bError = true;
		}
		
		if(!m_bError)
		{
			pageRedirect = new PageReference('/' + this.m_sCampaign.Id);
			pageRedirect.setRedirect(true);
		}
		
		return pageRedirect;
	}
	
	/***********************************************************************************************************
    	LIST METHODS
    ***********************************************************************************************************/
    
    /***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	//
	private void LoadOpportunityDetails()
	{
		try
		{
			this.m_sOpportunity = [
									select 	id,
											IsWon,
											IsClosed,
											AccountId,
											Account.Name, 
											Employee_Donations_Available__c,
											Employee_Donation_Target__c,
											Employer_Donation_Matching__c,
											Vouchers_Available__c,
											Number_of_Vouchers__c,
											Kitetime_Corporate_Target__c,
											CampaignId
									from 	Opportunity 
									where 	id = :m_strID
								];
								
			if(!this.m_sOpportunity.IsClosed || !this.m_sOpportunity.IsWon)
				throw new QDataValidationException('The Opportunity is not Closed Won');
		}
		catch(Exception e)
		{
			System.Debug('LoadOpportunityDetails() - ' + this.m_strID + ' - ' + e);
			
			throw new QDataValidationException('Error Loading Opportunity - ' + e.getMessage());
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	private void SetupCampaign()
	{
		// lets get the record type we need to use for the Payment Records
		Schema.DescribeSObjectResult 		schemaDescribeCampaign		= Campaign.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeNameCampaign	= schemaDescribeCampaign.getRecordTypeInfosByName();
		
		this.m_sCampaign.Name 								= this.m_sOpportunity.Account.Name + ' - Kitetime';
		this.m_sCampaign.IsActive							= false;
		this.m_sCampaign.Status								= 'Planned';
		this.m_sCampaign.Primary_Objective__c				= 'Fundraising';
		this.m_sCampaign.Fundraising_Activity__c			= 'Kitetime';
		this.m_sCampaign.RecordTypeId						= mapRecordTypeNameCampaign.get('Kitetime - KTM').getRecordTypeId();
		this.m_sCampaign.Kitetime_Account__c				= this.m_sOpportunity.AccountId;
		this.m_sCampaign.Employee_Donations_Available__c	= this.m_sOpportunity.Employee_Donations_Available__c;
		this.m_sCampaign.Employer_Donation_Matching__c		= this.m_sOpportunity.Employer_Donation_Matching__c;
		this.m_sCampaign.Vouchers_Available__c				= this.m_sOpportunity.Vouchers_Available__c;
		this.m_sCampaign.Number_of_Vouchers_Purchased__c	= this.m_sOpportunity.Number_of_Vouchers__c;
		this.m_sCampaign.ExpectedRevenue					= this.m_sOpportunity.Kitetime_Corporate_Target__c;
		this.m_sCampaign.ParentId							= this.m_sOpportunity.CampaignId;
		
		string[] strAccountNameComponents = this.m_sOpportunity.Account.Name.split(' ');
		
		for(integer i = 0; i < strAccountNameComponents.size(); i++)
		{
			if(i == 0)
				this.m_sCampaign.Campaign_Code__c = strAccountNameComponents[i].substring(0,1).toUpperCase();
			else
				this.m_sCampaign.Campaign_Code__c += strAccountNameComponents[i].substring(0,1).toUpperCase();
		}
		
		integer iCounter = 0;
		
		for(Campaign[] arrCampaign : ([select id from Campaign where Kitetime_Account__c = :this.m_sOpportunity.AccountId]))
		{
			for(Campaign sCampaign : arrCampaign)
			{
				iCounter++;
			}
		}
		
		iCounter++;
		
		this.m_sCampaign.Campaign_Code__c += iCounter.format().replaceAll(',', '');
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testMethod void testQVFKitetimeOpptyToCampaignConvertion()
	{
		QVFKitetimeOpptyToCampaignConvertion oController;
		
		boolean bError = false;
		
		try
		{
			oController = new QVFKitetimeOpptyToCampaignConvertion();
		}
		catch(Exception e)
		{
			bError = true;	
		}
					
		System.Assert(bError);
		
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Opportunity.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
		
		Account	sAccount = new Account();
		sAccount.Name = 'Test Account';
		
		insert sAccount;
		System.Assert(sAccount.Id != null);
		
		Contact sContact = new Contact();
		sContact.AccountId 	= sAccount.id;
		sContact.FirstName 	= 'Test';
		sContact.LastName 	= 'Test';
		
		insert sContact;
		System.Assert(sContact.Id != null);
		
		Opportunity sOpportunity 	= new Opportunity();
		sOpportunity.AccountId 		= sAccount.id;
		sOpportunity.Name 			= sAccount.Name;
		sOpportunity.Contact__c 	= sContact.id;
		sOpportunity.Amount 		= 0;
		sOpportunity.StageName 		= 'Agreement Signed';
		sOpportunity.RecordTypeId 	= mapRecordTypeName.get('Kitetime').getRecordTypeId();
		sOpportunity.CloseDate 		= System.Today();
		
		insert sOpportunity;
		System.Assert(sOpportunity.Id != null);
		
		ApexPages.currentPage().getParameters().put('id', sOpportunity.Id);

		oController = new QVFKitetimeOpptyToCampaignConvertion();
		oController.Save();
		oController.Cancel();
		
		sOpportunity = [select id from Opportunity where id = :sOpportunity.Id];
	}
}