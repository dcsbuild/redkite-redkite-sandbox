public class QUtils 
{
	/***********************************************************************
	* Exception Classes
	*/
	public class QDBException extends Exception{}
	public class QException extends Exception{}
	
	/***********************************************************************
	* Campaign Loader - will return null if nothing is loaded	
	*/
	public static Campaign LoadCampaignById(id idCampaign)
	{
		Campaign sCampaign;
		
		try
		{
			sCampaign = [select id, Name from Campaign where id = :idCampaign];
		}
		catch(Exception e)
		{
			System.Debug('Campaign Not Found for Id:'+idCampaign+ ' - ' + e);
		}
			
		return sCampaign;
	}
	
	public static testmethod void testLoadCampaignById()
	{
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing Name abc123';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		Campaign sCampaignResults = QUtils.LoadCampaignById(sCampaign.Id);
		
		System.AssertEquals(sCampaign.Id, 	sCampaignResults.Id);
		// workflow is now changing the name!!!
		System.Assert(sCampaignResults.Name.contains(sCampaign.Name));
		
		// create our Campaign
		Account sAccount = new Account();
		sAccount.Name = 'Testing Account abc123';
		insert sAccount;
		
		// failure testing
		Account sAccountTest = [select id, Name from Account limit 1];
		
		sCampaignResults = LoadCampaignById(sAccountTest.Id);
		
		System.AssertEquals(null, sCampaignResults);
	}
	/**************************************************************************************************************************/
	
	/***********************************************************************
	* Income DB Methods - will return null if nothing is loaded	
	*/
	public static Database.SaveResult[] InsertIncomeListToDB(list <Income__c> theList)
	{
		Database.SaveResult[] result;
		
		if(theList.size() > 200)
		{
			throw new QDBException('To many elements to Insert!');
		}
		
		result = Database.Insert(theList, false);
			
		return result;
	}
	
	public static testmethod void testInsertIncomeListToDB()
	{
		list<Income__c> liIncome = new list<Income__c>();
		
		Income__c sIncome = new Income__c();
		
		liIncome.add(sIncome);
		
		InsertIncomeListToDB(liIncome);
	}	
	/**************************************************************************************************************************/
	
	/***********************************************************************
	* String Alpha Methods - will return 0 to 26 for the character of the first letter
	*/
	public static integer GetCharacterOffset(string strInput)
	{
		integer iRet = -1;
		
		string strChar = strInput.toLowerCase().subString(0,1);
		
		if(strChar == 'a'){iRet = 0;}
		else if(strChar == 'b'){iRet = 1;}
		else if(strChar == 'c'){iRet = 2;}
		else if(strChar == 'd'){iRet = 3;}
		else if(strChar == 'e'){iRet = 4;}
		else if(strChar == 'f'){iRet = 5;}
		else if(strChar == 'g'){iRet = 6;}
		else if(strChar == 'h'){iRet = 7;}
		else if(strChar == 'i'){iRet = 8;}
		else if(strChar == 'j'){iRet = 9;}
		else if(strChar == 'k'){iRet = 10;}
		else if(strChar == 'l'){iRet = 11;}
		else if(strChar == 'm'){iRet = 12;}
		else if(strChar == 'n'){iRet = 13;}
		else if(strChar == 'o'){iRet = 14;}
		else if(strChar == 'p'){iRet = 15;}
		else if(strChar == 'q'){iRet = 16;}
		else if(strChar == 'r'){iRet = 17;}
		else if(strChar == 's'){iRet = 18;}
		else if(strChar == 't'){iRet = 19;}
		else if(strChar == 'u'){iRet = 20;}
		else if(strChar == 'v'){iRet = 21;}
		else if(strChar == 'w'){iRet = 22;}
		else if(strChar == 'x'){iRet = 23;}
		else if(strChar == 'y'){iRet = 24;}
		else if(strChar == 'z'){iRet = 25;}
		else {iRet = 26;}
			
		return iRet;
	}
	
	public static testmethod void testGetCharacterOffset()
	{
		System.Assert(GetCharacterOffset('a') ==0);
		System.Assert(GetCharacterOffset('b') ==1);
		System.Assert(GetCharacterOffset('c') ==2);
		System.Assert(GetCharacterOffset('d') ==3);
		System.Assert(GetCharacterOffset('e') ==4);
		System.Assert(GetCharacterOffset('f') ==5);
		System.Assert(GetCharacterOffset('g') ==6);
		System.Assert(GetCharacterOffset('h') ==7);
		System.Assert(GetCharacterOffset('i') ==8);
		System.Assert(GetCharacterOffset('j') ==9);
		System.Assert(GetCharacterOffset('k') ==10);
		System.Assert(GetCharacterOffset('l') ==11);
		System.Assert(GetCharacterOffset('m') ==12);
		System.Assert(GetCharacterOffset('n') ==13);
		System.Assert(GetCharacterOffset('o') ==14);
		System.Assert(GetCharacterOffset('p') ==15);
		System.Assert(GetCharacterOffset('q') ==16);
		System.Assert(GetCharacterOffset('r') ==17);
		System.Assert(GetCharacterOffset('s') ==18);
		System.Assert(GetCharacterOffset('t') ==19);
		System.Assert(GetCharacterOffset('u') ==20);
		System.Assert(GetCharacterOffset('v') ==21);
		System.Assert(GetCharacterOffset('w') ==22);
		System.Assert(GetCharacterOffset('x') ==23);
		System.Assert(GetCharacterOffset('y') ==24);
		System.Assert(GetCharacterOffset('z') ==25);
		System.Assert(GetCharacterOffset('*') ==26);

	}	
	/**************************************************************************************************************************/
	
	/***********************************************************************
	* Task Creation Method
	*/
	public static Task CreateErrorTask(id idWhat, id idWho, string strSubject, string strMessage)
	{
		try
		{
			User sUser = [select id from User where Assign_APEX_Processing_Error_Task__c = true and IsActive = true limit 1];
			
			return CreateErrorTask(sUser.Id, idWhat, idWho, strSubject, strMessage);
		}
		catch(Exception e)
		{
			string strBody = 'A Processing exception has occured however I was unable to create a Task due to the following error.  Please inspect these details to see what has gone wrong \n';
			strBody += '\n\n Task Details - ' + strSubject + ' - ' + strMessage;
			strBody += '\n\n Exception Details - ' + e; 
			
			CreateErrorEmail('FATAL ERROR - EMAIL FOR TASK EXCEPTION HAS FAILED!', strBody);
		}
		
		return null;
	}
	
	public static Task CreateErrorTask(id OwnerId, id idWhat, id idWho, string strSubject, string strMessage)
	{
		Task sTask;
		
		try
		{
			sTask = new Task();
			sTask.OwnerId		= OwnerId;
			sTask.WhatId 		= idWhat;
			sTask.WhoId 		= idWho;
			sTask.Subject 		= strSubject;
			sTask.Description 	= strMessage;
			sTask.ActivityDate	= System.Today();
			
			Database.saveResult saveResult = Database.insert(sTask, false);
			
			if(!saveResult.isSuccess())
			{
				string strErrors = '';
				
				for(Database.Error dbError : saveResult.getErrors())
				{
					strErrors += ': Status Code - ' + dbError.getStatusCode() + ': Error Message - ' + dbError.getMessage();
				}
				
				throw new QDBException('Task Insert Error - ' + strErrors);
			}
		}
		catch(Exception e)
		{
			string strBody = 'A Processing exception has occured however I was unable to create a Task due to the following error.  Please inspect these details to see what has gone wrong \n';
			strBody += '\n\n Task Details - ' + strSubject + ' - ' + strMessage;
			strBody += '\n\n Exception Details - ' + e; 
			
			CreateErrorEmail('FATAL ERROR - EMAIL FOR TASK EXCEPTION HAS FAILED!', strBody);
		}
			
		return sTask;
	}
	
	public static testmethod void testCreateErrorTask()
	{
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing Name abc123';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		Task sTask = CreateErrorTask(sCampaign.Id, null, 'testing', 'testing');
	}
	
	/***********************************************************************
	* Error Email Creation Method
	*/
	public static void CreateErrorEmail(string strSubject, string strMessage)
	{
		
		try
		{
			User	sUser = [select id, name from User where IsActive = true and Send_Fatal_Error_Processing_Email__c = true limit 1];
			
			CreateEmail(sUser.Id, strSubject, strMessage);
			
		}
		catch(Exception e1)
		{
			System.Debug('FATAL ERROR - EMAIL FOR TASK EXCEPTION HAS FAILED! - ' + e1);
			throw new QException('FATAL ERROR - EMAIL FOR TASK EXCEPTION HAS FAILED! - ' + e1);
		}
	}
	
	public static testmethod void testCreateErrorEmail()
	{
		CreateErrorEmail('testing', 'testing');
	}
	
	/***********************************************************************
	* Email Creation Method
	*/
	public static void CreateEmail(id idRecipient, string strSubject, string strMessage)
	{
		
		try
		{
			// send login details e-mail
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			
			mail.setTargetObjectId(idRecipient);
			mail.setReplyTo('info@redkite.org.au');
			mail.setSenderDisplayName('RED');
			mail.setBccSender(false);
			mail.setUseSignature(false);
			mail.setSaveAsActivity(false);
			
			mail.setPlainTextBody(strMessage);
			mail.setSubject(strSubject);
			
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
		catch(Exception e1)
		{
			System.Debug('FATAL ERROR - EMAIL FOR TASK EXCEPTION HAS FAILED! - ' + e1);
			throw new QException('FATAL ERROR - EMAIL FOR TASK EXCEPTION HAS FAILED! - ' + e1);
		}
	}
	
	public static testmethod void testCreateEmail()
	{
		User	sUser = [select id from User where Id = :Userinfo.getUserId()];
		CreateEmail(sUser.Id, 'testing', 'testing');
	}
	
	public static string GetStateFromPostCode(string strPostCode)
	{
		if(strPostCode == null || strPostCode == '')
		{
			return 'INTL';
		}
		
		try
		{
			integer iPostCode = integer.valueOf(strPostCode);
			
			if(iPostCode >= 800 && iPostCode < 999)
			{
				return 'NT';
			}
			else if(iPostCode < 3000)
			{
				return 'NSW';
			}
			else if(iPostCode < 4000)
			{
				return 'VIC';
			}
			else if(iPostCode < 5000)
			{
				return 'QLD';
			}
			else if(iPostCode < 6000)
			{
				return 'SA';
			}
			else if(iPostCode < 7000)
			{
				return 'WA';
			}
			else if(iPostCode < 8000)
			{
				return 'TAS';
			}
			else if(iPostCode < 9000)
			{
				return 'VIC';
			}
			else if(iPostCode >= 9000)
			{
				return 'QLD';
			}
		}
		catch(Exception e)
		{
			return 'INTL';
		}
		
		// how do we get to here?
		return 'INTL';
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public static date ConvertSettlementDate(string strDate)
	{
		date dtRet;
				
		string[] arrStringSettlementDate = strDate.split('-');
		
		if(arrStringSettlementDate.size() == 3)
		{
			dtRet = date.newInstance(integer.valueOf(arrStringSettlementDate[2]), GetMonth(arrStringSettlementDate[1]), integer.valueOf(arrStringSettlementDate[0]));
		}
		
		return dtRet;
	}
	
	public static  integer GetMonth(string strMonth)
	{
		integer iRet = 1;
		
		if(strMonth.toLowerCase() == 'feb')
			iRet = 2;
		else if(strMonth.toLowerCase() == 'mar')
			iRet = 3;
		else if(strMonth.toLowerCase() == 'apr')
			iRet = 4;
		else if(strMonth.toLowerCase() == 'may')
			iRet = 5;
		else if(strMonth.toLowerCase() == 'jun')
			iRet = 6;
		else if(strMonth.toLowerCase() == 'jul')
			iRet = 7;
		else if(strMonth.toLowerCase() == 'aug')
			iRet = 8;
		else if(strMonth.toLowerCase() == 'sep')
			iRet = 9;
		else if(strMonth.toLowerCase() == 'oct')
			iRet = 10;
		else if(strMonth.toLowerCase() == 'nov')
			iRet = 11;
		else if(strMonth.toLowerCase() == 'dec')
			iRet = 12;
			
		return iRet;
	}
	
	public static testmethod void testGetStateFromPostcode()
	{
		GetStateFromPostCode('');
		GetStateFromPostCode('ACT');
		GetStateFromPostCode('NSW');
		GetStateFromPostCode('NT');
		GetStateFromPostCode('QLD');
		GetStateFromPostCode('SA');
		GetStateFromPostCode('TAS');
		GetStateFromPostCode('VIC');
		GetStateFromPostCode('WA');
		GetStateFromPostCode('Hello, World!');
	}
	
	/***********************************************************************
	* Credit Card Type Calculator Method
	*/
	public static string GetCreditCardType(string strCreditCardNumber)
	{
		if(strCreditCardNumber == null || strCreditCardNumber == '')
		{
			return '';
		}
		
		try
		{
			if(strCreditCardNumber.startsWith('4') && (strCreditCardNumber.length() == 13 || strCreditCardNumber.length() == 16))
			{
				return 'Visa';
			}
			else if(strCreditCardNumber.length() == 16)
			{
				// potentially a MasterCard
				integer iPrefix = integer.valueOf(strCreditCardNumber.substring(0, 2));
				if(iPrefix >= 51 && iPrefix <= 55)
				{
					return 'Mastercard';
				}
			}
			else if(strCreditCardNumber.length() == 15)
			{
				// potentially amex
				integer iPrefix = integer.valueOf(strCreditCardNumber.substring(0, 2));
				if(iPrefix == 34 || iPrefix == 37)
				{
					return 'Amex';
				}
			}
			else if(strCreditCardNumber.length() == 14)
			{
				integer iPrefix = integer.valueOf(strCreditCardNumber.substring(0, 3));
				if(iPrefix >= 300 && iPrefix <= 305)
				{
					return 'Diners';
				}
				
				iPrefix = integer.valueOf(strCreditCardNumber.substring(0, 2));
				if(iPrefix == 36 || iPrefix == 38)
				{
					return 'Diners';
				}
			}
		}
		catch(Exception e)
		{
		}
		
		return '';
	}
	
	public static testmethod void testGetCreditCardType()
	{
		System.Assert(GetCreditCardType('') == '');
		System.Assert(GetCreditCardType('4444444444444') == 'Visa');
		System.Assert(GetCreditCardType('5200000000000000') == 'Mastercard');
		System.Assert(GetCreditCardType('340000000000000') == 'Amex');
		System.Assert(GetCreditCardType('30100000000000') == 'Diners');
		System.Assert(GetCreditCardType('36000000000000') == 'Diners');
		System.Assert(GetCreditCardType('Hello, world!!') == '');
	}
	
	/* method not required, trigger commented out
	public static testmethod void testLeadNotificationsTrigger()
	{
		RecordType sRT = [select Id from RecordType where DeveloperName = 'Family_Services' limit 1];
		Lead sLead = new Lead();
		sLead.FirstName = 'Test';
		sLead.LastName = 'Method';
		sLead.RecordTypeId = sRT.Id;
		insert sLead;	
	}
	*/
	
	// New Method used to provide a formatted error message in tasks and emails for non technical users i.e. Finance.
	public static string strFormattedMessage(QProcessPayment.QProcessPaymentRet oPaymentProcessorStage1Ret, QProcessPayment.QProcessPaymentArgs oPaymentProcessorStage1Args)
	{
	string strMessage = '\n';
	
	// Amount is provided in cents, translate to currency format and convert to dollar context, also fix date and datetimeformat
	string 		strAmount			= '$' + decimal.valueof(oPaymentProcessorStage1Args.Amount / 100).setscale(2).format();
	string		strSettlementDate	= Date.valueof(oPaymentProcessorStage1Ret.IPPResponse.SettlementDate).format();
	
	strMessage = strMessage + 'CustRef - ' + oPaymentProcessorStage1Args.IPPTransactionRef + '\n';
	strMessage = strMessage + 'Amount - ' + strAmount + '\n';
	strMessage = strMessage + 'CardHolderName - ' + oPaymentProcessorStage1Args.CreditCardName + '\n';
	strMessage = strMessage + 'ResponseCode - ' + oPaymentProcessorStage1Ret.IPPResponse.ResponseCode + '\n';
	strMessage = strMessage + 'TimeStamp - ' + oPaymentProcessorStage1Ret.IPPResponse.TimeStamp + '\n';
	strMessage = strMessage + 'SettlementDate - ' + strSettlementDate + '\n';
	strMessage = strMessage + 'DeclinedCode - ' + oPaymentProcessorStage1Ret.IPPResponse.DeclinedCode + '\n';
	strMessage = strMessage + 'DeclinedMessage - ' + oPaymentProcessorStage1Ret.IPPResponse.DeclinedMessage + '\n';

	return strMessage;
	
	}
}