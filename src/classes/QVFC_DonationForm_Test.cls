@isTest(SeeAllData=true)
public with sharing class QVFC_DonationForm_Test 
{
	public static testmethod void testIndividual()
	{
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		QVFC_DonationForm controller = new QVFC_DonationForm();
		controller.Init();  
		QVFC_DonationForm.getDonationOptions();
		
		QVFC_DonationForm.JSRemoteStage1Arg arg = new QVFC_DonationForm.JSRemoteStage1Arg();
		//contact details
		arg.Title = 'Mr';
		arg.FirstName = 'Test';
		arg.LastName = 'Quattro';
		arg.Postal = 'Test Street';
		arg.Suburb = 'Test';
		arg.PostCode = '3000';
		arg.State = 'VIC';
		arg.Country = 'Australia';
		arg.Email = 'test@quattro.com';
		arg.Phone = '0456789123';
		arg.PhoneType = 'Mobile';
		
		//credit card details
		arg.CCName = 'Test Card';
		arg.CCNumber = '4111111111111111';
		arg.CCEXPY = '2015';
		arg.CCEXPM = '10';
		arg.CCCVS = '123';
		//donation details
		arg.MainCampaignId = sCampaign.Id;
		arg.SubCampaignId = sCampaign.Id;
		arg.Amount = '500';
		arg.Comments = 'Test donation';
		arg.DonationSource = 'Testing';
		arg.DonationType = 'Individual';
		
		QVFC_DonationForm.JSRemoteStage1Ret ret = QVFC_DonationForm.performIndividualStage1(arg);
		
		System.Assert(!ret.InError);
		
		QVFC_DonationForm.JSRemoteStage2Arg arg2 = new QVFC_DonationForm.JSRemoteStage2Arg();
		
		arg2.Amount = ret.Amount;
		arg2.CampaignId = ret.CampaignId;
		arg2.CCCVS = ret.CCCVS;
		arg2.CCEXPM = ret.CCEXPM;
		arg2.CCEXPY = ret.CCEXPY;
		arg2.CCName = ret.CCName;
		arg2.CCNumber = ret.CCNumber;
		arg2.Comments = ret.Comments;
		arg2.UserId = ret.UserId;
		arg2.DonationSource = ret.DonationSource;
		arg2.DonationType = ret.DonationType;
		
		QVFC_DonationForm.JSRemoteStage2Ret ret2 = QVFC_DonationForm.performPayment(arg2);
		
		// This will always fail as its trying to do a call out
		//System.Assert(ret2.InError);
		
	}
	
	public static testmethod void testBusiness()
	{
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		QVFC_DonationForm controller = new QVFC_DonationForm();
		controller.Init();  
		QVFC_DonationForm.getDonationOptions();
		
		QVFC_DonationForm.JSRemoteStage1Arg arg = new QVFC_DonationForm.JSRemoteStage1Arg();
		//contact details
		arg.Title = 'Mr';
		arg.FirstName = 'Test';
		arg.LastName = 'Quattro';
		arg.Postal = 'Test Street';
		arg.Suburb = 'Test';
		arg.PostCode = '3000';
		arg.State = 'VIC';
		arg.Country = 'Australia';
		arg.Email = 'test@quattro.com';
		arg.Phone = '0456789123';
		arg.PhoneType = 'Mobile';
		
		arg.ParentName = 'Test Company';
		arg.ParentWebsite = 'www.testsite.com';
		arg.ParentPhone = '0123654987';
		arg.ParentPostal = 'Postal';
		arg.ParentSuburb = 'Suburb';
		arg.ParentPostcode = '1000';
		arg.ParentState = 'ACT';
		arg.ParentCountry = 'Australia';
		arg.ParentEmail = 'test@testing.com';
		
		//credit card details
		arg.CCName = 'Test Card';
		arg.CCNumber = '4111111111111111';
		arg.CCEXPY = '2015';
		arg.CCEXPM = '10';
		arg.CCCVS = '123';
		//donation details
		arg.MainCampaignId = sCampaign.Id;
		arg.SubCampaignId = sCampaign.Id;
		arg.Amount = '500';
		arg.Comments = 'Test donation';
		arg.DonationSource = 'Testing';
		arg.DonationType = 'Organisation';
		
		QVFC_DonationForm.JSRemoteStage1Ret ret = QVFC_DonationForm.performNonIndividualStage1(arg);
		
		System.Assert(!ret.InError);
		
		QVFC_DonationForm.JSRemoteStage2Arg arg2 = new QVFC_DonationForm.JSRemoteStage2Arg();
		
		arg2.Amount = ret.Amount;
		arg2.CampaignId = ret.CampaignId;
		arg2.CCCVS = ret.CCCVS;
		arg2.CCEXPM = ret.CCEXPM;
		arg2.CCEXPY = ret.CCEXPY;
		arg2.CCName = ret.CCName;
		arg2.CCNumber = ret.CCNumber;
		arg2.Comments = ret.Comments;
		arg2.UserId = ret.UserId;
		arg2.DonationSource = ret.DonationSource;
		arg2.DonationType = ret.DonationType;
		
		QVFC_DonationForm.JSRemoteStage2Ret ret2 = QVFC_DonationForm.performPayment(arg2);
		
		// This will always fail as its trying to do a call out
		//System.Assert(ret2.InError);
		
	}
}