@isTest(SeeAllData=true)
public with sharing class QCorporateWebsite_Testing 
{
	public static testmethod void TopLevel_TestCampaignUpdateRequest()
	{
		QCorporateWebsiteWebServices.QUpdateCampaignRequest args = new QCorporateWebsiteWebServices.QUpdateCampaignRequest();
		QCorporateWebsiteWebServices.QUpdateCampaignResults res = new QCorporateWebsiteWebServices.QUpdateCampaignResults();
		boolean bError = false;
				
		try	{QCorporateWebsiteWebServices.CampaignUpdateRequest(args);}
		catch(Exception e) {bError = true;}
		System.Assert(bError);
		
		bError = false;
		args.UniqueCampaignCode = '12345';
		try	{QCorporateWebsiteWebServices.CampaignUpdateRequest(args);}
		catch(Exception e) {bError = true;}
		System.Assert(bError);
	}
	
	public static testmethod void TopLevel_TestCampaignRegisterUserRequest()
	{
		QCorporateWebsiteWebServices.QCampaignRegisterUserRequest args = new QCorporateWebsiteWebServices.QCampaignRegisterUserRequest();
		QCorporateWebsiteWebServices.QCampaignRegisterUserResults res;
	
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Title is a required field.');}

		args.Title = 'Chief';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'First Name is a required field.');}
		
		args.FirstName = 'Jim';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Last Name is a required field.');}

		args.LastName = 'Bob';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Phone Number is a required field.');}

		args.PhoneNumber = '0202020202';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Email is a required field.');}

		args.Email = 'somewhere@somesite.com';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Street is a required field.');}

		args.Street = 'Some St';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Suburb is a required field.');}

		args.Suburb = 'Someburb';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'State is a required field.');}

		args.State = 'State';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Country is a required field.');}

		args.Country = 'Someland';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'PostalCode is a required field.');}

		args.PostalCode = '1000';
		
		boolean bError = false;
		try {res = QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {bError = true;}
		System.Assert(!bError);
	}
	public static testmethod void TopLevel_TestCampaignRegisterUserRequest2()
	{
		QCorporateWebsiteWebServices.QCampaignRegisterUserRequest args = new QCorporateWebsiteWebServices.QCampaignRegisterUserRequest();
		QCorporateWebsiteWebServices.QCampaignRegisterUserResults res;
	
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Title is a required field.');}

		args.Title = 'Mr';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'First Name is a required field.');}
		
		args.FirstName = 'David';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Last Name is a required field.');}

		args.LastName = 'Test';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Phone Number is a required field.');}

		args.PhoneNumber = '0123456789';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Email is a required field.');}

		args.Email = 'test@test.com';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Street is a required field.');}

		args.Street = 'market';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Suburb is a required field.');}

		args.Suburb = 'melbourne';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'State is a required field.');}

		args.State = 'vic';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Country is a required field.');}

		args.Country = 'Australia';
		try {QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'PostalCode is a required field.');}

		args.PostalCode = '3000';
		
		boolean bError = false;
		try {res = QCorporateWebsiteWebServices.CampaignRegisterUserRequest(args);}
		catch(Exception e) {bError = true;}
		//System.Assert(!bError);
	}
	
	static testmethod void TopLevel_TestCampaignDonationRequest()
	{
		QCorporateWebsiteWebServices.QCampaignDonationRequest args = new QCorporateWebsiteWebServices.QCampaignDonationRequest();
		QCorporateWebsiteWebServices.QCampaignDonationResults res;
		
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'User Id is a required field.');}
		
		args.UserId = '1234';
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Credit Card Number is a required field.');}

		args.CreditCardNumber = '1234123412341234';
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Card Name is a required field.');}

		args.CardName = 'Card Owner';
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Expiry Year is a required field.');}

		args.ExpiryYear = 2000;
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Expiry Month is a required field.');}
		
		args.ExpiryMonth = 23;
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'CVC is a required field.');}

		args.CVC = '123';

		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Amount is a required field.');}

		args.amount = -10;
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Amount must be a postive number.');}
		
		args.amount = 10;
		args.EndDate = date.today().addDays(-2);
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'End Date must be in the future.');}

		args.EndDate = date.today().addDays(2);
		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.Assert(e.getMessage().startsWith('The Expiry Year specified is invalid:'));}
		
		args.ExpiryYear = system.today().year();
		integer iExpiryMonth = Integer.valueOf(args.ExpiryMonth);

		try {QCorporateWebsiteWebServices.CampaignDonationRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Expiry Month must be in the range 1 to 12.');}

		args.ExpiryMonth = 3;
	}

	public static testmethod void TopLevel_TestEventUpdateRequest()
	{
		QCorporateWebsiteWebServices.QEventUpdateRequest args = new QCorporateWebsiteWebServices.QEventUpdateRequest();
		QCorporateWebsiteWebServices.QEventUpdateResults res;
		boolean bError = false;
		
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'test campaign 5000';
		insert sCampaign;
		
		try {QCorporateWebsiteWebServices.EventUpdateRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Unique Event Code is a required field.');}
	
		args.UniqueEventCode = '1234';
		try {QCorporateWebsiteWebServices.EventUpdateRequest(args);}
		catch(Exception e) {bError = true;}
		System.Assert(bError);
		
		bError = false;
		args.UniqueEventCode = sCampaign.id;
		try {QCorporateWebsiteWebServices.EventUpdateRequest(args);}
		catch(Exception e) {bError = true;}
		System.Assert(!bError);		
	}
	
	public static testmethod void TopLevel_TestShopUpdateProductRequest()
	{
		Product2 sProduct = new Product2();
		sProduct.Name = 'Awesomething 5000';
		sProduct.ProductCode = 'AWTH5000';
		sProduct.Minimum_Shipping_Cost__c = 0.01;
		sProduct.Title_For_Website__c = 'Awesomething 5000';
		insert sProduct;
		
		PricebookEntry sPBEntry = new PricebookEntry();
		sPBEntry.UnitPrice = 100.0;
		sPBEntry.Product2Id = sProduct.Id;
		sPBEntry.Pricebook2Id = QCacheWebsiteMetadata.getShopStandardPricebook();
		insert sPBEntry;
		
		boolean bError = false;

		QCorporateWebsiteWebServices.QShopUpdateProductRequest args = new QCorporateWebsiteWebServices.QShopUpdateProductRequest();
		QCorporateWebsiteWebServices.QShopUpdateProductResults res;

		try {QCorporateWebsiteWebServices.ShopUpdateProductRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Unique Product Code is a required field.');}
		
		args.UniqueProductCode = sProduct.id;
		try {QCorporateWebsiteWebServices.ShopUpdateProductRequest(args);}
		catch(Exception e) {bError = true;}
		System.Assert(!bError);
	}	
	
	public static testmethod void TopLevel_TestShopRegisterUserRequestAndPurchase()
	{
		QCorporateWebsiteWebServices.QShopRegisterUserRequest args = new QCorporateWebsiteWebServices.QShopRegisterUserRequest();
		QCorporateWebsiteWebServices.QShopRegisterUserResults res;
		
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Title is a required field.');}

		args.Title = 'Chief';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'First Name is a required field.');}
		
		args.FirstName = 'Jim';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Last Name is a required field.');}

		args.LastName = 'Bob';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Phone Number is a required field.');}

		args.PhoneNumber = '0202020202';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Email is a required field.');}

		args.Email = 'somewhere@somesite.com';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Street is a required field.');}

		args.Street = 'Some St';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Suburb is a required field.');}

		args.Suburb = 'Someburb';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'State is a required field.');}

		args.State = 'State';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Country is a required field.');}

		args.Country = 'Someland';
		try {QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'PostalCode is a required field.');}

		args.PostalCode = '1000';
		
		boolean bError = false;
		try {res = QCorporateWebsiteWebServices.ShopRegisterUserRequest(args);}
		catch(Exception e) {bError = true;}
		System.Assert(!bError);
		System.Assert(!res.InError);

		/*
			Shop Purchase
		*/
		QCorporateWebsiteWebServices.QShopPurchaseRequest args2 = new QCorporateWebsiteWebServices.QShopPurchaseRequest();
		QCorporateWebsiteWebServices.QShopPurchaseResults res2;
		
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'User Id is a required field.');}

		args2.UserId = '123';
		args2.TotalProductCount = 0;
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Total Product Count must be greater than zero.');}
	
		args2.TotalProductCount = 1;
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Shipping amount is a required value.');}
		
		args2.ShippingAmount = - 10.0;
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Negative shipping amount specified.');}

		args2.ShippingAmount = 10.0;
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Credit Card Number is a required field.');}

		args2.CreditCardNumber = '1234123412341234';
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Card Name is a required field.');}

		args2.CardName = 'Card Owner';
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Expiry Year is a required field.');}

		args2.ExpiryYear = '2000';
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Expiry Month is a required field.');}
		
		args2.ExpiryMonth = '23';
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'CVC is a required field.');}

		args2.CVC = '123';
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'The Expiry Year specified is invalid.');}
		
		args2.ExpiryYear = '' + System.today().year();
		try {QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {System.AssertEquals(e.getMessage(), 'Expiry Month must be in the range 1 to 12.');}
		
		args2.ExpiryMonth = '10';
		
		QCorporateWebsiteWebServices.QProduct p = new QCorporateWebsiteWebServices.QProduct();
		p.UniqueProductCode	= '1234';
		p.Quantity = 1;
		p.UnitPrice	= 100.0;
		p.GSTAmount	= 10.0;
		
		args2.Product = new list<QCorporateWebsiteWebServices.QProduct>();
		args2.Product.add(p);
		
		bError = false;
		try {res2 = QCorporateWebsiteWebServices.ShopPurchaseRequest(args2);}
		catch(Exception e) {bError = true; System.Debug(e);}
		//System.Assert(!bError);
		
		// should have failed due to invalid user id
		//System.Assert(res2.InError);
	}	
	
	public static testmethod void Test_Campaign_Processing()
	{
		// 1 - Create new Account
		Account sAccount		= new Account();
		sAccount.Name 			= 'Test Method Account';
		insert sAccount;

		// 2 - Create new Contacts
		Contact sContact1				= new Contact();
		sContact1.FirstName 			= 'Timmy';
		sContact1.LastName 				= 'TestMethod';
		sContact1.AccountId				= sAccount.Id;
		sContact1.email 				= 'test@quattroinnovation.com';
		insert sContact1;			
		
		
		QCorporateWebsiteWebServices.QUpdateCampaignRequest oArgs = new QCorporateWebsiteWebServices.QUpdateCampaignRequest();
		QCorporateWebsiteWebServices.QUpdateCampaignResults oRes;
		boolean bError = false;
		
		RRule__c sRRule = new RRule__c();
		sRRule.Interval_Length__c = 1;
		sRRule.Interval_Type__c = 'day';
		sRRule.End_Date__c = date.today().addDays(5);
		sRRule.Start_Date__c = date.today();
		sRRule.Recurrance_Count__c = 1;
		sRRule.RRule_Translation__c = 'hello world';
		sRRule.RRule__c = 'hello world';
		insert sRRule;		
		
		Campaign sCampaign = new Campaign();
		sCampaign.name = 'test campaign 4000';
		sCampaign.Is_Visible_On_Website__c = true;
		sCampaign.Title_For_Website__c = 'Mega Campaign 4000';
		sCampaign.Description_For_Website__c = 'Awesome.';
		sCampaign.RRule__c = sRRule.id;
		sCampaign.Event_Location_For_Website__c = 'Here';
		sCampaign.Event_Date__c = date.today().addDays(-30);
		sCampaign.EndDate = date.today().addDays(2);
		sCampaign.Event_Time_For_Website__c = '0800';
		sCampaign.Event_Cost_For_Website__c = '200';
		sCampaign.Year_Long_Event__c = false;
		sCampaign.Allow_Website_Registrations__c = true;
		sCampaign.Registration_Code_For_Website__c = 'helloworld';
		sCampaign.Event_Link__c = 'http://www.google.com.au';
		sCampaign.Event_Link_Description__c = 'google it';
		insert sCampaign;
		
		try
		{
			oRes = QCorporateWebsiteCampaignProcessing.GetCampaignDetails(oArgs);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(bError);
		bError = false;
		
		sCampaign = new Campaign();
		sCampaign.name = 'test campaign 5000';
		sCampaign.Is_Visible_On_Website__c = true;
		sCampaign.Title_For_Website__c = 'Mega Campaign 5000';
		sCampaign.Description_For_Website__c = 'Awesome.';
		sCampaign.RRule__c = sRRule.id;
		sCampaign.Event_Location_For_Website__c = 'Here';
		sCampaign.Event_Date__c = date.today();
		sCampaign.EndDate = date.today().addDays(2);
		sCampaign.Event_Time_For_Website__c = '0900';
		sCampaign.Event_Cost_For_Website__c = '100';
		sCampaign.Year_Long_Event__c = true;
		sCampaign.Allow_Website_Registrations__c = true;
		sCampaign.Registration_Code_For_Website__c = 'hello';
		sCampaign.Event_Link__c = 'http://www.google.com';
		sCampaign.Event_Link_Description__c = 'google it';
		insert sCampaign;
		
		try
		{
			oRes = QCorporateWebsiteCampaignProcessing.GetCampaignDetails(oArgs);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(bError);
		bError = false;
		
		try
		{
			oArgs.UniqueCampaignCode = sCampaign.id;
			oRes = QCorporateWebsiteCampaignProcessing.GetCampaignDetails(oArgs);
		}
		catch(Exception e)
		{
			bError = true;
		}
		System.Assert(!bError);
		
		Attachment sAttachment = new Attachment();
		sAttachment.ParentId = sCampaign.Id;
		sAttachment.Body = Blob.valueOf('Testing');
		sAttachment.Name = 'Test Method Stuff';
		insert sAttachment;
		
		sCampaign.Website_Main_Image_Id__c = sAttachment.Id;
		sCampaign.Website_Sub_Image_1_Id__c = sAttachment.Id;
		sCampaign.Website_Sub_Image_2_Id__c = sAttachment.Id;
		sCampaign.Website_Sub_Image_3_Id__c = sAttachment.Id;
		sCampaign.Website_Sub_Image_4_Id__c = sAttachment.Id;
		sCampaign.Website_Sub_Image_5_Id__c = sAttachment.Id;
		update sCampaign;
		
		bError = false;
		
		try
		{
			oArgs.UniqueCampaignCode = sCampaign.id;
			oRes = QCorporateWebsiteCampaignProcessing.GetCampaignDetails(oArgs);
		}
		catch(Exception e)
		{
			bError = true;
		}
		System.Assert(!bError);

		// register user
		QCorporateWebsiteWebServices.QCampaignRegisterUserRequest oArgs2 = new QCorporateWebsiteWebServices.QCampaignRegisterUserRequest();
		QCorporateWebsiteWebServices.QCampaignRegisterUserResults oRes2;
		bError = false;

		oArgs2.FirstName = 'test';
		oArgs2.LastName = 'man';
		oArgs2.State = 'vic';
		
		oArgs2.Title = 'Dude';
		oArgs2.PhoneNumber = '0202020202';
		oArgs2.PhoneType = 'Mobile';
		oArgs2.OtherPhoneNumber = '0303030303';
		oArgs2.Street = 'Some St';
		oArgs2.Suburb = 'Someburb';
		oArgs2.Country = 'Someland';
		oArgs2.PostalCode = '1000';
		oArgs2.MarketingOptIn = true;
		oArgs2.FurtherInformationOptIn = true;
		oArgs2.Email = 'test@test.com';
	
		try
		{
			oRes2 = QCorporateWebsiteWebServices.CampaignRegisterUserRequest(oArgs2);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(!bError);
		System.Assert(!oRes2.InError);
		
		// fire again
		
		oArgs2.PhoneNumber = '0101010101';
		oArgs2.PhoneType = 'Work';
		oArgs2.Street = 'Other St';
		
		try
		{
			oRes2 = QCorporateWebsiteWebServices.CampaignRegisterUserRequest(oArgs2);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		
		// ProcessDonation Test using invalid then the new user ID
		QCorporateWebsiteWebServices.QCampaignDonationRequest oArgs3 = new QCorporateWebsiteWebServices.QCampaignDonationRequest();
		QCorporateWebsiteWebServices.QCampaignDonationResults oRes3;
		bError = false;
		
		oArgs3.Amount = 1000;
		oArgs3.CampaignId = sCampaign.id;
		oArgs3.CardName = 'Some dude';
		oArgs3.CreditCardNumber = '1234123412341234';
		oArgs3.CVC = '123';
		oArgs3.EndDate = date.today().addYears(1);
		oArgs3.ExpiryMonth = 10;
		oArgs3.ExpiryYear = 2011;
		oArgs3.UserId = '1234';
		
		try
		{
			oRes3 = QCorporateWebsiteCampaignProcessing.ProcessDonation(oArgs3, false, true);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		//System.Assert(!bError);
		//System.Assert(oRes3.InError);
		
		// valid ID
		try
		{
			oArgs3.UserId = oRes2.UserId;
			oRes3 = QCorporateWebsiteCampaignProcessing.ProcessDonation(oArgs3, false, true);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		//System.Assert(!bError);
//		System.Assert(!oRes3.InError);
		
		
		// remove the campaign id and set up an recurring donation using the standard campaign
		oArgs3.CampaignId = null;
		oArgs3.UserId = '1234';
		bError = false;
		
		try
		{
			oRes3 = QCorporateWebsiteCampaignProcessing.ProcessRecurringDonation(oArgs3, true, sContact1);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		//System.Assert(!bError);
//		System.Assert(oRes3.InError);
		
		// valid user but will fail due to a lack of a call out switch
		try
		{
			oArgs3.UserId = oRes2.UserId;
			oRes3 = QCorporateWebsiteCampaignProcessing.ProcessRecurringDonation(oArgs3, true, sContact1);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		//System.Assert(!bError);
//		System.Assert(oRes3.InError);
	}
	
	/******************************************************************************************************************
		Test Methods
	******************************************************************************************************************/
	public static testmethod void Test_Shop_GetProductDetails()
	{
		QCorporateWebsiteWebServices.QShopUpdateProductRequest oArgs = new QCorporateWebsiteWebServices.QShopUpdateProductRequest();
		QCorporateWebsiteWebServices.QShopUpdateProductResults oRes;
		boolean bError = false;
				
		Product2 sProduct = new Product2();
		sProduct.Name = 'Awesomeo 5000';
		sProduct.Is_Visible_On_Website__c = true;
		sProduct.Title_For_Website__c = 'Awesome Product';
		sProduct.Description_For_Website__c = 'This is an awesome product';
		sProduct.Website_Main_Image_Id__c = '123';
		sProduct.GST_Applicable__c = true;
		sProduct.Product_Height__c = 5;
		sProduct.Product_Width__c = 5;
		sProduct.Product_Depth__c = 5;
		sProduct.Product_Weight__c = 5;
		sProduct.Product_Category_For_Website__c = 'Stuff';
		sProduct.Minimum_Shipping_Cost__c = 1;
		sProduct.Use_External_Shipping_Cost__c = false;
		sProduct.Is_Campaign_Ticket__c = false;
		sProduct.Campaign_Id_For_Website__c = null;
		sProduct.ProductCode = 'HELLO';
		insert sProduct;
				
		PricebookEntry sPBEntry = new PricebookEntry();
		sPBEntry.UnitPrice = 100.0;
		sPBEntry.Product2Id = sProduct.Id;
		sPBEntry.Pricebook2Id = QCacheWebsiteMetadata.getShopStandardPricebook();
		insert sPBEntry;
		
		// invalid product
		try
		{
			oRes = QCorporateWebsiteShopProcessing.GetProductDetails(oArgs);
		}
		catch(Exception e)
		{
			bError = true;
		}
		System.Assert(bError);
		
		try
		{
			bError = false;
			oArgs.UniqueProductCode = sProduct.Id;
			oRes = QCorporateWebsiteShopProcessing.GetProductDetails(oArgs);
		}
		Catch(Exception e)
		{
			bError = true;
		}
		System.Assert(!bError);
	}
	
	public static testmethod void Test_Shop_RegisterUserAndPurchase()
	{
		QCorporateWebsiteWebServices.QShopRegisterUserRequest args = new QCorporateWebsiteWebServices.QShopRegisterUserRequest();
		QCorporateWebsiteWebServices.QShopRegisterUserResults res;
		
		args.Title = 'Chief';
		args.FirstName = 'Jim';
		args.LastName = 'Bob';
		args.PhoneNumber = '0202020202';
		args.Email = 'somewhere@somesite.com';
		args.Street = 'Some St';
		args.Suburb = 'Someburb';
		args.State = 'State';
		args.Country = 'Someland';
		args.PostalCode = '1000';
		args.FurtherInformationOptIn = true;
		args.MarketingOptIn = true;
		
		res = QCorporateWebsiteShopProcessing.RegisterUser(args);
		System.Assert(!res.InError);
		
		QCorporateWebsiteWebServices.QShopPurchaseRequest args2 = new QCorporateWebsiteWebServices.QShopPurchaseRequest();
		QCorporateWebsiteWebServices.QShopPurchaseResults res2;
		
		args2.UserId = '123';
		args2.TotalProductCount = 1;	
		args2.CreditCardNumber = '1234123412341234';
		args2.CardName = 'Card Owner';
		args2.ExpiryYear = '' + System.today().year();
		args2.ExpiryMonth = '' + System.today().addMonths(1).month();
		args2.CVC = '123';
		args2.ShippingAmount = 100.0;
		
		QCorporateWebsiteWebServices.QProduct p = new QCorporateWebsiteWebServices.QProduct();
		p.UniqueProductCode	= '1234';
		p.Quantity = 1;
		p.UnitPrice	= 100.0;
		p.GSTAmount	= 10.0;
		
		args2.Product = new list<QCorporateWebsiteWebServices.QProduct>();
		args2.Product.add(p);

		// fail due to invalid user
		res2 = QCorporateWebsiteShopProcessing.ProcessPurchase(args2, true);
		System.Assert(res2.InError);
		
		// callout will fail
		args2.UserId = res.UserId;
		res2 = QCorporateWebsiteShopProcessing.ProcessPurchase(args2, true);
		System.Assert(res2.InError);
		
		// fake callout - returns error
		res2 = QCorporateWebsiteShopProcessing.ProcessPurchase(args2, false);
		System.Assert(res2.InError);
	}
	
	public static testmethod void Test_Event_QCorporateWebsiteEventProcessing()
	{
		RRule__c sRRule = new RRule__c();
		sRRule.Interval_Length__c = 1;
		sRRule.Interval_Type__c = 'day';
		sRRule.End_Date__c = date.today().addDays(5);
		sRRule.Start_Date__c = date.today();
		sRRule.Recurrance_Count__c = 1;
		sRRule.RRule_Translation__c = 'hello world';
		sRRule.RRule__c = 'hello world';
		insert sRRule;		
		
		Campaign sCampaign = new Campaign();
		
		sCampaign.name = 'test campaign 5000';
		sCampaign.Is_Visible_On_Website__c = true;
		sCampaign.Title_For_Website__c = 'Mega Campaign 5000';
		sCampaign.Description_For_Website__c = 'Awesome.';
		sCampaign.Website_Main_Image_Id__c = null;
		sCampaign.RRule__c = sRRule.id;
		sCampaign.Event_Location_For_Website__c = 'Here';
		sCampaign.Event_Date__c = date.today();
		sCampaign.EndDate = date.today().addDays(2);
		sCampaign.Event_Time_For_Website__c = '0900';
		sCampaign.Event_Cost_For_Website__c = '100';
		sCampaign.Year_Long_Event__c = true;
		sCampaign.Allow_Website_Registrations__c = true;
		sCampaign.Registration_Code_For_Website__c = 'hello';
		sCampaign.Event_Link__c = 'http://www.google.com';
		sCampaign.Event_Link_Description__c = 'google it';

		insert sCampaign;
		
		QCorporateWebsiteWebServices.QEventUpdateRequest args = new QCorporateWebsiteWebServices.QEventUpdateRequest();
		args.UniqueEventCode = '12345';
		
		boolean bError = false;
		QCorporateWebsiteWebServices.QEventUpdateResults res;

		// will fail
		try
		{
			res = QCorporateWebsiteEventProcessing.GetEventDetails(args);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(bError);
		bError = false;
		
		// will pass
		try
		{
			args.UniqueEventCode = sCampaign.id;
			res = QCorporateWebsiteEventProcessing.GetEventDetails(args);
		}
		catch(Exception e)
		{
			bError = true;
		}
		
		System.Assert(!bError);
	}
}