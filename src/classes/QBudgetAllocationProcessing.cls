public class QBudgetAllocationProcessing 
{
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static void CheckBudgetAllocationParentIsNull(list<Budget_Allocation__c> liProcessList)
	{
		map<id, integer>	mapCampaign2Index 			= new map<id, integer>();
		map<id, integer>	mapFinAssist2Index 			= new map<id, integer>();
		map<id, integer>	mapOpportunity2Index 		= new map<id, integer>();
	
		map<id, Campaign>		mapCampaign			= new map<id, Campaign>();
		//map<id, Fin_Assist__c>	mapFinAssist		= new map<id, Fin_Assist__c>();
		map<id, Opportunity>	mapOpportunity		= new map<id, Opportunity>();
		
		// lets get everything that were locking into
		for(integer i = 0; i < liProcessList.size(); i++)	
		{
			if(liProcessList[i].Campaign__c != null)
			{
				if(mapCampaign2Index.containsKey(liProcessList[i].Campaign__c))
				{
					liProcessList[i].Campaign__c.addError('Multiple Budget Allocation Objects for Single Campaign Detected');
					liProcessList[mapCampaign2Index.get(liProcessList[i].Campaign__c)].addError('Multiple Budget Allocation Objects for Single Campaign Detected');
				}
				else
				{
					mapCampaign2Index.put(liProcessList[i].Campaign__c, i);
				}
			}

//			if(liProcessList[i].Financial_Assistance__c != null)
//			{
//				if(mapFinAssist2Index.containsKey(liProcessList[i].Financial_Assistance__c))
//				{
//					liProcessList[i].Financial_Assistance__c.addError('Multiple Budget Allocation Objects for Single Financial Assistance Detected');
//					liProcessList[mapFinAssist2Index.get(liProcessList[i].Financial_Assistance__c)].addError('Multiple Budget Allocation Objects for Single Financial Assistance Detected');
//				}
//				else
//				{
//					mapFinAssist2Index.put(liProcessList[i].Financial_Assistance__c, i);
//				}
//			}

			if(liProcessList[i].Opportunity__c != null)
			{
				if(mapOpportunity2Index.containsKey(liProcessList[i].Opportunity__c))
				{
					liProcessList[i].Opportunity__c.addError('Multiple Budget Allocation Objects for Single Opportunity Detected');
					liProcessList[mapOpportunity2Index.get(liProcessList[i].Opportunity__c)].addError('Multiple Budget Allocation Objects for Single Financial Assistance Detected');
				}
				else
				{
					mapOpportunity2Index.put(liProcessList[i].Opportunity__c, i);
				}
			}
		}
		
		// now we need to load the campaigns 
		if(mapCampaign2Index.size() > 0)
		{
			for(Campaign[] arrCampaign : [select id, Budget_Allocation__c from Campaign where id in :mapCampaign2Index.keySet()])
			{
				for(Campaign sCampaign : arrCampaign)
				{
					// Campaign Already has a budget!
					if(sCampaign.Budget_Allocation__c != null)
					{
						liProcessList[mapCampaign2Index.get(sCampaign.Id)].addError('The Campaign already has a Budget Allocation assosiated');
					}
					else
					{
						// nothing to do at the moment!
					}
				}	
			}
		}
//		
//		// and now the fin assst objects
//		if(mapFinAssist2Index.size() > 0)
//		{
//			for(Fin_Assist__c[] arrFinAssist : [select id, Budget_Allocation__c from Fin_Assist__c where id in :mapFinAssist2Index.keySet()])
//			{
//				for(Fin_Assist__c sFinAssist : arrFinAssist)
//				{
//					if(sFinAssist.Budget_Allocation__c != null)
//					{
//						liProcessList[mapFinAssist2Index.get(sFinAssist.Id)].addError('Financial Assistance object already has a Budget Allocation assosiated');
//					}
//					else
//					{
//						// nothing to do at the moment!
//					}
//				}	
//			}
//		}

		
		// and now opportunities
		if(mapOpportunity2Index.size() > 0)
		{
			for(Opportunity[] arrOpportunity : [select id, Budget_Allocation__c from Opportunity where id in :mapOpportunity2Index.keySet()])
			{
				for(Opportunity sOpportunity : arrOpportunity)
				{
					if(sOpportunity.Budget_Allocation__c != null)
					{
						liProcessList[mapOpportunity2Index.get(sOpportunity.Id)].addError('Opportunity object already has a Budget Allocation assosiated');
					}
					else
					{
						// nothing to do at the moment!
					}
				}	
			}
		}
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static void LinkParentToBudgetAllocation(list<Budget_Allocation__c> liProcessList)
	{
		map<id, integer>	mapCampaign2Index 		= new map<id, integer>();
		map<id, integer>	mapFinAssist2Index 		= new map<id, integer>();
		map<id, integer>	mapOpportunity2Index 	= new map<id, integer>();
		
		list<Campaign>		liCampaign				= new list<Campaign>();
		//list<Fin_Assist__c>	liFinAssist				= new list<Fin_Assist__c>();
		list<Opportunity>	liOpportunity			= new list<Opportunity>();
		
		// lets get everything that were locking into
		for(integer i = 0; i < liProcessList.size(); i++)	
		{
			if(liProcessList[i].Campaign__c != null)
			{
				mapCampaign2Index.put(liProcessList[i].Campaign__c, i);
			}

//			if(liProcessList[i].Financial_Assistance__c != null)
//			{
//				mapCampaign2Index.put(liProcessList[i].Financial_Assistance__c, i);
//			}

			if(liProcessList[i].Opportunity__c != null)
			{
				mapOpportunity2Index.put(liProcessList[i].Opportunity__c, i);
			}
		}
		
		// now we need to load the campaigns 
		if(mapCampaign2Index.size() > 0)
		{
			for(Campaign[] arrCampaign : [select id, Budget_Allocation__c from Campaign where id in :mapCampaign2Index.keySet()])
			{
				for(Campaign sCampaign : arrCampaign)
				{
					sCampaign.Budget_Allocation__c = liProcessList[mapCampaign2Index.get(sCampaign.Id)].Id;
					liCampaign.add(sCampaign);
				}	
			}
		}
		
		// and now the opportunity objects
//		if(mapFinAssist2Index.size() > 0)
//		{
//			for(Fin_Assist__c[] arrFinAssist : [select id, Budget_Allocation__c from Fin_Assist__c where id in :mapFinAssist2Index.keySet()])
//			{
//				for(Fin_Assist__c sFinAssist : arrFinAssist)
//				{
//					sFinAssist.Budget_Allocation__c = liProcessList[mapFinAssist2Index.get(sFinAssist.Id)].Id;
//					liFinAssist.add(sFinAssist);
//				}	
//			}
//		}

		
		// and now the fin assst objects
		if(mapOpportunity2Index.size() > 0)
		{
			for(Opportunity[] arrOpportunity : [select id, Budget_Allocation__c from Opportunity where id in :mapOpportunity2Index.keySet()])
			{
				for(Opportunity sOpportunity : arrOpportunity)
				{
					sOpportunity.Budget_Allocation__c = liProcessList[mapOpportunity2Index.get(sOpportunity.Id)].Id;
					liOpportunity.add(sOpportunity);
				}	
			}
		}
		
		if(liCampaign.size() > 0)
			update liCampaign;
			
//		if(liFinAssist.size() > 0)
//			update liFinAssist;
			
		if(liOpportunity.size() > 0)
			update liOpportunity;
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static void SetPrior12MonthsFinancialAssistanceForDiagnosedPerson(id idDiagnosedPerson, DateTime tmEffecDate)
	{
		double dApprovedAmount 	= 0.0;
		double dRejectedAmount 	= 0.0;
		double dOpenAmount 		= 0.0;
		
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribeIncome			= Budget_Allocation__c.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeNameIncome 		= schemaDescribeIncome.getRecordTypeInfosByName();
		
		// get the next aniversary
		while(tmEffecDate < System.Now())
		{
			tmEffecDate = tmEffecDate.addYears(1);
		}
		// now move back one
		tmEffecDate = tmEffecDate.addYears(-1);
		
		// now lets load
		for(Budget_Allocation__c[] arrBudgetAllocation : [
															select 	id,
																	Status__c,
																	Approval_Hidden__c,
																	Approved_Amount__c,
																	Rejected_Amount__c,
																	Budget_Amount__c,
																	Approval_Date_Time__c,
																	CreatedDate
															from	Budget_Allocation__c
															where	Diagnosed_Person__c = :idDiagnosedPerson
															and		RecordTypeId = :mapRecordTypeNameIncome.get('Fin Assist BillsVouchers').RecordTypeId
															AND		(CreatedDate >= :tmEffecDate OR Approval_Date_Time__c >= :tmEffecDate)
														])
		{
			for(Budget_Allocation__c sBudgetAllocation : arrBudgetAllocation)
			{
				// migration
				if(sBudgetAllocation.CreatedDate <= Datetime.newInstance(2009, 2, 9) && sBudgetAllocation.Approval_Date_Time__c != null)
				{
					if(sBudgetAllocation.Approval_Date_Time__c >= tmEffecDate)
					{
						if(sBudgetAllocation.Approved_Amount__c != null)
						{
							dApprovedAmount += sBudgetAllocation.Approved_Amount__c;
						}
						else if(sBudgetAllocation.Budget_Amount__c != null)
						{
							dApprovedAmount += sBudgetAllocation.Budget_Amount__c;
						}
							
						if(sBudgetAllocation.Rejected_Amount__c != null)
						{
							dRejectedAmount += sBudgetAllocation.Rejected_Amount__c;
						}
					}
				}
				else
				{
					if(sBudgetAllocation.Approved_Amount__c != null)
						dApprovedAmount += sBudgetAllocation.Approved_Amount__c;
						
					if(sBudgetAllocation.Rejected_Amount__c != null)
						dRejectedAmount += sBudgetAllocation.Rejected_Amount__c;
					
					if(		sBudgetAllocation.Status__c != 'Closed' 
						&& 	sBudgetAllocation.Status__c != 'Approved'
						&& 	sBudgetAllocation.Status__c != 'Rejected'
						&& 	sBudgetAllocation.Budget_Amount__c != null)
					{
						dOpenAmount += sBudgetAllocation.Budget_Amount__c;
					}
				}
			} 
		}
		
		Contact sDiagnosedPerson = [
										select 	id,
												IsPersonAccount,
												Financial_Assist_12_Mths__c,
												Financial_Assist_12_Mths_Open__c,
												Financial_Assist_12_Mths_Rejected__c 
										from 	Contact 
										where 	Id = :idDiagnosedPerson];
		
		
		if(!sDiagnosedPerson.IsPersonAccount)
		{
			sDiagnosedPerson.Financial_Assist_12_Mths__c 			= dApprovedAmount;
			sDiagnosedPerson.Financial_Assist_12_Mths_Rejected__c 	= dRejectedAmount;
			sDiagnosedPerson.Financial_Assist_12_Mths_Open__c 		= dOpenAmount;
				
			update sDiagnosedPerson;
		}
		else
		{	
			Account sDiagnosedPersonAccount = [
												select 	id,
														Financial_Assist_12_Mths__pc,
														Financial_Assist_12_Mths_Open__pc,
														Financial_Assist_12_Mths_Rejected__pc 
												from 	Account 
												where 	PersonContactId = :idDiagnosedPerson];
			
			sDiagnosedPersonAccount.Financial_Assist_12_Mths__pc 			= dApprovedAmount;
			sDiagnosedPersonAccount.Financial_Assist_12_Mths_Rejected__pc 	= dRejectedAmount;
			sDiagnosedPersonAccount.Financial_Assist_12_Mths_Open__pc 		= dOpenAmount;
				
			update sDiagnosedPersonAccount;
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	public static void PushDetailToParentCampaign(list<Budget_Allocation__c> liProcessList)
	{
		map<Id, Campaign>	mapCampaign = new map<Id, Campaign>();
		
		// extract campaign list
		for(Budget_Allocation__c sBudgetAllocation : liProcessList)
		{
			if(sBudgetAllocation.Campaign__c != null)
			{
				mapCampaign.put(sBudgetAllocation.Campaign__c, null);
			}
		}
		
		// load campaigns
		for(Campaign[] arrCampaign : [
										select 	id,
												Actual_Purchase_Order_Value__c,
												Actual_Invoice_Value__c,
												All_Purchase_Orders__c
										from 	Campaign 
										where 	id in :mapCampaign.keySet()
									])
		{
			for(Campaign sCampaign : arrCampaign)
			{
				mapCampaign.put(sCampaign.Id, sCampaign);
			}
		}
		
		// now lets set the values
		for(Budget_Allocation__c sBudgetAllocation : liProcessList)
		{
			if(sBudgetAllocation.Campaign__c != null)
			{
				Campaign sCampaign = mapCampaign.get(sBudgetAllocation.Campaign__c);
				
				if(sBudgetAllocation.Approved_Purchase_Orders__c != null)
					sCampaign.Actual_Purchase_Order_Value__c 	= sBudgetAllocation.Approved_Purchase_Orders__c;
				
				if(sBudgetAllocation.Actual_Invoices__c != null)	
					sCampaign.Actual_Invoice_Value__c 			= sBudgetAllocation.Actual_Invoices__c;
				
				if(sBudgetAllocation.All_Purchase_Orders__c != null)
					sCampaign.All_Purchase_Orders__c			= sBudgetAllocation.All_Purchase_Orders__c;
				
				mapCampaign.put(sCampaign.Id, sCampaign);
			}
		}
		
		// stick it all back in
		update mapCampaign.values();
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/	
	public static testmethod void testBudgetAllocation121ForCampaign()
	{
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Budget_Allocation__c.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
	
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		insert sCampaign;
		
		// now the budget allocation
		Budget_Allocation__c sBudgetAllocation = new Budget_Allocation__c();
		sBudgetAllocation.Campaign__c 	= sCampaign.Id;
		sBudgetAllocation.RecordTypeId 	= mapRecordTypeName.get('Campaign').getRecordTypeId();
		insert sBudgetAllocation;
		
		// reload campaign
		sCampaign = [select id, Budget_Allocation__c from Campaign where id = :sCampaign.Id];
		
		// make sure the 1 to 1 is there
		System.AssertEquals(sBudgetAllocation.Id, sCampaign.Budget_Allocation__c);
		
	}
	
	public static testmethod void testBudgetAllocation121ForOpportunity()
	{
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Budget_Allocation__c.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
		
		// create our Opportunity
		Opportunity sOpportunity = new Opportunity();
		sOpportunity.Name 		= 'Testing';
		sOpportunity.StageName 	= 'Testing';
		sOpportunity.CloseDate	= System.Today();
		insert sOpportunity;
		
		// now the budget allocation
		Budget_Allocation__c sBudgetAllocation = new Budget_Allocation__c();
		sBudgetAllocation.Opportunity__c 	= sOpportunity.Id;
		sBudgetAllocation.RecordTypeId 		= mapRecordTypeName.get('Opportunity').getRecordTypeId();
		insert sBudgetAllocation;
		
		// reload campaign
		sOpportunity = [select id, Budget_Allocation__c from Opportunity where id = :sOpportunity.Id];
		
		// make sure the 1 to 1 is there
		System.AssertEquals(sBudgetAllocation.Id, sOpportunity.Budget_Allocation__c);
		
	}
	
	public static testmethod void testBudgetAllocationFASummary()
	{
		// lets get the record type we need to use for the Income Records
		Schema.DescribeSObjectResult 		schemaDescribe 		= Budget_Allocation__c.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeName 	= schemaDescribe.getRecordTypeInfosByName();
		
		// create person accoutn
		Account sPersonAccount = new Account();
		
		sPersonAccount.LastName = 'Test Person Account';
		sPersonAccount.FA_Commencement__pc = Date.newInstance(System.Today().Year(), 1, 1);
		
		insert sPersonAccount;
		
		sPersonAccount = [select id, PersonContactId, IsPersonAccount from Account where id = :sPersonAccount.Id];
		
		System.Assert(sPersonAccount.IsPersonAccount);
		System.Assert(sPersonAccount.PersonContactId <> null);
		
		// now the budget allocation
		Budget_Allocation__c sBudgetAllocation = new Budget_Allocation__c();
		sBudgetAllocation.Diagnosed_Person__c	= sPersonAccount.PersonContactId;
		sBudgetAllocation.RecordTypeId 			= mapRecordTypeName.get('Fin Assist BillsVouchers').getRecordTypeId();
		
		sBudgetAllocation.Approval_Hidden__c	= true;
		sBudgetAllocation.Status__c				= 'Approved';
		sBudgetAllocation.Approved_Amount__c	= 1000;
		sBudgetAllocation.Rejected_Amount__c	= 0;
		sBudgetAllocation.Budget_Amount__c		= 1000;
		
		insert sBudgetAllocation;
		
		sPersonAccount = [
							select 	id, 
									PersonContactId,
									Financial_Assist_12_Mths__pc, 
									Financial_Assist_12_Mths_Rejected__pc, 
									Financial_Assist_12_Mths_Open__pc
							from 	Account 
							where 	id = :sPersonAccount.Id
						];
		
		
		// make sure the 1 to 1 is there
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths__pc, 			0);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Rejected__pc, 	0);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Open__pc, 		0);
		
		// now an open one
		sBudgetAllocation = new Budget_Allocation__c();
		sBudgetAllocation.Diagnosed_Person__c	= sPersonAccount.PersonContactId;
		sBudgetAllocation.RecordTypeId 			= mapRecordTypeName.get('Fin Assist BillsVouchers').getRecordTypeId();
		
		sBudgetAllocation.Approval_Hidden__c	= false;
		sBudgetAllocation.Status__c				= 'Open';
		sBudgetAllocation.Approved_Amount__c	= 0;
		sBudgetAllocation.Rejected_Amount__c	= 0;
		sBudgetAllocation.Budget_Amount__c		= 1000;
		
		insert sBudgetAllocation;
		
		sPersonAccount = [
							select 	id, 
									PersonContactId,
									Financial_Assist_12_Mths__pc, 
									Financial_Assist_12_Mths_Rejected__pc, 
									Financial_Assist_12_Mths_Open__pc 
							from 	Account 
							where 	id = :sPersonAccount.Id
						];
		
		
		// make sure the 1 to 1 is there
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths__pc, 			1000);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Rejected__pc, 	0);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Open__pc, 		0);
		
		// now a rejected one
		sBudgetAllocation = new Budget_Allocation__c();
		sBudgetAllocation.Diagnosed_Person__c	= sPersonAccount.PersonContactId;
		sBudgetAllocation.RecordTypeId 			= mapRecordTypeName.get('Fin Assist BillsVouchers').getRecordTypeId();
		
		sBudgetAllocation.Approval_Hidden__c	= false;
		sBudgetAllocation.Status__c				= 'Rejected';
		sBudgetAllocation.Approved_Amount__c	= 0;
		sBudgetAllocation.Rejected_Amount__c	= 1000;
		sBudgetAllocation.Budget_Amount__c		= 1000;
		
		insert sBudgetAllocation;
		
		sPersonAccount = [
							select 	id, 
									Financial_Assist_12_Mths__pc, 
									Financial_Assist_12_Mths_Rejected__pc, 
									Financial_Assist_12_Mths_Open__pc,
									PersonContactId
							from 	Account 
							where 	id = :sPersonAccount.Id
						];
		
		
		// make sure the 1 to 1 is there
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths__pc, 			1000);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Rejected__pc, 	0);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Open__pc, 		1000);
		
		PageReference pageRef = new PageReference('/apex/Recalculate_Budget_Allocation');
		Test.setCurrentPage(pageRef);
		
		ApexPages.currentPage().getParameters().put('Id', 		sPersonAccount.Id);
		ApexPages.currentPage().getParameters().put('Origin', 	'Account');
		
		QVFRecalculateBudgetAllocation objUpdater = new QVFRecalculateBudgetAllocation();
		
		objUpdater.InitRecalculateBudgetAllocation();
		
		sPersonAccount = [
							select 	id, 
									Financial_Assist_12_Mths__pc, 
									Financial_Assist_12_Mths_Rejected__pc, 
									Financial_Assist_12_Mths_Open__pc,
									PersonContactId
							from 	Account 
							where 	id = :sPersonAccount.Id
						];
		
		// make sure the 1 to 1 is there
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths__pc, 			1000);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Rejected__pc, 	1000);
		System.AssertEquals(sPersonAccount.Financial_Assist_12_Mths_Open__pc, 		1000);
		
		update sBudgetAllocation;
	}
	

}