public without sharing class SubscriptionsController
{
	private final		Contact		sContact;
	public				boolean		bAppeals		{get;set;}
	public				boolean		bEvents			{get;set;}
	public				boolean		bGeneralInfo	{get;set;}
	
	public SubscriptionsController()
	{
		system.debug(ApexPages.currentPage().getParameters().get('id'));
		
		sContact = [SELECT Id, FirstName, email, HasOptedOutOfEmail, Email_Communication__c
					FROM Contact 
					WHERE Id = : ApexPages.currentPage().getParameters().get('id')
					AND IsPersonAccount = true
					limit 1];
						
		//Break down EmailCommunication preferences into booleans on load & avoid null reference
		if(sContact.Email_Communication__c != null && sContact.Email_Communication__c != '')
		{
			if(sContact.Email_Communication__c.contains('Appeals'))
			{
				bAppeals = true;
			}
			if(sContact.Email_Communication__c.contains('Events'))
			{
				bEvents = true;
			}
			if(sContact.Email_Communication__c.contains('General Info'))
			{
				bGeneralInfo = true;
			}
		}
	}
	
	public Contact getsContact()
	{
		return sContact;
	}
	
	public PageReference actionSave()
	{
		// Update communication preferences
		String strPreferences = sContact.Email_Communication__c;

		// if communications have never been set, switch from null to empty string to avoid null reference.
		if(strPreferences == null)
		{
			strPreferences = '';
		}
		else
		// Clear out all the current values that we want to set in the landing page, leaving all other manually set values.
		{
			strPreferences = strPreferences.replace('Appeals;', '');
			strPreferences = strPreferences.replace('Appeals', '');
			strPreferences = strPreferences.replace('Events;', '');
			strPreferences = strPreferences.replace('Events', '');
			strPreferences = strPreferences.replace('General Info;', '');
			strPreferences = strPreferences.replace('General Info', '');
		}
		
		if(bAppeals)
		{
			if(strPreferences != '')
			{
				if(strPreferences.substring(0, strPreferences.length() - 1) != ';')
				{
					strPreferences += 'Appeals;';
				}
				else
				{
					strPreferences += ';Appeals;';
				}
			}
			else
			{
				strPreferences = 'Appeals;';
			}
		}
		if(bEvents)
		{
			strPreferences += 'Events;';
		}
		if(bGeneralInfo)
		{
			strPreferences += 'General Info';
		}
		
		// Remove the last ; from the string
		if(strPreferences.endswith(';'))
		{
			strPreferences = strPreferences.substring(0, strPreferences.length() - 1);
		}
		
		// If there are no preferences then communications are opted out
		if(strPreferences.length() < 1)
		{
			sContact.HasOptedOutOfEmail = true;
		}
		else
		{
			sContact.HasOptedOutOfEmail = false;
			sContact.Email_Communication__c = strPreferences;
		}
		
		update sContact;
		
		pagereference pageref = new pagereference('/SubscriptionsThankYou');
		return pageref;
	}
	
	public PageReference actionUnsubscribeAll()
	{
		sContact.HasOptedOutOfEmail = true;
		
		update sContact;
		
		return null;
	}
	
	
}