// needs more comments

public with sharing class QClose_OpportunityCampaign {
	
	public	list<SelectOption>				mp_liNewCampaign 			{get; set;}
	public	list<SelectOption>				mp_liOldCampaign 			{get; set;}
	
	public	string							mp_strNewCampaign			{get; set;}
	public	string							mp_strOldCampaign			{get; set;}
	
	private  integer 						m_OPPORTUNITY_NUMBER;
	private  map<id, Campaign> 				m_mapCampaign;
	private	 string							m_oldCampaignIdFromParam;
	
	public QClose_OpportunityCampaign()
	{
		m_OPPORTUNITY_NUMBER = 0;
		m_oldCampaignIdFromParam = Apexpages.currentPage().getParameters().get('oldCampaignId');
	}
	
	public pageReference Init()
	{	
		m_mapCampaign 	 = new map<id, Campaign>();
		//init both selectlists	of the old and new Campaigns
		mp_liOldCampaign = new list<SelectOption>();
		mp_liNewCampaign = new list<SelectOption>();
		
		mp_liOldCampaign.add( new SelectOption('--None--','--None--'));
		mp_liNewCampaign.add( new SelectOption('--None--','--None--'));
		
		integer thisYear =  Math.mod(Date.today().year(), 2000);
		for(Campaign sCampaign : [ select Id, Name, Job_Code_Year__c from Campaign 
									where isActive = true and RecordType.Name = 'Club Red CRD'   //only for ClubRed
									order by Job_Code_Year__c, Name ])
		{
			m_mapCampaign.put(sCampaign.Id,sCampaign);
			System.debug('ID CAMPAIGN:'+sCampaign.Id);
			if(Integer.valueOf(sCampaign.Job_Code_Year__c) <= thisYear) //if the year of the Campaign is <= Today.year, it s an old Campaign
			{
				mp_liOldCampaign.add(new Selectoption(sCampaign.Id,sCampaign.Name));	
			}
			else
			{
				mp_liNewCampaign.add(new Selectoption(sCampaign.Id,sCampaign.Name));
			}
		}
		System.debug('OldCampaigns Map :'+m_mapCampaign.keySet());
		if(m_oldCampaignIdFromParam != null && m_oldCampaignIdFromParam != '')
		{
			for(string strId : m_mapCampaign.keySet())
			{
				if(strId.contains(m_oldCampaignIdFromParam))
				{
					mp_strOldCampaign = strId;
					break;
				}
			}
			System.debug('OldCampaign ID :'+m_oldCampaignIdFromParam);
			
		}
		else
		{
			mp_strOldCampaign = '--None--';
		}
		mp_strNewCampaign = '--None--';
		
		return null;
	}
	/** Count the number of opportunities that will be changed
	*/
	public void CountOpportunity()
	{
		if(mp_strOldCampaign <> '--None--' && mp_strNewCampaign <> '--None--')
		{
			try
			{
				AggregateResult aggr = [select count(Id) from Opportunity where StageName = 'Club Red Ongoing' and CampaignId = :mp_strOldCampaign];
				m_OPPORTUNITY_NUMBER = (integer) aggr.get('expr0');
				string mp_strOppNumber = ''+m_OPPORTUNITY_NUMBER+' Opportunities will be closed for '+m_mapCampaign.get(mp_strOldCampaign).Name+' and added to '+m_mapCampaign.get(mp_strNewCampaign).Name ;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, mp_strOppNumber));		
			}catch(Exception e){}
		}	
	}
	//close the opportunities of a old campaign and link them to a new campaign
	public Pagereference DoAction()
	{
		if( mp_strOldCampaign == '--None--' ) //need to choose a old campaign
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Choose an old campaign'));
			return null;
		}
		if( mp_strNewCampaign == '--None--' ) //need to choose a new campaign
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Choose a new campaign'));
			return null;
		}
		try
		{				//run batch
			QClubRedOpportunityEndOfYearBatch qrop = new QClubRedOpportunityEndOfYearBatch(mp_strOldCampaign,mp_strNewCampaign);
	
			Database.executeBatch(qrop); 
			
			string mp_strOppNumber = ''+m_OPPORTUNITY_NUMBER+' Opportunities have been closed for '+m_mapCampaign.get(mp_strOldCampaign).Name+' and added to '+m_mapCampaign.get(mp_strNewCampaign).Name ;
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, mp_strOppNumber));
			
		}catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
		}	
		
		return null;
	}
}