public with sharing class QVFMelonMailUpdateManagerController
{
	public integer	m_iNumToAddToMarketingDB		{get; set;}
	public integer	m_iNumToAddToFurtherInfoDB		{get; set;}
	public integer	m_iNumToUnsubFromMarketingDB	{get; set;}
	public integer	m_iNumToUnsubFromFurtherInfoDB	{get; set;}
	
	public boolean	m_bAddButtonDisabled			{get; set;}
	public boolean	m_bUnsubButtonDisabled			{get; set;}
	
	private	boolean	m_bEnableCallouts				= true;

	private set<id>	m_setMarketingContacts			= new set<id>();
	private set<id>	m_setFurtherInfoContacts		= new set<id>();
	private set<id>	m_setUnsubMarketingContacts		= new set<id>();
	private set<id>	m_setUnsubFurtherInfoContacts	= new set<id>();

	private map<id, Contact> m_mapIdToContact		= new map<id, Contact>();
	private map<id, Contact> m_mapIdToContactUpd	= new map<id, Contact>();

	private QMelonMailProcessing							m_Processor = null;
	private QMelonMailProcessing.QMelonMailProcessingArgs	m_Args		= new QMelonMailProcessing.QMelonMailProcessingArgs();
	private QMelonMailProcessing.QMelonMailProcessingRet	m_Ret		= null;

	/**************************************************************************
		Constructor
	**************************************************************************/	
	public QVFMelonMailUpdateManagerController()
	{
		m_iNumToAddToMarketingDB		= 0;
		m_iNumToAddToFurtherInfoDB		= 0;
		m_iNumToUnsubFromMarketingDB	= 0;
		m_iNumToUnsubFromFurtherInfoDB	= 0;
	}
	
	/**************************************************************************
		Page Actions
	**************************************************************************/
	public void Init()
	{
		UpdateStats();
	}

	public apexPages.PageReference DoAdditions()
	{
		integer iCounter = 0;
		integer iFlags = 0;

		// clear the update map
		m_mapIdToContactUpd.clear();
		
		m_Args.m_eBatchParam	= QMelonMailProcessing.QMelonMailBatchParam.MM_ADD_CONTACTS;
		m_Args.m_eDatabase		= QMelonMailProcessing.QMelonMailDB.DB_MARKETING;
		m_Args.m_setContactIds	= m_setMarketingContacts;
		
		// these shouldn't matter here
		m_Args.m_iMaxThreads	= 10;
		m_Args.m_iMaxCallouts	= 10;
		
		if(m_setMarketingContacts.size() > 0)
		{
			m_Processor = new QMelonMailProcessing(m_Args);
			
			if(!m_bEnableCallouts)
			{
				m_Processor.TESTONLYDisableCallouts();
			}
			
			m_Ret = m_Processor.ProcMain();
			
			System.Debug('**************************************\n\nm_Ret = ' + m_Ret);
	
			// don't stop processing on an error - m_iNumRecordsProcessed will let us deal with what was done		
			if(m_Ret.m_bError)
			{
				for(string strError : m_Ret.m_liErrorMessages)
				{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strError));
				}
			}
	
			// the counter allows us to omit rows which weren't processed
			// however, they should all get processed when doing additions
			for(id sId : m_setMarketingContacts)
			{
				if(iCounter < m_Ret.m_iNumRecordsProcessed)
				{
					// remove the marketing opt-in flag
					m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c = m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c.intValue() 
																			& ~ QMelonMailProcessing.m_iMarketingOptIn;
																			
					m_mapIdToContactUpd.put(sId, m_mapIdToContact.get(sId));
				}				
				iCounter ++;			
			}
			
			iCounter = 0;
			m_Args.m_iMaxCallouts -= m_Ret.m_iNumCallouts;
		}
				
		// again should always be true for sanity's sake we check
		if(m_Args.m_iMaxCallouts >= 2 && m_setFurtherInfoContacts.size() > 0)
		{
			m_Args.m_eDatabase		= QMelonMailProcessing.QMelonMailDB.DB_FURTHER_INFO;
			m_Args.m_setContactIds	= m_setFurtherInfoContacts;
			
			m_Processor = new QMelonMailProcessing(m_Args);
			
			if(!m_bEnableCallouts)
			{
				m_Processor.TESTONLYDisableCallouts();
			}
			
			m_Ret = m_Processor.ProcMain();
			
			if(m_Ret.m_bError)
			{
				for(string strError : m_Ret.m_liErrorMessages)
				{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strError));
				}
			}
			
			for(id sId : m_setFurtherInfoContacts)
			{
				if(iCounter < m_Ret.m_iNumRecordsProcessed)
				{
					// remove the further info opt-in flag
					m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c = m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c.intValue()
																		& ~ QMelonMailProcessing.m_iFurtherInfoOptIn;
					
					// doesn't matter that it's overwriting as the source object in m_mapIdToContact was updated each time
					m_mapIdToContactUpd.put(sId, m_mapIdToContact.get(sId));
				}
				iCounter ++;
			}
		}
		
		if(m_mapIdToContactUpd.size() > 0)
		{
			// turn off the update trigger even though we're not changing the communications field as it'll save resources
			QMelonMailProcessing.m_bDisableUpdateTrigger = true;
			
			try
			{
				update m_mapIdToContactUpd.values();
			}
			catch(QueryException qe)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Query Exception thrown while updating contacts: ' + qe));
			}
			catch(Exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception thrown while updating contacts: ' + e));
			}
			
			QMelonMailProcessing.m_bDisableUpdateTrigger = false;
		}
		
		UpdateStats();
		return null;	
	}
	
	public apexPages.PageReference DoUnsubscribes()
	{
		integer iCounter = 0;
		integer iFlags = 0;

		// clear the update map
		m_mapIdToContactUpd.clear();
		
		m_Args.m_eBatchParam	= QMelonMailProcessing.QMelonMailBatchParam.MM_UNSUBSCRIBE;
		m_Args.m_eDatabase		= QMelonMailProcessing.QMelonMailDB.DB_MARKETING;
		m_Args.m_setContactIds	= m_setUnsubMarketingContacts;
		
		// max threads doesn't matter here
		m_Args.m_iMaxThreads	= 10;
	
		// max callouts is important as if we've got more than 9 contacts to unsub we'll smack the governor limit
		m_Args.m_iMaxCallouts	= 10;
		
		if(m_setUnsubMarketingContacts.size() > 0)
		{
			m_Processor = new QMelonMailProcessing(m_Args);
			
			if(!m_bEnableCallouts)
			{
				m_Processor.TESTONLYDisableCallouts();
			}
			
			m_Ret = m_Processor.ProcMain();
			
			if(m_Ret.m_bError)
			{
				for(string strError : m_Ret.m_liErrorMessages)
				{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strError));
				}
			}
	
			// the counter allows us to omit rows which weren't processed
			// however, they should all get processed when doing additions
			for(id sId : m_setUnsubMarketingContacts)
			{
				if(iCounter < m_Ret.m_iNumRecordsProcessed)
				{
					// remove the marketing opt-in flag
					m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c = m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c.intValue() 
																			& ~ QMelonMailProcessing.m_iMarketingOptOut;
																			
					m_mapIdToContactUpd.put(sId, m_mapIdToContact.get(sId));
				}				
				iCounter ++;			
			}
			
			iCounter = 0;
			m_Args.m_iMaxCallouts -= m_Ret.m_iNumCallouts;
		}
		
		// again should always be true for sanity's sake we check
		if(m_Args.m_iMaxCallouts >= 2 && m_setUnsubFurtherInfoContacts.size() > 0)
		{
			m_Args.m_eDatabase		= QMelonMailProcessing.QMelonMailDB.DB_FURTHER_INFO;
			m_Args.m_setContactIds	= m_setUnsubFurtherInfoContacts;
			
			m_Processor = new QMelonMailProcessing(m_Args);
			
			if(!m_bEnableCallouts)
			{
				m_Processor.TESTONLYDisableCallouts();
			}
			
			m_Ret = m_Processor.ProcMain();
			
			if(m_Ret.m_bError)
			{
				for(string strError : m_Ret.m_liErrorMessages)
				{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strError));
				}
			}
			
			for(id sId : m_setUnsubFurtherInfoContacts)
			{
				if(iCounter < m_Ret.m_iNumRecordsProcessed)
				{
					// remove the further info opt-in flag
					m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c = m_mapIdToContact.get(sId).Melon_Mail_Sync_Flags__c.intValue()
																		& ~ QMelonMailProcessing.m_iFurtherInfoOptOut;
					
					// doesn't matter that it's overwriting as the source object in m_mapIdToContact was updated each time
					m_mapIdToContactUpd.put(sId, m_mapIdToContact.get(sId));
				}
				iCounter ++;
			}
		}
		
		if(m_mapIdToContactUpd.size() > 0)
		{
			// turn off the update trigger even though we're not changing the communications field as it'll save resources
			QMelonMailProcessing.m_bDisableUpdateTrigger = true;
			
			try
			{
				update m_mapIdToContactUpd.values();
			}
			catch(QueryException qe)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Query Exception thrown while updating contacts: ' + qe));
			}
			catch(Exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception thrown while updating contacts: ' + e));
			}
			
			QMelonMailProcessing.m_bDisableUpdateTrigger = false;
		}
		
		UpdateStats();
		return null;
	}


	/**************************************************************************
		Workers
	**************************************************************************/	
	private void UpdateStats()
	{
		integer iFlags = 0;

		// clear everything first		
		m_mapIdToContact.clear();
		m_setMarketingContacts.clear();
		m_setFurtherInfoContacts.clear();
		m_setUnsubMarketingContacts.clear();
		m_setUnsubFurtherInfoContacts.clear();
		
		try
		{
			for(Contact sContact : [select	id, FirstName, LastName, MobilePhone, Email, Melon_Mail_Sync_Flags__c
									from	Contact
									where	Melon_Mail_Sync_Flags__c != 0
									and		Melon_Mail_Sync_Flags__c != null
									limit	1000])
			{
				m_mapIdToContact.put(sContact.Id, sContact);
				
				iFlags = sContact.Melon_Mail_Sync_Flags__c.intvalue();
				
				// add ids to the appropriate sets
				if((iFlags & QMelonMailProcessing.m_iMarketingOptIn) != 0)
				{
					m_setMarketingContacts.add(sContact.Id);
				}
				
				if((iFlags & QMelonMailProcessing.m_iFurtherInfoOptIn) != 0)
				{
					m_setFurtherInfoContacts.add(sContact.Id);
				}
				
				if((iFlags & QMelonMailProcessing.m_iMarketingOptOut) != 0)
				{
					m_setUnsubMarketingContacts.add(sContact.Id);
				}
				
				if((iFlags & QMelonMailProcessing.m_iFurtherInfoOptOut) != 0)
				{
					m_setUnsubFurtherInfoCOntacts.add(sContact.Id);
				}
			}
		
			m_iNumToAddToMarketingDB = m_setMarketingContacts.size();
			m_iNumToAddToFurtherInfoDB = m_setFurtherInfoContacts.size();
			m_iNumToUnsubFromMarketingDB = m_setUnsubMarketingContacts.size();
			m_iNumToUnsubFromFurtherInfoDB = m_setUnsubFurtherInfoContacts.size();
			
			m_bAddButtonDisabled = ((m_setMarketingContacts.size() + m_setFurtherInfoContacts.size()) == 0);
			m_bUnsubButtonDisabled = ((m_setUnsubMarketingContacts.size() + m_setUnsubFurtherInfoContacts.size()) == 0);
		}
		catch(QueryException qe)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Query Exception thrown finding contacts: ' + qe));
		}
		catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception thrown finding contacts: ' + e));
		}
	}
	
	/**************************************************************************
		Test Methods
	**************************************************************************/
	public static testmethod void TestQVFMelonMailUpdateManagerController()
	{
		QMelonMailProcessing.m_bDisableUpdateTrigger = true;
		/*
		string rctId = [select id from RecordType where DeveloperName = 'Organisation_Account'].Id;
		
		Account sAccount = new Account();
		sAccount.Name = 'TestAcct';
		sAccount.RecordTypeId = rctId;
		insert sAccount;
		
		Account sAccount2 = new Account();
		sAccount2.Name = 'TestAcct2';
		sAccount2.RecordTypeId = rctId;
		insert sAccount2;
		
		Contact sContact = [select	id, firstname, lastname, mobilephone, email, melon_mail_sync_flags__c, accountid
							from	contact
							where	email != null and firstname != null and lastname != null
							limit 1];

		sContact.FirstName = 'Test';
		sContact.LastName = 'Man';
		sContact.MobilePhone = '040404040404';
		sContact.Email = 'surethisaddress@doesnotexist.com';
		sContact.Melon_Mail_Sync_Flags__c = 5;
		sContact.AccountId = sAccount.Id;
		update sContact;
		
		Contact sContact = new Contact();
		sContact.FirstName = 'Test';
		sContact.LastName = 'Man';
		sContact.MobilePhone = '040404040404';
		sContact.Email = 'surethisaddress@doesnotexist.com';
		sContact.Melon_Mail_Sync_Flags__c = 5;
		sContact.AccountId = sAccount.Id;
		insert sContact;*/
		
		Account personAccount = new Account();
		personAccount.FirstName = 'Test';
		personAccount.LastName = 'Man';
		personAccount.RecordTypeId = [select id from RecordType where DeveloperName = 'Individual'].Id;
		personAccount.PersonMobilePhone = '040404040404';
		personAccount.PersonEmail = 'surethisaddress2@doesnotexist.com';
		personAccount.Melon_Mail_Sync_Flags__pc = 4;
		insert personAccount;
		
		QMelonMailProcessing.m_bDisableUpdateTrigger = false;

		// run it		
		QVFMelonMailUpdateManagerController mmc = new QVFMelonMailUpdateManagerController();
		mmc.Init();
		mmc.m_bEnableCallouts = false;
		mmc.DoAdditions();
		
		Contact sContact = [select id, Melon_Mail_Sync_Flags__c from Contact where AccountId = :personAccount.id];
		System.Debug(sContact);
		//System.Assert(sContact.Melon_Mail_Sync_Flags__c == 0);
		
		// now do some removals
		QMelonMailProcessing.m_bDisableUpdateTrigger = true;
		personAccount.Melon_Mail_Sync_Flags__pc = 10;
		update personAccount;
		QMelonMailProcessing.m_bDisableUpdateTrigger = false;

		// run the removals
		mmc = new QVFMelonMailUpdateManagerController();
		mmc.Init();
		mmc.m_bEnableCallouts = false;
		mmc.DoUnsubscribes();
		
		sContact = [select id, Melon_Mail_Sync_Flags__c from Contact where AccountId = :personAccount.id];
		System.Debug(sContact);
		//System.Assert(sContact.Melon_Mail_Sync_Flags__c == 0);		
	}
}