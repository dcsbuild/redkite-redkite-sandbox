public without sharing class QCorporateWebsiteCampaignProcessing 
{
	public class QCorporateWebsiteWebServicesParameterException extends Exception{}
	public class QCorporateWebsiteWebServicesException 			extends Exception{}
	
	private static Account m_Account;
	private static Campaign m_Campaign;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public static QCorporateWebsiteWebServices.QUpdateCampaignResults GetCampaignDetails(QCorporateWebsiteWebServices.QUpdateCampaignRequest oArgs)
	{
		// return object
		QCorporateWebsiteWebServices.QUpdateCampaignResults oRet = new QCorporateWebsiteWebServices.QUpdateCampaignResults();
		Attachment  sWebsiteSubImage;
		
		try
		{
			m_Campaign = [
							select 	id, Is_Visible_On_Website__c, Title_For_Website__c, Description_For_Website__c,
									Enquiries_For_Website__c, Microsite_Location__c, Is_Recurring_Donation_For_Website__c,
									
									Website_Main_Image_Id__c, Website_Sub_Image_1_Id__c,  Website_Sub_Image_2_Id__c,
									Website_Sub_Image_3_Id__c, Website_Sub_Image_4_Id__c, Website_Sub_Image_5_Id__c,
									
									Website_Sub_Image_1_Amount__c, Website_Sub_Image_2_Amount__c, Website_Sub_Image_3_Amount__c,
									Website_Sub_Image_4_Amount__c, Website_Sub_Image_5_Amount__c
									
							from 	Campaign 
							where 	id = :oArgs.UniqueCampaignCode 
						];

			oRet.UniqueCampaignCode		= m_Campaign.id;
			oRet.IsActive				= m_Campaign.Is_Visible_On_Website__c;
			oRet.Name					= m_Campaign.Title_For_Website__c;
			oRet.Description			= m_Campaign.Description_For_Website__c;
			oRet.Enquiries				= m_Campaign.Enquiries_For_Website__c;
			oRet.MicrositeLocation		= m_Campaign.Microsite_Location__c;
			oRet.IsRecurringDonation	= m_Campaign.Is_Recurring_Donation_For_Website__c;
		}
		catch(Exception e)
		{
			System.debug('Exception - Trying to Load Campaign Details - ' + e);
			throw new QCorporateWebsiteWebServicesException('Error Loading - ' + oArgs.UniqueCampaignCode);
		}

		if(m_Campaign.Website_Main_Image_Id__c != null)
		{
			try
			{
				oRet.MainImage = [select Body from Attachment where id = :m_Campaign.Website_Main_Image_Id__c].Body;
			}
			catch(Exception e)
			{
			}
		}

		if(m_Campaign.Website_Sub_Image_1_Id__c != null)
		{
			try
			{
				sWebsiteSubImage 		= [select Body from Attachment where id = :m_Campaign.Website_Sub_Image_1_Id__c];
				oRet.SubImage1 			= sWebsiteSubImage.Body;
				oRet.SubImageAmount1 	= m_Campaign.Website_Sub_Image_1_Amount__c == null ? 0 : m_Campaign.Website_Sub_Image_1_Amount__c.intValue();
			}
			catch(Exception e)
			{
			}
		}

		if(m_Campaign.Website_Sub_Image_2_Id__c != null)
		{
			try
			{
				sWebsiteSubImage 		= [select Body from Attachment where id = :m_Campaign.Website_Sub_Image_2_Id__c];
				oRet.SubImage2			= sWebsiteSubImage.Body;
				oRet.SubImageAmount2 	= m_Campaign.Website_Sub_Image_2_Amount__c == null ? 0 : m_Campaign.Website_Sub_Image_2_Amount__c.intValue();
			}
			catch(Exception e)
			{
			}
		}

		if(m_Campaign.Website_Sub_Image_3_Id__c != null)
		{
			try
			{
				sWebsiteSubImage 		= [select Body from Attachment where id = :m_Campaign.Website_Sub_Image_3_Id__c];
				oRet.SubImage3 			= sWebsiteSubImage.Body;
				oRet.SubImageAmount3 	= m_Campaign.Website_Sub_Image_3_Amount__c == null ? 0 : m_Campaign.Website_Sub_Image_3_Amount__c.intValue();
			}
			catch(Exception e)
			{
			}
		}

		if(m_Campaign.Website_Sub_Image_4_Id__c != null)
		{
			try
			{
				sWebsiteSubImage 		= [select Body from Attachment where id = :m_Campaign.Website_Sub_Image_4_Id__c];
				oRet.SubImage4 			= sWebsiteSubImage.Body;
				oRet.SubImageAmount4 	= m_Campaign.Website_Sub_Image_4_Amount__c == null ? 0 : m_Campaign.Website_Sub_Image_4_Amount__c.intValue();
			}
			catch(Exception e)
			{
			}
		}

		if(m_Campaign.Website_Sub_Image_5_Id__c != null)
		{
			try
			{
				sWebsiteSubImage 		= [select Body from Attachment where id = :m_Campaign.Website_Sub_Image_5_Id__c];
				oRet.SubImage5 			= sWebsiteSubImage.Body;
				oRet.SubImageAmount5 	= m_Campaign.Website_Sub_Image_5_Amount__c == null ? 0 : m_Campaign.Website_Sub_Image_5_Amount__c.intValue();
			}
			catch(Exception e)
			{
			}
		}
		
		System.Debug(oRet);

		return oRet;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public static QCorporateWebsiteWebServices.QCampaignRegisterUserResults RegisterUser(QCorporateWebsiteWebServices.QCampaignRegisterUserRequest oArgs)
	{
		return RegisterUserOriginal(oArgs, true);
	} 
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public static QCorporateWebsiteWebServices.QCampaignRegisterUserResults RegisterOrganisaiton(QCorporateWebsiteWebServices.QCampaignRegisterUserRequest oArgs)
	{
		return RegisterUserOriginal(oArgs, false);
	} 
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private static QCorporateWebsiteWebServices.QCampaignRegisterUserResults RegisterUserOriginal(QCorporateWebsiteWebServices.QCampaignRegisterUserRequest oArgs, boolean usePersonAccount)
	{
		System.Debug(oArgs);
		
		// return object
		QCorporateWebsiteWebServices.QCampaignRegisterUserResults oRet = new QCorporateWebsiteWebServices.QCampaignRegisterUserResults();

		oRet.InError = false;
		oRet.Error = 'Success';
		
		/*********************************************
			SEARCH FOR THE CLIENT
		*********************************************/
		QClientSearch 					oClientSearcherStage;
		QClientSearch.QClientSearchArgs oClientSearchArgsStage = new QClientSearch.QClientSearchArgs();
		QClientSearch.QClientSearchRet 	oClientSearchRetStage;
				
		if(usePersonAccount)
		{
		    oClientSearchArgsStage.DedupeMethod = QClientSearch.DedupeMethod.CORPORATE;
		    oClientSearchArgsStage.CreateAction	= QClientSearch.CreateAction.PERSON_ACCOUNT;
			oClientSearchArgsStage.FirstName	= oArgs.FirstName;
			oClientSearchArgsStage.LastName		= oArgs.LastName;
		}
		else
		{
			oClientSearchArgsStage.DedupeMethod 	= QClientSearch.DedupeMethod.CORPORATE;
			oClientSearchArgsStage.CreateAction		= QClientSearch.CreateAction.ACCOUNT;
			oClientSearchArgsStage.CorporateName 	= oArgs.FirstName;
			oClientSearchArgsStage.Website			= oArgs.Website;
		}
		
		oClientSearchArgsStage.CreateTaskForPossibleDupe	= true;
		oClientSearchArgsStage.Email						= oArgs.Email;
		oClientSearchArgsStage.State						= oArgs.State;
		
		oClientSearchArgsStage.Title						= oArgs.Title;
		oClientSearchArgsStage.PhoneNumber					= oArgs.PhoneNumber;
		oClientSearchArgsStage.OtherPhoneNumber				= oArgs.OtherPhoneNumber;
		oClientSearchArgsStage.Street						= oArgs.Street;
		oClientSearchArgsStage.Suburb						= oArgs.Suburb;
		oClientSearchArgsStage.Country						= oArgs.Country;	
		oClientSearchArgsStage.PostalCode					= oArgs.PostalCode;
		oClientSearchArgsStage.MarketingOptIn				= oArgs.MarketingOptIn;
		oClientSearchArgsStage.FurtherInformationOptIn		= oArgs.FurtherInformationOptIn;

		oClientSearchArgsStage.DOB							= oArgs.DOB;
		oClientSearchArgsStage.PhoneType					= oArgs.PhoneType;
		oClientSearchArgsStage.DonationSource				= oArgs.DonationSource;

		// now lets create our search object
		oClientSearcherStage = new QClientSearch(oClientSearchArgsStage);
		oClientSearchRetStage = oClientSearcherStage.ProcMain();	// SEARCH!
		
		// if either the Account or Contact Id didnt come back we had a massive problem!!!
		if(oClientSearchRetStage.AccountId == null || oClientSearchRetStage.ContactId == null)
		{
			QUtils.CreateErrorEmail('Corporate WebService Issue - Client Search', 'Trying to Search Details - ' + oClientSearchArgsStage);
			System.debug('Corporate WebService Issue - Client Search - Trying to Search Details - ' + oClientSearchArgsStage);
			
			oRet.InError 	= true;
    		oRet.Error		= 'Error - Trying to Register User';
		}
		else
		{
			oRet.UserId = oClientSearchRetStage.AccountId;
		}
		
		// whatever happened we now store the submitted address in the temporary fields on contact
		try
		{
			if(usePersonAccount)
			{
				Contact sContact = [select	id, IsPersonAccount, OwnerId, Birthdate, Phone, MobilePhone, HomePhone, OtherPhone,
											MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
											Email_Communication__c
									from	Contact
									where	id = : oClientSearchRetStage.ContactId];
				system.debug('ContactLoad:::' + sContact);
				system.debug('oArgs:::' + oArgs);
				
				// Email_Communication__c and Mail_Communication__c begin as null so set to blank if null to avoid null reference error
				if(sContact.Email_Communication__c == null)
				{
					sContact.Email_Communication__c = '';
				}
				
				if(sContact.Birthdate == null)
				{
					sContact.Birthdate = oArgs.DOB;
				}
					
				if(oArgs.PhoneType == null || oArgs.PhoneType == '')
				{
					// old way
					sContact.Phone = oArgs.PhoneNumber;
				}
				
				if(oArgs.PhoneType == 'Home')
				{
					sContact.HomePhone = oArgs.PhoneNumber;
				}
				
				if(oArgs.PhoneType == 'Mobile')
				{
					sContact.MobilePhone = oArgs.PhoneNumber;
				}

				if(oArgs.PhoneType == 'Work')
				{
					sContact.Phone = oArgs.PhoneNumber;
				}
					
				if(oArgs.FurtherInformationOptIn && oArgs.MarketingOptIn)
				{
					// Create a string of preferences to add
					string strPreferences = '';
					
					// Check if the last character is a ; or not
					if(sContact.Email_Communication__c != '' && sContact.Email_Communication__c.substring(0, sContact.Email_Communication__c.length() - 1) != ';')
					{
						strPreferences += ';';
					}
					
					// Check if we need to add anything and build string
					if(sContact.Email_Communication__c.contains('Chairman\'s Appeal') == false)
					{
						strPreferences += 'Chairman\'s Appeal;';
					}
					if(sContact.Email_Communication__c.contains('Christmas Appeal') == false)
					{
						strPreferences += 'Christmas Appeal;';
					}
					if(sContact.Email_Communication__c.contains('Event Material') == false)
					{
						strPreferences += 'Event Material;';
					}
					if(sContact.Email_Communication__c.contains('Newsletter') == false)
					{
						strPreferences += 'Newsletter;';
					}
					
					// add our preferences
					sContact.Email_Communication__c = sContact.Email_Communication__c + strPreferences;
					sContact.HasOptedOutOfEmail = false;
					
				}
				else if(oArgs.FurtherInformationOptIn)
				{
					// Create a string of preferences to add
					string strPreferences = '';
					
					// Check if the last character is a ; or not
					if(sContact.Email_Communication__c != '' && sContact.Email_Communication__c.substring(0, sContact.Email_Communication__c.length() - 1) != ';')
					{
						strPreferences += ';';
					}
					
					if(sContact.Email_Communication__c.contains('Newsletter'))
					{
						strPreferences += 'Newsletter;';
					}
					
					// add our preferences
					sContact.Email_Communication__c = sContact.Email_Communication__c + strPreferences;
					sContact.HasOptedOutOfEmail = false;
					
				}
				else if(oArgs.MarketingOptIn)
				{
					// Create a string of preferences to add
					string strPreferences = '';
					
					// Check if the last character is a ; or not
					if(sContact.Email_Communication__c != '' && sContact.Email_Communication__c.substring(0, sContact.Email_Communication__c.length() - 1) != ';')
					{
						strPreferences += ';';
					}
					
					// Check if we need to add anything and build string
					if(sContact.Email_Communication__c.contains('Chairman\'s Appeal') == false)
					{
						strPreferences += 'Chairman\'s Appeal;';
					}
					if(sContact.Email_Communication__c.contains('Christmas Appeal') == false)
					{
						strPreferences += 'Christmas Appeal;';
					}
					if(sContact.Email_Communication__c.contains('Event Material') == false)
					{
						strPreferences += 'Event Material;';
					}
					if(sContact.Email_Communication__c.contains('Newsletter') == false)
					{
						strPreferences += 'Newsletter;';
					}
					
					// add our preferences
					sContact.Email_Communication__c = sContact.Email_Communication__c + strPreferences;
					sContact.HasOptedOutOfEmail = false;
					
				}
				else if(oArgs.MarketingOptIn == false)
				{
					sContact.HasOptedOutOfEmail = true;
					
					// Update Email Communication Preferences
					if(sContact.Email_Communication__c.contains('Chairman\'s Appeal;'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Chairman\'s Appeal;', '');
					}
					if(sContact.Email_Communication__c.contains('Chairman\'s Appeal'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Chairman\'s Appeal', '');
					}
					if(sContact.Email_Communication__c.contains('Christmas Appeal;'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Christmas Appeal;', '');
					}
					if(sContact.Email_Communication__c.contains('Christmas Appeal'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Christmas Appeal', '');
					}
					if(sContact.Email_Communication__c.contains('Event Material;'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Event Material;', '');
					}
					if(sContact.Email_Communication__c.contains('Event Material'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Event Material', '');
					}
					if(sContact.Email_Communication__c.contains('Newsletter;'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Newsletter;', '');
					}
					if(sContact.Email_Communication__c.contains('Newsletter'))
					{
						sContact.Email_Communication__c = sContact.Email_Communication__c.replace('Newsletter', '');
					}
					
				}
				
				sContact.Donation_Addr_Street_1__c	= oArgs.Street;
				sContact.Donation_Addr_City__c		= oArgs.Suburb;
				sContact.Donation_Addr_State__c		= oArgs.State;
				sContact.Donation_Addr_Postcode__c	= oArgs.PostalCode;
				sContact.Donation_Addr_Country__c	= oArgs.Country;
					
				if(
					(sContact.MailingStreet 	!= oArgs.Street) ||
					(sContact.MailingCity 		!= oArgs.Suburb) ||
					(sContact.MailingState 		!= oArgs.State) ||
					(sContact.MailingCountry 	!= oArgs.Country) ||
					(sContact.MailingPostalCode != oArgs.PostalCode)
					)
				{
					try
					{
						Task sTask;
		
						sTask = new Task();
						//sTask.OwnerId		= sContact.OwnerId;
						sTask.OwnerId		= QCacheWebsiteMetadata.getCorporateDonationOwnerForState(oArgs.State);
						sTask.WhoId 		= sContact.Id;
						sTask.Subject 		= 'Address Check Required';
						sTask.Description 	= 'Contact has different address to what has been supplied through website donation process';
						sTask.ActivityDate	= System.Today();
						
						Database.saveResult saveResult = Database.insert(sTask, false);
						
						QUtils.CreateEmail(
												QCacheWebsiteMetadata.getCorporateDonationOwnerForState(oArgs.State),
												'Address Check Required',
												'Contact has different address to what has been supplied through website donation process ' + 'https://eu3.salesforce.com/' + sContact.Id
											);
					}
					catch(Exception e)
					{
						system.debug('TaskInsert:::' + e.getmessage());
					}
				}
				system.debug('ContactAfter:::' + sContact);
				update sContact;
			}
			else
			{
				Account sAccount = [
										select id, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, OwnerId
										from Account 
										where id = :oClientSearchRetStage.AccountId];
			
				sAccount.Donation_Addr_Street_1__c	= oArgs.Street;
				sAccount.Donation_Addr_City__c		= oArgs.Suburb;
				sAccount.Donation_Addr_State__c		= oArgs.State;
				sAccount.Donation_Addr_Postcode__c	= oArgs.PostalCode;
				sAccount.Donation_Addr_Country__c	= oArgs.Country;
				
				if(
							(sAccount.BillingStreet 		!= oArgs.Street)
						||	(sAccount.BillingCity 			!= oArgs.Suburb)
						||	(sAccount.BillingState 			!= oArgs.State)
						||	(sAccount.BillingCountry 		!= oArgs.Country)
						||	(sAccount.BillingPostalCode 	!= oArgs.PostalCode)
					)
				{
					Task	sTask 		= new Task();
					sTask.OwnerId		= sAccount.OwnerId;
					sTask.WhatID 		= sAccount.Id;
					sTask.Subject 		= 'Address Check Required';
					sTask.Description 	= 'PersonAccount has different address to what has been supplied through website donation process';
					sTask.ActivityDate	= System.Today();
				
					Database.saveResult saveResult = Database.insert(sTask, false);
					
					QUtils.CreateEmail(
												QCacheWebsiteMetadata.getCorporateDonationOwnerForState(oArgs.State),
												'Address Check Required',
												'Account has different address to what has been supplied through website donation process ' + 'https://eu3.salesforce.com/' + sAccount.Id
											);
				}
					update sAccount;
			}
		}
		catch(Exception e)
		{
			System.Debug('Exception Thrown : ' + e.getMessage());
			oRet.InError = true;
			oRet.Error = 'Error storing address on Account/Contact.';
		}
		
		System.Debug(oRet);
				
		return oRet;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public static QCorporateWebsiteWebServices.QCampaignDonationResults ProcessDonation(QCorporateWebsiteWebServices.QCampaignDonationRequest oArgs, boolean bAllowCallOut, boolean usePersonAccount)
	{
		// return object
		QCorporateWebsiteWebServices.QCampaignDonationResults oRet = new QCorporateWebsiteWebServices.QCampaignDonationResults();
			
		System.Debug(oArgs);
		System.Debug(bAllowCallOut);
		
		Contact sPersonContactForEmail;
		
		try
		{
			m_Account 	= [
							select 	id, OwnerId, Name, BillingState, ShippingState, IPP_Client_Id__c, IsPersonAccount, PersonContactId 
							from	Account 
							where 	id = :oArgs.UserId
						];
						
			if(usePersonAccount && m_Account.IsPersonAccount)
			{
				sPersonContactForEmail = [select id, Email from Contact where id = :m_Account.PersonContactId];
			}
			else
			{
				usePersonAccount = false;
			}
		}
		catch(Exception e)
		{
			oArgs.CreditCardNumber = '****-****-****-****';
			
			QUtils.CreateErrorEmail('Corporate WebService Issue - Donation Processing', 'Trying to Load Contact Details - ' + oArgs);
			System.debug('Corporate WebService Issue - Donation Processing - Trying to Load Contact Details - ' + oArgs);
    		
    		oRet.InError 			= true;
    		oRet.error				= 'The Payment Portal is currently unavailable, please try again later';
    		return oRet;
		}
		
		string strRecurringDonationCampaign = QCacheWebsiteMetadata.getRedClubRecurringDonationsCampaign();
		
		// if we passed one
		if(oArgs.CampaignId != null)
		{
			try
			{
				m_Campaign = [select id from Campaign where id = :oArgs.CampaignId]; 
			}
			catch(Exception e)
			{
				//QUtils.CreateErrorEmail('Corporate WebService Warning - Donation Processing', 'Invalid Campaign Reference Passed from the Web Service - ' + oArgs + ' - \n\n' + e);
			}
		}
		
		// looks like we havent loaded anything yet so lets get the default
		if(m_Campaign == null)
		{
			try
			{
				m_Campaign = [select id from Campaign where id = :QCacheWebsiteMetadata.getDefaultCampaignForDonations()];
			}
			catch(Exception e)
			{
				QUtils.CreateErrorEmail('Corporate WebService Issue - Donation Processing', 'Trying to Load Default Campaign for Processing');
				System.debug('Corporate WebService Issue - Donation Processing - Trying to Load Default Campaign for Processing');
	    		
	    		oRet.InError 	= true;
	    		oRet.error		= 'The Payment Portal is currently unavailable, please try again later';
    			return oRet;
			}
		}
		else if(
						strRecurringDonationCampaign != null 
					&& 	strRecurringDonationCampaign.length() >= 15
					&& 	oArgs.CampaignId != null
					&& 	oArgs.CampaignId.length() >= 15
					&& 	oArgs.CampaignId.subString(0,15) == strRecurringDonationCampaign.subString(0,15)
				)
		{
			// lets find out if we have the right campaign
			/*
			if(m_Account.IsPersonAccount)
			{
				oArgs.CampaignId = CheckClubRedCampaign(m_Account.ShippingState, oArgs.CampaignId);
			}
			else
			{
				oArgs.CampaignId = CheckClubRedCampaign(m_Account.ShippingState, oArgs.CampaignId);
			}
			*/
			oArgs.CampaignId = CheckClubRedCampaign(m_Account.ShippingState, oArgs.CampaignId);
			
			m_Campaign = [select id, Name from Campaign where id = :oArgs.CampaignId];			//David B. 17/05/2013
			System.debug('====BRO : '+m_Campaign);

			oRet = ProcessRecurringDonation(oArgs, usePersonAccount, sPersonContactForEmail);
			
			// this is a recurring donation - before we go ahead and process let's update the account
			// and contact owner
			try
			{
				string strRecurringOwnerId = QCacheWebsiteMetadata.getCorporateDonationOwnerClubRed();
				
				m_Account.OwnerId = strRecurringOwnerId;
				update m_Account;
				
				if(usePersonAccount)
				{
					CreateCampaignMember(m_Account.PersonContactId, oArgs.CampaignId);
				}

			}
			catch(Exception e)
			{
				// this isn't really critical so let's just debug it. Maybe we should add a task or something?
				System.Debug('Failed to update account and contact owner for recurring donation: ' + e);
			}
			
			return oRet;
		}

		// lets see if there is a child campaign to use....
		
		string strChildCampaignId;
		
		strChildCampaignId = CheckClubRedCampaign(m_Account.ShippingState, m_Campaign.Id);
		
		/*
		if(m_Account.IsPersonAccount)
		{
			strChildCampaignId = CheckClubRedCampaign(m_Account.ShippingState, m_Campaign.Id);
		}
		else
		{
			strChildCampaignId = CheckClubRedCampaign(m_Account.ShippingState, m_Campaign.Id);
		}
		*/
		m_Campaign = [select id from Campaign where id = :strChildCampaignId];
		
		
		/******************************************************************************************
		 	SEND THE PAYMENT!  NEED TO SEND THE PAYMENT BEFORE WE START HITTING THE DATABASE
		*******************************************************************************************/
		QProcessPayment 					oPaymentProcessorStage1;
		QProcessPayment.QProcessPaymentArgs oPaymentProcessorStage1Args;
		QProcessPayment.QProcessPaymentRet	oPaymentProcessorStage1Ret;

		oPaymentProcessorStage1Args = new QProcessPayment.QProcessPaymentArgs();
		
		oPaymentProcessorStage1Args.AllowCallout			= bAllowCallout;
		oPaymentProcessorStage1Args.IPPClientId				= m_Account.IPP_Client_Id__c;
		
		integer iIPPPaymentRef = QPoolCounter.GetPoolCounter(QConstants.CORPORATE_IPP_INCOME_COUNTER_NAME);
		
		oPaymentProcessorStage1Args.IPPTransactionRef		= iIPPPaymentRef;

		oPaymentProcessorStage1Args.Amount 					= oArgs.Amount;
		oPaymentProcessorStage1Args.CreditCardNumber		= oArgs.CreditCardNumber;
		oPaymentProcessorStage1Args.CreditCardName			= oArgs.CardName;
		oPaymentProcessorStage1Args.CreditCardExpiryYear 	= Integer.valueOf(oArgs.ExpiryYear);
		oPaymentProcessorStage1Args.CreditCardExpiryMonth 	= Integer.valueOf(oArgs.ExpiryMonth);
		oPaymentProcessorStage1Args.CreditCardCVN			= oArgs.CVC;
		oPaymentProcessorStage1Args.CreditCardType			= QUtils.GetCreditCardType(oArgs.CreditCardNumber);
		oPaymentProcessorStage1Args.PaymentProcess			= QProcessPayment.PaymentProcess.SINGLE_PAYMENT_CALLOUT_ONLY;
		
		oPaymentProcessorStage1 	= new QProcessPayment(oPaymentProcessorStage1Args);
		oPaymentProcessorStage1Ret 	= oPaymentProcessorStage1.ProcMain();						// FIRE!!!
		
		QPoolCounter.IncrementCounter(QConstants.CORPORATE_IPP_INCOME_COUNTER_NAME);
		
		// so we had big failure sending to the bank, this is a major exception not insufficient funds etc
		if(oPaymentProcessorStage1Ret.InError || oPaymentProcessorStage1Ret.IPPResponse == null)
    	{
    		// New method call for formatted message for finance users
    		String strMessage = QUtils.strFormattedMessage(oPaymentProcessorStage1Ret, oPaymentProcessorStage1Args);
    		
    		// Changed from oPaymentProcessorStage1Ret to strMessage
    		QUtils.CreateErrorTask(m_Account.Id, null, 'Corporate WebService Issue - Payment Processing', 'Trying to Make Payment - ' + strMessage);
    		QUtils.CreateErrorEmail('Corporate WebService Issue - Payment Processing', 'Trying to Make Payment - ' + strMessage);
			System.debug('Corporate WebService Issue - Payment Processing - Trying to Make Payment - ' + oPaymentProcessorStage1Ret);
    		
    		oRet.InError 	= true;
    		oRet.Error		= 'The Payment Portal is currently unavailable, please try again later';
    		return oRet;
    	}
    	// otherwise send back the banking error, this is the insufficient funds type errors
    	else if(!oPaymentProcessorStage1Ret.IsPaymentSuccessful)
    	{
    		// New method call for formatted message for finance users
    		String strMessage = QUtils.strFormattedMessage(oPaymentProcessorStage1Ret, oPaymentProcessorStage1Args);
    		
    		// Changed from oPaymentProcessorStage1Ret to strMessage
    		QUtils.CreateErrorTask(m_Account.Id, null, 'Corporate WebService Issue - Payment Processing', 'Trying to Make Payment - ' + strMessage);
    		QUtils.CreateErrorEmail('Corporate WebService Issue - Payment Processing', 'Trying to Make Payment - ' + strMessage);
			System.debug('Corporate WebService Issue - Payment Processing - Trying to Make Payment - ' + oPaymentProcessorStage1Ret);
    		
    		oRet.InError 	= true;
    		oRet.Error		= 'There has been a problem processing your credit card, please check your details and try again or call us on 1800 0REDKITE (1800 733 548)';
    		return oRet;
    	}

		if(usePersonAccount)
		{
			CreateCampaignMember(m_Account.PersonContactId, oArgs.CampaignId);
		}
	
		// can start creating the Opportunity, Income and Transaction Records
		/*********************************************
			 RECORD THE PAYMENT
		*********************************************/
		QProcessPayment 					oPaymentProcessorStage2;
		QProcessPayment.QProcessPaymentArgs oPaymentProcessorStage2Args = new QProcessPayment.QProcessPaymentArgs();
		QProcessPayment.QProcessPaymentRet	oPaymentProcessorStage2Ret;
		
		oPaymentProcessorStage2Args.AllowCallout			= bAllowCallout;
		oPaymentProcessorStage2Args.IPPTransactionRef		= iIPPPaymentRef;
		oPaymentProcessorStage2Args.AccountId 				= m_Account.Id;
		oPaymentProcessorStage2Args.CampaignId 				= m_Campaign.Id;
		
		oPaymentProcessorStage2Args.PostReceipt				= oArgs.PostReceipt;
		oPaymentProcessorStage2Args.IncomeComments			= oArgs.Comments;
		oPaymentProcessorStage2Args.DonationSource			= oArgs.DonationSource;
		
		oPaymentProcessorStage2Args.Amount 					= oArgs.Amount;
		oPaymentProcessorStage2Args.CreditCardNumber		= oArgs.CreditCardNumber;
		oPaymentProcessorStage2Args.CreditCardName			= oArgs.CardName;
		oPaymentProcessorStage2Args.CreditCardExpiryYear 	= oArgs.ExpiryYear;
		oPaymentProcessorStage2Args.CreditCardExpiryMonth 	= oArgs.ExpiryMonth;
		oPaymentProcessorStage2Args.CreditCardCVN			= oArgs.CVC;
		oPaymentProcessorStage2Args.CreditCardType			= QUtils.GetCreditCardType(oArgs.CreditCardNumber);
		oPaymentProcessorStage2Args.PaymentProcess			= QProcessPayment.PaymentProcess.CORPORATE_RECORD_SINGLE_PAYMENT;
		
		// super important, send the processor the response that we got when we made the payment
		oPaymentProcessorStage2Args.IPPResponse				= oPaymentProcessorStage1Ret.IPPResponse;
		
		oPaymentProcessorStage2 	= new QProcessPayment(oPaymentProcessorStage2Args);
		oPaymentProcessorStage2Ret 	= oPaymentProcessorStage2.ProcMain();					// FIRE!!!
		
		// something bad happened while recording the payment information
		if(oPaymentProcessorStage2Ret.InError)
    	{
    		oPaymentProcessorStage2Args.CreditCardNumber = '****-****-****-****';
    		
    		QUtils.CreateErrorTask(m_Account.Id, null, 'Corporate WebService Issue - Payment Recording', 'Trying to Record Details - ' + oPaymentProcessorStage2Args);
    		QUtils.CreateErrorEmail('Corporate WebService Issue - Payment Processing', 'Trying to Make Payment - ' + oPaymentProcessorStage2Ret);
			System.debug('Corporate WebService Issue - Payment Recording - Trying to Record Details - ' + oPaymentProcessorStage2Args);
			
    		oRet.InError 	= true;
    		oRet.Error		= 'An error has been encountered while processing your Payment, a Redkite representative will contact you shortly';
    		return oRet;
    	}
    	
	   	/*************************************************
			 Now send the emails
		**************************************************/
    	boolean bError = false;
    	if(oPaymentProcessorStage1Ret.IsPaymentSuccessful)
    	{
			try
			{
				if(usePersonAccount)
				{
					// Now we need to send the Donor the Redkite Corporate Reciept Email
					Income__c sIncome = [select Id, Personal_Donation__c from Income__c where Id = :oPaymentProcessorStage2Ret.Income.Id];
					EmailTemplate 			sEmailTemplate = null;
					//Business or Personal Donation ?
					if(sIncome.Personal_Donation__c) 
						sEmailTemplate 	= [SELECT Id, Name FROM EmailTemplate WHERE Name = :'Corporate Donation Visualforce Template' AND IsActive = TRUE];
					else 
						sEmailTemplate 	= [SELECT Id, Name FROM EmailTemplate WHERE Name = :'Corporate Business Donation Visualforce Template' AND IsActive = TRUE];
					
					OrgWideEmailAddress 	sEmailAddress 	= [select id, Address from OrgWideEmailAddress where DisplayName = 'Redkite'];
					
					// send login details e-mail
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					
					mail.setWhatId(oPaymentProcessorStage2Ret.Income.Id);
					mail.setTargetObjectId(sPersonContactForEmail.Id);
					mail.setBccSender(false);
					mail.setUseSignature(false);
					mail.setOrgWideEmailAddressId(sEmailAddress.Id);
					
					mail.setTemplateId(sEmailTemplate.Id);
					
					system.debug('***************mail:' + mail);
					
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}
				else
				{
					bError = true;
				}
			}
			catch(Exception e)
			{
				bError = true;
				QUtils.CreateErrorTask(oPaymentProcessorStage2Args.IncomeId, oPaymentProcessorStage2Args.ContactId, 'Corporate Website Issue - Donation Email', 'Email Template - Corporate Donation Visualforce Template - ' + e);
			}
			
			//DavidCode
			if(bError)
			{
				try
				{
					User sUser = [SELECT Id, Email FROM User WHERE Id = : UserInfo.getUserId()];
					
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new String[] {sUser.Email});
					mail.setBccSender(false);
					mail.setUseSignature(false); 
					mail.setSubject('Donation Receipt Error Notification');
					
					string strBody = '<p>There was an error sending a notifcation for Income Record: '
									+ '<a href=\'http://eu3.salesforce.com/' + oPaymentProcessorStage2Ret.Income.Id + '\'>'
									+ oPaymentProcessorStage2Ret.Income.Name + '</a>'
									+ '.<br/>Errors:<br/>';
					
					for(string str : oPaymentProcessorStage2Ret.ErrorList)
					{
						strBody += str + '<br/>';
					}
					
					strBody += '<br/>You may need to send a receipt manually.';
					
					mail.setHtmlBody(strBody);
					mail.setSaveAsActivity(false);
				
					Messaging.sendEmail(new Messaging.Singleemailmessage[] {mail});
				}
				catch(Exception e)
				{					
					
				}
			}

			if(oPaymentProcessorStage2Ret.IsPaymentSuccessful && oArgs.PostReceipt)
			{
				/*EmailTemplate sEmailTemplate 	= [select	Id, Name
													from	EmailTemplate
													where	Name = :'Postal Receipt Required Notification'
													and		IsActive = TRUE];*/
													
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setToAddresses(new String[] {QCacheWebsiteMetadata.getPostalReceiptNotificationEmail()});
				// mail.setToAddresses(new String[] {'matt.lacey@quattroinnovation.com'});
				mail.setBccSender(false);
				mail.setUseSignature(false);
				mail.setSaveAsActivity(false);
				mail.setSubject('RED: Posted Receipt Required');
				mail.setSenderDisplayName('RED');
				mail.setHTMLBody('A posted receipt is required for donation income ' +
									'<a href=\'http://eu3.salesforce.com/' + oPaymentProcessorStage2Ret.Income.Id + '\'>'
									+ oPaymentProcessorStage2Ret.Income.Id + '</a>');
			
				Messaging.sendEmail(new Messaging.Singleemailmessage[] {mail});				
			}
			
			// if the donation was for more than $500 we send notification to the account owner at RK
			if((oPaymentProcessorStage2Args.Amount / 100) >= 500)
			{
				try
				{
					EmailTemplate sEmailTemplate 	= [SELECT Id, Name FROM EmailTemplate WHERE Name = :'Large Donation Notification' AND IsActive = TRUE];
					User sUser						= [select Id, Email from User where Id = : m_Account.OwnerId];
					
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
//					mail.setToAddresses(new String[] {sAccount.OwnerId__r.});
					mail.setTargetObjectId(sUser.Id);
					mail.setBccSender(false);
					mail.setUseSignature(false);
					mail.setSubject('Large Donation Notification');
					mail.setSaveAsActivity(false);
					mail.setPlainTextBody('Person Account ID ' + m_Account.Id + ' (' + m_Account.Name + ') has made a donation of $' + oPaymentProcessorStage2Args.Amount / 100);
				
					Messaging.sendEmail(new Messaging.Singleemailmessage[] {mail});
				}
				catch(Exception e)
				{					
					QUtils.CreateErrorTask(oPaymentProcessorStage2Args.IncomeId, oPaymentProcessorStage2Args.ContactId, 'Corporate Website Issue - Donation Notification Email', 'Failed to send notification of donation > $500 - ' + e);
				}
			}
    	}

		oRet.InError 				= false;
    	oRet.Error					= 'Success';
		
		return oRet;
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Lets go and see if we can find a better campaign for this contact
	private static string CheckClubRedCampaign(string strState, string strCampaign)
	{
		System.debug('====BRO STATE: '+strState+' - PARENT : '+strCampaign);
		string strRet = '';
		
		try
		{
			strRet = [
								select 	Id 
								from 	Campaign where ParentId = :strCampaign 
								and 	Job_Code_State__c =:strState limit 1].Id;
		}catch(Exception e)
		{
			// we didnt find one so we stay the same 
			strRet = strCampaign;
		}
		
		return strRet;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// create the contact campaign member
	private static void CreateCampaignMember(string strContactId, string strCampaignId)
	{	
		
		CampaignMember sCampaignMember 	= new CampaignMember();
		sCampaignMember.ContactId 		= strContactId;
		sCampaignMember.CampaignId 		= strCampaignId;
		sCampaignMember.Status 			= 'Attended';
		try
		{
			upsert sCampaignMember;
		}
		catch(Exception e)
		{
			// this is normally be becauser they are already a member of the campaign so no need to worry about it
			System.Debug('UPSERT OF CAMPAIGN MEMBER DID NOT WORK! ************* ' + e);
		}		
	}

	
	public static QCorporateWebsiteWebServices.QCampaignDonationResults ProcessRecurringDonation(QCorporateWebsiteWebServices.QCampaignDonationRequest oArgs, boolean isPersonAccount, Contact sPersonContactForEmail)
	{
		QCorporateWebsiteWebServices.QCampaignDonationResults oRet = new QCorporateWebsiteWebServices.QCampaignDonationResults();

		QProcessPayment 					oPaymentProcessor;
		QProcessPayment.QProcessPaymentArgs oPaymentProcessorArgs = new QProcessPayment.QProcessPaymentArgs();
		QProcessPayment.QProcessPaymentRet	oPaymentProcessorRet;
		
		oPaymentProcessorArgs.AccountId 				= m_Account.Id;
		oPaymentProcessorArgs.CampaignId 				= m_Campaign.Id;
		oPaymentProcessorArgs.Amount 					= oArgs.Amount;
		
		oPaymentProcessorArgs.CreditCardNumber			= oArgs.CreditCardNumber;
		oPaymentProcessorArgs.CreditCardName			= oArgs.CardName;
		oPaymentProcessorArgs.CreditCardExpiryYear 		= oArgs.ExpiryYear;
		oPaymentProcessorArgs.CreditCardExpiryMonth 	= oArgs.ExpiryMonth;
		oPaymentProcessorArgs.CreditCardCVN				= oArgs.CVC;
		oPaymentProcessorArgs.IncomeComments			= oArgs.Comments;
		oPaymentProcessorArgs.CreditCardType			= QUtils.GetCreditCardType(oArgs.CreditCardNumber);
		oPaymentProcessorArgs.EndDate					= oArgs.EndDate;
		oPaymentProcessorArgs.PaymentProcess			= QProcessPayment.PaymentProcess.CORPORATE_SETUP_RECURRING_PAYMENT_TOKEN;				
		
		// create the processor and run it
		oPaymentProcessor 	= new QProcessPayment(oPaymentProcessorArgs);
		oPaymentProcessorRet = oPaymentProcessor.ProcMain();					// FIRE!!!
		
		// something bad happened while recording the payment information
		if(oPaymentProcessorRet.InError)
    	{
    		oPaymentProcessorArgs.CreditCardNumber = '****-****-****-****';
    		
    		QUtils.CreateErrorTask(m_Account.Id, null, 'Corporate WebService Issue - Payment Recording', 'Trying to Record Details - ' + oPaymentProcessorArgs);
			System.debug('Corporate WebService Issue - Payment Recording - Trying to Record Details - ' + oPaymentProcessorArgs);
			
    		oRet.InError 	= true;
    		oRet.Error		= 'An error has been encountered while processing your Payment, a Redkite representative will contact you shortly';
    	}
    	else
    	{
    		OrgWideEmailAddress 	sEmailAddress;
    		
    		try
			{
				sEmailAddress				 	= [select id, Address from OrgWideEmailAddress where DisplayName = 'Redkite'];
				EmailTemplate 	sEmailTemplate 	= [SELECT Id, Name FROM EmailTemplate WHERE Name = :'Corporate Club Red Donation Template' AND IsActive = TRUE];
				
				// send login details e-mail
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

				mail.setWhatId(oPaymentProcessorRet.CROpportunity);
				
				if(isPersonAccount && sPersonContactForEmail != null)
				{
					mail.setTargetObjectId(sPersonContactForEmail.Id);
				}
				else
				{
					// this will still fail but its a business process problem not code
					mail.setTargetObjectId(m_Account.Id);
				}
				mail.setBccSender(false);
				mail.setUseSignature(false);
				mail.setOrgWideEmailAddressId(sEmailAddress.Id);
				
				mail.setTemplateId(sEmailTemplate.Id);
				
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
			}
			catch(Exception e)
			{
				QUtils.CreateErrorTask(m_Account.Id, null, 'Corporate Website Issue - Club Red Email', 'Email Template - Corporate Club Red Donation Template - ' + e);
			}
    		
    		try
			{
				if(sEmailAddress == null)
				{
					sEmailAddress = [select id, Address from OrgWideEmailAddress where DisplayName = 'Redkite'];
				}
				
				// send login details e-mail
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();				

				mail.setBccSender(false);
				mail.setUseSignature(false);
				mail.setOrgWideEmailAddressId(sEmailAddress.Id);
				
				mail.setToAddresses(new String[] {QCacheWebsitemetadata.getNewClubRedNotificationAddress()});
//				mail.setToAddresses(new String[] {'matt.lacey@quattroinnovation.com'});
				
				mail.setSubject('New Club Red Member');
				mail.setHTMLBody('<html><body><p>Account ' + m_Account.Id + ' has set up a recurring donation to Club Red. Please see <a href="http://eu3.salesforce.com/' +
								 m_Account.Id + '">http://eu3.salesforce.com/' + m_Account.Id + '</a> for more information.</body></html>');
				
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
			catch(Exception e)
			{
				QUtils.CreateErrorTask(m_Account.Id, null, 'Corporate Website Issue - Recurring Donation Email', 'Email Template - Corporate Club Red Donation Visualforce Template - ' + e);
			}
			
			oRet.InError	= false;
			oRet.Error		= 'Success';
    	}
		
		return oRet;
	}
}