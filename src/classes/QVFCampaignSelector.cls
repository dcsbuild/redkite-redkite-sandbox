public class QVFCampaignSelector 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class IncomeManagementScreenParameterException 	extends Exception{}
	public class IncomeManagementDataValidationException 	extends Exception{}
	
	////////////////////////////////////////////
	// Campaign Member Classes
	public class QCampaign
	{
		public Id		CampaignId{get; set;}
		public string 	Name{get; set;}
		public string	CampaignState{get; set;}
		public string	CampaignType{get; set;}
		
		public pageReference Redirect()
		{
			PageReference pageRedirect = new PageReference('/apex/Income_Management');
			
			pageRedirect.getParameters().putAll(ApexPages.currentPage().getParameters());
			pageRedirect.getParameters().put('campaignId', this.CampaignId);
			//pageRedirect.getParameters().put('origin','PersonAccount'); // I don't why it's here ==> to delete
			
			pageRedirect.setRedirect(true);
			
			System.Debug(pageRedirect);
			
			return pageRedirect;
		}
		public QCampaign()
		{}
		public QCampaign(Campaign oCampaign)
		{
			this.CampaignId 	= oCampaign.Id;
			this.Name 			= oCampaign.Name;
			this.CampaignState	= oCampaign.Job_Code_State__c;
			this.CampaignType	= oCampaign.Type;
		}
	}
	
	////////////////////////////////////////////
	// Singular
	string	m_strID;
	public QCampaign 			mp_SearchParams				{get; set;}
	public list<SelectOption>	mp_liStateFilterOptions		{get; set;}
	public list<SelectOption>	mp_liTypeFilterOptions		{get; set;}
	
	////////////////////////////////////////////
	// Collections
	public list <QCampaign> 	m_liQCampaign				{get; set;}

	////////////////////////////////////////////
	// Constructor
	public QVFCampaignSelector()
	{
		// lets grab the screen parameters
		if(null == (m_strID = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new IncomeManagementScreenParameterException('Id Parameter not found');	
		}
		mp_SearchParams = new QCampaign();
		mp_SearchParams.CampaignState = 'All';
		mp_SearchParams.CampaignType  = 'All';
		//State List
		mp_liStateFilterOptions = new list<SelectOption>();
		mp_liStateFilterOptions.add(new SelectOption('All','All'));
		list<string> li = new list<string>();
		for(Schema.Picklistentry sState : Campaign.Job_Code_State__c.getDescribe().getPicklistValues())
		{
			li.add(sState.getValue());
		}
		li.sort();
		for(string strState : li )
		{
			mp_liStateFilterOptions.add(new SelectOption(strState,strState));	
		}
		//Type List
		mp_liTypeFilterOptions = new list<SelectOption>();
		mp_liTypeFilterOptions.add(new SelectOption('All','All'));
		for(RecordType sRecordType : [select Id, DeveloperName, Name from RecordType where SObjectType = 'Campaign' order by Name])
		{
			mp_liTypeFilterOptions.add(new SelectOption(sRecordType.DeveloperName, sRecordType.Name));	
		}
		/*for(Schema.Picklistentry sType : Campaign.Type.getDescribe().getPicklistValues())
		{
			mp_liTypeFilterOptions.add(new SelectOption(sType.getValue(),sType.getValue()));
		}*/
		m_liQCampaign = new list<QCampaign>();
		LoadQCampaigns();
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	/*public list<QCampaign> getQCampaignList()
	{
		if(m_liQCampaign == null)
		{
			
		}else
		{
			LoadQCampaignsWithParams();	
		}
		return m_liQCampaign;
	}*/
	
	public pageReference Cancel()
	{
		PageReference pageRedirect = new PageReference('/' + m_strID);
		pageRedirect.setRedirect(true);
		
		System.Debug(pageRedirect);
		
		return pageRedirect;
	}
	
	public pageReference FilterCampaigns()
	{
		LoadQCampaignsWithParams();
		return null;	
	}
	public pageReference ClearSearch()
	{
		mp_SearchParams.Name			 	= '';
		mp_SearchParams.CampaignState 		= 'All';
		mp_SearchParams.CampaignType 		= 'All';
		return null;	
	}
	
	/***********************************************************************************************************
    	LIST METHODS
    ***********************************************************************************************************/
	private void LoadQCampaignsWithParams()
	{
		m_liQCampaign.clear();
		
		string strQuery = 'select id, Name, Job_Code_State__c, Type from Campaign where IsActive = true AND ';
		
		if(mp_SearchParams.CampaignState == null || mp_SearchParams.CampaignState == 'All')
			mp_SearchParams.CampaignState = '';
		strQuery		+= ' Job_Code_State__c like \'%'+mp_SearchParams.CampaignState+'%\' AND ';
		
		if(mp_SearchParams.CampaignType == null || mp_SearchParams.CampaignType == 'All')
			mp_SearchParams.CampaignType = '';
		strQuery		+= ' RecordType.DeveloperName like \'%'+mp_SearchParams.CampaignType+'%\' AND ';
		
		if(mp_SearchParams.Name == null || mp_SearchParams.Name == 'All')
			mp_SearchParams.Name = '';
		strQuery		+= ' Name like \'%'+mp_SearchParams.Name+'%\' ';
		
		strQuery		+= ' order by Name';
		for(Campaign [] arr : Database.query(strQuery + ' limit 501'))
		{
			for(Campaign s : arr)
			{
				m_liQCampaign.add(new QCampaign(s));	
			}
		}
	}
    
    /***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	private void LoadQCampaigns()
	{
		m_liQCampaign.clear();
		for(Campaign[] arrCampaign : [select id, Name, Job_Code_State__c, Type from Campaign where IsActive = true order by Name])
		{
			for(Campaign sCampaign : arrCampaign)
			{
				m_liQCampaign.add(new QCampaign(sCampaign));
			}	
		}
	}
	
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void testExceptionAndCancel()
	{
		PageReference pageRef = new PageReference('/apex/Campaign_Selector');
		Test.setCurrentPage(pageRef);
		
		QVFCampaignSelector controller;
		
		boolean bExceptionThrown = false;
		
		// break it due to no params
		try
		{
			controller = new QVFCampaignSelector();
		}
		catch(Exception e)
		{
			bExceptionThrown = true;
		}
		
		System.AssertEquals(true, bExceptionThrown);
		
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', sCampaign.Id);
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFCampaignSelector();
		
		string pageReturn = controller.Cancel().getURL();
		
		System.assertEquals('/'+sCampaign.Id, pageReturn);

	}
	
	public static testmethod void testEnd2End()
	{
		// create our Campaign
		Campaign sCampaign = new Campaign();
		sCampaign.Name = 'Testing';
		sCampaign.IsActive = true;
		sCampaign.Approval_Hidden__c = true;
		insert sCampaign;
		
		PageReference pageRef = new PageReference('/apex/Campaign_Selector');
		Test.setCurrentPage(pageRef);
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', sCampaign.Id);
		
		QVFCampaignSelector controller;
		
		controller = new QVFCampaignSelector(); 
		
		// run the load details logic
		//controller.getQCampaignList();
	}
}