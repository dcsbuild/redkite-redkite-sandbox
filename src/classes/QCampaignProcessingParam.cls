global class QCampaignProcessingParam 
{
	/***********************************************************************************************************
		ENUMS
	***********************************************************************************************************/
	global enum ActionCode
	{
		PROCESS_ALL_OPPORTUNITIES_BY_CAMPAIGN,
		PROCESS_OPPORTUNITY_PAYMENTS
	}
	
	global enum ParentType
	{
		PERSON_ACCOUNT,
		ACCOUNT
	}
	
	/***********************************************************************************************************
		ARGUMENT OBJECT
	***********************************************************************************************************/
	global class QCampaignProcessingArg 
	{
		public id 			Target{		get; private set;}
		public list<id>		TargetList{	get; private set;}
		public ActionCode 	ActionCode{	get; private set;}
		public ParentType 	ParentType{	get; private set;}
		
		public QCampaignProcessingArg()
		{
		}
		
		public QCampaignProcessingArg(ActionCode theActionCode, id theTarget)
		{
			this.ActionCode = theActionCode;
			this.Target 	= theTarget;
		}
		
		public QCampaignProcessingArg(ActionCode theActionCode, ParentType theParentType, list<id> theTarget)
		{
			this.ActionCode = theActionCode;
			this.ParentType = theParentType;
			this.TargetList	= theTarget;
		}
	}
	
	/***********************************************************************************************************
		RETURN OBJECT
	***********************************************************************************************************/
	global class QCampaignProcessingRet 
	{
		public integer 	TotalOpportunities{get; set;}
		public integer	TotalPersonAccountOpportunities{get; set;}
		public integer	TotalAccountOpportunities{get; set;}
		
		public integer	TotalThreadsFired{get; set;}
		
		public double 	TotalIncomeCreated{get; set;}
		public string 	Message{get; set;}
		public boolean 	ReprocessRequired{get; set;}
		public boolean 	MalformedOpportunity{get; set;}
			
		public QCampaignProcessingRet()
		{
			this.TotalOpportunities 				= 0;
			
			this.TotalPersonAccountOpportunities 	= 0;
			this.TotalAccountOpportunities 			= 0;
		
			this.TotalThreadsFired 					= 0;
			
			this.TotalIncomeCreated 				= 0;
			
			this.ReprocessRequired					= false;
			this.MalformedOpportunity				= false;
		}
	}
}