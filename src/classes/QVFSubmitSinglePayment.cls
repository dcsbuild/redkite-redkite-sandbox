public class QVFSubmitSinglePayment 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////
	// Exception Classes
	public class QScreenParameterException extends Exception{}
	
	////////////////////////////////////////////
	// Singular
	private string		m_strId;
	private string		m_strIncomeId;
	private string		m_strReturnId;
	
	public 	string		mp_strCreditCardNumber	{get; set;}
	public 	string		mp_strCreditCardName	{get; set;}
	public 	string		mp_strCreditCardExpM	{get; set;}
	public 	string		mp_strCreditCardExpY	{get; set;}
	public 	string		mp_strCreditCardCVC		{get; set;}
	
	private Opportunity m_sOpportunity;
	private Account		m_sAccount;
	private Income__c	m_sIncome;
	private Income__c	m_sDeclinedIncome;

	public	QProcessIPPPayment.QProcessIPPPaymentArgs				CreditCardStub				{get; private set;}
	public	QProcessIPPPayment.QProcessIPPPaymentRet 				ReturnStub					{get; private set;}
	
	public	QProcessIPPPayment_V2.QCreditCardByReference			CreditCardByReferenceStub;
	public	QProcessIPPPayment_V2.QCreditCardByReferenceReturn		RefReturnStub;
	
	// hack to stop the decimal place disapearing issue
	public	string 	mp_strScreenAmount							{get; private set;}
	public	string 	mp_strReturnErrorString						{get; private set;}
	public	boolean mp_bShowNewCardDetailInput					{get; private set;}
	public 	boolean mp_bIncomeAlreadyProcessed					{get; private set;}
	public	string 	mp_strCredit_Card_Last_4_Digit				{get; private set;}	
	
	public	boolean mp_bIsRejected 								{get; set;}
	public	string	mp_strAcctRedirect							{get; set;}
	
	// Data returned from the payment request
	private	string m_strSettlementDate, m_strResponseCode, m_strReceipt, m_strDeclinedMessage, m_strDeclinedCode;
	
	//Only use for testing
	private static boolean blForTest = false;
	private static boolean blResultFromBankForTest = true;
	private boolean CloseOpportunity;

	////////////////////////////////////////////
	// Collections
	list<selectOption> m_liExpMonth;
	list<selectOption> m_liExpYear;
	public set<String> setOpptyRecordTypesforExclude			{get;set;}

	
	////////////////////////////////////////////
	// Constructor
	public QVFSubmitSinglePayment()
	{
		// 	lets grab the screen parameters - ID we came from		
		if(null == (m_strId = ApexPages.currentPage().getParameters().get('Id')))
		{
			throw new QScreenParameterException('Id Parameter not found');	
		}
		// 	Income ID we came from		
		if(null == (m_strIncomeId = ApexPages.currentPage().getParameters().get('IncomeId')))
		{
			throw new QScreenParameterException('Income Id Parameter not found');	
		}

		// Where we should return to
		if(null == (m_strReturnId = ApexPages.currentPage().getParameters().get('ReturnId')))
		{
			// No need to throw an error for this one at the moment
			//throw new RecalculateBudgetAllocationScreenParameterException('Origin Parameter not found');	
		}
		
		CloseOpportunity = Boolean.valueOf(ApexPages.currentPage().getParameters().get('bCloseOppty'));
		
	}
	
	public void Init()
	{
		// Memory for the details
		CreditCardStub								= new QProcessIPPPayment.QProcessIPPPaymentArgs();// New Credit Card details stub
		CreditCardStub.AllowCallout					= true;
		CreditCardStub.PaymentProcess 				= QProcessIPPPayment.PaymentProcess.SINGLE_PAYMENT;		
		
		CreditCardByReferenceStub					= new QProcessIPPPayment_V2.QCreditCardByReference();		// Existing payment token stub
		
		// Used to exclude opportunity record types from deletion and stage updates.
		setOpptyRecordTypesforExclude = new set<String>();
		setOpptyRecordTypesforExclude.add('Corporates');
		
		LoadData();
		
		// If we already have a payment token, display details of the credit card and ask if we want to use it
		// Otherwise go get credit card details
		boolean bExpiredCard = m_sAccount.Credit_Card_Expire_Year__c == null
								|| Integer.valueOf(m_sAccount.Credit_Card_Expire_Year__c) < Datetime.now().year()
								|| (Integer.valueOf(m_sAccount.Credit_Card_Expire_Year__c) == Datetime.now().year() 
									&& Integer.valueOf(m_sAccount.Credit_Card_Expire_Month__c ) < Datetime.now().month() );
								
		if (m_sAccount.IPPayment_Customer_Token__c != null && !bExpiredCard)
		{
			mp_strCredit_Card_Last_4_Digit = 'XXXX-XXXX-XXXX-' + m_sAccount.Credit_Card_Last_4_Digit__c;
			mp_bShowNewCardDetailInput = false;
		}
		else
		{
			mp_bShowNewCardDetailInput = true;
		}
		
		// ok something has happened before
		if(m_sIncome.Payment_Gateway_Response_Code__c != null && m_sIncome.Payment_Gateway_Response_Code__c != '')
		{
			mp_bIncomeAlreadyProcessed = true;
			
			if(m_sIncome.Payment_Gateway_Transaction_Approved__c == true)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Payment Transaction has already been processed successfully'));
			}
			else
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Payment Transaction has already been sent to the bank, please check Income record for details of any errors'));
			}
		}
		else
		{
			mp_bIncomeAlreadyProcessed = false;
		}
 
		mp_strScreenAmount = String.valueOf(m_sIncome.Amount_A__c);
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Cancel Button - will return to ReturnID if present or the entity we started from
	/*public pageReference Cancel2()
	{
		PageReference pageRedirect;
		
		if(m_sIncome != null && !m_sIncome.Income_Processed_A__c)
		{
			try
			{
				m_sIncome.Status__c = 'Cancelled';
				m_sIncome.Amount_A__c = 0;
				update m_sIncome;
			}
			catch (Exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to mark income record cancelled.'));
				return null;
			}
		}
		
		if(m_strReturnId != null)
		{
			pageRedirect = new PageReference('/' + m_strReturnId);
		}
		else
		{	
			pageRedirect = new PageReference('/' + m_strId);
		}
		
		pageRedirect.setRedirect(true);
		
		return pageRedirect;
	}*/
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Cancel Button - will return to ReturnID if present or the entity we started from
	public pageReference Cancel()
	{	//when we cancel ==> need to delete the income and the oppty (if the oppty doesn't have any other income)
		PageReference pageRedirect;
		string strAccountId ;
		if(m_sIncome != null && !m_sIncome.Income_Processed_A__c)
		{
			try
			{
				mp_strAcctRedirect = m_sIncome.AccountId__c;
				string strOpptyId = m_sIncome.Opportunity__c;  
				delete m_sIncome;				//we don't want to keep anything when we cancel.
				
				Opportunity sOppty = [select id, (select Id from Donations__r where Declined__c = false) from Opportunity where Id = :strOpptyId];
				if(sOppty.Donations__r == null || sOppty.Donations__r.isEmpty())	//delete the oppty if no other income
				{
					delete sOppty;
				}
				 
			}
			catch (Exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to delete income record.'));
				return null;
			}
		}
		
		pageRedirect = new PageReference('/' + mp_strAcctRedirect);
		pageRedirect.setRedirect(true);
		
		return pageRedirect;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Have requested new card details - set boolean and redisplay page
	public pageReference NewDetails()
	{
		mp_bShowNewCardDetailInput = true;
		return null;	
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Send Payment Button - will send info to payment platform
	public pagereference SendPayment()
	{
		PageReference pr;
		
		mp_bIsRejected = false;
		
		if(ProcessPayment())
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Payment Transaction has processed successfully'));
		}
		else
		{
			CleanUpData();
			//Cancel(); 	
			mp_bIsRejected = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Payment has not been successful and the income has been deleted.') );
			return null;
		}
		
		// mark it as processed so the buttons go
		mp_bIncomeAlreadyProcessed = true;
		
		
		if(m_sIncome != null && m_sIncome.Id != null)
		{
			pr = new PageReference('/' + m_sIncome.Id);
		}
		else if(m_sOpportunity != null && m_sOpportunity.Id != null)
		{
			pr = new PageReference('/' + m_sOpportunity.Id);
		}

		return pr;
	}
	
	public Pagereference Back()
	{
		PageReference pageRedirect = new PageReference('/' + mp_strAcctRedirect);
		pageRedirect.setRedirect(true);
		
		return pageRedirect;
	}
	
	/**************************************************************************************************************
    	LIST METHODS
    **************************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
    public list<selectOption> getExpYear()
    {
    	if(m_liExpYear == null)
    	{
    		m_liExpYear = new list<selectOption>();
    		
    		for(integer i = 0; i < 10; i++)
    		{
    			string strYear = String.ValueOf(System.Today().Year() + i);
    			m_liExpYear.add(new selectOption(strYear, strYear));
    		}
    	}
    	return m_liExpYear;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
    public list<selectOption> getExpMonth()
    {
    	if(m_liExpMonth == null)
    	{
    		m_liExpMonth = new list<selectOption>();
    		
    		m_liExpMonth.add(new selectOption('01',	'01'));
    		m_liExpMonth.add(new selectOption('02', '02'));
    		m_liExpMonth.add(new selectOption('03', '03'));
    		m_liExpMonth.add(new selectOption('04', '04'));
    		m_liExpMonth.add(new selectOption('05', '05'));
    		m_liExpMonth.add(new selectOption('06', '06'));
    		m_liExpMonth.add(new selectOption('07', '07'));
    		m_liExpMonth.add(new selectOption('08', '08'));
    		m_liExpMonth.add(new selectOption('09', '09'));
    		m_liExpMonth.add(new selectOption('10', '10'));
    		m_liExpMonth.add(new selectOption('11', '11'));
    		m_liExpMonth.add(new selectOption('12', '12'));
    	}
    	
    	return m_liExpMonth;
    }
    
    /**************************************************************************************************************
    	WORKER METHODS
    **************************************************************************************************************/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// If Income is decline. Then Delete Income and Opportunity
    private void CleanUpData()
    {
    	if(m_sIncome != null && !m_sIncome.Income_Processed_A__c)
		{
			try
			{
				string strOpptyId = m_sIncome.Opportunity__c;
				
				delete m_sIncome;				//we don't want to keep anything when we cancel.
				
				Opportunity sOppty = [select id, RecordType.DeveloperName, (select Id from Donations__r where Declined__c = false) from Opportunity where Id = :strOpptyId];
				if((sOppty.Donations__r == null || sOppty.Donations__r.isEmpty()) 
					&& setOpptyRecordTypesforExclude.contains(sOppty.RecordType.DeveloperName) == false)
					//delete the oppty if no other income and not an excluded recordtype
				{
					delete sOppty;
				}
				 
			}
			catch (Exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to delete income record.'));
			}
		}
    }
    
    private void LoadData()
    {
    	try
    	{
    		m_sOpportunity =	[	
    								select	id,
	    									AccountId,
    										Name,
    										Amount,
    										Contact__c,
    										RecordType.DeveloperName
    								from	Opportunity
    								where	Id = :m_strId
    							];

			m_sIncome =	[	
							select	Id,
									Name,
									Amount_A__c,
									Income_Processed_A__c, 
									Payment_Type_A__c,
									Payment_Gateway_Declined_Message__c,
									Payment_Gateway_Receipt__c,
									Payment_Gateway_Response_Code__c,
									Payment_Gateway_Response_Received__c,
									Payment_Gateway_Response_Settlement_Date__c,
									Payment_Gateway_Response_Text__c,
									Payment_Gateway_Settlement_Date__c,
									Payment_Gateway_Transaction_Approved__c,
									Red_Rejection_Message__c,
									Opportunity__c,
									//Contact__c,
									Personal_Donation__c,
									Thank_You_Letter__c,
									Income_Source__c,
									Account_Code__c,
									RecordType.Name
							from	Income__c
							where	Id = :m_strIncomeId
							];
			try
			{
				m_sAccount = [
								select 	id, IPPayment_Customer_Token__c, Credit_Card_Last_4_Digit__c, Credit_Card_Type__c,
										Credit_Card_Expire_Month__c, Credit_Card_Expire_Year__c, IPP_Client_Id__c 	
								from	Account
								where	id = :m_sOpportunity.AccountId];
			}
			catch(Exception e)
			{
				System.debug('##########This is not a contact');
			}
			
			
    	}
    	catch(Exception e)
    	{
    		System.Debug('Exception thrown - ' + e);
			ApexPages.addMessages(e);
    	}
    }
    
	private boolean SendPaymentForNewCard()
	{
		boolean bRet = false;
		CreditCardStub.CustomerNumber			=  m_sAccount.IPP_Client_Id__c;
		CreditCardStub.CreditCardName			= mp_strCreditCardName;
		CreditCardStub.CreditCardNumber			= mp_strCreditCardNumber;
		CreditCardStub.CreditCardExpiryMonth	= Integer.valueof(mp_strCreditCardExpM);
		CreditCardStub.CreditCardExpiryYear		= Integer.valueof(mp_strCreditCardExpY);
		CreditCardStub.CreditCardCVN			= mp_strCreditCardCVC;
		CreditCardStub.Amount 					= '' + (m_sIncome.Amount_A__c * 100).intValue();
		CreditCardStub.CustomerRef				= m_sIncome.Name;

		try
		{
			QProcessIPPPayment sProc = new QProcessIPPPayment(CreditCardStub);
			// just to be sure
			ReturnStub = null;
			ReturnStub = sProc.ProcMain();
		}
		catch(Exception e)
    	{
    		System.Debug('Exception thrown - ' + e);
			ApexPages.addMessages(e);
				
			m_sIncome.Payment_Gateway_Declined_Message__c 	= e.getMessage();
			m_sIncome.Payment_Gateway_Response_Code__c 		= '-1';
			m_sOpportunity.Payment_Gateway_Status__c 		= QConstants.STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_ERROR;
    	}

		if(ReturnStub != null)
		{
			// we got something
			bRet = true;
			
			// lets store the respose for debugging
			m_sIncome.Payment_Gateway_Response__c	= ReturnStub.Response;
			if(m_sIncome.Credit_Card_A__c == null)
			{
				// update by Broheim 10/02/2012
				m_sIncome.Credit_Card_A__c				= CreditCardByReferenceStub.CreditCardType;
			}

			// store some additional information
			if(CreditCardStub.CreditCardNumber.length() > 4)
			{
				//m_sAccount.Credit_Card_Last_4_Digit__c	= CreditCardStub.CreditCardNumber.substring(CreditCardStub.CreditCardNumber.length() - 4, CreditCardStub.CreditCardNumber.length());
				//m_sIncome.Credit_Card_No_A__c = '**** **** **** ' + m_sAccount.Credit_Card_Last_4_Digit__c;
				m_sIncome.Credit_Card_A__c 				= QUtils.GetCreditCardType(CreditCardStub.CreditCardNumber);
				m_sIncome.Credit_Card_No_A__c = '**** **** **** ' + CreditCardStub.CreditCardNumber.substring(CreditCardStub.CreditCardNumber.length() - 4, CreditCardStub.CreditCardNumber.length());
			}
			//m_sAccount.IPPayment_Customer_Token__c	= CreditCardStub.CreditCardToken;
			//m_sAccount.Credit_Card_Expire_Month__c	= mp_strCreditCardExpM;
			//m_sAccount.Credit_Card_Expire_Year__c		= mp_strCreditCardExpY;

			m_strSettlementDate		= ReturnStub.SettlementDateText;
			m_strResponseCode		= ReturnStub.ResponseCode;
			m_strReceipt			= ReturnStub.Receipt;
			m_strDeclinedMessage	= ReturnStub.DeclinedMessage;
			m_strDeclinedCode		= ReturnStub.DeclinedCode;
    	}
    	
    	return bRet;
	}
	
	private boolean SendPaymentForExistingCard()
	{ 
		boolean bRet = false;
		
		CreditCardByReferenceStub.CustomerNumber		= m_sAccount.IPPayment_Customer_Token__c;
		CreditCardByReferenceStub.CustomerReference		= m_sIncome.Name;
		CreditCardByReferenceStub.Amount				= '' + (m_sIncome.Amount_A__c * 100).intValue();
		CreditCardByReferenceStub.CreditCardExpiryMonth = m_sAccount.Credit_Card_Expire_Month__c;
		CreditCardByReferenceStub.CreditCardExpiryYear 	= m_sAccount.Credit_Card_Expire_Year__c;
		
		try
		{
			// just to make sure
			RefReturnStub = null;
			RefReturnStub = QProcessIPPPayment_V2.CalloutIPPaymentsSubmitCreditCardRegisteredPayment(CreditCardByReferenceStub);
			m_sIncome.Credit_Card_A__c = m_sAccount.Credit_Card_Type__c;
		}
		catch(Exception e)
    	{
    		System.Debug('Exception thrown - ' + e);
			ApexPages.addMessages(e);
				
			m_sIncome.Payment_Gateway_Declined_Message__c 	= e.getMessage();
			m_sIncome.Payment_Gateway_Response_Code__c 		= '-1';
			m_sOpportunity.Payment_Gateway_Status__c 		= QConstants.STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_ERROR;
    	}
		
		if(RefReturnStub != null)
		{
			// we got something
			bRet = true;
	
			// lets store the respose for debugging
			m_sIncome.Payment_Gateway_Response__c	= RefReturnStub.Response;
		
			// Move some return fields to strings so we can use common code below :>)
			m_strSettlementDate		= RefReturnStub.SettlementDate;
			m_strResponseCode		= RefReturnStub.ResponseCode;
			m_strReceipt			= RefReturnStub.Receipt;
			m_strDeclinedMessage	= RefReturnStub.DeclinedMessage;
			m_strDeclinedCode		= RefReturnStub.DeclinedCode;
    	}
    	
    	return bRet;
	}
		
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
    private boolean ProcessPayment()
    {
    	// lets get the record type we need to use for the Payment Records
		Schema.DescribeSObjectResult 		schemaDescribeIncome			= Income__c.SObjectType.getDescribe();
		map<string, Schema.RecordTypeInfo> 	mapRecordTypeNameIncome 		= schemaDescribeIncome.getRecordTypeInfosByName();

    	boolean bPaymentOK = false;
    	boolean	bResponseFromBankReceived = false;

		if(mp_bShowNewCardDetailInput /*&& blForTest*/) // Using new CC details entered on the screen
		{
				System.Debug('go to validate');
			
			// check what we got sent - just return if its garbage
			if(!ValidateInput())
			{
				// We never want to update the stage of certain record types (corporate)
				
				
				// update the opportunity status to Closed Lost as we'll be marking the income declined.
				if(setOpptyRecordTypesforExclude.contains(m_sOpportunity.RecordType.DeveloperName) == false)
				{
					m_sOpportunity.StageName = 'Closed Lost';
				}
				
				try
				{
					update m_sOpportunity;
				}
				catch (Exception e)
				{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error updating the opportunity: ' + e.getMessage()));
				}

				return false;
			}
			bResponseFromBankReceived = SendPaymentForNewCard();
		}
		else // Make payment using stored IP token
		{
			bResponseFromBankReceived = SendPaymentForExistingCard();
		}

		// so lets do the update if we got something back
		if(bResponseFromBankReceived)
		{
			id idRecordType = null;
			
			try
			{
				idRecordType = [select	Id
				
								from	RecordType 
								where	SobjectType = 'Income__c'
								and		DeveloperName = 'Income_One_Off' limit 1].Id;
			}
			catch(Exception e)
			{
				System.Debug('Exception: ' + e.getMessage());
			}
			
			if(idRecordType != null)
			{				
				m_sIncome.RecordTypeId = idRecordType;
			}
			
			m_sIncome.Process_Payment_Through_Payment_Gateway__c		= true;
			m_sIncome.Payment_Gateway_Response_Settlement_Date__c		= m_strSettlementDate;
			m_sIncome.Payment_Gateway_Settlement_Date__c				= QUtils.ConvertSettlementDate(m_strSettlementDate);
			m_sIncome.Payment_Gateway_Response_Code__c					= m_strResponseCode;
			m_sIncome.Payment_Gateway_Response_Received__c				= true;

			if(m_strResponseCode == QConstants.STR_IPPAYMENTS_TRANSACTION_CODE_APPROVED && blResultFromBankForTest)
			{
				// everything looks good
				bPaymentOK = true;
				
				m_sIncome.Income_Processed_A__c						= true;
				m_sIncome.Income_Processed_Date_A__c				= system.now();
				m_sIncome.Payment_Gateway_Receipt__c				= m_strReceipt;
				m_sIncome.Payment_Gateway_Transaction_Approved__c 	= true;
				m_sIncome.Reconciled_A__c							= true;
				m_sIncome.Payment_Gateway_Response_Text__c			= QConstants.STR_IPPAYMENTS_TRANSACTION_TEXT_APPROVED;
				m_sIncome.Payment_Gateway_Declined_Message__c		= ' ';
				m_sIncome.Payment_Gateway_Declined_Code__c			= ' ';
				m_sIncome.Declined__c								= false;
				m_sIncome.Declined_Date__c							= null;
				m_sIncome.Declined_Amount__c						= null;				
				
				// set the amount in case it's been set back to zero by a declined income....
				m_sOpportunity.Amount								= m_sIncome.Amount_A__c;
				if(CloseOpportunity)
				{
					//here is where it should be processed
					m_sOpportunity.StageName						= 'Closed Won';
					m_sOpportunity.CloseDate						= system.today();
				}
				m_sOpportunity.Payment_Gateway_Status__c 			= QConstants.STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_APPROVED;
			}
			else
			{
				m_sDeclinedIncome = m_sIncome;
				m_sIncome = m_sDeclinedIncome.clone(false, true);

				m_sIncome.Income_Processed_A__c							= false;
				m_sDeclinedIncome.Payment_Gateway_Response_Text__c		= QConstants.STR_IPPAYMENTS_TRANSACTION_TEXT_DECLINED;
				m_sDeclinedIncome.Payment_Gateway_Declined_Message__c	= m_strDeclinedMessage;
				m_sDeclinedIncome.Payment_Gateway_Declined_Code__c		= m_strDeclinedCode;
				m_sDeclinedIncome.Declined__c							= true;
				m_sDeclinedIncome.Declined_Amount__c					= m_sDeclinedIncome.Amount_A__c;
				m_sDeclinedIncome.Amount_A__c							= 0;
				if(setOpptyRecordTypesforExclude.contains(m_sOpportunity.RecordType.DeveloperName) == false)
				{
					m_sOpportunity.StageName								= 'Closed Lost';
				}
				m_sOpportunity.Payment_Gateway_Status__c	 			= QConstants.STR_OPPORTUNITY_PAYMENT_GATEWAY_STAGE_DECLINED;
					
				mp_strReturnErrorString = m_strResponseCode + ' - ' + m_strDeclinedMessage;				
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Response from Gateway: ' + mp_strReturnErrorString));
			}
		}
			
		try
		{
			if(!bPaymentOK)
			{
				upsert m_sDeclinedIncome;
				insert m_sIncome;
			}
			else
			{
				update m_sIncome;
			}
			
	    	update m_sOpportunity;
	    	update m_sAccount;
		}
		catch(Exception e)
    	{
    		System.Debug('Error updating Transaction records - ' + e);
			ApexPages.addMessages(e);
    	}
	
		return bPaymentOK;
    }
    
    private boolean ValidateInput()
    {
    	boolean bRet = true;
    	string strErrorMessage = '';
    	
    	// Validate input
		if(mp_strCreditCardName == null || mp_strCreditCardName.length() == 0)
		{
			strErrorMessage += 'Please enter a Credit Card Name. ';
			bRet = false;
		}

		if(mp_strCreditCardNumber == null)
		{
			strErrorMessage += 'Please enter a valid Credit Card Number. ';
			bRet = false;
		}
		else
		{
			mp_strCreditCardNumber = mp_strCreditCardNumber.replace(' ', '');
			mp_strCreditCardNumber = mp_strCreditCardNumber.replace('-', '');
	
			if(QUtils.GetCreditCardType(mp_strCreditCardNumber) == '')
			{
				strErrorMessage += 'An invalid Credit Card Number has been specified. ';
				bRet = false;
			}
		}
		
		if(!bRet)
		{
			m_sIncome.Payment_Gateway_Response_Text__c		= '';
			m_sIncome.Payment_Gateway_Declined_Message__c	= 'Rejected by Red';
			m_sIncome.Payment_Gateway_Declined_Code__c		= '';
			m_sIncome.Status__c								= QConstants.STR_RED_PAYMENT_DETAILS_DECLIEND;
			m_sIncome.Red_Rejection_Message__c				= strErrorMessage.length() > 255 ? strErrorMessage.subString(0, 255) : strErrorMessage;
			m_sIncome.Declined__c							= true;
			
			m_sOpportunity.Payment_Gateway_Status__c 		= QConstants.STR_RED_PAYMENT_DETAILS_DECLIEND;
			
			try
			{				
				m_sDeclinedIncome = m_sIncome;
				m_sIncome = m_sDeclinedIncome.clone(false, true);
				
				insert m_sIncome;
				update m_sDeclinedIncome;
				update m_sOpportunity;
				
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, strErrorMessage));
			}
			catch (Exception e)
			{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was a problem updating the Income Record: ' + e.getMessage()));
			}
		}

/*		
		if(mp_strCreditCardCVC == null || mp_strCreditCardCVC.length() == 0)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a Credit Card CVC number'));
			bRet = false;
		}
*/		
		return bRet;
    }
    
/*	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//		
	private void SendInvoice(Income__c theTrans, OpportunityContactRole theOCR)
	{
		// send login details e-mail
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		mail.setTargetObjectId(theOCR.ContactId);
		mail.setWhatId(theTrans.Id);
		mail.setReplyTo('stockval_support@clime.com.au');
		mail.setSenderDisplayName('StockVal Support');
		mail.setBccSender(false);
		mail.setUseSignature(false);
		
		mail.setTemplateId('00X200000016MCG');
		
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
*/    
	/***********************************************************************************************************
		Test Methods
	***********************************************************************************************************/
	public static testmethod void testListMethodsOnly()
	{

		PageReference pageRef = new PageReference('/apex/SubmitSinglePayment');
		Test.setCurrentPage(pageRef);

		Contact sContact1								= new Contact();
		Contact sContact2								= new Contact();
		Account sAccount								= new Account();
		Opportunity sOpportunity1						= new Opportunity();
		Opportunity sOpportunity2						= new Opportunity();
		
		Income__c sIncome1			= new Income__c();
		Income__c sIncome2			= new Income__c();

		// 1 - Create new Account
		sAccount.Name 			= 'Test Method Account';
		insert sAccount;
			
		// 2 - Create new Contacts
		sContact1.FirstName 			= 'Timmy';
		sContact1.LastName 				= 'TestMethod';
		sContact1.AccountId				= sAccount.Id;
		sContact1.email 				= 'test@quattroinnovation.com';
		insert sContact1;			

		sContact2.FirstName 			= 'Tabitha';
		sContact2.LastName 				= 'TestMethod';
		sContact2.AccountId				= sAccount.Id;
		sContact2.email 				= 'test@quattroinnovation.com';
		insert sContact2;		

		// 3 - Create Our Opportunities
		sOpportunity1.Name 							= 'Test Method Opty 2';
		sOpportunity1.StageName                     ='Club Red Ongoing';
		sOpportunity1.Contact__c = sContact1.Id;
		sOpportunity1.CloseDate						= System.Today().addDays(30);
		sOpportunity1.AccountId						= sAccount.Id;
		sOpportunity1.Amount						= 123.45;
		insert sOpportunity1;

		sOpportunity2.Name 							= 'Test Method Opty 3';
		sOpportunity2.StageName                     ='Club Red Ongoing';
	    sOpportunity2.Contact__c = sContact2.Id;
		sOpportunity2.CloseDate						= System.Today().addDays(30);
		sOpportunity2.AccountId						= sAccount.Id;
		sOpportunity2.Amount						= 123.45;
		insert sOpportunity2;

		// Create payment Incomes
		sIncome1.Amount_A__c			= 456.78;
		//sIncome1.Contact__c      = sContact1.Id;
		sIncome1.Opportunity__c	= sOpportunity1.Id;
		insert sIncome1;

		sIncome2.Amount_A__c			= 453.73;
		//sIncome2.Contact__c      = sContact2.Id;
		sIncome2.Opportunity__c	= sOpportunity2.Id;
		insert sIncome2;

		// Now valid parameters
		ApexPages.currentPage().getParameters().put('Id', sOpportunity1.Id);
		ApexPages.currentPage().getParameters().put('IncomeId', sIncome1.Id);
		ApexPages.currentPage().getParameters().put('bCloseOppty','false');
		QVFSubmitSinglePayment controller2 = new QVFSubmitSinglePayment();
        controller2.init();
    	controller2.SendPayment();

		// Try and run it again - should error this time
		ApexPages.currentPage().getParameters().put('Id', sOpportunity2.Id);
		ApexPages.currentPage().getParameters().put('IncomeId', sIncome2.Id);
		ApexPages.currentPage().getParameters().put('bCloseOppty','false');
		QVFSubmitSinglePayment controller2b = new QVFSubmitSinglePayment();
        controller2b.init();
        blResultFromBankForTest = false;
    	controller2b.SendPayment();
    	
    	sOpportunity2								= new Opportunity();
    	sOpportunity2.Name 							= 'Test Method Opty 3';
		sOpportunity2.StageName                     = 'Club Red Ongoing';
	    sOpportunity2.Contact__c 					= sContact2.Id;
		sOpportunity2.CloseDate						= System.Today().addDays(30);
		sOpportunity2.AccountId						= sAccount.Id;
		sOpportunity2.Amount						= 123.45;
		insert sOpportunity2;
		
		sIncome2						= new Income__c();
		sIncome2.Amount_A__c			= 453.73;
		//sIncome2.Contact__c      		= sContact2.Id;
		sIncome2.Opportunity__c			= sOpportunity2.Id;
		insert sIncome2;
    	
    	// Now for a contact that does not have a payment token stored
		ApexPages.currentPage().getParameters().put('Id', sOpportunity2.Id);
		ApexPages.currentPage().getParameters().put('IncomeId', sIncome2.Id);
		ApexPages.currentPage().getParameters().put('bCloseOppty','false');
		QVFSubmitSinglePayment controller3 = new QVFSubmitSinglePayment();
		controller3.init();

    	controller3.CreditCardStub.CreditCardName			= 'Quattro Test';
    	controller3.CreditCardStub.CreditCardNumber			= '4242424242424242';
    	controller3.CreditCardStub.CreditCardCVN			= '123';
    	controller3.CreditCardStub.CreditCardExpiryMonth	= 12;
    	controller3.CreditCardStub.CreditCardExpiryYear		= 2015;

    	
    	controller3.SendPayment();

		// Misc remaining methods
    	controller3.Cancel();
    	controller3.NewDetails();
    	controller3.GetExpYear();
    	controller3.GetExpMonth();
		controller3.Back();

	}
	
	public static testmethod void testQVFSubmitSinglePaymentUseNewCard()
	{
		blForTest = true;
		try{
			QVFSubmitSinglePayment controller3 = new QVFSubmitSinglePayment();
		}catch(Exception e){}
		PageReference pageRef = new PageReference('/apex/SubmitSinglePayment');
		Test.setCurrentPage(pageRef);

		boolean bHasErrored = false;

		Contact sContact2								= new Contact();
		Account sAccount								= new Account();
		Opportunity sOpportunity2						= new Opportunity();

		Income__c sIncome2			= new Income__c();

		// 1 - Create new Account
		sAccount.Name 			= 'Test Method Account';
		insert sAccount;
					

		sContact2.FirstName 			= 'Tabitha';
		sContact2.LastName 				= 'TestMethod';
		sContact2.AccountId				= sAccount.Id;
		sContact2.email 				= 'test@quattroinnovation.com';
		insert sContact2;		


		sOpportunity2.Name 							= 'Test Method Opty 3';
		sOpportunity2.StageName                     ='Club Red Ongoing';
	    sOpportunity2.Contact__c = sContact2.Id;
		sOpportunity2.CloseDate						= System.Today().addDays(30);
		sOpportunity2.AccountId						= sAccount.Id;
		sOpportunity2.Amount						= 123.45;
		insert sOpportunity2;



		sIncome2.Amount_A__c			= 453.73;
		//sIncome2.Contact__c      = sContact2.Id;
		sIncome2.Opportunity__c	= sOpportunity2.Id;
		insert sIncome2;


		// Now for a contact that does not have a payment token stored
		ApexPages.currentPage().getParameters().put('Id', sOpportunity2.Id);
		ApexPages.currentPage().getParameters().put('IncomeId', sIncome2.Id);
		ApexPages.currentPage().getParameters().put('bCloseOppty','true');
		QVFSubmitSinglePayment controller3 = new QVFSubmitSinglePayment();
		controller3.init();

    	controller3.mp_strCreditCardName = 'Test credit card holder';
    	controller3.mp_strCreditCardNumber = '4242424242424242';
    	controller3.mp_strCreditCardCVC = '123';
    	controller3.mp_strCreditCardExpM = '12';
    	controller3.mp_strCreditCardExpY = '2015';
    	
    	controller3.SendPayment();
    	
    	// Misc remaining methods
    	controller3.Cancel();
    	controller3.NewDetails();
    	controller3.GetExpYear();
    	controller3.GetExpMonth();
	}
	public static testmethod void testQVFSubmitSinglePaymentUseExistingCard()
	{
		blForTest = true;
		try{
			QVFSubmitSinglePayment controller3 = new QVFSubmitSinglePayment();
		}catch(Exception e){}
		PageReference pageRef = new PageReference('/apex/SubmitSinglePayment');
		Test.setCurrentPage(pageRef);

		boolean bHasErrored = false;

	
		Account sAccount 	= new Account();
		sAccount.Name 		= 'Test Account';
		insert sAccount;
		
		Contact sContact2 		= new Contact();
		sContact2.FirstName 	= 'Test';
		sContact2.LastName 		= 'Contact';
		sContact2.Email 		= 'testing@test.com';
		sContact2.AccountId 	= sAccount.Id;
		insert sContact2;
		
		Opportunity sOpportunity2	= new Opportunity();
		Income__c sIncome2			= new Income__c();

		sOpportunity2.Name 			= 'Test Method Opty 3';
		sOpportunity2.StageName     ='Club Red Ongoing';
	    sOpportunity2.Contact__c 	= sContact2.Id;
		sOpportunity2.CloseDate		= System.Today().addDays(30);
		sOpportunity2.AccountId		= sAccount.Id;
		sOpportunity2.Amount		= 123.45;
		insert sOpportunity2;

		sIncome2.Amount_A__c	= 453.73;
		//sIncome2.Contact__c     = sContact2.Id;
		sIncome2.Opportunity__c	= sOpportunity2.Id;
		insert sIncome2;


		// Now for a contact that does not have a payment token stored
		ApexPages.currentPage().getParameters().put('Id', sOpportunity2.Id);
		ApexPages.currentPage().getParameters().put('IncomeId', sIncome2.Id);
		ApexPages.currentPage().getParameters().put('bCloseOppty','false');
		QVFSubmitSinglePayment controller3 = new QVFSubmitSinglePayment();
		controller3.init();

    	controller3.mp_strCreditCardName = 'Test credit card holder';
    	controller3.mp_strCreditCardNumber = '4242424242424242';
    	controller3.mp_strCreditCardCVC = '123';
    	controller3.mp_strCreditCardExpM = '12';
    	controller3.mp_strCreditCardExpY = '2015';
    	
    	controller3.SendPayment();
	}
	
	public static testmethod void lastTest()
	{
		blForTest = true;
		try{
			QVFSubmitSinglePayment controller3 = new QVFSubmitSinglePayment();
		}catch(Exception e){}
		PageReference pageRef = new PageReference('/apex/SubmitSinglePayment');
		Test.setCurrentPage(pageRef);

		boolean bHasErrored = false;

	
		Account sAccount 	= new Account();
		sAccount.Name 		= 'Test Account';
		insert sAccount;
		
		Contact sContact2 		= new Contact();
		sContact2.FirstName 	= 'Test';
		sContact2.LastName 		= 'Contact';
		sContact2.Email 		= 'testing@test.com';
		sContact2.AccountId 	= sAccount.Id;
		insert sContact2;
		
		Opportunity sOpportunity2	= new Opportunity();
		Income__c sIncome2			= new Income__c();

		sOpportunity2.Name 			= 'Test Method Opty 3';
		sOpportunity2.StageName     ='Club Red Ongoing';
	    sOpportunity2.Contact__c 	= sContact2.Id;
		sOpportunity2.CloseDate		= System.Today().addDays(30);
		sOpportunity2.AccountId		= sAccount.Id;
		sOpportunity2.Amount		= 123.45;
		insert sOpportunity2;

		sIncome2.Amount_A__c	= 453.73;
		//sIncome2.Contact__c     = sContact2.Id;
		sIncome2.Opportunity__c	= sOpportunity2.Id;
		insert sIncome2;
		
		try
		{
			QVFSubmitSinglePayment controller3b = new QVFSubmitSinglePayment();
		}catch(Exception e ){}
		
		ApexPages.currentPage().getParameters().put('Id', sOpportunity2.Id);
		try
		{
			QVFSubmitSinglePayment controller3b = new QVFSubmitSinglePayment();
		}catch(Exception e ){}
		
		ApexPages.currentPage().getParameters().put('Id', sOpportunity2.Id);
		ApexPages.currentPage().getParameters().put('IncomeId', sIncome2.Id);
		ApexPages.currentPage().getParameters().put('bCloseOppty','false');
		QVFSubmitSinglePayment controller3c = new QVFSubmitSinglePayment();
		controller3c.init();
		controller3c.Back();
		
		// Now for a contact that does not have a payment token stored
		ApexPages.currentPage().getParameters().put('Id', 'a09345678ghhh');
		ApexPages.currentPage().getParameters().put('IncomeId', sIncome2.Id);
		ApexPages.currentPage().getParameters().put('bCloseOppty','false');
		QVFSubmitSinglePayment controller3d = new QVFSubmitSinglePayment();
		try
		{
			controller3d.init();
		}catch(Exception e ){}
	}
}