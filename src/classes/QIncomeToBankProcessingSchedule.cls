global with sharing class QIncomeToBankProcessingSchedule implements Schedulable 
{
	global void execute(SchedulableContext SC) 
	{
		Database.executeBatch(new QIncomeToBankProcessing(), 1);
	}
	
	global static testMethod void testQIncomeToBankProcessingSchedule() 
	{
		Test.startTest(); 
		
		System.Schedule('QIncomeToBankProcessingSchedule', '20 30 8 10 2 ?', new QIncomeToBankProcessingSchedule());

		Test.stopTest();
    }
}