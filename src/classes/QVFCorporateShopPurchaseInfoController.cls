public class QVFCorporateShopPurchaseInfoController
{
	public	id		m_idOpportunity	{get; set;}
	public	double	m_dTotal		{get; set;}
	public	double	m_dShipping		{get; set;}
	public	double	m_dGST			{get; set;}
	
	private	list<OpportunityLineItem>	liLineItems;
	
	public QVFCorporateShopPurchaseInfoController()
	{
		m_idOpportunity = null;	
		System.Debug('\n\nQVFCorporateShopPurchaseInfoController constructor');
	}
	
	public list<OpportunityLineItem> getLineItems()
	{
		System.Debug('\n\nGetLineItems()');
		System.Debug(m_idOpportunity);
		
		if(m_idOpportunity == null)
		{
			return null;
		}
		
		try
		{
			liLineItems = [select	id, quantity, unitprice, PricebookEntry.product2.name, PricebookEntryId,
									PricebookEntry.product2.GST_Applicable__c
							from	OpportunityLineItem
							where	Opportunityid = :m_idOpportunity];
		}
		catch(QueryException qe)
		{
			System.Debug('QueryException thrown while retrieving line items related to purchase: ' + qe);
		}
		catch(Exception e)
		{
			System.Debug('Exception thrown while retrieving line items related to purchase: ' + e);
		}
		
		System.debug(liLineItems);
		
		// put GST and shipping at the end - shipping comes first
		for(integer i = 0; i < liLineItems.size(); i++)
		{
			if(liLineItems[i].PricebookEntryId == QCacheWebsiteMetadata.getDefaultPricebookEntryForShipping()
				|| liLineItems[i].PricebookEntryId == QCacheWebsiteMetadata.getDefaultPricebookEntryForExpressShipping())
			{
				m_dShipping = liLineItems.remove(i).UnitPrice;
				break;	
			}
		}
		
		for(integer i = 0; i < liLineItems.size(); i++)
		{
			if(liLineItems[i].PricebookEntryId == QCacheWebsiteMetadata.getDefaultPricebookEntryForGST())
			{
				m_dGST = liLineItems.remove(i).UnitPrice;
				break;	
			}
		}
		
		// sanity checks...
		if(m_dShipping == null)
		{
			m_dShipping = 0;
		}
		
		if(m_dGST == null)
		{
			m_dGST = 0;
		}
		
		m_dTotal = m_dShipping + m_dGST;
		for(integer i = 0; i < liLineItems.size(); i++)
		{
			m_dTotal += (liLineItems[i].UnitPrice * liLineItems[i].Quantity);
		}		
		
		return liLineItems;
	}
	
	public static testmethod void TestQVFCorporateShopPurchaseInfoController()
	{
		QVFCorporateShopPurchaseInfoController c = new QVFCorporateShopPurchaseInfoController();
		
		System.Assert(c.getLineItems() == null);
		
		// any old id
		c.m_idOpportunity = [select id from opportunity limit 1].Id;
		c.getLineItems();
	}
}