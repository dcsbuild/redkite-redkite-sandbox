public with sharing class QProcessIPPPayment
{
	/***************************************************************************************
		Exception Classes
	***************************************************************************************/
	public class QPaymentProcessingException 			extends Exception{}
	public class QPaymentProcessingParameterException 	extends Exception{}
	
	public enum PaymentProcess {SINGLE_PAYMENT, SINGLE_PAYMENT_REGESTERED, SINGLE_PAYMENT_WITH_REGISTER, SINGLE_REFUND}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information for Processing a Credit Card
	public class QProcessIPPPaymentArgs
	{
		public PaymentProcess	PaymentProcess{get; set;}
		
		// common info
		public string 	Amount					{get; set;}		
		public boolean 	AllowCallout			{get; set;}
		
		// single payment info
		public string	CustomerNumber				{get; set;}
		public string	CustomerRef					{get; set;}

		public string 	CreditCardNumber			{get; set;}
		public string 	CreditCardName				{get; set;}
		public integer 	CreditCardExpiryYear		{get; set;}
		public integer 	CreditCardExpiryMonth		{get; set;}
		public string 	CreditCardCVN				{get; set;}
		public string	CreditCardType				{get; set;}
		
		// single refund info
		public string	Receipt					{get; set;}
		
		public QProcessIPPPaymentArgs()
		{
			this.CreditCardNumber 		= '';
			this.CreditCardName 		= '';
			this.CreditCardExpiryYear 	= 0;
			this.CreditCardExpiryMonth 	= 0;
			this.CreditCardCVN 			= '0';
			this.CreditCardType			= '';
			this.Amount 				= '';
			this.Receipt				= '';
		}
	}	
	
	////////////////////////////////////////////////////////////////////////////////////////
	// Information returned when Processing a Credit Card
	public class QProcessIPPPaymentRet
	{
		// error handling
		public boolean InError			{get; set;}
		public List<string>	ErrorList	{get; set;}
		
		// actual data
		public string	ResponseCode		{get; set;}
		public string	Timestamp			{get; set;}
		public string 	Receipt				{get; set;}
		public string 	SettlementDateText	{get; set;}
		public Date 	SettlementDate		{get; set;}
		public string 	DeclinedCode		{get; set;}
		public string 	DeclinedMessage		{get; set;}
		public string 	Request				{get; set;}
		public string 	Response			{get; set;}
		
		public QProcessIPPPaymentRet()
		{
			this.InError 	= false;
			this.ErrorList 	= new List<string>();
			
			this.ResponseCode 		= '';
			this.Timestamp 			= '';
			this.Receipt 			= '';
			this.SettlementDateText	= '';
			this.DeclinedCode 		= '';
			this.DeclinedMessage 	= '';
			this.Request			= '';
			this.Response			= '';
		}
	}
	
	/***************************************************************************************
		Members
	***************************************************************************************/
	string m_strEndPoint, m_strUsername, m_strPassword;
	
	string mCreditCardExpiryYearText, mCreditCardExpiryMonthText;
	
	QProcessIPPPaymentArgs	mArgs;
	QProcessIPPPaymentRet	mRet;
	
	/***************************************************************************************
		Constructor
	***************************************************************************************/
	public QProcessIPPPayment(QProcessIPPPaymentArgs oArgs)
	{
		this.mArgs = oArgs;
		mRet = new QProcessIPPPaymentRet();
	}
	
	/***********************************************************************************************************
		Access Methods
	***********************************************************************************************************/
	public QProcessIPPPaymentRet ProcMain()
	{
		try
		{
			System.Debug(mArgs);
			// load everything we need
			Init();
			
			if(mArgs.PaymentProcess == PaymentProcess.SINGLE_PAYMENT)
			{
				CalloutIPPaymentsSubmitSingleCreditCardPayment();
			}
			else if(mArgs.PaymentProcess == PaymentProcess.SINGLE_PAYMENT_REGESTERED)
			{
				CalloutIPPaymentsSubmitSinglePreRegesterdCreditCardPayment();
			}
			else if(mArgs.PaymentProcess == PaymentProcess.SINGLE_PAYMENT_WITH_REGISTER)
			{
				CalloutIPPaymentsSubmitSingleCreditCardPaymentWithRegister();
			}
			else if(mArgs.PaymentProcess == PaymentProcess.SINGLE_REFUND)
			{
				CalloutIPPaymentsSubmitSingleRefundPayment();
			}
			else
			{
				throw new QPaymentProcessingException('Unknown Payment Processing Option - ' + mArgs.PaymentProcess);
			}
		}
		catch(Exception e)
		{
			System.Debug('Exception - ' + e);
			mRet.InError = true;
			mRet.ErrorList.add(e.getMessage());
		}
		
		return mRet;
	}
	
	/***************************************************************************************
		Payment Processing Logic
	***************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////
	// Config Loading
	private void Init()
	{		
		m_strEndPoint = QCachePaymentGatewayMetadata.getDTSServiceLocation();
		// now lets set up the text representation of the exp dates
		mCreditCardExpiryYearText 	= mArgs.CreditCardExpiryYear.format().replace(',','');
			
		if(mArgs.CreditCardExpiryMonth < 10)
			mCreditCardExpiryMonthText = '0' + mArgs.CreditCardExpiryMonth.format().replace(',','');
		else
			mCreditCardExpiryMonthText = mArgs.CreditCardExpiryMonth.format().replace(',','');
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	private void CalloutIPPaymentsSubmitSinglePreRegesterdCreditCardPayment()
	{
		mRet.Request = 		'<Transaction>'+
								'<CustNumber>' 	+ mArgs.CustomerNumber	+ '</CustNumber>'+
								'<CustRef>'	 	+ mArgs.CustomerRef 	+ '</CustRef>'+
								'<Amount>' 		+ mArgs.Amount 			+ '</Amount>'+
								'<TrnType>'		+ '1' 									+ '</TrnType>'+
								'<CreditCard Registered="True">'+
								'</CreditCard>'+
								'<Security>'+
									'<UserName>' + QCachePaymentGatewayMetadata.getUsername()  	+ '</UserName>'+
									'<Password>' + QCachePaymentGatewayMetadata.getPassword()	+ '</Password>'+
								'</Security>'+
								'<UserDefined></UserDefined>'+
							'</Transaction>';
					
		if(mArgs.AllowCallout)
		{
			IPPaymentsDTS.dtsSoap IPSOAPMessage = new IPPaymentsDTS.dtsSoap(m_strEndPoint);
			IPSOAPMessage.timeout_x = 60000;
			mRet.Response = IPSOAPMessage.SubmitSinglePayment(mRet.Request);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			mRet.Response = '<Response><ResponseCode>0</ResponseCode><Timestamp>2009-02-12 19:28:05</Timestamp><Receipt>100001337</Receipt><SettlementDate>12-feb-2009</SettlementDate><DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>';
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	private void CalloutIPPaymentsSubmitSingleCreditCardPayment()
	{
		string strRequest 	= 	'<Transaction>'+
									'<CustNumber>' 	+ mArgs.CustomerNumber	+ '</CustNumber>'+
									'<CustRef>'	 	+ mArgs.CustomerRef 	+ '</CustRef>'+
									'<Amount>' 		+ mArgs.Amount			+ '</Amount>'+
									'<TrnType>'		+ '1' 					+ '</TrnType>'+
									'<CreditCard Registered="False">'+
										'<CardNumber>'		+mArgs.CreditCardNumber							+ '</CardNumber>'+
										'<ExpM>'			+mCreditCardExpiryMonthText						+ '</ExpM>'+
										'<ExpY>'			+mCreditCardExpiryYearText						+ '</ExpY>'+
										'<CVN>'				+mArgs.CreditCardCVN							+ '</CVN>'+
										'<CardHolderName>'	+mArgs.CreditCardName							+ '</CardHolderName>'+
									'</CreditCard>'+
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername()  	+ '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword()	+ '</Password>'+
									'</Security>'+
									'<UserDefined></UserDefined>'+
								'</Transaction>';
		
		// one that we return to what called us, no confidential info in it
		mRet.Request = 			'<Transaction>'+
									'<CustNumber>' 	+ mArgs.CustomerNumber	+ '</CustNumber>'+
									'<CustRef>'	 	+ mArgs.CustomerRef 	+ '</CustRef>'+
									'<Amount>' 		+ mArgs.Amount			+ '</Amount>'+
									'<TrnType>'		+ '1' 					+ '</TrnType>'+
									'<CreditCard Registered="False">'+
										'<CardNumber>****-****-****-****</CardNumber>'+
										'<ExpM>'			+mCreditCardExpiryMonthText		+ '</ExpM>'+
										'<ExpY>'			+mCreditCardExpiryYearText		+ '</ExpY>'+
										'<CVN>***</CVN>'+
										'<CardHolderName>'	+mArgs.CreditCardName			+ '</CardHolderName>'+
									'</CreditCard>'+
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername()  	+ '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword()	+ '</Password>'+
									'</Security>'+
									'<UserDefined></UserDefined>'+
								'</Transaction>';
		System.Debug(strRequest);
		System.Debug(mRet.Request);
		
		if(mArgs.AllowCallout)
		{
			IPPaymentsDTS.dtsSoap IPSOAPMessage = new IPPaymentsDTS.dtsSoap(m_strEndPoint);
			IPSOAPMessage.timeout_x = 60000;
			mRet.Response = IPSOAPMessage.SubmitSinglePayment(strRequest);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			mRet.Response = '<Response><ResponseCode>0</ResponseCode><Timestamp>2009-02-12 19:28:05</Timestamp><Receipt>100001337</Receipt><SettlementDate>12-feb-2009</SettlementDate><DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>';
		}
		
		System.Debug(mRet.Response);
		
		XMLDom objXMLDom = new XMLDom(mRet.Response);
		
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			System.Debug(element);
			
			if(element.nodeName == 'ResponseCode')
			{
				mRet.ResponseCode = element.nodeValue;
			}
			else if(element.nodeName == 'Timestamp')
			{
				mRet.Timestamp = element.nodeValue;
			}
			else if(element.nodeName == 'Receipt')
			{
				mRet.Receipt = element.nodeValue;
			}
			else if(element.nodeName == 'SettlementDate')
			{
				mRet.SettlementDateText = element.nodeValue;
				mRet.SettlementDate	 	= ConvertSettlementDate(element.nodeValue);
			}
			else if(element.nodeName == 'DeclinedCode')
			{
				mRet.DeclinedCode = element.nodeValue;
			}
			else if(element.nodeName == 'DeclinedMessage')
			{
				mRet.DeclinedMessage = element.nodeValue;
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	private void CalloutIPPaymentsSubmitSingleCreditCardPaymentWithRegister()
	{
		string strRequest 	= 	'<Transaction>'+
									'<CustNumber>' 	+ mArgs.CustomerNumber	+ '</CustNumber>'+
									'<CustRef>'	 	+ mArgs.CustomerRef 	+ '</CustRef>'+
									'<Amount>' 		+ mArgs.Amount			+ '</Amount>'+
									'<TrnType>'		+ '1' 					+ '</TrnType>'+
									'<CreditCard Registered="True">'+
									'</CreditCard>'+
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername()  	+ '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword()	+ '</Password>'+
									'</Security>'+
									'<UserDefined></UserDefined>'+
									'<Register>'+
										'<Customer>'+
											'<CustNumber>'+ mArgs.CustomerNumber +'</CustNumber>'+
											'<Contact>'+
										/*
												'<FirstName>a</FirstName>'+
												'<LastName>a</LastName>'+
												'<PrimaryCode>a</PrimaryCode>'+
												'<PrimaryPhone>a</PrimaryPhone>'+
												'<SecondaryCode>a</SecondaryCode>'+
												'<SecondaryPhone>a</SecondaryPhone>'+
												'<CustPassword>a</CustPassword>'+
										*/
											'</Contact>'+
											'<Address>'+
										/*
												'<AddressL1>a</AddressL1>'+
												'<AddressL2>a</AddressL2>'+
												'<Suburb>a</Suburb>'+
												'<State>a</State>'+
												'<PostCode>a</PostCode>'+
												'<Country>a</Country>'+
										*/
											'</Address>'+ 
											'<CreditCard>'+
												'<CardNumber>'		+mArgs.CreditCardNumber							+ '</CardNumber>'+
												'<ExpM>'			+mCreditCardExpiryMonthText						+ '</ExpM>'+
												'<ExpY>'			+mCreditCardExpiryYearText						+ '</ExpY>'+
												'<CVN>'				+mArgs.CreditCardCVN							+ '</CVN>'+
												'<CardHolderName>'	+mArgs.CreditCardName							+ '</CardHolderName>'+
											'</CreditCard>'+
											'<CustUserDefined></CustUserDefined >'+ 
										'</Customer>'+
									'</Register>'+
								'</Transaction>';
		
		// one that we return to what called us, no confidential info in it
		mRet.Request = 			'NOT BUILT YET!';
		
		if(mArgs.AllowCallout)
		{
			IPPaymentsDTS.dtsSoap IPSOAPMessage = new IPPaymentsDTS.dtsSoap(m_strEndPoint);
			IPSOAPMessage.timeout_x = 60000;
			mRet.Response = IPSOAPMessage.SubmitSinglePayment(strRequest);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			mRet.Response = '<Response><ResponseCode>0</ResponseCode><Timestamp>2009-02-12 19:28:05</Timestamp><Receipt>100001337</Receipt><SettlementDate>12-feb-2009</SettlementDate><DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>';
		}
		
		XMLDom objXMLDom = new XMLDom(mRet.Response);
		
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			System.Debug(element);
			
			if(element.nodeName == 'ResponseCode')
			{
				mRet.ResponseCode = element.nodeValue;
			}
			else if(element.nodeName == 'Timestamp')
			{
				mRet.Timestamp = element.nodeValue;
			}
			else if(element.nodeName == 'Receipt')
			{
				mRet.Receipt = element.nodeValue;
			}
			else if(element.nodeName == 'SettlementDate')
			{
				mRet.SettlementDateText = element.nodeValue;
				mRet.SettlementDate	 	= ConvertSettlementDate(element.nodeValue);
			}
			else if(element.nodeName == 'DeclinedCode')
			{
				mRet.DeclinedCode = element.nodeValue;
			}
			else if(element.nodeName == 'DeclinedMessage')
			{
				mRet.DeclinedMessage = element.nodeValue;
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	// 
	private void CalloutIPPaymentsSubmitSingleRefundPayment()
	{
		string strRequest = '<Refund>'+
									'<Receipt>' + mArgs.Receipt	+ '</Receipt>'+
									'<Amount>'	+ mArgs.Amount	+ '</Amount>'+
									'<Security>'+
										'<UserName>' + QCachePaymentGatewayMetadata.getUsername() + '</UserName>'+
										'<Password>' + QCachePaymentGatewayMetadata.getPassword() + '</Password>'+
									'</Security>'+
								'</Refund>';
		
		if(mArgs.AllowCallout)
		{
			IPPaymentsDTS.dtsSoap IPSOAPMessage = new IPPaymentsDTS.dtsSoap(m_strEndPoint);
			IPSOAPMessage.timeout_x = 60000;
			mRet.Response = IPSOAPMessage.SubmitSingleRefund(strRequest);
		}
		else
		{
			// we probably didnt send it as were in a test class so lets just return something that looks right
			mRet.Response = '<Response><ResponseCode>0</ResponseCode><Timestamp>2009-02-12 19:28:05</Timestamp><Receipt>100001337</Receipt><SettlementDate>12-feb-2009</SettlementDate><DeclinedCode></DeclinedCode><DeclinedMessage></DeclinedMessage></Response>';
		}
		
		XMLDom objXMLDom = new XMLDom(mRet.Response);
				
		XMLDom.Element root = objXMLDom.root.firstChild();
		
		// for each element returned
		for(XMLDom.Element element : root.childNodes)
		{
			System.Debug(element);

			if(element.nodeName == 'ResponseCode')
			{
				mRet.ResponseCode = element.nodeValue;
			}
			else if(element.nodeName == 'Timestamp')
			{
				mRet.Timestamp = element.nodeValue;
			}
			else if(element.nodeName == 'Receipt')
			{
				mRet.Receipt = element.nodeValue;
			}
			else if(element.nodeName == 'SettlementDate')
			{
				mRet.SettlementDateText = element.nodeValue;
				mRet.SettlementDate	 	= ConvertSettlementDate(element.nodeValue);
			}
			else if(element.nodeName == 'DeclinedCode')
			{
				mRet.DeclinedCode = element.nodeValue;
			}
			else if(element.nodeName == 'DeclinedMessage')
			{
				mRet.DeclinedMessage = element.nodeValue;
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private static date ConvertSettlementDate(string strSettlementDate)
	{
		date dtRet;
				
		try
		{
			string[] arrStringSettlementDate = strSettlementDate.split('-');
			
			if(arrStringSettlementDate.size() == 3)
			{
				dtRet = date.newInstance(integer.valueOf(arrStringSettlementDate[2]), GetMonth(arrStringSettlementDate[1]), integer.valueOf(arrStringSettlementDate[0]));
			}
		}
		catch(Exception e)
		{
			System.Debug(e);
		}
		
		return dtRet;
	}
	
	private static integer GetMonth(string strMonth)
	{
		integer iRet = 1;
		
		if(strMonth.toLowerCase() == 'feb')
			iRet = 2;
		else if(strMonth.toLowerCase() == 'mar')
			iRet = 3;
		else if(strMonth.toLowerCase() == 'apr')
			iRet = 4;
		else if(strMonth.toLowerCase() == 'may')
			iRet = 5;
		else if(strMonth.toLowerCase() == 'jun')
			iRet = 6;
		else if(strMonth.toLowerCase() == 'jul')
			iRet = 7;
		else if(strMonth.toLowerCase() == 'aug')
			iRet = 8;
		else if(strMonth.toLowerCase() == 'sep')
			iRet = 9;
		else if(strMonth.toLowerCase() == 'oct')
			iRet = 10;
		else if(strMonth.toLowerCase() == 'nov')
			iRet = 11;
		else if(strMonth.toLowerCase() == 'dec')
			iRet = 12;
			
		return iRet;
	}
	
	/***************************************************************************************
		Test Methods
	***************************************************************************************/
	public static testMethod void testIPPMethodSinglePayment()
	{
		QProcessIPPPayment 		oProcessor;
		QProcessIPPPaymentArgs 	oArgs = new QProcessIPPPaymentArgs();
		QProcessIPPPaymentRet 	oRet;
		
		oArgs.AllowCallout 		= false;
		oArgs.PaymentProcess 	= PaymentProcess.SINGLE_PAYMENT;
		
		oProcessor = new QProcessIPPPayment(oArgs);
		
		oRet = oProcessor.ProcMain();
		
		System.AssertEquals(oRet.ResponseCode, 			'0');
		System.AssertEquals(oRet.Timestamp, 			'2009-02-12 19:28:05');
		System.AssertEquals(oRet.Receipt, 				'100001337');
		System.AssertEquals(oRet.SettlementDateText, 	'12-feb-2009');
		System.AssertEquals(oRet.SettlementDate, 		date.newInstance(2009, 2, 12));
		System.AssertEquals(oRet.DeclinedCode, 			'');
		System.AssertEquals(oRet.DeclinedMessage, 		'');
		
	}
	
	public static testMethod void testIPPMethodSinglePaymentRegestered()
	{
		QProcessIPPPayment 		oProcessor;
		QProcessIPPPaymentArgs 	oArgs = new QProcessIPPPaymentArgs();
		QProcessIPPPaymentRet 	oRet;
		
		oArgs.AllowCallout 		= false;
		oArgs.PaymentProcess 	= PaymentProcess.SINGLE_PAYMENT_REGESTERED;
		
		oProcessor = new QProcessIPPPayment(oArgs);
		
		oRet = oProcessor.ProcMain();
		
		/*
			Currently Not Implemented
			
		System.AssertEquals(oRet.ResponseCode, 			'0');
		System.AssertEquals(oRet.Timestamp, 			'2009-02-12 19:28:05');
		System.AssertEquals(oRet.Receipt, 				'100001337');
		System.AssertEquals(oRet.SettlementDateText, 	'12-feb-2009');
		System.AssertEquals(oRet.SettlementDate, 		date.newInstance(2009, 2, 12));
		System.AssertEquals(oRet.DeclinedCode, 			'');
		System.AssertEquals(oRet.DeclinedMessage, 		'');
		*/
	}
	
	public static testMethod void testIPPMethodSinglePaymentWithRegester()
	{
		QProcessIPPPayment 		oProcessor;
		QProcessIPPPaymentArgs 	oArgs = new QProcessIPPPaymentArgs();
		QProcessIPPPaymentRet 	oRet;
		
		oArgs.AllowCallout 		= false;
		oArgs.PaymentProcess 	= PaymentProcess.SINGLE_PAYMENT_WITH_REGISTER;
		
		oProcessor = new QProcessIPPPayment(oArgs);
		
		oRet = oProcessor.ProcMain();
		
		System.AssertEquals(oRet.ResponseCode, 			'0');
		System.AssertEquals(oRet.Timestamp, 			'2009-02-12 19:28:05');
		System.AssertEquals(oRet.Receipt, 				'100001337');
		System.AssertEquals(oRet.SettlementDateText, 	'12-feb-2009');
		System.AssertEquals(oRet.SettlementDate, 		date.newInstance(2009, 2, 12));
		System.AssertEquals(oRet.DeclinedCode, 			'');
		System.AssertEquals(oRet.DeclinedMessage, 		'');
	}
	
	public static testMethod void testIPPMethodRefund()
	{
		QProcessIPPPayment 		oProcessor;
		QProcessIPPPaymentArgs 	oArgs = new QProcessIPPPaymentArgs();
		QProcessIPPPaymentRet 	oRet;
		
		oArgs.AllowCallout 		= false;
		oArgs.PaymentProcess 	= PaymentProcess.SINGLE_REFUND;
		
		oProcessor = new QProcessIPPPayment(oArgs);
		
		oRet = oProcessor.ProcMain();
		
		System.AssertEquals(oRet.ResponseCode, 			'0');
		System.AssertEquals(oRet.Timestamp, 			'2009-02-12 19:28:05');
		System.AssertEquals(oRet.Receipt, 				'100001337');
		System.AssertEquals(oRet.SettlementDateText, 	'12-feb-2009');
		System.AssertEquals(oRet.SettlementDate, 		date.newInstance(2009, 2, 12));
		System.AssertEquals(oRet.DeclinedCode, 			'');
		System.AssertEquals(oRet.DeclinedMessage, 		'');
	}
	
	public static testMethod void testDateConversionMethods()
	{
		System.Assert(date.newInstance(2009, 1, 1) 	== ConvertSettlementDate('1-jan-2009'));
		System.Assert(date.newInstance(2009, 2, 1) 	== ConvertSettlementDate('1-feb-2009'));
		System.Assert(date.newInstance(2009, 3, 1) 	== ConvertSettlementDate('1-mar-2009'));
		System.Assert(date.newInstance(2009, 4, 1) 	== ConvertSettlementDate('1-apr-2009'));
		System.Assert(date.newInstance(2009, 5, 1) 	== ConvertSettlementDate('1-may-2009'));
		System.Assert(date.newInstance(2009, 6, 1) 	== ConvertSettlementDate('1-jun-2009'));
		System.Assert(date.newInstance(2009, 7, 1) 	== ConvertSettlementDate('1-jul-2009'));
		System.Assert(date.newInstance(2009, 8, 1) 	== ConvertSettlementDate('1-aug-2009'));
		System.Assert(date.newInstance(2009, 9, 1) 	== ConvertSettlementDate('1-sep-2009'));
		System.Assert(date.newInstance(2009, 10, 1) == ConvertSettlementDate('1-oct-2009'));
		System.Assert(date.newInstance(2009, 11, 1) == ConvertSettlementDate('1-nov-2009'));
		System.Assert(date.newInstance(2009, 12, 1) == ConvertSettlementDate('1-dec-2009'));
	}
}