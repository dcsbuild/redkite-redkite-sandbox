public class QVFRelationshipTreeview 
{
	/***********************************************************************************************************
		Members
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Exception Classes
	public class QVFScreenParameterException extends Exception{}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Payment Classes
	public class QEntity
	{
		public string 			Name{get; set;}
		public Id				ID{get; set;}
		public list<QEntity> 	Relationships{get;set;}
		public list<QEntity> 	Contacts{get;set;}
		public list<QEntity> 	Partners{get;set;}
		
		public boolean			IsAccount{get; set;}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//	Constructor
		public QEntity(id ID, string strName, boolean bIsAccount)
		{
			this.Id				= ID;
			this.Name 			= strName;
			this.IsAccount		= bIsAccount;
			this.Relationships 	= new list<QEntity>();
			this.Contacts 		= new list<QEntity>();
			this.Partners 		= new list<QEntity>();
		}
		
		public void addRelationship(QEntity thisEntity)
		{
			this.Relationships.add(thisEntity);		
		}
		
		public void addContact(QEntity thisEntity)
		{
			this.Contacts.add(thisEntity);		
		}
		
		public void addPartner(QEntity thisEntity)
		{
			this.Partners.add(thisEntity);		
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Singular
	string m_strOrigin;
	string m_strId;
	
	Contact m_sContact;
	Account m_sAccount;
	
	QEntity objRoot;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Collections
	map<id, QEntity> 	mapQSeeds;
	set<id>				setProcessedIDs;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Constructor
	public QVFRelationshipTreeview()
	{
		// lets grab the screen parameters
		if(null == (m_strId = ApexPages.currentPage().getParameters().get('id')))
		{
			throw new QVFScreenParameterException('Id Parameter not found');	
		}
		
		if(null == (m_strOrigin = ApexPages.currentPage().getParameters().get('origin')))
		{
			throw new QVFScreenParameterException('Origin Parameter not found');	
		}
		
		setProcessedIDs = new set<id>();
		mapQSeeds 		= new map<id, QEntity>();
		
		LoadBaseEntityData();
				
		mapQSeeds.put(objRoot.ID, objRoot);
		setProcessedIDs.add(objRoot.ID);
		
		LoadRelationships(mapQSeeds);
	}
	
	/***********************************************************************************************************
		Page Access Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	public string getTreeString()
	{
		string strRet = BuildTreeCode();

				
		
		return strRet;
	}
	
	/***********************************************************************************************************
		Worker Methods
	***********************************************************************************************************/
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private string BuildTreeCode()
	{
		string strRet = '\n';
		
		if(m_strOrigin == 'Account')
		{
			strRet += '\tfoldersTree = gFld("'+ m_sAccount.Name + '", "/apex/Relationship_Treeview_Detail_Account?id=' + m_sAccount.Id + '&IsAccount=true"); \n';
		}
		else if(m_strOrigin == 'Contact')
		{
			strRet += '\tfoldersTree = gFld("'+ m_sContact.Name + '", "/apex/Relationship_Treeview_Detail_Contact?id=' + m_sContact.Id + '&IsAccount=false"); \n';
		}
		else if(m_strOrigin == 'PersonAccount')
		{
			strRet += '\tfoldersTree = gFld("'+ m_sAccount.Name + '", "/apex/Relationship_Treeview_Detail_Account?id=' + m_sAccount.Id + '&IsAccount=true"); \n';
		}
				
		strRet += '\tfoldersTree.treeID = "MyTree"; \n';
		
		strRet += BuildTreeNode(objRoot, 'foldersTree', 0);
		
      	return strRet;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	private string BuildTreeNode(QEntity objEntity, string strParent, integer iLevel)
	{
		System.Debug(objEntity);
		
		string strRet = '';
		
		if(objEntity.Contacts.size() > 0)
		{
			strRet += 'Caux'+iLevel+' = insFld('+strParent+', gFld("Contacts", "javascript:parent.DoNothing()")); \n';
			
			for(integer i = 0; i < objEntity.Contacts.size(); i++)
			{
				if(objEntity.Contacts[i].IsAccount)
					strRet += 'Caux' + (iLevel+1) + ' = insFld(Caux'+iLevel+', gFld("' + objEntity.Contacts[i].Name + '", "/apex/Relationship_Treeview_Detail_Account?id=' + objEntity.Contacts[i].Id + '&IsAccount=' + objEntity.Contacts[i].IsAccount + '")); \n';
				else
					strRet += 'Caux' + (iLevel+1) + ' = insFld(Caux'+iLevel+', gFld("' + objEntity.Contacts[i].Name + '", "/apex/Relationship_Treeview_Detail_Contact?id=' + objEntity.Contacts[i].Id + '&IsAccount=' + objEntity.Contacts[i].IsAccount + '")); \n';
				
				strRet += BuildTreeNode(objEntity.Contacts[i], 'Caux'+(iLevel+1), iLevel+1);
			}
		}
		
		if(objEntity.Relationships.size() > 0)
		{	
			strRet += 'Raux'+iLevel+' = insFld('+strParent+', gFld("Relationships", "javascript:parent.DoNothing()")); \n';
			
			for(integer i = 0; i < objEntity.Relationships.size(); i++)
			{
				if(objEntity.Relationships[i].IsAccount)
					strRet += 'Raux' + (iLevel+1) + ' = insFld(Raux'+iLevel+', gFld("' + objEntity.Relationships[i].Name + '", "/apex/Relationship_Treeview_Detail_Account?id=' + objEntity.Relationships[i].Id + '&IsAccount=' + objEntity.Relationships[i].IsAccount + '")); \n';
				else
					strRet += 'Raux' + (iLevel+1) + ' = insFld(Raux'+iLevel+', gFld("' + objEntity.Relationships[i].Name + '", "/apex/Relationship_Treeview_Detail_Contact?id=' + objEntity.Relationships[i].Id + '&IsAccount=' + objEntity.Relationships[i].IsAccount + '")); \n';
					
				strRet += BuildTreeNode(objEntity.Relationships[i], 'Raux'+(iLevel+1), iLevel+1);
			}
		}
		
		if(objEntity.Partners.size() > 0)
		{	
			strRet += 'Paux'+iLevel+' = insFld('+strParent+', gFld("Partners", "javascript:parent.DoNothing()")); \n';
			
			for(integer i = 0; i < objEntity.Partners.size(); i++)
			{
				if(objEntity.Partners[i].IsAccount)
					strRet += 'Paux' + (iLevel+1) + ' = insFld(Paux'+iLevel+', gFld("' + objEntity.Partners[i].Name + '", "/apex/Relationship_Treeview_Detail_Account?id=' + objEntity.Partners[i].Id + '&IsAccount=' + objEntity.Partners[i].IsAccount + '")); \n';
				else
					strRet += 'Paux' + (iLevel+1) + ' = insFld(Paux'+iLevel+', gFld("' + objEntity.Partners[i].Name + '", "/apex/Relationship_Treeview_Detail_Contact?id=' + objEntity.Partners[i].Id + '&IsAccount=' + objEntity.Partners[i].IsAccount + '")); \n';
					
				strRet += BuildTreeNode(objEntity.Partners[i], 'Paux'+(iLevel+1), iLevel+1);
			}
		}
		
		return strRet;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private void LoadRelationships(map<id, QEntity> mapSeedList)
	{
		System.Debug('SeedList - ' + mapSeedList);
		
		map<id, QEntity> mapTempQSeeds = new map<id, QEntity>();
		
		// First lets load any contacts for the seed lists
		for(Contact[] arrContact : [
										select 	id,
												Name,
												AccountId
										from 	Contact 
										where 	AccountId in :mapSeedList.keySet()
										and		IsPersonAccount = false
										order by Name
									])
		{
			for(Contact sContact : arrContact)
			{
				if(setProcessedIDs.size() < 1000)
				{
					// we need to add the new entity to the seed list
					QEntity objTempEntity = new QEntity(sContact.id, sContact.Name, false);
					mapSeedList.get(sContact.AccountId).addContact(objTempEntity);
					
					// if we havent seen it before so its been looked up
					if(!setProcessedIDs.contains(sContact.id))
					{
						mapTempQSeeds.put(sContact.id, objTempEntity);
						setProcessedIDs.add(sContact.id);
					}
				}
			}
		}
		
		// Now lets load the Relationships
		for(Relationship__c[] arrRelationship : [
													select 	id,
															Contact_at_Account__r.Name,
															Contact__r.Name,
															Contact__r.IsPersonAccount,
															Contact__r.AccountId,
															AccountPersonAccount__r.Name
													from 	Relationship__c 
													where 	Contact_at_Account__c 		in :mapSeedList.keySet()
													or		AccountPersonAccount__c 	in :mapSeedList.keySet()
													or 		Contact__c 					in :mapSeedList.keySet()
													or		(Contact__r.AccountId in :mapSeedList.keySet() and Contact__r.IsPersonAccount = true)
												])
		{
			for(Relationship__c sRelationship : arrRelationship)
			{
				if(setProcessedIDs.size() < 1000)
				{
					// We started at the contact
					if(mapSeedList.containsKey(sRelationship.Contact__c))
					{
						// if we havent seen it before so its been looked up
						if(!setProcessedIDs.contains(sRelationship.AccountPersonAccount__c))
						{
							// we need to add the new entity to the seed list
							QEntity objTempEntity = new QEntity(sRelationship.AccountPersonAccount__c, sRelationship.AccountPersonAccount__r.Name, true);
							mapSeedList.get(sRelationship.Contact__c).addRelationship(objTempEntity);

							mapTempQSeeds.put(sRelationship.AccountPersonAccount__c, objTempEntity);
							setProcessedIDs.add(sRelationship.AccountPersonAccount__c);
						}
					}
					// We started from an Account
					else if(mapSeedList.containsKey(sRelationship.AccountPersonAccount__c))
					{
						// if we havent seen it before so its been looked up
						if(!setProcessedIDs.contains(sRelationship.Contact__c))
						{
							// we need to add the new entity to the seed list
							QEntity objTempEntity = new QEntity(sRelationship.Contact__c, sRelationship.Contact__r.Name, false);
							mapSeedList.get(sRelationship.AccountPersonAccount__c).addRelationship(objTempEntity);
													
							mapTempQSeeds.put(sRelationship.Contact__c, objTempEntity);
							setProcessedIDs.add(sRelationship.Contact__c);
						}
					}
					// We hit from the account side of a person account
					else if(mapSeedList.containsKey(sRelationship.Contact__r.AccountId))
					{
						// if we havent seen it before so its been looked up
						if(!setProcessedIDs.contains(sRelationship.AccountPersonAccount__c))
						{
							// we need to add the new entity to the seed list
							QEntity objTempEntity = new QEntity(sRelationship.Contact__r.AccountId, sRelationship.Contact__r.Name, false);
							mapSeedList.get(sRelationship.Contact__r.AccountId).addRelationship(objTempEntity);
													
							mapTempQSeeds.put(sRelationship.Contact__r.AccountId, objTempEntity);
							setProcessedIDs.add(sRelationship.Contact__r.AccountId);
						}
					}
				}
			}
		}
		
		// Now the partners
		for(AccountPartner[] arrAccountPartner : [
													select 	id,
															AccountFrom.PersonContactId,
															AccountFromId,
															AccountToId,
															AccountTo.Name,
															AccountTo.IsPersonAccount,
															AccountTo.PersonContactId,
															Role
													from 	AccountPartner 
													where 	AccountFromId in :mapSeedList.keySet()
													or		AccountFrom.PersonContactId in :mapSeedList.keySet()
												])
		{
			for(AccountPartner sAccountPartner : arrAccountPartner)
			{
				if(setProcessedIDs.size() < 1000)
				{
					// we need to add the new entity to the seed list
					QEntity objTempEntity = new QEntity(sAccountPartner.AccountToId, sAccountPartner.AccountTo.Name, true);
					
					// small hack for person accounts
					if(mapSeedList.containsKey(sAccountPartner.AccountFromId))
					{
						mapSeedList.get(sAccountPartner.AccountFromId).addPartner(objTempEntity);
					}
					else if(mapSeedList.containsKey(sAccountPartner.AccountFrom.PersonContactId))
					{
						mapSeedList.get(sAccountPartner.AccountFrom.PersonContactId).addPartner(objTempEntity);
					}
					
					// if we havent seen it before so its been looked up
					if(		(!setProcessedIDs.contains(sAccountPartner.AccountToId) && sAccountPartner.AccountTo.IsPersonAccount == false))
					{
						mapTempQSeeds.put(sAccountPartner.AccountToId, objTempEntity);
						setProcessedIDs.add(sAccountPartner.AccountToId);
					}
				}
			}
		}
		
		if(mapTempQSeeds.size() > 0)
			LoadRelationships(mapTempQSeeds);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	private void LoadBaseEntityData()
	{
		if(m_strOrigin == 'Account')
		{
			m_sAccount 	= [select id, Name from Account where id = :m_strId];
			
			objRoot 	= new QEntity(m_sAccount.Id, m_sAccount.Name, true);
			
		}
		else if(m_strOrigin == 'Contact')
		{
			m_sContact 	= [select Id, Name, AccountId from Contact where id = :m_strId];
			
			objRoot 	= new QEntity(m_sContact.Id, m_sContact.Name, false);
		}
		else if(m_strOrigin == 'PersonAccount')
		{
			m_sAccount = [select id, Name from Account where id = :m_strId];
			m_sContact = [select Id, Name, AccountId from Contact where AccountId = :m_sAccount.Id];
			
			objRoot = new QEntity(m_sContact.Id, m_sContact.Name, false);
		}
		else
		{
			throw new QVFScreenParameterException('Unknown Origin Passed - ' + m_strOrigin);
		}
	}
	
	public static testmethod void testEnd2End()
	{
		PageReference pageRef = new PageReference('/apex/RelationshipTimeline');
		Test.setCurrentPage(pageRef);
		
		QVFRelationshipTreeview controller;

		Account thisAccount 	= new Account();
		thisAccount.Name 		= 'Test Account';
		insert thisAccount;
		
		Contact thisContact 	= new Contact();
		thisContact.AccountID 	= thisAccount.Id;
		thisContact.FirstName 	= 'Test';
		thisContact.LastName 	= 'CampaignMember';
		insert thisContact;
		
		Account thisAccount2 	= new Account();
		thisAccount2.Name 		= 'Test Account';
		insert thisAccount2;
		
		Relationship__c sRelationship = new Relationship__c();
		
		sRelationship.AccountPersonAccount__c 	= thisAccount2.Id;
		sRelationship.Contact__c 				= thisContact.Id;
		insert sRelationship;
		
		// Add parameters to page URL
		ApexPages.currentPage().getParameters().put('id', thisAccount2.Id);
		ApexPages.currentPage().getParameters().put('origin', 'Account');
		
		// Instantiate a new controller with all parameters in the page
		controller = new QVFRelationshipTreeview();
		
		controller.getTreeString();

		
	}
	
	
	
	
	
	
	
	
	
	
	
}