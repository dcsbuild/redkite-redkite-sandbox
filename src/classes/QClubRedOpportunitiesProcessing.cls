// change the name of this class to something like QClubRedOpportunityEndOfYearBatch
// needs comments
// CLASS TO DELETE
global class QClubRedOpportunitiesProcessing implements Database.Batchable<SObject>
{	
	private	string		strOldCampaign;
	private	string		strNewCampaign;
	
	global QClubRedOpportunitiesProcessing(string strOldCampaign, string strNewCampaign)
	{
		this.strOldCampaign = strOldCampaign;
		this.strNewCampaign = strNewCampaign; 
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		string	strQuery = '	select 	';
		for(Schema.SObjectField f : Schema.sObjectType.Opportunity.fields.getMap().Values())
		{
			strQuery+= f+' ,';		
		}
		strQuery = strQuery.substring(0,strQuery.length()-2);
		strQuery +=
			'	from 	Opportunity '+
			'	where 	StageName = \'Club Red Ongoing\' '+
			'	and		Campaign.Name = \''+strOldCampaign+'\'';
		return Database.getQueryLocator(strQuery);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		list<Opportunity> 	liNewOpportunity 	= new list<Opportunity>();
		
		for( sObject o : scope )
		{
			//clone
			Opportunity sOldOpportunity 	= (Opportunity)o;
			Opportunity sNewOpportunity 	= sOldOpportunity.clone(false, true);
			/////Old Opp
			sOldOpportunity.StageName 		= 'Club Red Closed';
			////New Opp
			sNewOpportunity.Campaign.Name 		= strNewCampaign;
			liNewOpportunity.add(sNewOpportunity);	
		}
		
		insert liNewOpportunity;
		update scope;
	}
	
	global void finish(Database.BatchableContext BC)
	{
		system.debug('Finished');
	}

}