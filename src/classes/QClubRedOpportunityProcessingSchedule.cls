global with sharing class QClubRedOpportunityProcessingSchedule implements Schedulable 
{
	global void execute(SchedulableContext SC) 
	{
		Database.executeBatch(new QClubRedOpportunityProcessing());
	}
	
	global static testMethod void testQClubRedOpportunityProcessingSchedule() 
	{
		Test.startTest(); 
		
		System.Schedule('QClubRedOpportunityProcessingSchedule', '20 30 8 10 2 ?', new QClubRedOpportunityProcessingSchedule());

		Test.stopTest();
    }
}