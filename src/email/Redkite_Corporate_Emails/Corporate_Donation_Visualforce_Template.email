<messaging:emailTemplate subject="Redkite Donation" recipientType="Contact" relatedToType="Income__c" >
    <messaging:plainTextEmailBody >
    Please find attached your thank you letter and tax receipt.  The Redkite Team.
    </messaging:plainTextEmailBody>
    <messaging:htmlEmailBody >
    <html>
        <body>
            <div style="FONT-SIZE: 10pt; COLOR: black; mso-fareast-language: EN-AU; font-family : Arial Unicode MS">
                <p>
                    Thank you for your kind donation which will have an immediate impact on the lives of children and young people with cancer and their families.<br/>
                    <br/>
                    Today, at least four families will receive the devastating news that their child or teenager has cancer. Thousands of others will be undergoing treatment, which can last up to three years. Thanks to your support, these young people and their families will not have to go through their cancer journey alone.<br/>
                    <br/>
                    Redkite is the national charity in Australia that supports children, teenagers and young adults from birth to age 24. We are there providing essential support from the moment of diagnosis, throughout long and difficult treatment and on return home from hospital.<br/>
                    <br/>
                    Redkite receives no government funding which is why your donation is so important to us. We rely on the support of generous individuals like you to help us be there for the families where and when they need us.<br/>
                    <br/>
                    Thank you for choosing to support Redkite. Please find your tax-deductible receipt attached. We look forward to keeping you up to date on how your donation is making a difference.<br/>
                    <br/>
                    Kind regards,<br/>
                    <br/>
                    The Redkite Team.<br/>
                </p>
            </div>
        </body>
    </html>
    </messaging:htmlEmailBody>
    <messaging:attachment renderAs="pdf" filename="Redkite Thank You and Tax Receipt.pdf">
<html> 
    <style>
        .hr { page-break-after: always; }
        
        .pagebreak { page-break-before:always }
    </style>
    <body>
        <div style="FONT-SIZE: 10pt; COLOR: black; mso-fareast-language: EN-AU; font-family : Arial Unicode MS">
            <table id="topTable" cellpadding="0" cellspacing="0" width="100%" >
                <tr valign="top" >
                    <td align="center">
                        <img style="margin-top: 2em;" src="{!$Resource.Header_For_Corporate_VF_Template}"/>
                    </td>
                    <td align="right" >
                        <img src="{!$Resource.Redkite_Logo_For_VF_Template}"/>
                    </td>
                </tr>
            </table>
 
            <div style="width: 80%; margin-left: auto; margin-right: auto; margin-top: 3em;">
                <div style="text-align: center; font-weight: bold; width: 100%;">
                    <h1 style="text-decoration: underline;">Thank you for your donation!</h1>
                </div>
                <div style="font-weight: bold; text-align: center; width: 100%">
                    Tax Deductible Receipt
                </div>
                <table style="margin-top: 2em; margin-bottom: 2em;" id="receiptTable" cellpadding="0" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td width="40%">Receipt Number:</td>
                        <td width="60%"><apex:outputText value="{!relatedTo.Income_Identification__c}" /></td>
                    </tr>
                    <tr valign="top">
                        <td>Date Receipted:</td>
                        <td>
                            <apex:outputText value="{0,date, dd MMMM yyyy}">
                                <apex:param value="{!relatedTo.Local_Date__c}" />
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>Supporter ID:</td>
                        <td><apex:outputText value="{!recipient.Donor_ID__c}" /></td>
                    </tr>
                    <tr valign="top">
                        <td>Received From:</td>
                        <td>
                            <p>
                                <apex:outputText value="{!recipient.name}" /><br />
                                <apex:outputText value="{!recipient.Donation_Addr_Street_1__c}" /><br/>
                                <apex:outputPanel rendered="{!LEN(recipient.Donation_Addr_Street_2__c) > 0}">
                                    <apex:outputText value="{!recipient.Donation_Addr_Street_2__c}" /><br/>
                                </apex:outputPanel>
                                <apex:outputText value="{!recipient.Donation_Addr_City__c}" /><br/>
                                <apex:outputText value="{!recipient.Donation_Addr_State__c}" />,&nbsp;
                                <apex:outputText value="{!recipient.Donation_Addr_Postcode__c}" />,&nbsp;
                                <apex:outputText value="{!recipient.Donation_Addr_Country__c}" /><br/>
                            </p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <apex:outputText value="Amount:" />
                        </td>
                        <td>
                            <apex:outputText value="{0,number,Currency}">
                                <apex:param value="{!relatedTo.Total_Amount__c}" />
                            </apex:outputText>
                        </td>
                    </tr>
                    <apex:variable var="v" value="" rendered="{!IF(relatedTo.Opportunity__r.Campaign.Receipt_Phrase__c == '', false, true)}">
                        <tr valign="top">
                            <td>
                                <apex:outputText value="For:" />
                            </td>
                            <td>
                                <apex:outputText value="{!relatedTo.Opportunity__r.Campaign.Receipt_Phrase__c}" />
                            </td>
                        </tr>
                    </apex:variable>
                </table>
                
                <p>Thank you for your generous support for children and young people with cancer and their families</p>
                <img style="margin-top: 2em;" src="{!$Resource.JS_Sig_For_Corporate_VF_Template}" />
                <p>
                    Jenni Seton<br />
                    Chief Executive Officer
                </p>
                <p style="margin-top: 3em; font-size: 0.7em; color: #555555;">
                    National Office, Level 8, Tower 1, 1 Lawson Square, Redfern NSW 2016  PO Box 3220 Redfern NSW 2016<br />
                    Telephone (02) 9219 4000 Facsimile (02) 9280 1518 Email <a href="mailto:info@redkite.org.au">info@redkite.org.au</a> <a href="www.redkite.org.au">www.redkite.org.au</a><br />
                    CFN 12637 Tax No. DGR:51033 ABN 65 104 710 787<br />
                    The Malcolm Sargent Cancer Fund for Children in Australia trading as Redkite<br />              
                </p>    
            </div>
        </div>
    </body>
</html>
        
    </messaging:attachment>
</messaging:emailTemplate>