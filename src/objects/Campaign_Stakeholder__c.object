<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to link Campaigns with Accounts, Contacts and Person Accounts.  This was primarily designed to allow connections that are &quot;Non&quot; members.  Members can only be contacts but at this Many to Many object is designed to link suppliers and other vendor companies to the campaigns.  The advantage being that any involvement is easy to identify both ways.</description>
    <enableActivities>true</enableActivities>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Campaign_Supplier_Contact__c</fullName>
        <inlineHelpText>Select the contact at a supplier if known.  For example this would be the venue manager, AV operator, etc.  Optional Field</inlineHelpText>
        <label>Campaign Supplier Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>R00N20000001TClNEAW</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Campaign_Supplier__c</fullName>
        <inlineHelpText>Used to identify the Account or Person Account</inlineHelpText>
        <label>Campaign Supplier</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>R00N20000001TCkvEAG</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <type>MasterDetail</type>
    </fields>
    <fields>
        <fullName>Campaign_Supply__c</fullName>
        <formula>Campaign_Supplier__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Campaign Supply</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Campaign__c</fullName>
        <label>Campaign</label>
        <referenceTo>Campaign</referenceTo>
        <relationshipName>R00N20000001TClDEAW</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <type>MasterDetail</type>
    </fields>
    <fields>
        <fullName>Invitation_Status__c</fullName>
        <inlineHelpText>Status of invitations</inlineHelpText>
        <label>Invitation Status</label>
        <picklist>
            <controllingField>Relationship_Status__c</controllingField>
            <picklistValues>
                <fullName>Invited</fullName>
                <controllingFieldValues>Celebrity</controllingFieldValues>
                <controllingFieldValues>Redkite Spokesperson</controllingFieldValues>
                <controllingFieldValues>VIP Guest</controllingFieldValues>
                <controllingFieldValues>Volunteer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Accepted</fullName>
                <controllingFieldValues>Celebrity</controllingFieldValues>
                <controllingFieldValues>Redkite Spokesperson</controllingFieldValues>
                <controllingFieldValues>VIP Guest</controllingFieldValues>
                <controllingFieldValues>Volunteer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Attended</fullName>
                <controllingFieldValues>Volunteer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Declined</fullName>
                <controllingFieldValues>Celebrity</controllingFieldValues>
                <controllingFieldValues>Redkite Spokesperson</controllingFieldValues>
                <controllingFieldValues>VIP Guest</controllingFieldValues>
                <controllingFieldValues>Volunteer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Invite_Notes__c</fullName>
        <inlineHelpText>Explain any notes relating to the Celebrity invitation including notes on acceptance or decline to allow for better understanding of future invitation limitations.</inlineHelpText>
        <label>Invite Notes</label>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Relationship_Group__c</fullName>
        <inlineHelpText>Please select the group the stakeholder falls into.</inlineHelpText>
        <label>Relationship Group</label>
        <picklist>
            <picklistValues>
                <fullName>Supplier</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Invitee</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Volunteer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Referrer</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Relationship_Status__c</fullName>
        <inlineHelpText>Select the appropriate option above.  If your choice does not appear please contact System Admin requesting the choice and the reason for new addition.  This is a madatory Field.</inlineHelpText>
        <label>Relationship Status</label>
        <picklist>
            <controllingField>Relationship_Group__c</controllingField>
            <picklistValues>
                <fullName>Audio Visual</fullName>
                <controllingFieldValues>Supplier</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cancer Charity/Support</fullName>
                <controllingFieldValues>Referrer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Caterer</fullName>
                <controllingFieldValues>Supplier</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Celebrity</fullName>
                <controllingFieldValues>Invitee</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Dept of Education</fullName>
                <controllingFieldValues>Referrer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Education institute</fullName>
                <controllingFieldValues>Referrer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hospital/Oncology</fullName>
                <controllingFieldValues>Referrer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Media Consultant</fullName>
                <controllingFieldValues>Supplier</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Organiser Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Primary Organiser Account</fullName>
                <controllingFieldValues>Supplier</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Primary Organiser Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Print/Collateral</fullName>
                <controllingFieldValues>Supplier</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Redkite Spokesperson</fullName>
                <controllingFieldValues>Invitee</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sponsor</fullName>
                <controllingFieldValues>Supplier</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Venue</fullName>
                <controllingFieldValues>Supplier</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VIP Guest</fullName>
                <controllingFieldValues>Invitee</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Volunteer</fullName>
                <controllingFieldValues>Volunteer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Supplier_Status__c</fullName>
        <label>Supplier Status</label>
        <picklist>
            <controllingField>Relationship_Status__c</controllingField>
            <picklistValues>
                <fullName>Tentative</fullName>
                <controllingFieldValues>Audio Visual</controllingFieldValues>
                <controllingFieldValues>Caterer</controllingFieldValues>
                <controllingFieldValues>Media Consultant</controllingFieldValues>
                <controllingFieldValues>VIP Guest</controllingFieldValues>
                <controllingFieldValues>Venue</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Confirmed</fullName>
                <controllingFieldValues>Audio Visual</controllingFieldValues>
                <controllingFieldValues>Caterer</controllingFieldValues>
                <controllingFieldValues>Media Consultant</controllingFieldValues>
                <controllingFieldValues>VIP Guest</controllingFieldValues>
                <controllingFieldValues>Venue</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Venue__c</fullName>
        <formula>CASE( Relationship_Status__c , &quot;Venue&quot;,  Campaign_Supplier__r.Name , &quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Venue</label>
        <type>Text</type>
    </fields>
    <label>Campaign Stakeholder</label>
    <nameField>
        <displayFormat>CS-{000000}</displayFormat>
        <label>ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Campaign Stakeholder</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Campaign_Supply__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Relationship_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Supplier_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Invitation_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Campaign_Supplier__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Campaign_Supplier_Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Relationship_Status__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Relationship_Status__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Campaign_Supplier__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
