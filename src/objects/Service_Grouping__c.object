<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This is used to hold the high level information on Redkite Services.  It is not for the granular level as used in the Real Estate report.  This will provide all Redkite persons to gain an understanding of the individual programs as linked in the Service and Supporter Level object records.  This should be managed by the Services Division.</description>
    <enableActivities>true</enableActivities>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Brand_Value__c</fullName>
        <description>Rollup summary of Brand Value of all Projects associated with this program</description>
        <label>Brand Value</label>
        <summarizedField>Svc_Sup__c.Brand_Value__c</summarizedField>
        <summaryForeignKey>Svc_Sup__c.Svc_Grouping__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Used to describe the Program.  This is typically taken from general Redkite collateral.</description>
        <label>Description</label>
        <length>32000</length>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Est_Actual_Cost__c</fullName>
        <description>Roll Up summary for each project related to the relevent program</description>
        <label>Est Actual Cost</label>
        <summarizedField>Svc_Sup__c.Est_Actual_Cost__c</summarizedField>
        <summaryForeignKey>Svc_Sup__c.Svc_Grouping__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Target_Audience__c</fullName>
        <description>Rollup summary of total size of Target Audience of all Projects associated with this program</description>
        <label>Target Audience</label>
        <summarizedField>Svc_Sup__c.No_Target_Audience__c</summarizedField>
        <summaryForeignKey>Svc_Sup__c.Svc_Grouping__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Value__c</fullName>
        <description>Rollup summary of Total Value of all Projects associated with this program</description>
        <label>Total Value</label>
        <summarizedField>Svc_Sup__c.Total_Value__c</summarizedField>
        <summaryForeignKey>Svc_Sup__c.Svc_Grouping__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <type>Summary</type>
    </fields>
    <label>Service Grouping</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Program Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Service_Grouping</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total_Value__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>LAST_ACTIVITY</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Total_Value__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LAST_ACTIVITY</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Total_Value__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LAST_ACTIVITY</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
</CustomObject>
