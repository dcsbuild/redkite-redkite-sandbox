<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Linking object to inventory listing.  This provides the ability to &quot;book out&quot; and &quot;book in&quot; inventory items.  Some items are one way such as vouchers.  Others are returned to stock.  Each item is tagged and linked to the appropriate object within RED.</description>
    <enableActivities>true</enableActivities>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Account_State__c</fullName>
        <formula>Account__r.BillingState</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account State</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Resources</relationshipLabel>
        <relationshipName>Resources</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Actual_Return_Date__c</fullName>
        <inlineHelpText>The actual return date</inlineHelpText>
        <label>Actual Return Date</label>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Allocated_Entity__c</fullName>
        <formula>if(len( Account__c ) &gt; 0, &apos;Account&apos;,
if(len( Campaign__c ) &gt; 0, &apos;Campaign&apos;,
if(len( Case__c ) &gt; 0, &apos;Case&apos;,
if(len( Contact__c ) &gt; 0, &apos;Contact&apos;,
if(len( Opportunity__c ) &gt; 0, &apos;Opportunity&apos;,
if(len( Purchase_Order__c ) &gt; 0, &apos;PO&apos;,
&apos;Unknown&apos;))))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Allocated Entity</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Campaign__c</fullName>
        <label>Campaign</label>
        <referenceTo>Campaign</referenceTo>
        <relationshipLabel>Resources</relationshipLabel>
        <relationshipName>Resources</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Resources</relationshipLabel>
        <relationshipName>Resources</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contact_Contact_State__c</fullName>
        <formula>Contact__r.Contact_State__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Contact (Contact) State</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Contact_State__c</fullName>
        <description>This identifies the state to which the vouchers are being sent to.</description>
        <formula>Purchase_Order__r.Budget_Allocation__r.Diagnosed_Person__r.Contact_State__c</formula>
        <inlineHelpText>This identifies the state to which the vouchers are being sent to.</inlineHelpText>
        <label>Contact State</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Resources</relationshipLabel>
        <relationshipName>Resources</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Diagnosed_Family__c</fullName>
        <formula>Purchase_Order__r.Budget_Allocation__r.Diagnosed_Person__r.Account.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Only applies to financial assistance claims</inlineHelpText>
        <label>Diagnosed Family</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Discrepancy_Description__c</fullName>
        <label>Discrepancy Description</label>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Event_Details__c</fullName>
        <inlineHelpText>Enter a Campaign name of event information</inlineHelpText>
        <label>Event Details</label>
        <length>255</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Inventory_ID__c</fullName>
        <formula>Inventory_Item__r.Inventory_ID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Inventory ID</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Inventory_Item__c</fullName>
        <label>Inventory Item</label>
        <referenceTo>Inventory__c</referenceTo>
        <relationshipLabel>Resources</relationshipLabel>
        <relationshipName>Resources</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <type>MasterDetail</type>
    </fields>
    <fields>
        <fullName>Inventory_Type__c</fullName>
        <label>Inventory Type</label>
        <length>100</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Inventory_Value__c</fullName>
        <formula>Inventory_Item__r.Voucher_Value__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Inventory Value</label>
        <precision>18</precision>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <formula>Inventory_Item__r.Location_Text__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Location</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Lost__c</fullName>
        <defaultValue>false</defaultValue>
        <inlineHelpText>Please tick if the inventory item was lost or never returned</inlineHelpText>
        <label>Lost</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Resources</relationshipLabel>
        <relationshipName>Resources</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Purchase_Order__c</fullName>
        <label>Purchase Order</label>
        <referenceTo>PurchaseOrder__c</referenceTo>
        <relationshipLabel>Resources</relationshipLabel>
        <relationshipName>Resources</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Returned__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Has the Item been returned?</description>
        <inlineHelpText>Has the Item been returned?</inlineHelpText>
        <label>Returned</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Scheduled_Return_Date__c</fullName>
        <inlineHelpText>The scheduled return date for this Item</inlineHelpText>
        <label>Scheduled Return Date</label>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Units_Allocated__c</fullName>
        <inlineHelpText>The number of Units being allocated</inlineHelpText>
        <label>Units Allocated</label>
        <precision>7</precision>
        <scale>0</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>Units_Lost__c</fullName>
        <description>If the Lost field is ticked the value of this field will change to 1.</description>
        <formula>IF ( Lost__c=TRUE,1, 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Please tick this field if the inventory item has been lost. Remember to also create an Inventory Adjust record and fill in the Adjust Decrease field.</inlineHelpText>
        <label>Units Lost</label>
        <precision>18</precision>
        <scale>0</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>Units_Returned__c</fullName>
        <inlineHelpText>The number of units returned</inlineHelpText>
        <label>Units Returned</label>
        <precision>7</precision>
        <scale>0</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Resources</relationshipName>
        <type>Lookup</type>
    </fields>
    <label>Resource</label>
    <listViews>
        <fullName>Collection_Tins_VIC_Office</fullName>
        <columns>NAME</columns>
        <columns>Inventory_Item__c</columns>
        <columns>Account__c</columns>
        <columns>Inventory_Type__c</columns>
        <columns>Scheduled_Return_Date__c</columns>
        <columns>Location__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Inventory_Type__c</field>
            <operation>equals</operation>
            <value>Collection Tin</value>
        </filters>
        <filters>
            <field>Location__c</field>
            <operation>equals</operation>
            <value>Victorian Office</value>
        </filters>
        <label>Collection Tins - VIC Office</label>
    </listViews>
    <nameField>
        <displayFormat>R-{000000}</displayFormat>
        <label>Resources Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Resources</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Lost_Reason_Supplied</fullName>
        <active>true</active>
        <errorConditionFormula>AND
(
     len( Discrepancy_Description__c ) = 0,
      Units_Lost__c &gt; 0
)</errorConditionFormula>
        <errorDisplayField>Discrepancy_Description__c</errorDisplayField>
        <errorMessage>If units are lost a description must be supplied</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Number_Returned_Entered_At_Correct_Time</fullName>
        <active>true</active>
        <errorConditionFormula>OR
(
      AND
      (
           NOT(Returned__c),
           NOT(ISNULL(Units_Returned__c ))
      ),
      AND
      (
           Returned__c,
           ISNULL(Units_Returned__c )
      )
)</errorConditionFormula>
        <errorDisplayField>Units_Returned__c</errorDisplayField>
        <errorMessage>Invalid Data - Please check Returned Flag or Supplied Number of Units Returned</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Number_Returned_Greater_Number_Sent</fullName>
        <active>true</active>
        <errorConditionFormula>OR
(
    Units_Returned__c &gt; Units_Allocated__c,
    Units_Returned__c &lt; 0
)</errorConditionFormula>
        <errorDisplayField>Units_Returned__c</errorDisplayField>
        <errorMessage>Invalid number of units returned</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Return_Date_Entered</fullName>
        <active>true</active>
        <errorConditionFormula>AND
(
     Returned__c,
     isnull( Actual_Return_Date__c )
)</errorConditionFormula>
        <errorDisplayField>Actual_Return_Date__c</errorDisplayField>
        <errorMessage>Return Date Required</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Single_Entity_Resource_Allocation_Check</fullName>
        <active>true</active>
        <errorConditionFormula>if(len( Account__c ) &gt; 0, 1, 0) +
if(len( Campaign__c ) &gt; 0, 1, 0) +
if(len( Case__c ) &gt; 0, 1, 0) +
if(len( Contact__c ) &gt; 0, 1, 0) +
if(len( Opportunity__c ) &gt; 0, 1, 0) +
if(len( Purchase_Order__c ) &gt; 0, 1, 0)

&gt; 1</errorConditionFormula>
        <errorMessage>Resources can only be allocated to a single entity</errorMessage>
    </validationRules>
</CustomObject>
