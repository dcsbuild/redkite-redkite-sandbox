<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email Alert%3A Notifies Bonnie Harris of a donation greater or equal to %241000</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>bharris@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cibbott@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rhatchard@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/Donation_greater_or_equal_to_1000</template>
    </alerts>
    <alerts>
        <fullName>Email Alert%3A Notifies DDCoordinator of a donation greater or equal to %24500</fullName>
        <ccEmails>red@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>cmarchetti@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Donation_greater_or_equal_to_500</template>
    </alerts>
    <alerts>
        <fullName>Receipt Required Email Alert</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/Receipt_Required_Email_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Acct Code ClubRed</fullName>
        <description>Updates the account code for club red with correct code.</description>
        <field>Account_Code__c</field>
        <formula>&quot;245&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>All Inc Proc Date</fullName>
        <description>Updates All Income Processed Date with current date when All Income Processed is true.</description>
        <field>All_Income_Processed_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AllIncomeProcessedDate</fullName>
        <description>Puts the date in the All Income Processed Date field</description>
        <field>All_Income_Processed_Date__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AllIncomeProcessedUpdate</fullName>
        <description>Ticks the All Income Processed tickbox field</description>
        <field>All_Income_Processed__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AllRecDate</fullName>
        <description>Puts the date in the All Reconciled Date field.</description>
        <field>All_Reconciled_Date__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bank Rec Tick</fullName>
        <description>Ticks Bank Rec Tick which activates validation rules which lock down the income record for editing.</description>
        <field>Rec_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate Pending Rec A Lockdown</fullName>
        <field>Income_Proc_A_Lockdown__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate Pending Rec B Lockdown</fullName>
        <field>Income_Proc_B_Lockdown__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate Rec A Lockdown</fullName>
        <field>Rec_A_Lockdown__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate Rec B Lockdown</fullName>
        <field>Rec_B_Lockdown__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Declined Date Income One Off</fullName>
        <description>For incomes with Income One Off record type only. Inserts current date/time into Declined Date field</description>
        <field>Declined_Date__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Declined Date Update</fullName>
        <description>Puts current date/time in Declined Date field</description>
        <field>Declined_Date__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Declined Status Income One Off</fullName>
        <description>For incomes with Income One Off record type only. Updates Status to &quot;Declined&quot;</description>
        <field>Status__c</field>
        <literalValue>Declined</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Declined Status Update</fullName>
        <description>Changes Status picklist value to Declined</description>
        <field>Status__c</field>
        <literalValue>Declined</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete Amount A</fullName>
        <description>Removes the value from the Amount A field</description>
        <field>Amount_A__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GIK Amount Update</fullName>
        <description>Enters the GIK Amount field value into the GIK Amount Received field</description>
        <field>GIKPB_Amount_Received__c</field>
        <formula>GIKPB_Amount__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GIK Income Status Update</fullName>
        <description>Updates the Income Status field to indicate that the GIK has been received.</description>
        <field>Status__c</field>
        <literalValue>Gifts/Services Received</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GIK Received Date Update</fullName>
        <description>Updates the GIK Received Date</description>
        <field>GIKPB_Received_Date__c</field>
        <formula>Today()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN IncomeProcDateNow</fullName>
        <description>Checks if there is an Online Processing Date and inserts that value when All Income Porcessed otherwise inserts the date/time of check box being updated</description>
        <field>Income_Processed_Date_A__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_AllRecCheckbox</fullName>
        <field>All_Reconciled__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_BankRecLock</fullName>
        <description>Updates the bank rec flag to checked.  Used in validations to prevent the user from making changes after processing.</description>
        <field>Rec_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_IncmePrcLock</fullName>
        <field>Income_Proc_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_IncomePrcLockA</fullName>
        <field>Income_Processed_A__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_IncomeProc Lockdown</fullName>
        <field>Income_Proc_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_IncomeProcDateB</fullName>
        <field>Income_Processed_Date_B__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_ProBonoAmt</fullName>
        <field>Pro_Bono_Estimated_Value__c</field>
        <formula>Pro_Bono_Hours__c  *  Pro_Bono_Rate_Hr__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_Status Income Process</fullName>
        <description>Change status to income processed</description>
        <field>Status__c</field>
        <literalValue>Income Processed</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_Status Income Processed</fullName>
        <field>Status__c</field>
        <literalValue>Income Processed</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_Status Reconciled</fullName>
        <field>Status__c</field>
        <literalValue>Reconciled</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_Update Acct Code</fullName>
        <description>Updates the account code from the lookup used by pro bono, GIK and volunteer.</description>
        <field>Account_Code__c</field>
        <formula>Account_CodeHidden__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_UpdateBankRecDate</fullName>
        <field>Reconciled_Date_A__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IN_Updates ProcADate Only</fullName>
        <field>Income_Proc_Date_A__c</field>
        <formula>TODAY ()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Income Processed Date A Update</fullName>
        <description>Inserts current date/time into Income Processed Date (A) field</description>
        <field>Income_Processed_Date_A__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Income Processed Date Update</fullName>
        <description>Inserts current date/time in Income Processed Date field</description>
        <field>All_Income_Processed_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Income Processed Update</fullName>
        <description>Ticks Income Processed tickbox.</description>
        <field>All_Income_Processed__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MYOB All RecB</fullName>
        <description>Ticks MYOB All Reconciled Tickbox when MYOB Reconciled A, B are ticked and Amount B field has a value</description>
        <field>MYOB_All_Reconciled__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MYOB All Reconciled Date</fullName>
        <description>Inserts the date when MYOB Reconciled (A) is ticked and there is no value in Amount (B)</description>
        <field>MYOB_All_Reconciled_Date__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MYOB All Reconciled Tick</fullName>
        <description>Updates MYOB All Reconciled Tickbox when MYOB Reconciled (A) is ticked and there is no value in Amount (B)</description>
        <field>MYOB_All_Reconciled__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MYOB Reconciled Date %28B%29 Update</fullName>
        <description>Updates MYOB Reconciled Date (B) field when tickbox is ticked</description>
        <field>MYOB_Reconciled_Date_B__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MYOB ReconciledA Date Update</fullName>
        <description>Updates MYOB Reconciled (A) Date field with the date when MYOB Reconciled (A) tickbox is ticked</description>
        <field>MYOB_Reconciled_Date_A__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Part A Lockdown</fullName>
        <field>Rec_A_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Part B Lockdown</fullName>
        <field>Rec_B_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending Rec A Lockdown</fullName>
        <field>Income_Proc_A_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pending Rec B Lockdown</fullName>
        <description>Locksdown Pending Rec B</description>
        <field>Income_Proc_B_Lockdown__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReconciledADate</fullName>
        <description>Updates the Reconciled Date A field and inserts the date</description>
        <field>Reconciled_Date_A__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReconciledBDate</fullName>
        <description>Updates the Reconciled Date B field and inserts the date.</description>
        <field>Reconciled_Date_B__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset Update Flag</fullName>
        <field>Update_Receipt_History__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckPendingRecA</fullName>
        <field>Income_Processed_Date_A__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckPendingRecB</fullName>
        <field>Income_Processed_Date_B__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckReconciled</fullName>
        <field>All_Reconciled_Date__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckReconciledA</fullName>
        <field>Reconciled_Date_A__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckReconciledB</fullName>
        <field>Reconciled_Date_B__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Al Rec Date</fullName>
        <field>All_Reconciled_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Declined Amount</fullName>
        <description>Copies the value in Total Value field</description>
        <field>Declined_Amount__c</field>
        <formula>Total_Value__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Letter Print Date</fullName>
        <field>Letter_Printed_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Quiz Account Code on Opp</fullName>
        <description>Update the Account code field on the Opportunity for all Quiz Incomes</description>
        <field>Account_Code__c</field>
        <formula>Account_Code__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Receipt Date</fullName>
        <field>Receipt_Letter_Printed_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Receipt History</fullName>
        <field>Receipt_Print_History__c</field>
        <formula>&quot;Date Printed:  &quot;&amp;  TEXT(NOW()) &amp;&quot;   &quot;&amp; Receipt_Print_History__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Receipt Printed</fullName>
        <description>Updates the receipt printed date</description>
        <field>Receipt_Printed_Date_Time__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDateRECA</fullName>
        <field>Reconciled_Date_A__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDateRECB</fullName>
        <field>Reconciled_Date_B__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateReconciledDate</fullName>
        <field>All_Reconciled_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ClubRed IncomeLookup</fullName>
        <actions>
            <name>Acct Code ClubRed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Club Red</value>
        </criteriaItems>
        <description>Updates the account code only on the Income record creation to reflect the Club Red Account code of 245.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Declined Income Automation</fullName>
        <actions>
            <name>Declined Date Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Declined Status Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Delete Amount A</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Declined Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Declined__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Income - One Off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Ticking Declined tickbox moves amount A to Declined Amount, inserts current date in Declined Date and updates Status to Declined</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Declined Income One Off</fullName>
        <actions>
            <name>Declined Date Income One Off</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Declined Status Income One Off</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Income - One Off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Declined__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>For incomes with record type Income One Off only. Updates Declined Date and Status fields only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Donations over %241000</fullName>
        <actions>
            <name>Email Alert%3A Notifies Bonnie Harris of a donation greater or equal to %241000</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Total_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>1000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Status__c</field>
            <operation>equals</operation>
            <value>Income Processed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Identifies donations equal or greater to $1000 excluding ticket sales (account code 105)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Donations over %24500</fullName>
        <actions>
            <name>Email Alert%3A Notifies DDCoordinator of a donation greater or equal to %24500</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Total_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>500</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Status__c</field>
            <operation>equals</operation>
            <value>Income Processed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Corporates,T&amp;F/Major Gifts,Trusts and Foundations</value>
        </criteriaItems>
        <description>Identifies donations equal or greater to $500 excluding ticket sales (account code 105)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>GIK Received</fullName>
        <actions>
            <name>GIK Amount Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GIK Income Status Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>GIK Received Date Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Gifts In Kind,Pro Bono</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.GIK_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the GIK Amount Received, GIK Date Received and Income Status fields</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Generate Receipt Required Email</fullName>
        <actions>
            <name>Receipt Required Email Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.All_Income_Processed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Income - Payment Gateway,Income Refund - Payment Gateway</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>notEqual</operation>
            <value>Melon Media,Chris Mail,Matt Lacey,Roberta Steuart</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Donation_Type__c</field>
            <operation>equals</operation>
            <value>Receipt Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends an email alert to the Opportunity owner to let them know that their income is ready to be receipted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN ReconciledA</fullName>
        <actions>
            <name>ReconciledADate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Income__c.Reconciled_A__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>*Puts the date in the Reconciled Date A field. No longer active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN ReconciledB</fullName>
        <actions>
            <name>ReconciledBDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Income__c.Reconciled_B__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>*Puts the date in the Reconciled Date B field. No longer active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN_BankRec_flags</fullName>
        <actions>
            <name>IN_AllRecCheckbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_IncmePrcLock</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_IncomePrcLockA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_Status Income Process</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_UpdateBankRecDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Al Rec Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.All_Reconciled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Creates a series of actions based on checking the Reconciled checkbox.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN_BankRec_flagsB</fullName>
        <actions>
            <name>AllRecDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_AllRecCheckbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_BankRecLock</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_Status Reconciled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Income__c.Reconciled_A__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Amount_B__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Reconciled_B__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>*Creates a series of actions based on checking the bank reconciliation A &amp; B checkbox. No longer active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN_IncomeProcessed Updates</fullName>
        <actions>
            <name>IN_IncomeProc Lockdown</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_Status Income Processed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_Updates ProcADate Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.All_Income_Processed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Used when Income Processed is checked and will trigger workflow updates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN_IncomeProcessedA</fullName>
        <actions>
            <name>IN IncomeProcDateNow</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_Updates ProcADate Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Income__c.Income_Processed_A__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>*Places a time date stamp in processed check box. No longer active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN_IncomeProcessedB</fullName>
        <actions>
            <name>IN_IncomeProcDateB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Income__c.Income_Processed_B__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>*Places a time date stamp in processed check box. No longer active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN_IncomeProcessedUpdatesB</fullName>
        <actions>
            <name>AllIncomeProcessedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>AllIncomeProcessedUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_IncomeProc Lockdown</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IN_Status Income Processed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Income__c.Income_Processed_A__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Amount_B__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Processed_B__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>*Used when Income Processed A &amp; B are checked and there is a Part B payment will trigger workflow updates. No longer active</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IN_ProBono</fullName>
        <actions>
            <name>IN_ProBonoAmt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Pro Bono</value>
        </criteriaItems>
        <description>As rollups cannot work on formulas this is designed to compelte the pro bono currency field thereby allowing rollups to both contacts or opportunity records.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Income Processed Payment Gateway Fix</fullName>
        <actions>
            <name>IN_Status Income Processed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Income Processed Date A Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Income Processed Date Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Income Processed Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Club Red - Payment Gateway,Income - Payment Gateway,Income Refund - Payment Gateway,Income - One Off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Declined__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Processed_A__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Temporary fix -Income Processed is ticked and Income Processed Date is filled in for Payment Gateway incomes (integration code only ticks Income Processed (A) tickbox). Can be deactivated once integration code is fixed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quiz Income Account Code</fullName>
        <actions>
            <name>Update Quiz Account Code on Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Job_Code__c</field>
            <operation>contains</operation>
            <value>QZ</value>
        </criteriaItems>
        <description>Identify a processed Quiz Income</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Code</fullName>
        <actions>
            <name>IN_Update Acct Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates Account Code used in income code for all Non Club Red Incomes.  Checks if old Open Alms Ledger Code used.  Else updates with new code.</description>
        <formula>AND(LEN(Account_CodeHidden__c)&gt;0, RecordTypeId &lt;&gt;&quot;01I200000005WMf&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Credit card type</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>1/1/2009</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Letter Only Date</fullName>
        <actions>
            <name>Update Letter Print Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Letter_Printed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Income__c.Income_Data_Cleansing__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Time stamps the date/time a letter was generated for an income.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Receipt History</fullName>
        <actions>
            <name>Reset Update Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Receipt History</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Update_Receipt_History__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the receipt history text field by timestamping the date/time.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Receipt Letter Date</fullName>
        <actions>
            <name>Update Receipt Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Receipt_Letter_Printed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the receipt/letter history by timestamping the date/time.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Receipt Only</fullName>
        <actions>
            <name>Update Receipt Printed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Income__c.Receipt_Printed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the receipt history by timestamping the date/time.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Receipt Required</fullName>
        <assignedToType>owner</assignedToType>
        <description>This donation has been processed and is now ready for receipting.  Please generate a receipt through the Receipt Button on the Income page layout for this record.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
    </tasks>
</Workflow>
