<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cheryl%27s Lead Email Alert</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>cibbott@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Web_to_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>Services Website Lead Email Notification</fullName>
        <ccEmails>support@redkite.org.au</ccEmails>
        <protected>false</protected>
        <template>System_Emails/ServicesWeb_to_Lead_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lead Record Type Conversion Update</fullName>
        <field>Lead_Record_Type_Text__c</field>
        <formula>$RecordType.DeveloperName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Company Name Field</fullName>
        <description>Update Company Name field for all Family Services Leads with Surname + Household, however if a value is entered in &quot;Name of Child&quot; field updates the Company name with Name of Child + Household</description>
        <field>Company</field>
        <formula>IF(ISBLANK( Name_of_Child__c ), LastName &amp; &quot; &quot;&amp; &quot;Household&quot;, Name_of_Child__c &amp; &quot; &quot;&amp; &quot;Household&quot;)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Is Family Services Lead</fullName>
        <description>Update Is Family Services Lead based on value in Record Type= Family Services</description>
        <field>Is_Family_Services_Leads__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Record Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Community_Fundraising</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Record Type to Community Fundraising</fullName>
        <actions>
            <name>Update Record Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.I_am_hosting_my_event_as_an__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cheryl%27s Lead Email Alert</fullName>
        <actions>
            <name>Cheryl%27s Lead Email Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Donor Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Cheryl Mathews</value>
        </criteriaItems>
        <description>Alerts Cheryl when any leads are created by Web to Lead owned by her by default.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Company Name for Family Services Lead</fullName>
        <actions>
            <name>Update Company Name Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Family Services</value>
        </criteriaItems>
        <description>Identifies if a lead is on a Family Services Record type and updates the Company Name field based on naming convention of Surname + Household</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Identify %22Is family Services Lead%22</fullName>
        <actions>
            <name>Update Is Family Services Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Family Services</value>
        </criteriaItems>
        <description>Identify if this Is Family Services Lead</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Record Type Conversion Update</fullName>
        <actions>
            <name>Lead Record Type Conversion Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>1/1/1990</value>
        </criteriaItems>
        <description>This is used to default certain lead types to person accounts</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Services Website Lead Notification</fullName>
        <actions>
            <name>Services Website Lead Email Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead Creation Email Notification Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Family Lead,Family Services</value>
        </criteriaItems>
        <description>Duplicating website integration web to lead notifications with standard workflow functionality</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Lead Creation Email Notification Sent</fullName>
        <assignedToType>owner</assignedToType>
        <description>This task is to log that an email notification was sent to the original lead owner upon creation of this lead.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
    </tasks>
</Workflow>
