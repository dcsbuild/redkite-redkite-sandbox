<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Deletion Request</fullName>
        <ccEmails>audit@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/Deletion_RequestAcct</template>
    </alerts>
    <alerts>
        <fullName>EMail Notification Merge Acct</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>cibbott@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Merge_RequestAcct</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change Merge PC Name</fullName>
        <field>Name</field>
        <formula>Name &amp; &quot;** MERGE REQUEST **&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Name</fullName>
        <field>Name</field>
        <formula>Name &amp; &quot;**DELETE PENDING **&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Name Merge</fullName>
        <description>Inserts the word Merge after the name</description>
        <field>Name</field>
        <formula>Name &amp; &quot;** MERGE REQUEST **&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change PC Name delete</fullName>
        <field>LastName</field>
        <formula>LastName &amp; &quot;** DELETION REQUEST **&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check Date</fullName>
        <field>Checked_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do Not Call Opt Out PC</fullName>
        <field>PersonDoNotCall</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email Opt Out - Local - Update Date</fullName>
        <description>This workflow rule will update the Date Email Opt Out field with the date and internal user alias when the email opt out field is changed to true</description>
        <field>Date_Email_Opt_Out__c</field>
        <formula>TEXT(TODAY()) &amp; &quot; - &quot; &amp; $User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email Opt Out - Portal - Clear Date</fullName>
        <description>This field update will write the date and method of change (Donation Portal) to the Date email opt out field when a donor opts out of email communications.</description>
        <field>Date_Email_Opt_Out__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email Opt Out - Portal - Update Date</fullName>
        <description>This field update will write the date and method of change (Donation Portal) to the Date email opt out field when a donor opts out of email communications.</description>
        <field>Date_Email_Opt_Out__c</field>
        <formula>TEXT(TODAY()) &amp; &quot; - Donation Form&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email Optout PC</fullName>
        <field>PersonHasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fax Opt Out PC</fullName>
        <field>PersonHasOptedOutOfFax</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead to Account conversion- Family to Fa</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Family_Household</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead to Account conversion- General Cont</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Individual</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead to Account conversion- Subscribe to</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Individual</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead to Account conversion- fam servs to</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Family_Household</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead to Account conversion-Commfund Rais</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Individual</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MailOptOut</fullName>
        <description>Tickst MailOptout on person account</description>
        <field>Mail_Opt_Out__pc</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MailOptOut PC</fullName>
        <field>Mail_Opt_Out__pc</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OptOutMail</fullName>
        <field>Mail_Opt_Out__pc</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PC Changed Name Merge</fullName>
        <field>LastName</field>
        <formula>LastName &amp; &quot;** MERGE REQUEST **&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Timestamp Return To Sender</fullName>
        <field>Return_to_Sender_Date__pc</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Alternate COUNTRY</fullName>
        <description>Updates the Account alternate COUNTRY address with the Account mailing COUNTRY.</description>
        <field>BillingCountry</field>
        <formula>ShippingCountry</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Alternate City</fullName>
        <description>Updates the Account alternate CITY address with the Account mailing CITY.</description>
        <field>BillingCity</field>
        <formula>ShippingCity</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Alternate POSTCODE</fullName>
        <description>Updates the Account alternate POSTCODE address with the Account mailing POSTCODE.</description>
        <field>BillingPostalCode</field>
        <formula>ShippingPostalCode</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Alternate STATE</fullName>
        <description>Updates the Account alternate STATE address with the Account mailing STATE.</description>
        <field>BillingState</field>
        <formula>ShippingState</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Alternate Street</fullName>
        <description>Updates the Account alternate STREET address with the Account mailing STREET.</description>
        <field>BillingStreet</field>
        <formula>ShippingStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update City %28Mailing%29 from PA</fullName>
        <field>PersonMailingCity</field>
        <formula>ShippingCity</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Date Email Opt Out</fullName>
        <field>Date_Email_Opt_Out__c</field>
        <formula>TEXT(TODAY()) &amp; &quot; - &quot; &amp; $User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Is Board Member</fullName>
        <description>Board Member graphic prompt</description>
        <field>Is_Board_Member__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update MailOptOut</fullName>
        <description>If Return to Sender is ticked, tick Mail Opt Out on Accounts</description>
        <field>Mail_Opt_Out__pc</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Postal City Address field</fullName>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Postal Country Address field</fullName>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Postal Post code address field</fullName>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Postal State Address field</fullName>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Postal Street Address</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Postcode %28Mailing%29 from PA</fullName>
        <field>PersonMailingPostalCode</field>
        <formula>ShippingPostalCode</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update RecordType to Family Services</fullName>
        <description>Changes the Record Type for all converted Family Services Leads from Organisation Account to &quot;Family Account&quot; Business Account</description>
        <field>RecordTypeId</field>
        <lookupValue>Family_Household</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update State%28Mailing%29 from PA</fullName>
        <field>PersonMailingState</field>
        <formula>ShippingState</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Street%28Mailing%29 on PAccoun</fullName>
        <description>Copies the postal address (Shipping address) on a person account record to the mailing address fields (used on a person account)</description>
        <field>PersonMailingStreet</field>
        <formula>ShippingStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateSalutTxt PC</fullName>
        <description>Due to limitations of 5000 characters in formulas this workaround updates the necessary data and effectively resets the counter.</description>
        <field>Salutation_Txt__pc</field>
        <formula>Salutation_Text_PC__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Address details for Income letters</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Organisation Account,Relationship Fundraising,Individual</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change RecordType for Converted Family Services Leads</fullName>
        <actions>
            <name>Update RecordType to Family Services</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Lead_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Family_Services</value>
        </criteriaItems>
        <description>Identifies all converted Family Services Leads and changes the Business Account Record tTpe from &quot;organisation account&quot; to &quot;family Services&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Checked Record Acct</fullName>
        <actions>
            <name>Check Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.Record_Checked__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used in account cleaning to time stamp the date when the record was checked as &quot;clean&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deceased Opt Outs PC</fullName>
        <actions>
            <name>Do Not Call Opt Out PC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email Optout PC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fax Opt Out PC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>MailOptOut</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Deceased__pc</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPersonAccount</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to trigger actions of opt out when a contact flag is listed as deceased</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt In - Local Change</fullName>
        <actions>
            <name>Email Opt Out - Local - Update Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule captures when the Email Opt in field is changed to true by an internal salesforce user.</description>
        <formula>AND(PersonHasOptedOutOfEmail == True, $User.FirstName != &quot;DonationForm&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt In - Portal Change</fullName>
        <actions>
            <name>Email Opt Out - Portal - Update Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule captures when the Email Opt in field is changed to true by a portal user.</description>
        <formula>AND(PersonHasOptedOutOfEmail == True, $User.FirstName == &quot;DonationForm&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt Out</fullName>
        <actions>
            <name>Update Date Email Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.PersonHasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Identifies if &quot;Email Opt out&quot; = true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt Out - Portal Change</fullName>
        <actions>
            <name>Email Opt Out - Portal - Clear Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule captures when the Email Opt in field is changed to false by a portal user.</description>
        <formula>AND(PersonHasOptedOutOfEmail == False, $User.FirstName == &quot;DonationForm&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt Out unticked</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.PersonHasOptedOutOfEmail</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Identifies if an Email Opt Out has been unticked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead to Account conversion- General Contact us to Individual Account</fullName>
        <actions>
            <name>Lead to Account conversion- General Cont</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Lead_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>General_Contact_Us</value>
        </criteriaItems>
        <description>this workflow rule converts a General Contact us Lead RCT to a Individual RCT</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead to Account conversion- Subscribe to Individual Account</fullName>
        <actions>
            <name>Lead to Account conversion- Subscribe to</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Lead_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Subscribe</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead to Account conversion- fam servs to fam acc</fullName>
        <actions>
            <name>Lead to Account conversion- fam servs to</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Lead_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>family_services</value>
        </criteriaItems>
        <description>this workflow rule converts a family services Lead RCT to a family Account RCT</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead to Account conversion-Commfund Raising to Individual</fullName>
        <actions>
            <name>Lead to Account conversion-Commfund Rais</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Lead_Record_Type_Text__c</field>
            <operation>equals</operation>
            <value>Community_Fund_Raising</value>
        </criteriaItems>
        <description>this workflow rule converts a Community Fund Raising Lead RCT to a Individual Account RCT</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Pre Check Flags PC</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.PreDataCleansed__pc</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used in account cleaning. Create a set of actions designed to assist data cleansing.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Return to Sender PC</fullName>
        <actions>
            <name>Timestamp Return To Sender</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update MailOptOut</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Return_To_Sender__pc</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Time stamps the Return To Sender checkbox and also opts out for further mail.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Alternate Address</fullName>
        <actions>
            <name>Update Alternate COUNTRY</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Alternate City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Alternate POSTCODE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Alternate STATE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Alternate Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Update_with_Postal_Address__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Alternate Address with the Accounts Postal Address.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Mailing Address for Person Accounts</fullName>
        <actions>
            <name>Update City %28Mailing%29 from PA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Postcode %28Mailing%29 from PA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update State%28Mailing%29 from PA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Street%28Mailing%29 on PAccoun</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <description>Copies the postal address (Shipping) to the mailing address fields (for Contacts) for all Person Accounts - this way address details display on campaign member page</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Postal Address details for converted leads</fullName>
        <actions>
            <name>Update Postal City Address field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Postal Country Address field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Postal Post code address field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Postal State Address field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Postal Street Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Lead_Record_Type_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Copies the alternate address and updates the postal address fields for all Person Accounts created through Lead Conversion</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Salutation Text PC</fullName>
        <actions>
            <name>UpdateSalutTxt PC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Individual,Independent Social/Health Worker</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
