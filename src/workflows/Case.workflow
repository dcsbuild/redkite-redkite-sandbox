<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/New_Case_Referral</template>
    </alerts>
    <alerts>
        <fullName>New Case Referral</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <template>System_Emails/New_Case_Referral</template>
    </alerts>
    <alerts>
        <fullName>Send automated email to Case Contact</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <template>System_Emails/External_Case_Assignment_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update Case Treatment Ctr</fullName>
        <description>Updates the treament centre as a static field</description>
        <field>Treatment_Centre__c</field>
        <formula>Diagnosed_Person__r.Treatment_Centre__r.Name</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Casenote Referral - CBS Team</fullName>
        <actions>
            <name>Casenote Referral - CBS Team</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Refer_Casenote_to__c</field>
            <operation>equals</operation>
            <value>CBS Consultant</value>
        </criteriaItems>
        <description>This workflow rule triggers tasks and emails to the Services CBS team if selected in the &apos;Refer Casenote to&apos; field on the casenote layout page.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Casenote Referral - ECS Team</fullName>
        <actions>
            <name>Casenote Referral - ECS Team</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Refer_Casenote_to__c</field>
            <operation>equals</operation>
            <value>ECS Consultant</value>
        </criteriaItems>
        <description>This workflow rule triggers tasks and emails to the Services ECS team if selected in the &apos;Refer Casenote to&apos; field on the casenote layout page.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Log Treatment Centre</fullName>
        <actions>
            <name>Update Case Treatment Ctr</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Considered Closed,Open</value>
        </criteriaItems>
        <description>Alpha Review testing.  This was part of the design to &quot;automate&quot; a snapshot of static data fields from the contact record thereby reducing data input.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Quattro Case Assignment</fullName>
        <actions>
            <name>Send automated email to Case Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Quattro Support Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>This is a custom rule created by Quattro to send an email to Quattro Support notifying them of a new support case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Referred to RK Contact</fullName>
        <actions>
            <name>Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Referred to RK Contact</value>
        </criteriaItems>
        <description>Generates an email notifying the Redkite Contact and the Case Manager that the case has been referred to the Redkite contact for further details.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Casenote Referral - CBS Team</fullName>
        <assignedTo>rsharma@redkite.org.au.redkite</assignedTo>
        <assignedToType>user</assignedToType>
        <description>This casenote has been referred to the CBS team by an ECS Consultant. The Task Due Date = the communication date on the casenote record.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Case.Contact_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
    </tasks>
    <tasks>
        <fullName>Casenote Referral - ECS Team</fullName>
        <assignedTo>pcameron@redkite.org.au.redkite</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new casenote has been referred to the ECS Team by the CBS Team.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Case.Contact_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
    </tasks>
</Workflow>
