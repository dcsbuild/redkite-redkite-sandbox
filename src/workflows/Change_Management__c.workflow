<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CM Approval Status</fullName>
        <description>Change Management Approval Status</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CM Approved Status</fullName>
        <description>Updates CM Status to Approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CR Approval Date</fullName>
        <description>Puts date/time in Approval Date/Time field</description>
        <field>Approval_Date_Time__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CR Approval Recalled Status</fullName>
        <description>Updates CM Status back to Draft</description>
        <field>Status__c</field>
        <literalValue>Draft</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CR Approved Checkbox</fullName>
        <description>Ticks the CR Approved Checkbox</description>
        <field>CR_Approved__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <tasks>
        <fullName>Change Request Approved</fullName>
        <assignedTo>cibbott@redkite.org.au.redkite</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Change Request has been approved and can be completed.</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Change_Management__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
    </tasks>
    <tasks>
        <fullName>Change Request Rejected</fullName>
        <assignedTo>cibbott@redkite.org.au.redkite</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Change Request Rejected, check request and find out why.</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Change_Management__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
    </tasks>
</Workflow>
