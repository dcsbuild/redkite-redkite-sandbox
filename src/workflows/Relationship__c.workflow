<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update Business Address</fullName>
        <field>Business_street_addresss__c</field>
        <formula>Street_Address_Relationship__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountPersonAccount__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Use Business Account for Mailing List</fullName>
        <field>Mailing_List_Address__c</field>
        <formula>AccountPersonAccount__r.SFDC_TEST__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountPersonAccount__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Use Business Account for Mailing List</fullName>
        <actions>
            <name>Use Business Account for Mailing List</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Relationship__c.Use_for_Mailing_Lists__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Use Business Account for Mailing ListV2</fullName>
        <actions>
            <name>Update Business Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Relationship__c.Use_for_Mailing_Lists__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
