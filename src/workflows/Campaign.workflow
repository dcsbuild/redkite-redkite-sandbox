<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval Decided</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/Approved_Template</template>
    </alerts>
    <alerts>
        <fullName>Status Update on Approval</fullName>
        <ccEmails>audit@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/Approved_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Authority Date Updt</fullName>
        <description>Updates the authority to fundraise date when Yes is chosen on the sent picklist</description>
        <field>Authority_to_Fundraise_Date__c</field>
        <formula>TODAY()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CA_Update Campaign Name</fullName>
        <description>Inserts a suffix for Naming convention standards</description>
        <field>Name</field>
        <formula>RIGHT($RecordType.Name,3) &amp; &quot; &quot;&amp;Job_Code_State_Hidden__c &amp;&quot; &quot;&amp;Name&amp;&quot; &quot;&amp; Job_Code_Year_Hidden__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Changed Status Approved</fullName>
        <field>Status</field>
        <literalValue>Proceeding</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Changed Status Approved Box</fullName>
        <field>Approval_Hidden__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Changed Status Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDH Account Code</fullName>
        <field>EDH_Account_Code_2__c</field>
        <formula>IF( CONTAINS( Job_Codes__c , &quot;SS&quot;) ,&quot;120&quot;,
IF( CONTAINS( Job_Codes__c, &quot;CR&quot;) , &quot;810&quot;,  
IF( CONTAINS( Job_Codes__c, &quot;GD&quot;) , &quot;240&quot;,
IF( CONTAINS( Job_Codes__c, &quot;CC&quot;) , &quot;310&quot;,
IF(CONTAINS( Job_Codes__c , &quot;CO&quot;), &quot;830&quot;,
IF(CONTAINS( Job_Codes__c , &quot;PC&quot;), &quot;310&quot;,
&quot;ERROR&quot; ) ) ) ) ))</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Letter ID Update</fullName>
        <field>Thank_You_Letter_ID__c</field>
        <formula>CASE( Thank_You_Letter__c,
&quot;Birthday GIK&quot;,&quot;a0Q20000000Thll &quot;,
&quot;Chairman Appeal&quot;,&quot;a0Q20000000Thlb&quot;,
&quot;Christmas Appeal&quot;,&quot;a0Q20000000Thlg&quot;,
&quot;Custom&quot;,&quot;Custom&quot;,
&quot;Deceased GIK&quot;,&quot;a0Q20000000ThmU&quot;,
&quot;General Donation&quot;,&quot;GeneralDonation&quot;,
&quot;Hamper Appeal&quot;,&quot;a0Q20000000ThoL&quot;,
&quot;Live Auction&quot;,&quot;a0Q20000000Thql&quot;,
&quot;Musicathon&quot;,&quot;a0Q20000000ThmA&quot;,
&quot;Newsletter&quot;,&quot;a0Q20000000Thll&quot;,
&quot;School Support&quot;,&quot;a0Q20000000ThmB&quot;,
&quot;Silent Auction&quot;,&quot;a0Q20000000Thqg&quot;,
&quot;Wedding GIK&quot;,&quot;a0Q20000000ThmF&quot;,
&quot;&quot;)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 1 Date</fullName>
        <field>Meeting_1__c</field>
        <formula>First_Meeting_Date__c  +7</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 2 Date</fullName>
        <field>Meeting_2__c</field>
        <formula>First_Meeting_Date__c +7</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 3 Update</fullName>
        <field>Meeting_3__c</field>
        <formula>First_Meeting_Date__c +14</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 4 Update</fullName>
        <field>Meeting_4__c</field>
        <formula>First_Meeting_Date__c +21</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 5 Update</fullName>
        <field>Meeting_5__c</field>
        <formula>First_Meeting_Date__c +28</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 6 Update</fullName>
        <field>Meeting_6__c</field>
        <formula>First_Meeting_Date__c +35</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 7 Update</fullName>
        <field>Meeting_7__c</field>
        <formula>First_Meeting_Date__c +42</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Meeting 8 Update</fullName>
        <field>Meeting_8__c</field>
        <formula>First_Meeting_Date__c +56</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset Rejection Flag</fullName>
        <field>Rejection_Hidden__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Services Campaign Name Update</fullName>
        <description>Updates the campaign name for Services campaigns (Family Day, Scholarship and Telegroup campaigns)</description>
        <field>Name</field>
        <formula>RIGHT($RecordType.Name,3) &amp; &quot; &quot;&amp;TEXT(Campaign_State__c)&amp;&quot; &quot;&amp;Name&amp;&quot; &quot;&amp; TEXT(Campaign_Year__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status Pending</fullName>
        <field>Status</field>
        <literalValue>Planned</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected by Event Manager</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status Submitted</fullName>
        <field>Status</field>
        <literalValue>Submitted for Approval</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Rejection Checkbox</fullName>
        <field>Rejection_Hidden__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Status</fullName>
        <field>Status</field>
        <literalValue>Main Program</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updt Job Code Static</fullName>
        <field>Job_Code_Static__c</field>
        <formula>Job_Codes__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>field statis update</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Authority to Fundraise Date</fullName>
        <actions>
            <name>Authority Date Updt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Authority_To_Fundraise__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Date stamps the authority to fundraise</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CA_Naming Convention</fullName>
        <actions>
            <name>CA_Update Campaign Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 and 2</booleanFilter>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>notContain</operation>
            <value>Open Alms Campaigns,Business Development - Event,Services - Scholarship_XSC,Services - Telegroup XTG,Services- Family Day XFD</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Job_Code_State__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This is designed to automatically take the 3 character code from the campaign record type and insert this into the campaign name automatically</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDH Account Code Update</fullName>
        <actions>
            <name>EDH Account Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT( ISBLANK( Job_Codes__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Services Campaign Naming Convention</fullName>
        <actions>
            <name>Services Campaign Name Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Services - Scholarship_XSC,Services - Telegroup XTG,Services- Family Day XFD,Services - General Communication XGC</value>
        </criteriaItems>
        <description>Updates the campaign name with the record type extension, campaign state and campaign year</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set 8 Events</fullName>
        <actions>
            <name>Meeting 1 Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meeting 2 Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meeting 3 Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meeting 4 Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meeting 5 Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meeting 6 Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meeting 7 Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Meeting 8 Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Create Events</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Generate Events/Tasks</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>contains</operation>
            <value>Telegroup</value>
        </criteriaItems>
        <description>Runs when status changed to generate events and tasks</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Letter ID</fullName>
        <actions>
            <name>Letter ID Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Event - Community Fundraising EVC,Business Development - Event,Event - Redkite EVR,Regular Communication_COM,Services - Scholarship_XSC,Event - Nominated EVN,Appeal APP,Services - Telegroup XTG,Club Red CRD,Holding Campaign HOL,Op</value>
        </criteriaItems>
        <description>Used to update the ID of the letter from the Conga Merge List</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Services - CBS Group Campaign Name</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Services - CBS Groups</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updt Job Code Static</fullName>
        <actions>
            <name>Updt Job Code Static</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>,Event - Community Fundraising EVC,Business Development - Event,Event - Redkite EVR,Regular Communication_COM,Services - Scholarship_XSC,Event - Nominated EVN,Appeal APP,Services - Telegroup XTG,Club Red CRD,Open Alms Campaigns,Holding Campaign HOL</value>
        </criteriaItems>
        <description>Updates the Job Code Static field to overocme the 5k character limit.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Create Events</fullName>
        <assignedToType>owner</assignedToType>
        <description>Check the 8 event dates and create eight event dates and associated newsletters.  Note that when creating the event also invite Resource  &quot;Services&quot;</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
    </tasks>
</Workflow>
