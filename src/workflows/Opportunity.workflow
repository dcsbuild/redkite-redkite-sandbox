<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Action Required%3A  A new T%26F or Major Gift Opportunity has been created for your to review</fullName>
        <ccEmails>cibbott@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>bharris@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/New_Major_Gift_Opporunity_Created</template>
    </alerts>
    <alerts>
        <fullName>Email Supporter with message to personalise e-card</fullName>
        <protected>false</protected>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <template>System_Emails/CGIL_2012_Purchaser_Email</template>
    </alerts>
    <alerts>
        <fullName>Send Email alert when WA Fashion Parade ticket purchased on website</fullName>
        <ccEmails>KBreidahl@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>jdungan@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lmijovska@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Website_Event_Ticket_Purchase</template>
    </alerts>
    <alerts>
        <fullName>Website Event Ticket Purchase Notification NSW</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>adaoust@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bpalmer@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dhughes@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ebreidahl@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mwhitby@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Website_Event_Ticket_Purchase</template>
    </alerts>
    <alerts>
        <fullName>Website Event Ticket Purchase Notification QLD</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>bpalmer@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fmann@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jfeltham@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mwhitby@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Website_Event_Ticket_Purchase</template>
    </alerts>
    <alerts>
        <fullName>Website Event Ticket Purchase Notification VIC</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>bpalmer@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>cdunstan@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ebreidahl@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jwales@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mwhitby@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Website_Event_Ticket_Purchase</template>
    </alerts>
    <alerts>
        <fullName>Website Event Ticket Purchase Notification WA</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>bpalmer@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>khubble@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ksimpson@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mwhitby@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Website_Event_Ticket_Purchase</template>
    </alerts>
    <alerts>
        <fullName>Website Shop Merchandise Purchase</fullName>
        <ccEmails>red@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>ilindley@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Website_Merchandise_Purchase</template>
    </alerts>
    <fieldUpdates>
        <fullName>2040 Club Opp Name Update</fullName>
        <description>Applies 2040 Club opportunity naming convention</description>
        <field>Name</field>
        <formula>&quot;2040Club &quot;&amp;  Account.FirstName  &amp; &quot; &quot; &amp;  Account.LastName  &amp;&quot; &quot; &amp; TEXT( Campaign.Job_Code_Year__c )</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CR Credit card expiry month</fullName>
        <field>Credit_Card_Expiry_Month__c</field>
        <formula>Account.Credit_Card_Expire_Month__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Club Red Opp Stage to CC Expired</fullName>
        <description>Update the Club Red Opportunity Stage to &apos;Club Red - CC Expired&quot; to identify the Members credit card details have expired so that it wont attempt to batch process on the next 15th of the month until new details are entered</description>
        <field>StageName</field>
        <literalValue>Club Red - CC Expired</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact Email for Website Purchases</fullName>
        <field>Contact_Email__c</field>
        <formula>Contact__r.Email</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact Firstname for Website Purchases</fullName>
        <description>Pulling Preferred Salutation</description>
        <field>Contact_First_Name__c</field>
        <formula>Contact__r.Preferred_Salutation_SVC__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity Name for Web Shop Purchases</fullName>
        <description>Updates Opportunity Name for website shop purchases based on record type and campaign job code item</description>
        <field>Name</field>
        <formula>$RecordType.Name &amp; &quot; - &quot; &amp; Account.FirstName &amp; &quot; &quot; &amp; Account.LastName &amp; &quot; - &quot; &amp; Close_Date_Text__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set Amount to Club Red Income Rollup</fullName>
        <description>Set Amount to Club Red Income Rollup</description>
        <field>Amount</field>
        <formula>Club_Red_Income__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set Opportunity - Is Recurring as False</fullName>
        <description>Set Opportunity - Is Recurring as False</description>
        <field>IsRecurringPayment__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set Opportunity - Is Recurring as True</fullName>
        <field>IsRecurringPayment__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update BBGG opportunity naming conventio</fullName>
        <field>Name</field>
        <formula>&quot;20&quot; &amp; TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp; IF( CONTAINS(Campaign.Job_Codes__c , &quot;KT&quot;), &quot;Be Bold Go Gold&quot;, &quot;error&quot;) &amp; &quot; - &quot; &amp; Account.Name &amp; &quot; &quot; &amp; 
Account.FirstName &amp; &quot; &quot; &amp; Account.LastName &amp; &quot; - &quot; &amp; Close_Date_Text__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Bequests Opportunity Name</fullName>
        <description>Updates Bequest Opportunity Name field</description>
        <field>Name</field>
        <formula>&quot;20&quot;&amp; TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp;  &quot;Bequest &quot; &amp; &quot; &quot; &amp;  Account.Name   &amp; &quot; - &quot; &amp;  Soft_Credit__r.FirstName  &amp; &quot; &quot; &amp; Soft_Credit__r.LastName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update CGIL Opportunity Name</fullName>
        <description>Updates CGIL Shop Product opportunity with naming convention</description>
        <field>Name</field>
        <formula>&quot;20&quot;&amp;TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp; &quot;CGIL&quot; &amp; &quot; -  &quot; &amp; Contact__r.FirstName &amp; &quot; &quot; &amp; Contact__r.LastName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Chairmans Appeal Opportunity Name</fullName>
        <description>Updates a Chairmans Appeal Opportunity Name field with a naming convention based on the campaign year, job code item and the income code</description>
        <field>Name</field>
        <formula>&quot;20&quot; &amp; TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp; IF( CONTAINS(Campaign.Job_Codes__c , &quot;CA&quot;), &quot;Chairman&apos;s Appeal&quot;, &quot;error&quot;) &amp; &quot; - &quot; &amp; Account.Name &amp;
 Account.FirstName &amp; &quot; &quot; &amp;  Account.LastName &amp; &quot; - &quot; &amp;   Close_Date_Text__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Close Date</fullName>
        <description>Updates the Close Date field with the date the opportunity stage is changed to &apos;closed lost&apos;</description>
        <field>CloseDate</field>
        <formula>IF( 
ISCHANGED(StageName) 
&amp;&amp; 
ISPICKVAL(StageName, &quot;Closed Lost&quot;), 
TODAY(),  CloseDate  )</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Coles Website Donation Opp Name</fullName>
        <description>Update website donation opportunity with Coles in the opportunity name</description>
        <field>Name</field>
        <formula>$RecordType.Name &amp; &quot; Coles&quot; &amp; &quot; - &quot; &amp; Account.FirstName &amp; &quot; &quot; &amp; Account.LastName &amp; &quot; - &quot; &amp; Close_Date_Text__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Community Fundraising Opportunity</fullName>
        <description>Update community fundraising opportunity name</description>
        <field>Name</field>
        <formula>Campaign.Name &amp; &quot; - &quot; &amp;  Account.Name &amp; Account.FirstName &amp; &quot; &quot; &amp; Account.LastName   &amp;  &quot; - &quot; &amp; TEXT( Activity__c ) &amp; &quot; - &quot; &amp; TEXT( CloseDate)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Corp Event Opportunity Name</fullName>
        <description>Updates Corporate event opportunity name based on the campaign record type and campaign job code</description>
        <field>Name</field>
        <formula>Campaign.Name &amp; &quot; - &quot; &amp; Account.Name &amp; Account.FirstName &amp; &quot; &quot; &amp; Account.LastName &amp; &quot; - &quot; &amp; TEXT( CloseDate)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Corporate Close Date</fullName>
        <description>Update Corporate Opportunity close date to the end of the calendar year eg: 31st December 2012</description>
        <field>CloseDate</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Corporate Opportunity Name</fullName>
        <description>Update Corporate Opportunity name</description>
        <field>Name</field>
        <formula>&quot;20&quot;&amp; TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp; TEXT( Revenue_Stream__c) &amp;  &quot; &quot; &amp; Account.Name &amp; Account.FirstName &amp; &quot; &quot; &amp; Account.LastName &amp; &quot; &quot; &amp; TEXT( Activity__c ) &amp; &quot; - &quot; &amp; Service_Level_Abbreviation__c &amp; &quot; - &quot; &amp; TEXT( State_for_funding__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Major Gifts Opportunity Name</fullName>
        <description>Updates the Opportunity name with a naming convention specific to Major Gifts which also includes Family Foundations</description>
        <field>Name</field>
        <formula>&quot;20&quot;&amp; TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp; &quot;Major Gifts &quot;&amp;   Account.Name   &amp; &quot; - &quot; &amp; Account.FirstName &amp; &quot; &quot; &amp; Account.LastName &amp; &quot; - &quot; &amp; Service_Level_Abbreviation__c &amp; &quot; - &quot; &amp; TEXT( State_for_funding__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update MajorGifts Presentation Date</fullName>
        <description>Auto updates Presentation Date for all Major Gifts based on the Opportunity stage to &apos;presentation&apos;</description>
        <field>Presentation_Date__c</field>
        <formula>IF( 
ISCHANGED(StageName) 
&amp;&amp; 
RecordTypeId = &quot;012200000009BkV&quot; 
&amp;&amp; 
ISPICKVAL(StageName, &quot;Presentation&quot;), 
TODAY(), Presentation_Date__c )</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opp Name</fullName>
        <field>Name</field>
        <formula>&quot;Corporate &quot; &amp; Name</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opp Name %28SQ%29</fullName>
        <description>For Corps, T&amp;F, Beq and Major Gifts Record types the workflow will add the word Record type description before the Opp name and the year of the close date for naming convention purposes.</description>
        <field>Name</field>
        <formula>$RecordType.Name  &amp; &quot; &quot;  &amp;  Name  &amp; &quot; &quot; &amp;  LEFT(Close_Date_Text__c, 4)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update OppName CRD</fullName>
        <field>Name</field>
        <formula>&quot;CRD &quot;&amp;  Account.FirstName   &amp; &quot; &quot; &amp;   Account.LastName  &amp;&quot; &quot; &amp;  TEXT( Campaign.Job_Code_Year__c )</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opportunity Amount</fullName>
        <field>Amount</field>
        <formula>Total_Income__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opportunity Owner</fullName>
        <description>Update the Oppty Owner field from &quot;DonationForm Site Guest user&quot; to Catherine Ibbott</description>
        <field>OwnerId</field>
        <lookupValue>cibbott@redkite.org.au.redkite</lookupValue>
        <lookupValueType>User</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Presentation Date</fullName>
        <description>Auto updates the presentation date field with the date when the Opportunity stage value is changed to &apos;presentation&apos;.  If no presentation is given this field will remain blank.  Applies to T&amp;F/Major Gifts and Corporate Opps</description>
        <field>Presentation_Date__c</field>
        <formula>IF( 
ISCHANGED(StageName) 
&amp;&amp; 
ISPICKVAL(StageName, &quot;Presentation&quot;), 
TODAY(), Presentation_Date__c )</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Quiz Event Opportunity Name</fullName>
        <description>Updates a Quiz Event Opportunity Name field with a naming convention based on the campaign year, job code item and the income code</description>
        <field>Name</field>
        <formula>&quot;20&quot; &amp; TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp; TEXT(Campaign.Job_Code_State__c) &amp; &quot; &quot; &amp; IF( CONTAINS(Campaign.Job_Codes__c , &quot;QZ&quot;), &quot;Quiz&quot;, &quot;error&quot;) &amp; &quot; - &quot; &amp;
Account.Name  &amp; Account.FirstName  &amp; &quot; &quot; &amp;  Account.LastName &amp;   &quot; - &quot; &amp; IF( RecordTypeId = &quot;012200000009VpK&quot;, &quot;Prize Donation&quot;, CASE( Account_Code__c , &quot;240&quot;, &quot;Donation&quot;, &quot;125&quot;, &quot;Live Auction&quot;, &quot;135&quot;, &quot;Silent Auction&quot;,
&quot;105&quot;, &quot;Ticket/Table&quot;, &quot;130&quot;,&quot;Sponsorship&quot;, &quot;140&quot;, &quot;Raffle Tickets&quot;, &quot;120&quot;, &quot;Error&quot;,&quot;Error&quot;))</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update S2S12 Opportunities</fullName>
        <field>Name</field>
        <formula>&quot;20&quot;&amp; TEXT( Campaign.Job_Code_Year__c ) &amp; &quot; &quot; &amp;  TEXT(Campaign.Job_Code_Item__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Submission Date</fullName>
        <description>Auto Updates the Submission/Proposal Date field with the date of when the opportunity stage is changed to &apos;proposal&apos;.  Will remain blank if this stage is never selected.  Applies to T&amp;F and Corporate opps</description>
        <field>Submission_Proposal_Date__c</field>
        <formula>IF( 
ISCHANGED(StageName) 
&amp;&amp; 
ISPICKVAL(StageName, &quot;Proposal&quot;), 
TODAY(),  Submission_Proposal_Date__c
)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update T%26F Opportunity Name</fullName>
        <description>Updates the Opportunity name with a naming convention specific to T&amp;F</description>
        <field>Name</field>
        <formula>&quot;20&quot;&amp; TEXT( Campaign.Job_Code_Year__c )  &amp; &quot; &quot; &amp; &quot;T&amp;F &quot;&amp;  Account.Name &amp; &quot; -  &quot; &amp;    Service_Level_Abbreviation__c  &amp; &quot; -  &quot; &amp;  TEXT( State_for_funding__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update T%26F%2FMG Funding Period End Date</fullName>
        <description>Updates the Funding Period End date for all T&amp;F/Major Gifts Opportunities based on the Close Date</description>
        <field>Funding_Period_End_Date__c</field>
        <formula>Funding_Period_Start_Date__c + 364</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update T%26F%2FMG Funding Period Start Date</fullName>
        <description>Updates the Funding Period Start Date for all T&amp;F / Major Gift Opportunities based on the closed Date</description>
        <field>Funding_Period_Start_Date__c</field>
        <formula>CloseDate + 1</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update T%26F%2FMajor Gifts Close Date</fullName>
        <description>Updates the Opportunity Close Date for all T&amp;F and Major Gifts with a date of 6months from the create date</description>
        <field>CloseDate</field>
        <formula>CreatedDate + 180</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Website Donation Opportunity Name</fullName>
        <description>Updates the Opportunity Name for all Website General Donations based on Opportunity record type = Website Donation and Campaign Job code = General Donations - GD</description>
        <field>Name</field>
        <formula>$RecordType.Name &amp; &quot; -  &quot;  &amp;   Account.FirstName &amp; &quot; &quot; &amp;  Account.LastName  &amp; &quot; - &quot; &amp;  Close_Date_Text__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update with Orig Opp Amt</fullName>
        <field>Budget__c</field>
        <formula>Amount</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Appeal Opportunity Naming Convention</fullName>
        <actions>
            <name>Update Chairmans Appeal Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Christmas Appeal - XA,Chairman&apos;s Appeal - CA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <description>Identifies a Christmas or Chaimans Appeal Opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BeBoldGoGold Opportunity Naming Convention</fullName>
        <actions>
            <name>Update BBGG opportunity naming conventio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Kitetime - KT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <description>Identifies a BeBoldGoGold Opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bequest Opportunity Naming Convention</fullName>
        <actions>
            <name>Update Bequests Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bequests</value>
        </criteriaItems>
        <description>Identifies a Bequest Opportunity and applies a naming convention.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CGIL 2012 Opportunities</fullName>
        <actions>
            <name>Update CGIL Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>EVR A Christmas Gift in Lieu Individuals 12</value>
        </criteriaItems>
        <description>Identifies CGIL Opportunities and applies an opportunity naming convention</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CGIL 2012 Purchase Notification</fullName>
        <actions>
            <name>Email Supporter with message to personalise e-card</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>General Donations - GD</value>
        </criteriaItems>
        <description>Sends an email notification when a CGIL 2012 item is purchased on the Redkite website. Looks at Opportunity record type and Campaign Job Code Item</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Club Red Members Expired Card Details</fullName>
        <actions>
            <name>Club Red Opp Stage to CC Expired</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CC_Alert__c</field>
            <operation>contains</operation>
            <value>/img/samples/light_red.gif</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <description>Identifies all Club Red members with Expired Credit Card Details</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Details for Website Purchases</fullName>
        <actions>
            <name>Contact Email for Website Purchases</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact Firstname for Website Purchases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <description>Pulls the contact email and first name from the contact record and drops on the website shop purchase record type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Corporate Opportunity Naming Convention</fullName>
        <actions>
            <name>Update Corporate Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporates</value>
        </criteriaItems>
        <description>Identifies a Corporate Opportunity and applies a naming convention.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fashion Parade Website Ticket Purchase Notification WA</fullName>
        <actions>
            <name>Send Email alert when WA Fashion Parade ticket purchased on website</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>equals</operation>
            <value>EVC W Fashion Parade - Morrison 14</value>
        </criteriaItems>
        <description>Sends an email notification when a Fashion Parade ticket is purchased on the Redkite website. Looks at Opportunity record type and Primary Campaign Source</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Major Gifts Opportunity Naming Convention</fullName>
        <actions>
            <name>Update Major Gifts Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>T&amp;F/Major Gifts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Stream__c</field>
            <operation>equals</operation>
            <value>Major Gifts</value>
        </criteriaItems>
        <description>Identifies a Major Gift Opportunity and applies a naming convention.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mark Opportunity as Recurring</fullName>
        <actions>
            <name>Set Opportunity - Is Recurring as True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsRecurringPayment__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Mark the opportunity as a recurring payment type opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Presentation Date</fullName>
        <actions>
            <name>Update Close Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Presentation Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>T&amp;F/Major Gifts,Corporates</value>
        </criteriaItems>
        <description>Updates the date of the Presentation once the opportunity stage value has been changed to &quot;presentation&apos;. Applies to T&amp;F/Major Gifts and Corporate opportunties</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quiz Event Opportunity Naming Convention</fullName>
        <actions>
            <name>Update Quiz Event Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Corporate Quiz - QZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <description>Identifies a Quiz Opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>S2S Opportunities</fullName>
        <actions>
            <name>Update S2S12 Opportunities</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>contains</operation>
            <value>Sea 2 Summit 2012</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Kitetime Engagement Task to Kitetime Admin</fullName>
        <actions>
            <name>Kitetime Opportunity has entered Verbal Agreement</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Kitetime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Verbal Agreement</value>
        </criteriaItems>
        <description>Send Kitetime Engagement Task to Kitetime Admin</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Soft Credit Donor ID for CR Opps</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.is_ClubRed_member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Submission Date</fullName>
        <actions>
            <name>Update Submission Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>T&amp;F/Major Gifts,Corporates</value>
        </criteriaItems>
        <description>Auto updates the Submission/Proposal Date field with the date of when the Opportunity stage is changed to &quot;proposal&apos;. Applies to T&amp;F and Corporates</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>T%26F Opportunity Naming Convention</fullName>
        <actions>
            <name>Update T%26F Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>T&amp;F/Major Gifts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Stream__c</field>
            <operation>equals</operation>
            <value>Trusts &amp; Foundations</value>
        </criteriaItems>
        <description>Identifies a T&amp;F Opportunity and applies a naming convention.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TFMajor Gifts not created by Bonnie Harris</fullName>
        <actions>
            <name>Action Required%3A  A new T%26F or Major Gift Opportunity has been created for your to review</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>T&amp;F/Major Gifts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revenue_Stream__c</field>
            <operation>equals</operation>
            <value>Trusts &amp; Foundations,Major Gifts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>notEqual</operation>
            <value>Bonnie Harris,Helen Wilson</value>
        </criteriaItems>
        <description>Identifies any T&amp;F or Major Gift Opportunities that have not been created by Bonnie Harris</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Un-Mark Opportunity as Recurring</fullName>
        <actions>
            <name>Set Opportunity - Is Recurring as False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsRecurringPayment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Un-Mark the opportunity as a recurring payment type opportunity</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update 2040 Club Opp Name</fullName>
        <actions>
            <name>2040 Club Opp Name Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.X2040Club_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used on creation to apply 2040 Club opportunity naming convention</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Club Red</fullName>
        <actions>
            <name>Update OppName CRD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.X2040Club_Member__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Used on creation to set flag and also set first payment date</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Coles Website Donation Opportunity Name</fullName>
        <actions>
            <name>Update Coles Website Donation Opp Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Donation,General Donations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Coles - CO</value>
        </criteriaItems>
        <description>Update the opportunity name for all coles website donations</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Community Fundraising Opportunity Name</fullName>
        <actions>
            <name>Update Community Fundraising Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>ClubRed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Kitetime - KT,Coles - CO,Community &amp; Clubs - CC,Partnership Community - PC</value>
        </criteriaItems>
        <description>Update Community Fundraising Opportunity name based on Primary Campaign source and campaign job code item. 
11/4/2013 - CI addedd campaign job code for Partnership Community - PC &amp; Coles - CO</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Corporate Event Opportunity Name</fullName>
        <actions>
            <name>Update Corp Event Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Events</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Corporate Rel&apos;p - CR</value>
        </criteriaItems>
        <description>Update Coporate Partnership Event Opportunity name based on Primary Campaign source and campaign job code item. 
11/4/2013 - CI addedd campaign job code for Partnership Community - PC &amp; Coles - CO</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opp Corp Name</fullName>
        <actions>
            <name>Update Opp Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporates</value>
        </criteriaItems>
        <description>Adds the word &quot;Corporate&quot; before the opprtunity name for naming convention purposes.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Amount</fullName>
        <actions>
            <name>Update Opportunity Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers an update to a currency field for all Incomes for that opportunity.</description>
        <formula>Total_Income__c != 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Amount when Recurring Payment</fullName>
        <actions>
            <name>Set Amount to Club Red Income Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsRecurringPayment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Triggers an update to a currency field for all Club Red Incomes for that opportunity.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update T%26F%2FMajor Gifts Close Date</fullName>
        <actions>
            <name>Update T%26F%2FMajor Gifts Close Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>T&amp;F/Major Gifts</value>
        </criteriaItems>
        <description>Updates T&amp;F/Major Gifts Close Date with a date of 6months from the Opportunity Create Date</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update T%26F%2FMajor Gifts Funding dates</fullName>
        <actions>
            <name>Update T%26F%2FMG Funding Period End Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update T%26F%2FMG Funding Period Start Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>T&amp;F/Major Gifts</value>
        </criteriaItems>
        <description>Auto update the Funding Period Start/End Dates for all T&amp;F and Major Gifts Opportunities based on the Close Date</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Website Donation Opportunity Name</fullName>
        <actions>
            <name>Update Opportunity Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Website Donation Opportunity Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Donation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>General Donations - GD</value>
        </criteriaItems>
        <description>Update the opportunity name for all website donations linked to general campaign</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Website Shop Purchase Opportunity Name</fullName>
        <actions>
            <name>Opportunity Name for Web Shop Purchases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Merchandise - MR</value>
        </criteriaItems>
        <description>Update the opportunity name for all website shop purchases</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update with Orig Opp Amt</fullName>
        <actions>
            <name>Update with Orig Opp Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>,Corporates,Bequests,T&amp;F/Major Gifts</value>
        </criteriaItems>
        <description>Takes the figure in the Opp Amt field and populates it into the Budget field to be saved for perpetuity.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Updates Opp Name</fullName>
        <actions>
            <name>Update Opp Name %28SQ%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporates,Foundation and Trusts,Major Gifts,Bequests</value>
        </criteriaItems>
        <description>For Corps, T&amp;F, Beq and Major Gifts Record types the workflow will add the word Record type description before the Opp name and the year of the close date for naming convention purposes.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Website Event Ticket Purchase Notification NSW</fullName>
        <actions>
            <name>Website Event Ticket Purchase Notification NSW</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Yes 4 Kids - YK,Corporate Quiz - QZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_State__c</field>
            <operation>equals</operation>
            <value>N</value>
        </criteriaItems>
        <description>Sends an email notification when a NSW event ticket is purchased on the Redkite website. Looks at Opportunity record type, Campaign Job Code Item, and Job Code State</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Website Event Ticket Purchase Notification QLD</fullName>
        <actions>
            <name>Website Event Ticket Purchase Notification QLD</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Corporate Quiz - QZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_State__c</field>
            <operation>equals</operation>
            <value>Q</value>
        </criteriaItems>
        <description>Sends an email notification when a QLD event ticket is purchased on the Redkite website. Looks at Opportunity record type and Job Code State and Campaign Job Code Item</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Website Event Ticket Purchase Notification VIC</fullName>
        <actions>
            <name>Website Event Ticket Purchase Notification VIC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Corporate Quiz - QZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_State__c</field>
            <operation>equals</operation>
            <value>V</value>
        </criteriaItems>
        <description>Sends an email notification when a VIC event ticket is purchased on the Redkite website. Looks at Opportunity record type and Job Code State and Campaign Job Code Item</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Website Event Ticket Purchase Notification WA</fullName>
        <actions>
            <name>Website Event Ticket Purchase Notification WA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>startsWith</operation>
            <value>EVR W,EVC W</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_State__c</field>
            <operation>equals</operation>
            <value>W</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>contains</operation>
            <value>QZ</value>
        </criteriaItems>
        <description>Sends an email notification when a WA event ticket is purchased on the Redkite website. Looks at Opportunity record type and Job Code State and Campaign Job Code Item</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Website Merchandise Purchase Notification</fullName>
        <actions>
            <name>Website Shop Merchandise Purchase</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Website Shop Purchase</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Campaign_Job_Code_Item__c</field>
            <operation>equals</operation>
            <value>Merchandise - MR</value>
        </criteriaItems>
        <description>Sends an email notification when a merchandise item is purchased on the Redkite website. Looks at Opportunity record type and Campaign Job Code Item
9/5/14 - Cibbo - deactivated this worklfow as there is no merchandise on website shop.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Kitetime Opportunity has entered Verbal Agreement</fullName>
        <assignedToType>owner</assignedToType>
        <description>This opportunity has entered Verbal Agreement.  Please perform the following tasks:

* Send Confirmation Email to Orgainsation

Red</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
    </tasks>
</Workflow>
