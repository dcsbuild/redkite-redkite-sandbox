<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Partnership Details - 1 month to Partnership End Date</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dhughes@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hardlie@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rsharma@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Partnership_Details_1_months_to_Partnership_End_Date</template>
    </alerts>
    <alerts>
        <fullName>Partnership Details - 3 months to Partnership End Date</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>dhughes@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hardlie@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rsharma@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Partnership_Details_3_months_to_Partnership_End_Date</template>
    </alerts>
    <fieldUpdates>
        <fullName>Partnership Details Naming Convention</fullName>
        <description>text(Agreement_Year__c)&amp; &quot; &quot; &amp; Account__r.Name &amp; &quot; &quot; &amp; &quot;Partnership&quot;</description>
        <field>Name</field>
        <formula>Agreement_Year__c &amp; &quot; &quot; &amp; Account__r.Name &amp; &quot; &quot; &amp; &quot;Partnership&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Partnership Details Naming Convention</fullName>
        <actions>
            <name>Partnership Details Naming Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Partnership_Details__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Partnerships</value>
        </criteriaItems>
        <description>Agreement_Year__c &amp; &quot; &quot; &amp; Account__r.Name &amp; &quot; &quot; &amp; &quot;Partnership&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partnership End Date - 1 mth</fullName>
        <actions>
            <name>Partnership Details - 1 month to Partnership End Date</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partnership_Details__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Partnerships</value>
        </criteriaItems>
        <description>Partnership end date 1 month warning</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Partnership End Date - 3 mth</fullName>
        <actions>
            <name>Partnership Details - 3 months to Partnership End Date</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Partnership_Details__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Partnerships</value>
        </criteriaItems>
        <description>Partnership end date 3 month warning</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
