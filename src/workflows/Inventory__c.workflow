<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify Voucher Allocator that hospital float is due to expire in 90 days</fullName>
        <ccEmails>cibbott@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>cmarchetti@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olines@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Voucher_Expiry_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Inventory Status Reverse</fullName>
        <description>Changes Inventory Status from Allocated back to Available when workflow criteria is met</description>
        <field>Status__c</field>
        <literalValue>Available</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sent to Hospital Field Update</fullName>
        <description>Inserts current date &amp; time in the Sent to Hospital field</description>
        <field>Sent_to_Hospital_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates Inventory Status</fullName>
        <description>Updates inventory Status field to &quot;Allocated&quot; when Current Units Allocated is greater than 0</description>
        <field>Status__c</field>
        <literalValue>Allocated</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Inventory Hospital Vouchers - Use By Date1</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Inventory__c.Location__c</field>
            <operation>notEqual</operation>
            <value>National Storage,National Office</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Status__c</field>
            <operation>equals</operation>
            <value>Available</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Inventory_Type__c</field>
            <operation>equals</operation>
            <value>Food Voucher,Fuel Voucher</value>
        </criteriaItems>
        <description>Identifies Hospital vouchers that are available and due to expire in 90days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inventory Status Reverse</fullName>
        <actions>
            <name>Inventory Status Reverse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Inventory__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Single Item</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Inventory_Type__c</field>
            <operation>equals</operation>
            <value>Food Voucher,Fuel Voucher</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Current_Units_Allocated__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Resource_Record_Count_Rollup__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Adjust_Decrease__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Changes Inventory Status from Allocated back to Available when a voucher resource is deleted (voucher un-allocation). Only applies to Food Voucher and Fuel Voucher Inventory records.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sent to Hospital Date</fullName>
        <actions>
            <name>Sent to Hospital Field Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Inventory__c.Location__c</field>
            <operation>equals</operation>
            <value>QLD - Princess Alexandra Hospital,NSW - John Hunter Hospital,NSW - Sydney Childrens Hospital,NSW - Westmead Children&apos;s Hospital,QLD - Royal Children&apos;s Hospital Brisbane</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Location__c</field>
            <operation>equals</operation>
            <value>VIC - Monash Medical Centre,WA - Princess Margaret Hospital,VIC - Peter Macallum Hospital,SA - Women&apos;s and Children&apos;s Hospital Adelaide,VIC - Royal Children&apos;s Hospital Melbourne</value>
        </criteriaItems>
        <description>Inserts the current date &amp; time in the Sent to Hospital date field when the location is changed to a hospital</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status Update</fullName>
        <actions>
            <name>Updates Inventory Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inventory__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Single Item</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Current_Units_Available__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Inventory_Type__c</field>
            <operation>equals</operation>
            <value>Food Voucher,Fuel Voucher,Collection Tin</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.Adjust_Decrease__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Single Item inventory items only. Updates the inventory Status when the inventory item has been allocated, ie when the Current Units Allocated field is greater than zero.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
