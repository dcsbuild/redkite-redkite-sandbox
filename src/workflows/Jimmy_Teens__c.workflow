<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JimmyTeens naming convention</fullName>
        <field>Name</field>
        <formula>Contact__r.Full_Name__c &amp;  &quot; - &quot;  &amp; &quot;Film&quot; &amp; &quot; &quot; &amp;  TEXT(Film_No__c )</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JimmyTeens naming convention</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Jimmy_Teens__c.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>1/1/2012</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Jimmyteens name</fullName>
        <actions>
            <name>JimmyTeens naming convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Jimmy_Teens__c.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>1/1/2012</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
