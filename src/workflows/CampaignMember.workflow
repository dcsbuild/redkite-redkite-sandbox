<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Red Evaluation Reminder</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <template>System_Emails/Training_Evaluation_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Red Evaluation Reminder 2</fullName>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <template>System_Emails/Training_Evaluation_Reminder</template>
    </alerts>
    <rules>
        <fullName>Reminder%3A Evaluation Incomplete</fullName>
        <active>false</active>
        <criteriaItems>
            <field>CampaignMember.Status</field>
            <operation>equals</operation>
            <value>Evaluation Sent</value>
        </criteriaItems>
        <description>Red training evaluation reminder email to staff</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reminder%3A Evaluation Incomplete v2</fullName>
        <active>false</active>
        <criteriaItems>
            <field>CampaignMember.Status</field>
            <operation>equals</operation>
            <value>Evaluation Sent</value>
        </criteriaItems>
        <description>Red training evaluation reminder email to staff</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
