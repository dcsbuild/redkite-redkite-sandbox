<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send Notification email to owner - BA</fullName>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/BA_Notice</template>
    </alerts>
    <alerts>
        <fullName>Sends Notification Email to Owner</fullName>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/FA_Notice</template>
    </alerts>
    <fieldUpdates>
        <fullName>BA_Update Child ID</fullName>
        <description>Updates the field for use</description>
        <field>Diagnosed_ID_Hidden__c</field>
        <formula>Diagnosed_ID__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Budget Status %3D Budget Provided</fullName>
        <description>Budget Status = Budget Provided for submission on new Approved BDGs.</description>
        <field>Budget_Status__c</field>
        <literalValue>Budget Provided</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Budget Status %3D Extra Budget Required</fullName>
        <description>Used by Finance to approve Budget Allocations outside of the annual budget process.</description>
        <field>Budget_Status__c</field>
        <literalValue>Extra Budget Required</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear Rejected Amount</fullName>
        <description>Clears the rejected amount</description>
        <field>Rejected_Amount__c</field>
        <formula>0</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lockdown PO Creation</fullName>
        <description>Checks the Lockdown Box on The BA object</description>
        <field>Lockdown_BA_Record__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject BDG Amount</fullName>
        <description>Rejection BDG for when the approval has declined and updates the rejected amount field</description>
        <field>Rejected_Amount__c</field>
        <formula>Budget_Amount__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status Change BA</fullName>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Approval Amount</fullName>
        <description>Updates the approval amount ot display what has been approved</description>
        <field>Approved_Amount__c</field>
        <formula>Budget_Amount__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Approval Date%2FTime</fullName>
        <description>Updates the approval date/time stamp</description>
        <field>Approval_Date_Time__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Closed - Grant Expired Date</fullName>
        <description>Services: FA Education - identifies the date and time the BDG Education Grant was closed - grant expired.</description>
        <field>Closed_Date_Time__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Hidden Approval</fullName>
        <description>Updaztes the hidden approval field</description>
        <field>Approval_Hidden__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Status %3D Pending Submission</fullName>
        <description>Update Status to Pending Submission when a BDG is recalled</description>
        <field>Status__c</field>
        <literalValue>Pending Submission</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Status BA</fullName>
        <description>Updates the status of the BA object for when the approval has been received</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Status rejected</fullName>
        <description>Rejection status update</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture BDG Closed - Grant Expired</fullName>
        <actions>
            <name>Update Closed - Grant Expired Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Budget_Allocation__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed - Grant Expired</value>
        </criteriaItems>
        <description>Identifies when an Education Grant has been closed due to the approval expiry date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Hidden ChildID</fullName>
        <actions>
            <name>BA_Update Child ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Budget_Allocation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fin Assist BillsVouchers,Fin Assist Education,Fin Assist Bereavement,Services - Scholarship,Services Exceptional Needs Assistance</value>
        </criteriaItems>
        <description>Used to update the hidden child ID</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
