<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email Deletion Contact</fullName>
        <ccEmails>audit@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/Deletion_RequestCont</template>
    </alerts>
    <alerts>
        <fullName>Email Merge Notice</fullName>
        <ccEmails>red@redkite.org.au</ccEmails>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <template>System_Emails/Merge_RequestCont</template>
    </alerts>
    <alerts>
        <fullName>Palliative or Deceased Email Notification</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>ccorin@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ctheiley@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dmitchell@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jsansone@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jsmith@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lbrown@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lkeay-smith@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lwade@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nmacarthur@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nmorgan@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olines@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pcameron@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rcathcart@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smacbride@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stwentyman@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>trogers@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/Palliative_or_Deceased_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI Contact Name Update</fullName>
        <description>Inserts Account Name into Last Name field</description>
        <field>LastName</field>
        <formula>Contact_Account_Name__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Record Type to BIC</fullName>
        <description>Changes the Contact Record Type to Business Income Contact</description>
        <field>RecordTypeId</field>
        <lookupValue>Business_Income_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Checks Opt Out Mail</fullName>
        <field>Mail_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear First Name Field</fullName>
        <description>Removes current First Name field value</description>
        <field>FirstName</field>
        <formula>&quot;&quot;</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear out Notes on Opt Out</fullName>
        <field>Notes_on_Opt_Out__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear out opt out details</fullName>
        <description>SERVICES:  Deletes the opt out details so the field is blank</description>
        <field>Opt_Out_Details__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do Not Call</fullName>
        <field>DoNotCall</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Do Not Call Opt Out</fullName>
        <description>Ticks Do Not Call Opt Out</description>
        <field>DoNotCall</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email Opt Out</fullName>
        <description>Ticks Email Opt Out</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email Optout</fullName>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fax Opt Out</fullName>
        <field>HasOptedOutOfFax</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fax Opt Out Tick</fullName>
        <description>Ticks Fax Opt Out</description>
        <field>HasOptedOutOfFax</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is Major Donor Prospect</fullName>
        <description>Untick if this was previously ticked</description>
        <field>Is_Major_Donor_Prospect__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mail Opt Out</fullName>
        <description>Ticks Mail Opt Out</description>
        <field>Mail_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re-Update Do Not Call</fullName>
        <description>SERVICES:  Reverses the opt out status on Do Not Call</description>
        <field>DoNotCall</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_Update Email Opt Out</fullName>
        <description>SERVICES:  Reverse the opt out status for Email Opt Out</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_Update Fax Opt Out</fullName>
        <description>SERVICES:  Reverse the opt out status for Fax Opt Out</description>
        <field>HasOptedOutOfFax</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_Update Mail Opt Out</fullName>
        <description>SERVICES:  Reverse the opt out status for MailOpt Out</description>
        <field>Mail_Opt_Out__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset Update Acct Flag</fullName>
        <field>Update_with_Acct_Addr__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset Update Ph%2FFax</fullName>
        <field>Update_Ph_Fax_Acct__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset Update Ph%2FFax Report To</fullName>
        <field>Update_Ph_Fax_ReportTo__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset Update ReportTo Flag</fullName>
        <field>Update_ReportTo_Addr__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Return to Sender Time Stamp</fullName>
        <description>Inserts the time/date into Return to Sender Date field.</description>
        <field>Return_to_Sender_Date__c</field>
        <formula>Now()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct City</fullName>
        <field>MailingCity</field>
        <formula>Account.BillingCity</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct City Postal</fullName>
        <field>OtherCity</field>
        <formula>Account.ShippingCity</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct PCode</fullName>
        <field>MailingPostalCode</field>
        <formula>Account.BillingPostalCode</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct PCode Postal</fullName>
        <field>OtherPostalCode</field>
        <formula>Account.ShippingPostalCode</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct Postal Street</fullName>
        <field>OtherStreet</field>
        <formula>Account.ShippingStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct State</fullName>
        <field>MailingState</field>
        <formula>Account.BillingState</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct State Postal</fullName>
        <field>OtherState</field>
        <formula>Account.ShippingState</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct Street</fullName>
        <field>MailingStreet</field>
        <formula>Account.BillingStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Acct Timestamp</fullName>
        <field>Update_Account_Addr__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Contact RecordType -FamilyService</fullName>
        <description>Updates the Contact Record Type from Organisation Contact to &quot;Family Contact&quot; based on the Account Name containing &quot;Household&quot;</description>
        <field>RecordTypeId</field>
        <lookupValue>Family_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Do Not Call</fullName>
        <description>SERVICES: If Opt Out ticked, update Do not Call field</description>
        <field>DoNotCall</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Email External ID</fullName>
        <field>Email_External_ID__c</field>
        <formula>Email</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Email Opt Out</fullName>
        <description>SERVICES: If Opt Out ticked, update Email Opt out = true</description>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Fax %28Acct%29</fullName>
        <field>Fax</field>
        <formula>Account.Fax</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Fax %28ReportTo%29</fullName>
        <field>Fax</field>
        <formula>ReportsTo.Fax</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Fax Opt Out</fullName>
        <description>SERVICES: If Opt Out = true, update Fax Opt Out to true</description>
        <field>HasOptedOutOfFax</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Mail Opt Out</fullName>
        <description>SERVICES: If Opt Out = true, update Mail Opt Out to true</description>
        <field>Mail_Opt_Out__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Notes on Opt Out %28deceased%29</fullName>
        <description>SERVICES: If deceased = true, Update Notes on Opt Out to &apos; Automatic Opt Out (deceased)&apos;</description>
        <field>Notes_on_Opt_Out__c</field>
        <literalValue>Automatic Opt Out (deceased)</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Notes on Opt Out 0-14</fullName>
        <description>SERVICES:  If Opt Out = true, and Age Category = PAEDIATRIC update Notes on Opt Out to &quot;Automatic Opt Out (0-14)&quot;</description>
        <field>Notes_on_Opt_Out__c</field>
        <literalValue>Automatic Opt Out (0-14 years)</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Notes on Opt Out Test</fullName>
        <field>Notes_on_Opt_Out__c</field>
        <literalValue>Automatic Opt Out (0-14 years)</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opt Out %28Deceased%29</fullName>
        <description>SERVICES: If Deceased = true, update Opt Out to true</description>
        <field>Opt_Out__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opt Out 0-14</fullName>
        <description>SERVICES</description>
        <field>Opt_Out__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opt Out Details</fullName>
        <description>SERVICES: Update with the user and date of when the Opt Out field was ticked</description>
        <field>Opt_Out_Details__c</field>
        <formula>TEXT(TODAY()) &amp; &quot; - &quot; &amp; $User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opt Out Details %28Deceased%29</fullName>
        <description>SERVICES:  Identifies the User and date the Deceased checkbox was ticked</description>
        <field>Opt_Out_Details__c</field>
        <formula>TEXT(TODAY()) &amp; &quot; - &quot; &amp; $User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opt Out Details 0-14</fullName>
        <description>SERVICES:  If Opt Out = true and Age Category = PAEDIATRIC enter username and date/time of the opt out.</description>
        <field>Opt_Out_Details__c</field>
        <formula>TEXT(TODAY()) &amp; &quot; - &quot; &amp; $User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Opt out 0-14</fullName>
        <description>SERVICES:  tick Opt Out based on age of CYP</description>
        <field>Opt_Out__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Ph %28Acct%29</fullName>
        <field>Phone</field>
        <formula>Account.Phone</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Ph %28ReportTo%29</fullName>
        <field>Phone</field>
        <formula>ReportsTo.Phone</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update ReportTo Ctry</fullName>
        <description>Updates the report to country into current contact</description>
        <field>MailingCountry</field>
        <formula>ReportsTo.MailingCountry</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update ReportTo PCode</fullName>
        <description>Updates the report to Postcode into current contact</description>
        <field>MailingPostalCode</field>
        <formula>ReportsTo.MailingPostalCode</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update ReportTo Postal Street</fullName>
        <field>OtherStreet</field>
        <formula>ReportsTo.OtherStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update ReportTo State</fullName>
        <description>Updates the report to State into current contact</description>
        <field>MailingState</field>
        <formula>ReportsTo.MailingState</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update ReportTo Street</fullName>
        <description>Updates the street</description>
        <field>MailingStreet</field>
        <formula>ReportsTo.MailingStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update ReportsTo Time</fullName>
        <description>Timestamps the last reports to change</description>
        <field>Update_ReportTo_Time__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Reportto City</fullName>
        <description>Updates the report to city into current contact</description>
        <field>MailingCity</field>
        <formula>ReportsTo.MailingCity</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Salutation Txt</fullName>
        <description>Used to update preferred salutation title</description>
        <field>Salutation_Txt__c</field>
        <formula>Salutation_Text__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Street Contact Org</fullName>
        <field>MailingStreet</field>
        <formula>ReportsTo.MailingStreet</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update is Major Donor</fullName>
        <field>Is_Major_Donor__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update is Major Donor Prospect</fullName>
        <field>Is_Major_Donor_Prospect__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates ReportTo Postal City</fullName>
        <field>OtherCity</field>
        <formula>ReportsTo.OtherCity</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates ReportTo Postal Cty</fullName>
        <field>OtherCountry</field>
        <formula>ReportsTo.OtherCountry</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates ReportTo Postal PCode</fullName>
        <field>OtherPostalCode</field>
        <formula>ReportsTo.OtherPostalCode</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updates ReportTo Postal State</fullName>
        <field>OtherState</field>
        <formula>ReportsTo.OtherState</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CYP Contacts 0-14 Years</fullName>
        <actions>
            <name>Update Notes on Opt Out 0-14</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Opt Out 0-14</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Opt Out Details 0-14</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Diagnosed Child YPerson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Age_Category__c</field>
            <operation>equals</operation>
            <value>PAEDIATRIC</value>
        </criteriaItems>
        <description>SERVICES:  Identify all CYP 0-14 and automatically opt out upon record creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Consent Followup</fullName>
        <actions>
            <name>Obtain written consent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Generates a task for the record owner to obtain written consent if the person is under 16 years of age or has no age indicated.  Designed to meet privacy requirements.</description>
        <formula>IF(RecordTypeId &lt;&gt;&quot;012200000009F6p&quot;,False,
IF( Age_in_Years__c &gt;=16,False,
IF(ISPICKVAL(Consent_Given__c ,&quot;Written Consent&quot;),False,True)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deceased Contacts</fullName>
        <actions>
            <name>Update Notes on Opt Out %28deceased%29</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Opt Out %28Deceased%29</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Opt Out Details %28Deceased%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Diagnosed Child YPerson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Deceased__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>SERVICES: Identifies diagnosed Child/AYA that are deceased. Deceased checkbox = true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deceased Opt Outs</fullName>
        <actions>
            <name>Checks Opt Out Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Do Not Call</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email Optout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fax Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Deceased__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Triggerred when the contact is flagged as deceased.  Turns all Opt Outs on.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Diagnosed Contact Deceased or Palliative Email Alert</fullName>
        <actions>
            <name>Palliative or Deceased Email Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Diagnosed Child YPerson</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Deceased__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Palliative_Care__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Email notification to the Services CBS Team when the Palliative or Deceased tickbox is ticked on a Diagnosed Child YPerson record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Diagnosed Opt Outs</fullName>
        <actions>
            <name>Checks Opt Out Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Do Not Call</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email Optout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fax Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Checks all opt out boxes when new diagnosed person contacts are created except if age is 16yrs or older at creation date.  Designed to meet privacy requirements.</description>
        <formula>IF(RecordTypeId &lt;&gt;&quot;012200000009F6p&quot;,False, 
IF( Age_in_Years__c &gt;=16,False, True))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email External ID Updt</fullName>
        <actions>
            <name>Update Email External ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Family Contact,Business Development,Social/Health Worker,Organisation Contact,Diagnosed Child YPerson</value>
        </criteriaItems>
        <description>As external ID fields cannot be formulas this is designed to complete the Email External ID to allow matching of email for data imports.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inactive Contact</fullName>
        <actions>
            <name>Checks Opt Out Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Do Not Call</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Email Optout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fax Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Inactive_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto updates Checkbox fields when this is ticked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Is Major Donor Prospect</fullName>
        <actions>
            <name>Update is Major Donor Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Last_Donation_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>1000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Last_Donation_Amount__c</field>
            <operation>lessOrEqual</operation>
            <value>4999</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Is_Major_Donor__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Identify major donor prospects by the last donation amount (processed incomes greater or equal to $1000 and less or equal to $4999</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Is Major Gifts</fullName>
        <actions>
            <name>Is Major Donor Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update is Major Donor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Last_Donation_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>5000</value>
        </criteriaItems>
        <description>Identify major gifts by the last donation amount (processed incomes greater or equal to $5000</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Person Account Created</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Individual</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>9/15/2013</value>
        </criteriaItems>
        <description>on	Fundraising/Events: Identifies when a new person account is created.
15/9/13 - Cibbo created to auto update mail and email communication preferences upon record creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opt Out Contacts</fullName>
        <actions>
            <name>Update Do Not Call</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Email Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Fax Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Mail Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Opt Out Details</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Diagnosed Child YPerson,Family Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Opt_Out__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>SERVICES: Family and AYA contacts with Opt Out = true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Return To Sender</fullName>
        <actions>
            <name>Mail Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Return to Sender Time Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Return_To_Sender__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Time stamps the Return To Sender checkbox and also opts out for further mail.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reverse Opt Out Contacts</fullName>
        <actions>
            <name>Clear out Notes on Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear out opt out details</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Re-Update Do Not Call</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Re_Update Email Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Re_Update Fax Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Re_Update Mail Opt Out</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Diagnosed Child YPerson,Family Contact</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Opt_Out__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Deceased__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>SERVICES: Family and AYA contacts with Opt Out = false
This might be where the individual has reversed their decision to be &apos;opted out&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Contact RecordType for converted Family Services Leads</fullName>
        <actions>
            <name>Update Contact RecordType -FamilyService</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Contact_Account_Name__c</field>
            <operation>contains</operation>
            <value>Household</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Is_Family_Services_Lead__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Identifies Contact Records converted from a Family Services Leads and changes the record type from Organisaiton Contact to Family Contact based on the Account Name containing &quot;Household&quot;. Note: Change will still be required if contact is ChildDiagnosed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Custom Salutation</fullName>
        <actions>
            <name>Update Salutation Txt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Development,Family Contact,Organisation Contact,Social/Health Worker,Diagnosed Child YPerson</value>
        </criteriaItems>
        <description>This is used as the 5k code limit was exceeded without this logic.  Therefore a workflow updates a std field and logic works from that field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Ph%2FFax %28ReportTo%29</fullName>
        <actions>
            <name>Reset Update Ph%2FFax Report To</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Fax %28ReportTo%29</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Ph %28ReportTo%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Update_Ph_Fax_ReportTo__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the phone and fax with the report to details as per the lookup</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Ph%2FFax Acct</fullName>
        <actions>
            <name>Reset Update Ph%2FFax</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Fax %28Acct%29</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Ph %28Acct%29</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Update_Ph_Fax_Acct__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates the phone and fax details from the account details</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update ReportTo Addr</fullName>
        <actions>
            <name>Reset Update ReportTo Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update ReportTo PCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update ReportTo Postal Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update ReportTo State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update ReportTo Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update ReportsTo Time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Reportto City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updates ReportTo Postal City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updates ReportTo Postal PCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updates ReportTo Postal State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates a contact with the parent contact organisations address</description>
        <formula>IF(AND( Update_ReportTo_Addr__c =True,LEN (ReportsTo.LastName)&gt;0),True,False)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update with Acct Addr</fullName>
        <actions>
            <name>Reset Update Acct Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct City Postal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct PCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct PCode Postal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct Postal Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct State Postal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Acct Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Update_with_Acct_Addr__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Updates a contact with the current account organisations address.  WIll reset flag after update.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updates account with contact deceased details</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Deceased__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Obtain written consent</fullName>
        <assignedToType>owner</assignedToType>
        <description>You have created a new diagnosed child record without written consent.  Please ensure written consent is obtained.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
    </tasks>
</Workflow>
