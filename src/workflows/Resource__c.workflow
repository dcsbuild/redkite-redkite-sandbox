<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Inventory Status Available</fullName>
        <field>Status__c</field>
        <literalValue>Available</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Inventory_Item__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Inventory Status value</fullName>
        <description>When resource &quot;lost&quot; checkbox is ticked the Status value on the associated inventory item is updated to lost.</description>
        <field>Status__c</field>
        <literalValue>Lost</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Inventory_Item__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Inventory Availability</fullName>
        <actions>
            <name>Inventory Status Available</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check if the Resource is returned, is the parent inventory now available.</description>
        <formula>AND((Units_Allocated__c = Units_Returned__c),  Returned__c,  OR(ISPICKVAL(Inventory_Item__r.Inventory_Type__c, &apos;Collection Bucket&apos;), ISPICKVAL(Inventory_Item__r.Inventory_Type__c, &apos;Collection Tin&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Resource item identified as lost</fullName>
        <actions>
            <name>Update Inventory Status value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Resource__c.Lost__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Identifies when an inventory item has been lost and the &quot;lost&quot; checkbox has been ticket on the associated Resource record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
