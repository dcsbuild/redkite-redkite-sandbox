<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email Recall Action</fullName>
        <protected>false</protected>
        <recipients>
            <field>Payment_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <template>System_Emails/PO_Notice_Recall</template>
    </alerts>
    <alerts>
        <fullName>Martine Pelly%27s Endorsement</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>mpelly@redkite.org.au.redkite</recipient>
            <type>user</type>
        </recipients>
        <template>System_Emails/PO_Endorsement_Request_500</template>
    </alerts>
    <alerts>
        <fullName>Notify PO Requester that PO has been approved%2E</fullName>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Payment_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <template>System_Emails/PO_Approved_Email</template>
    </alerts>
    <alerts>
        <fullName>PO Rejection Email</fullName>
        <protected>false</protected>
        <recipients>
            <field>Payment_Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <template>System_Emails/PO_Notice_Reject</template>
    </alerts>
    <alerts>
        <fullName>Send Email to Queue for Approval</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>CEO</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>CEO Assistant</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Deputy CEO</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Financial Controller</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>GM Services</recipient>
            <type>role</type>
        </recipients>
        <template>System_Emails/PO_Approval_Request_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval Amount PO</fullName>
        <field>Approval_Amount__c</field>
        <formula>PO_Amount__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval Date PO</fullName>
        <field>Approval_Date_Time__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change Status</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear PO Rejected Amount</fullName>
        <description>When a PO is approved</description>
        <field>Rejected_Amount__c</field>
        <formula>0</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FA Education%2FScholarship Status Approved</fullName>
        <description>Updates the Status field on FA Education and Scholarship POs to Invoice Received when PO is approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FA Status</fullName>
        <description>Updates the FA status</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FA Update Status</fullName>
        <description>Updates the approval status hidden field to true</description>
        <field>Approval_Hidden__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Generate PO Number</fullName>
        <description>Used to generate a unique PO number</description>
        <field>Purchase_Order_Number__c</field>
        <formula>PO_Number_Authority__c + RIGHT( GETSESSIONID(),5)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO Recalled</fullName>
        <description>Status Update when a PO is recalled</description>
        <field>Status__c</field>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO Update PO Amt</fullName>
        <description>Used to update PO amt from Invoice amount</description>
        <field>PO_Amount__c</field>
        <formula>Invoice_Amount__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO Update Status on Rejection</fullName>
        <description>PO Status is updated to PO Cancelled when the PO is rejected by any approver in the queue.</description>
        <field>Status__c</field>
        <literalValue>PO Cancelled</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO Update Status on Submit</fullName>
        <description>Change PO status to PO Approval Pending when submitted for approval</description>
        <field>Status__c</field>
        <literalValue>PO Approval Pending</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO Update Status on Submit</fullName>
        <field>Status__c</field>
        <literalValue>PO Approval Pending</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO Updates Inv Paid Date</fullName>
        <description>Updates the pending rec date to now()</description>
        <field>Invoice_Paid_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PO_UpdateBanlRecDate</fullName>
        <field>Bank_Reconciled_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected Amount</fullName>
        <field>Rejected_Amount__c</field>
        <formula>PO_Amount__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status - Invoice Paid</fullName>
        <description>Changes status to Invoice Paid</description>
        <field>Status__c</field>
        <literalValue>Invoice Paid</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status Update Supplier Selected</fullName>
        <description>Updates the status to Supplier Selected on the PO</description>
        <field>Status__c</field>
        <literalValue>Supplier Selected</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status- Approved</fullName>
        <description>Use don the PO Object for changing status to approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Approval Hidden Checkbox</fullName>
        <field>Approval_Hidden__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update BPAY Ref Code</fullName>
        <description>Updates the BPAY Ref code from the accounts to the PO field</description>
        <field>Supplier_BPAY_Ref_Number__c</field>
        <formula>Supplier_Account__r.BPay_Redkite_Reference_No__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Invoice Paid Amount</fullName>
        <description>Updates the Invoice Paid Amount field with the PO Amount once the &quot;invoice paid&quot; has been ticked</description>
        <field>Invoice_Amount__c</field>
        <formula>PO_Amount__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update Supplier Contact Email</fullName>
        <field>Supplier_Email_Update__c</field>
        <formula>Supplier_Contact__r.Email</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updt Status Awaiting Inv</fullName>
        <field>Status__c</field>
        <literalValue>Awaiting Invoice</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updt Supplier Notified Date</fullName>
        <field>Supplier_Notified_Date__c</field>
        <formula>NOW()</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Martine Pelly%27s Endorsement</fullName>
        <actions>
            <name>Martine Pelly%27s Endorsement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PurchaseOrder__c.Payment_Requestor__c</field>
            <operation>equals</operation>
            <value>Rosie Jones</value>
        </criteriaItems>
        <criteriaItems>
            <field>PurchaseOrder__c.PO_Amount__c</field>
            <operation>greaterThan</operation>
            <value>500.00</value>
        </criteriaItems>
        <criteriaItems>
            <field>PurchaseOrder__c.Status__c</field>
            <operation>notEqual</operation>
            <value>PO Cancelled,Invoice Paid,Approved</value>
        </criteriaItems>
        <description>For PO endorsements on amounts over $500 while Martine&apos;s role is being care-takered by Isobel</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PO Cancelled</fullName>
        <actions>
            <name>Adjust BA Due to Cancellation</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>PurchaseOrder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Financial Assistance</value>
        </criteriaItems>
        <criteriaItems>
            <field>PurchaseOrder__c.Status__c</field>
            <operation>equals</operation>
            <value>PO Cancelled</value>
        </criteriaItems>
        <description>Triggers a task to update BA Approved amount manually by deducting the amount of the cancelled PO.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PO_Generate PO Number</fullName>
        <actions>
            <name>Generate PO Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>PurchaseOrder__c.Approval_Hidden__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>PurchaseOrder__c.Purchase_Order_Number__c</field>
            <operation>equals</operation>
            <value>Not Yet Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>PurchaseOrder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Campaigns,General</value>
        </criteriaItems>
        <description>This workflow triggers the first time the approval is reached.  Note most fields will be locked after approval through the use of validation rules.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PO_Update BPAY Ref</fullName>
        <actions>
            <name>Update BPAY Ref Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PurchaseOrder__c.Redkite_BPAY_Ref__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This allows for regular Redkite Suppliers to automatically complete the Supplier Reference Code. Used to update the ref number with the standard account Redkite ref code</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PO_Update Bank Rec Date</fullName>
        <actions>
            <name>PO_UpdateBanlRecDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PurchaseOrder__c.Bank_Reconciled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Date/Time stamps bank reconciliation date when checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PO_Update PO Amount Field</fullName>
        <actions>
            <name>PO Update PO Amt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PurchaseOrder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Financial Assistance,Financial Assistance,Cash</value>
        </criteriaItems>
        <criteriaItems>
            <field>Budget_Allocation__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Fin Assist BillsVouchers,Fin Assist Education,Fin Assist Bereavement</value>
        </criteriaItems>
        <description>Used to update the PO Amount for FA Bills and Vouchers as all bills are Invoice Amt only the PO amount is not displayed on the page layout.  By ruinning this workflow it standardises info useful for analysing all PO within Redkite</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PO_Update on Invoice Paid</fullName>
        <actions>
            <name>PO Updates Inv Paid Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status - Invoice Paid</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update Invoice Paid Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PurchaseOrder__c.Invoice_Paid__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Date/Time stamps Invoice Paid date when and also changes &quot;Status&quot; to invoice paid.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Email on PO</fullName>
        <actions>
            <name>Update Supplier Contact Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PurchaseOrder__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Campaigns,General</value>
        </criteriaItems>
        <description>Updates the email for the PO Requester used in alerts to the requester for rejection of approval</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Supplier Notified Date</fullName>
        <actions>
            <name>Updt Status Awaiting Inv</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updt Supplier Notified Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PurchaseOrder__c.Supplier_Notified_Hidden__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Time stamps the supplier notified and also changes the status to &quot;Awaiting Invoice&quot; on the PO</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Adjust BA Due to Cancellation</fullName>
        <assignedTo>cibbott@redkite.org.au.redkite</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A PO has been cancelled by Finance for Financial Assistance.  In order to complete the process you must manually deduct the amount of the cancellation from the Budget Alloc Approved Amount.  This can only be changed by System Admin and is HIGH PRIORITY.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
    </tasks>
</Workflow>
